var docTitle = document.title;
var furniSearch = null;
var furniOwl = {
    items: 13,
    itemsCustom : false,
    itemsDesktop : [1199,13],
    itemsDesktopSmall : [980,10],
    itemsTablet: [768,7],
    itemsTabletSmall: false,
    itemsMobile : [479,4],
    navigation : true,
    navigationText : ["prev","next"],
};

var imagesSearch = null;


var badgesOwl = {
    items: 16,
    itemsCustom : false,
    itemsDesktop : [1199,16],
    itemsDesktopSmall : [980,13],
    itemsTablet: [768,10],
    itemsTabletSmall: false,
    itemsMobile : [479,5],
    navigation : true,
    navigationText : ["prev","next"],
};

var windowHeight = $(window).height();

$(window).scroll(function() {
    scrollMenu();
});

function scrollMenu () {
    var scroll = $(window).scrollTop();
    var height = $('section#main-slider').outerHeight()
    if(!height){
        height = $('#blog .banner').outerHeight()
    }

    if(!height){
        $('body:not(.addPaddingTop)').css('padding-top', parseInt($('body').css('padding-top')) + 80).addClass('addPaddingTop');
    }
    var menuHeight = $('header.navbar').outerHeight();

    height -= menuHeight + (menuHeight/2);

    if(scroll >= height){
        $('header.wet-asphalt').addClass('headerBackground');
    }else{
        $('header.wet-asphalt').removeClass('headerBackground');
    }
}


function messagerCounter(){
    $.getJSON( "/de/messages/new", function( data ) {
        if(data.count > 0){
            $('.waitBadge, .badge.bg-primary').replaceWith('<span class="badge bg-primary">'+ data.count +'</span>');
            document.title = '('+ data.count +') '+docTitle;
        }
        setTimeout(messagerCounter, 30000);
    });
}

$(document).ready(function () {

    // LIKES
    $("body").on('click', "a[id*='likeLink_']", function (e){

        e.preventDefault();

        $.getJSON($(this).attr('href'), function (data) {
            if(data.html && data.id){
                $('#'+data.id).replaceWith(data.html);
            }
        });

    });

    $("a[href^='#']").on('click', function(e) {

        // prevent default anchor click behavior
        e.preventDefault();

        // store hash
        var hash = this.hash;

        var height = $('section#main-slider').outerHeight()
        if(!height){
            height = $('#blog .banner').outerHeight()
        }

        // animate
        $('html, body').animate({
            scrollTop: $(hash).offset().top - height
        }, 300, function(){

            // when done, add hash to url
            // (default click behaviour)
            window.location.hash = hash;
        });

    });

    //getJson
    messagerCounter();

    //tokennize
    $('.tokenizeInput').tokenize();

    //USERMENU
    var userMenu = $('.usermenu');
    $(userMenu).hover(function () {
        $('.usermenu').addClass('openMenu');
    }, function () {

        $('.usermenu').removeClass('openMenu');
    });

    $('a[href="#openUsermenu"], .usermenu .bookmark').click(function (e){
        e.preventDefault();
        if($('.usermenu').hasClass('openMenu')){
            $('.usermenu').removeClass('openMenu');
        }else {
            $('.usermenu').addClass('openMenu');
        }
    });

    // HEADER HIGHT
    var slider = $('#main-slider .carousel.slide.wet-asphalt');
    if($(slider).length == 1){
        var menuHeight = $('header.navbar').outerHeight();
        var newHeight = windowHeight * 0.6;

        var silderHeight = (newHeight - menuHeight);
        if(silderHeight < 400){
            silderHeight = 400;
        }else if(silderHeight > 500){
            silderHeight = 400;
        }

        $('#main-slider .carousel .item').css('height', silderHeight + 'px');
        $( '.centered' ).each(function( e ) {
            $(this).css('margin-top',  ($('#main-slider').height() - $(this).height())/2);
        });
    }

     $("#owl-carousel").owlCarousel(furniOwl);

    scrollMenu();


    //FurniView
    $('#lastFurnis').on('click', '.furni[id*=furni]', function () {
        var furni = $(this).attr('id');
        if ($('[data-furni=' + furni + ']').length) {
            $('[data-furni]').hide();
            $('[data-furni=' + furni + ']').show();

            var act = $('[data-furni=' + furni + ']').find('.acts');
            if (!$(act).children('img').length) {
                $('[data-furni=' + furni + ']').find('#click').attr('id', '').html('');
            }
        }
    });

    $('#lastFurnis').on('click', '.furniImage #next', function () {
        var furni = $(this).parents('.furniImage');
        var directions = $(furni).children('.direction');
        var acts = $(furni).children('.acts');
        $(acts).children().hide();
        var next = $(directions).children('img:visible').first().hide().next();
        if (next.length != 0) {
            $(next).show();
        } else {
            $(directions).children('img').first().show();
        }
    });

    $('#lastFurnis').on('click', '.furniImage #prev', function () {
        var furni = $(this).parents('.furniImage');
        var directions = $(furni).children('.direction');
        var acts = $(furni).children('.acts');
        $(acts).children().hide();
        var prev = $(directions).children('img:visible').first().hide().prev();
        if (prev.length != 0) {
            $(prev).show();
        } else {
            $(directions).children('img').last().show();
        }
    });

    $('#lastFurnis').on('click', '.furniImage #click', function () {
        var furni = $(this).parents('.furniImage');
        var directions = $(furni).children('.direction');
        var acts = $(furni).children('.acts');
        $(directions).children().hide();

        if ($(acts).children('img:visible').length == 0) {
            $(acts).children('img').first().show();
            return true;
        }

        var next = $(acts).children('img:visible').first().hide().next();
        if (next.length != 0) {
            $(next).show();
        } else {
            $(directions).children('img').first().show();
            $(acts).children().hide();
        }
    });

    // FURNI SEARCH
    $('div.furni-search input').keyup(function (e) {
        if (furniSearch) {
            furniSearch.abort();
        }

        var searchString = $('div.furni-search input').val();

        furniSearch = $.getJSON(mainUrl + "/furni/search/" + searchString, function (data) {
            if (data.html.list) {
                $('.furniView div').first().html(data.html.list);
            }

            if (data.html.overview) {
                $('#overview.furnis').first().html(data.html.overview);
                $("#owl-carousel").owlCarousel(furniOwl);
            }
        });
    });

    //Staff Search
    $('#openStaffSearch').click( function (e){
        e.preventDefault();
        var search = $('.search.line');
        console.log(search);
        console.log($(search).hasClass('close'));
        if($(search).hasClass('close')){
            $(search).removeClass('close');
            $('.countryList').addClass('close');
            $('.search.min i').addClass('open').addClass('fa-reply').removeClass('fa-search');
        }else{
            $(search).addClass('close');
            $('.countryList').removeClass('close');
            $('.search.min i').removeClass('open').removeClass('fa-reply').addClass('fa-search');
        }
    });

    $('input#staffSearch').keyup(function (e) {
        e.preventDefault();
        searchStaff(imagesSearch, 1);
    });


    // Images SEARCH
    goodiesAjax();
    $('input#imagesSearch').keyup(function (e) {
        e.preventDefault();
        searchImages(imagesSearch, 1);
    });

    $('form.imagesForm').submit(function (e) {
        e.preventDefault();
        searchImages(imagesSearch, -1);
    })

    //Badges
    $("section#badges .overview").owlCarousel(badgesOwl);

    // LOGIN USE
    $('a[href*="#login"]').click(function (e) {
        e.preventDefault();

        $.getJSON(mainUrl + "/user/login", function (data) {
            if (data.html) {
                var options = {
                    message: data.html,
                    title: 'Login',
                    size: 'sm',
                    buttons: []
                };

                eModal.alert(options);
            }
        });
    });

    //$("*[data-toggle='lightbox']").each(function () {
    //    $( this ).imageLightbox();
    //});

    $("*[data-toggle='lightbox']").magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        image: {
            verticalFit: false
        },
        zoom: {
            enabled: true,
            duration: 300, // don't foget to change the duration also in CSS
            opener: function(element) {
                return element;
            }
        }
    });

    $("*[data-lightbox]").magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        image: {
            verticalFit: false
        },
        zoom: {
            enabled: true,
            duration: 300, // don't foget to change the duration also in CSS
            opener: function(element) {
                return element;
            }
        }
    });

    $(".ckelightboxgallery").magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        image: {
            verticalFit: false
        },
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300, // don't foget to change the duration also in CSS
            opener: function(element) {
                return element.find('img');
            }
        }
    });

    $(".blog-content a img, .lexicon .list a img").parent('a').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        image: {
            verticalFit: false
        },
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300, // don't foget to change the duration also in CSS
            opener: function(element) {
                return element.find('img');
            }
        }
    });

    $('.ajax-load').magnificPopup({
        type: 'ajax'
    });

    //Join
    $(".step .next").click(function () {

        var zthis = $('.step.active ~ .step').first();
        $('.step.active').removeClass('active');

        if(!zthis){
            zthis = $('.step').first();
        }
        $(zthis).addClass('active');

    });

    //$('.commentModal').click(function (e) {
    //    e.preventDefault();
    //
    //
    //    $.getJSON($(this).attr('href'), function (data) {
    //        if (data.html != undefined) {
    //            var options = {
    //                message: data.html,
    //                title: 'Kommentare',
    //                size: 'lg',
    //                buttons: []
    //            };
    //
    //            eModal.alert(options);
    //        }
    //    });
    //});

    //SHORT NEWS
    //$('.shortnews').click(function (event){
    //    if($(event.target).hasClass('btn') == false && $(event.target).prop("tagName") != 'A') {
    //        var zThis = $(this);
    //        var text = $(this).find('span.text');
    //        if (text) {
    //
    //            $(zThis).parent('.shortnewsList').children().removeClass('close');
    //
    //            if ($(text).hasClass('open')) {
    //                $(zThis).removeClass('open');
    //                $(text).removeClass('open');
    //            } else {
    //                $(zThis).parent('.shortnewsList').children().removeClass('open');
    //                $(zThis).parent('.shortnewsList').children('.shortnews').not(zThis).addClass('close');
    //                $(text).addClass('open');
    //                $(zThis).addClass('open');
    //            }
    //
    //            /*if($(text).is(':visible')){
    //             $(text).slideUp(2500);
    //             }else{
    //             $(text).slideDown(2500);
    //             }*/
    //        }
    //    }
    //});
});

function searchImages (imagesSearch, min) {
    if (imagesSearch) {
        imagesSearch.abort();
    }

    var searchString = $('input#imagesSearch').val();

    if(searchString.length > min) {
        $('.loader').show();
        imagesSearch = $.getJSON(mainUrl + "/goodies/images/search/" + searchString, function (data) {
            if (data.html) {
                $('.imagesList').first().html(data.html);
                $('.loader').hide();
                goodiesAjax();
            }
        });
    }
}

function searchStaff (staffSearch, min) {
    if (staffSearch) {
        staffSearch.abort();
    }

    var searchString = $('input#staffSearch').val();

    if(searchString.length > min) {
        $('.loader').show();
        imagesSearch = $.getJSON(mainUrl + "/lexicon/staff/search/" + searchString, function (data) {
            if (data.html) {
                $('.countryBoxList .replace').first().html(data.html);
                $('.loader').hide();
            }
        });
    }
}

function goodiesAjax () {
    $(".goodiesImages").magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        image: {
            verticalFit: false
        },
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300, // don't foget to change the duration also in CSS
            opener: function(element) {
                return element.find('div');
            }
        }
    });
}