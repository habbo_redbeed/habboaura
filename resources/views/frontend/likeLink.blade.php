<a href="{{ route('micro.like', array('token' => $token, 'type' => $type))  }}" id="likeLink_{{ $token }}_{{ $type }}">
    <i class="fa fa-thumbs-up"></i> {{ Lang::choice('main.likes', count($likes), array('count' => count($likes)))  }}
</a>