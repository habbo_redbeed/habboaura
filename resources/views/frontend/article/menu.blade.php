<div class="col-sm-4">
    <div class="newsMenu">
        @if(isset($article))
            <div class="button  head">
                <div class="icons home"></div>
                DIE AKTUELLSTEN NEWS
            </div>
            <a href="{!! URL::route('dashboard.category', array('category'=>$article->category->slug)) !!}">
                <div class="button activ">
                    <div class="icons {{ $article->category->icon_class  }}"></div>
                    {{ $article->category->title  }}
                    <i class="fa fa-caret-down pull-right font_{{ $article->category->color  }}"></i>
                </div>
            </a>


            <div class="button dropdown activ">
                <ul class="" role="menu">
                    @foreach($article->category->articles()->orderby('created_at', 'DESC')->take(7)->get() as $over)
                        <li>{{ $over->title  }}</li>
                    @endforeach
                </ul>
            </div>
            {{--<div class="button text-center more">--}}
            {{--<i class="fa fa-caret-down pull-left"></i>--}}
            {{--@lang('Weitere Kategorien')--}}
            {{--<i class="fa fa-caret-down pull-right"></i>--}}
            {{--</div>--}}
            {{--<div class="others">--}}
        @endif
        @foreach($categories as $cat)
            @if(!isset($article)  || $article->category->id != $cat->id)
                <a href="{!! URL::route('dashboard.category', array('category'=>$cat->slug)) !!}">
                    <div class="button">
                        <div class="icons {{ $cat->icon_class  }}"></div>
                        {{ $cat->title  }}
                        <i class="fa fa-caret-right pull-right font_{{ $cat->color  }}"></i>
                    </div>
                </a>
            @endif
        @endforeach
        @if(isset($article))
            {{--</div>--}}
        @endif
    </div>
</div>