@extends('frontend/layout/layout')

@section('content')

    <section itemscope itemtype="http://schema.org/NewsArticle" id="article" class="container-fluid headerClean">
        <div class="row">
            @if($article->path)
                <div class="col-sm-12 banner"
                     style="background-image: url( 'http://www.habboaura.com/{!! $article->path . $article->file_name !!}' )">
                    <div class="shadow">
                        <div class="container description">
                            <h1 itemprop="headline">{!! $article->title !!}</h1>
                            <span itemprop="description" class="text">{!! $article->description !!}</span>

                        </div>
                        <div class="categories hidden-xs hidden-sm">
                            <div class="container">
                                @foreach($categories as $category)
                                    <a href="{{ route('dashboard.category', ['slug' => $category->slug]) }}" class="{{ $category->id == $article->category->id ? 'active' : '' }}">{{ strtoupper($category->title) }}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>

        <div class="row">
            <div class="category">
                <div class="container">
                    <span class="cat">
                        <i class="fa fa-user"></i> {{ $article->users }}{{--{{ $article->category->title }}--}}
                    </span>
                    <div class="pull-right">
                        <a href="{{ route('dashboard') }}">Home</a> <i class="fa fa-angle-double-right"></i> <a href="{{ route('dashboard.category', ['slug' => $article->category->slug]) }}">{{ $article->category->title }}</a> <i class="fa fa-angle-double-right"></i> {{ str_limit($article->title, 50) }}
                        {{--<a href="#content">--}}
                            {{--{{ $article->users }} <i class="fa fa-user"></i>--}}
                        {{--</a>--}}
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="container infos">
                <div class="col-xs-12">
                    <span class="text"><i class="fa fa-clock-o"></i> {{ Carbon\Carbon::parse($article->created_at)->format('H:i') }} Uhr, {{ Carbon\Carbon::parse($article->created_at)->format('d.m.Y') }}</span>
                    {{--<span class="text"><i class="fa fa-user"></i> {{ $article->users }}</span>--}}
                    <span class="text">@include('frontend/likeLink', ['token' => $token, 'type' => $type])</span>
                    <span class="text"><i class="fa fa-eye"></i> {{ $views }}</span>
                    <a href="#commentsContent"><span class="text"><i class="fa fa-comments"></i> {{ count($comments) }}</span></a>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row articleContent">
            <div class="container">
                <div class="col-md-8">
                    <div itemprop="articleBody" class="content" id="content">
                        {!! $article->content !!}
                    </div>
                    <div class="clearfix"></div>

                    <hr>

                    <div class="commentsContent" id="commentsContent">
                        <h3>{{ ucfirst(Lang::choice('main.comment', count($article->comments()), array('count' => count($article->comments()))))  }}</h3>
                        @include('frontend/commentNew')
                        @include('frontend/comments')
                    </div>

                </div>
                <div class="col-md-4">

                    @include('frontend.article.include.quest')

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">@lang('news.show.emo')</h3>
                            <small>@lang('news.show.description.emo')</small>
                        </div>
                        <div class="likes panel-body">
                            @include('frontend.article.include.like')
                            <hr>
                        </div>
                    </div>

                    @if(count($lastShortnews))
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">@lang('news.show.shortnews')</h3>
                            </div>
                            <div class="panel-body">
                                @foreach($lastShortnews as $news)
                                    <div class="shortnews">
                                        <a href="{{ route('dashboard.shortnews', ['slug' => $news->category->slug]) }}">
                                            <div class="badge icons {{ $news->category->icon_class }}"></div>
                                        </a>
                                        <span>{{ str_limit(strip_tags($news), 70) }} <a href="{{ route('dashboard.shortnews', ['slug' => $news->category->slug]) }}">@lang('main.more')</a></span>
                                    </div>
                                    <div class="clearfix"></div>
                                    <hr>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    @if(count($recommended))
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">@lang('news.show.recommended')</h3>
                            </div>
                            <div class="panel-body">
                                @foreach($recommended as $news)
                                    <div class="recommand">
                                        <a href="{{ route('dashboard.article.show', ['slug' => $news->slug]) }}">
                                            <div class="banner" style="background-image: url( 'http://www.habboaura.com/{!! $news->path . $news->file_name !!}' )"></div>
                                        </a>
                                        <div class="desc">
                                            <div class="title">
                                                <a href="{{ route('dashboard.article.show', ['slug' => $news->slug]) }}">
                                                    {{ $news->title }}
                                                </a>
                                            </div>
                                            <span>{{ str_limit($news->description, 100) }}</span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <hr>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>

    </section>
@stop

@section('javascript')

    {!! HTML::style('ckeditor/contents.css') !!}
    {!! HTML::style('code_prettify/css/prettifycss') !!}
    {!! HTML::script('code_prettify/js/prettify.js') !!}
    {!! HTML::script('assets/js/moment-with-langs.min.js') !!}
    <script type="text/javascript">
        moment().format();
        moment.lang(mainLang);

        jQuery(document).ready(function ($) {
            var now = moment();
            $('.time').each(function (i, e) {

                var time = moment($(e).attr('datetime'));
                $(e).html('<i class="icon-calendar"> ' + time.from(now) + '</i>');

            });
        });
    </script>

    <script type="text/javascript">
        !function ($) {
            $(function () {
                window.prettyPrint && prettyPrint()
            })
        }(window.jQuery)
    </script>
@stop

@section('openGraph')

    <meta property="og:title" content="{!! $article->title !!}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="http://www.HabboAura.com/{{ Request::path() }}"/>
    <meta property="og:image" content="http://www.habboaura.com/{!! $article->path . $article->file_name !!}"/>
    <meta property="og:image" content="http://www.HabboAura.com/images/logo/big_1_background.png"/>
    <meta property="og:image:width" content="150" />
    <meta property="og:image:height" content="150" />
    <meta property="article:section" content="{{ $article->category->title  }}"/>
    <meta property="article:published_time" content="{!! \Carbon\Carbon::parse($article->created_at)->toIso8601String() !!}"/>
    <meta property="og:site_name" content="HabboAura.com"/>
    <meta property="og:description" content="{{ $article->description }}"/>
    <meta property="og:locale" content="{{ $article->lang }}_{{ strtoupper($article->lang) }}"/>

@stop


@section('meta_titles') {!! $article->title !!} @stop
@section('meta_description') {{ $article->description }} @stop