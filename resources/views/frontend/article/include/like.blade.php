@if(Sentry::check())
    <div class="row">
        <a href="{{ route('micro.like.emo', array('token' => $token, 'type' => $type, 'emo' => 'like'))  }}">
            <div class="col-xs-4 emos">
                <div class="row">
                    <div class="col-xs-4 emo like"></div>
                    <div class="col-xs-8 emo text">{{ $likesObject->likesEmo('like')->count() }}</div>
                </div>
            </div>
        </a>
        <a href="{{ route('micro.like.emo', array('token' => $token, 'type' => $type, 'emo' => 'okay'))  }}">
            <div class="col-xs-4 emos">
                <div class="row">
                    <div class="col-xs-4 emo okay"></div>
                    <div class="col-xs-8 emo text">{{ $likesObject->likesEmo('okay')->count() }}</div>
                </div>
            </div>
        </a>
        <a href="{{ route('micro.like.emo', array('token' => $token, 'type' => $type, 'emo' => 'sad'))  }}">
            <div class="col-xs-4 emos">
                <div class="row">
                    <div class="col-xs-4 emo sad"></div>
                    <div class="col-xs-8 emo text">{{ $likesObject->likesEmo('sad')->count() }}</div>
                </div>
            </div>
        </a>
    </div>
@else
    <div class="row">
        <div class="col-xs-4 emos">
            <div class="row">
                <div class="col-xs-3 emo like"></div>
                <div class="col-xs-8 emo text">{{ $likesObject->likesEmo('like')->count() }}</div>
            </div>
        </div>
        <div class="col-xs-4 emos">
            <div class="row">
                <div class="col-xs-3 emo okay"></div>
                <div class="col-xs-8 emo text">{{ $likesObject->likesEmo('okay')->count() }}</div>
            </div>
        </div>
        <div class="col-xs-4 emos">
            <div class="row">
                <div class="col-xs-3 emo sad"></div>
                <div class="col-xs-8 emo text">{{ $likesObject->likesEmo('sad')->count() }}</div>
            </div>
        </div>
    </div>
@endif
