@if($article->question && $article->question->description)
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{ $article->question->description }}</h3>
        </div>
        <div class="panel-body">
            <div class="col-sm-12 blog-content">
                @if(\Carbon\Carbon::parse($article->question->available_to) >= \Carbon\Carbon::now())
                    @if(Sentry::check())
                        @if(!$article->question->entrys()->where('user_id', Sentry::getUser()->id)->first())
                            {!! Form::open(array('route' => ['dashboard.article.quest',  $article->id], 'class' => 'form-horizontal') )!!}
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('ldlanswer', trans('news.question.label'), array('for'=>'answer'))!!}
                                    {!! Form::textarea('answer', Input::get('answer'), array('required', 'class'=>'form-control', 'placeholder' => trans('news.question.place'), 'rows' => 5)) !!}
                                    @if($errors->get('answer'))
                                        <div class="alert alert-danger" role="alert">{{ $errors->first('answer') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                    {!! Form::submit(trans('news.question.save'), array('class' => 'btn btn-block btn-primary')) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        @else
                            <div class="alert alert-success" role="alert">@lang('news.question.stop')</div>
                        @endif
                    @elseif($article->question->any == 1)
                        @if(!$article->question->entrys()->where('any_session', Session::getId())->first())
                            {!! Form::open(array('route' => ['dashboard.article.quest.any',  $article->id], 'class' => 'form-horizontal') )!!}
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            {!! Form::label('ldlhabbo', trans('news.question.habbo'), array('for'=>'answer'))!!}
                                            {!! Form::text('habbo', '', array('required', 'class'=>'form-control', 'placeholder' => trans('news.question.habbo'))) !!}
                                            @if($errors->get('habbo'))
                                                <div class="alert alert-danger" role="alert">{{ $errors->first('habbo') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-md-offset-1">
                                        <div class="form-group">
                                            {!! Form::label('ldllang', trans('news.question.lang'), array('for'=>'answer'))!!}
                                            {!! Form::select('lang', ['de' => 'Habbo.de', 'com' => 'Habbo.com', 'tr' => 'Habbo.com.tr'], '', array('required', 'class'=>'form-control')) !!}
                                            @if($errors->get('lang'))
                                                <div class="alert alert-danger" role="alert">{{ $errors->first('lang') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('ldlanswer', trans('news.question.label'), array('for'=>'answer'))!!}
                                    {!! Form::textarea('answer', Input::get('answer'), array('required', 'class'=>'form-control', 'placeholder' => trans('news.question.place'), 'rows' => 5)) !!}
                                    @if($errors->get('answer'))
                                        <div class="alert alert-danger" role="alert">{{ $errors->first('answer') }}</div>
                                    @endif
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {!! Recaptcha::render() !!}
                                            @if($errors->get('g-recaptcha-response'))
                                                <div class="alert alert-danger" role="alert">{{ $errors->first('g-recaptcha-response') }}</div>
                                            @endif
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {!! Form::submit(trans('news.question.save'), array('class' => 'btn btn-block btn-primary')) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        @else
                            <div class="alert alert-success" role="alert">@lang('news.question.stop')</div>
                        @endif
                    @else
                        <div class="alert alert-danger" role="alert">@lang('news.question.login')</div>
                    @endif
                @else
                    <div class="alert alert-info" role="alert">@lang('news.question.end', ['date' => \Carbon\Carbon::parse($article->question->available_to)->format('d.m.Y')])</div>
                @endif


            </div>
        </div>
    </div>
@endif