@extends('frontend/layout/layout')
@section('content')
    {!! HTML::script('assets/js/moment-with-langs.min.js') !!}
    <script type="text/javascript">
        moment().format();
        moment.lang('en');

        jQuery(document).ready(function ($) {
            var now = moment();
            $('.time').each(function (i, e) {

                var time = moment($(e).attr('datetime'));
                $(e).html('<i class="icon-calendar"> ' + time.from(now) + '</i>');

            });
        });
    </script>


    <section id="lastNews" class="container-fluid headerClean">


        <div class="row">

            <div class="col-sm-12 header">
                <div class="container" style="margin-top: 30px;">

                    @if($banner && $banner != null)
                        <div class="banner season {{ $banner }}">
                            <div class="description">
                                more #habbo15
                            </div>
                        </div>
                    @endif

                    <div class="col-sm-8">
                        @foreach($articles as $article)
                            @include('frontend/article/min_micro', ['size' => 'small', 'article' => $article])

                            @endforeach
                             <div class="clearfix"></div>       <!--/.pagination-->
                    </div>
                    <div class="col-md-4">
                        <div class="shortnewsList">

                            @foreach($shortnews as $key => $short)
                                @if($key > 3)
                                    <div class="hidden-xs">
                                        @include('frontend/layout/shortnews')
                                    </div>
                                @else
                                    @include('frontend/layout/shortnews')
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <!--/.col-md-8-->
        </div>
        <!--/.row-->
    </section><!--/#blog-->
@stop

