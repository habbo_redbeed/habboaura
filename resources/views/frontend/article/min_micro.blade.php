<article itemscope itemtype="http://schema.org/Article" class="col-md-6 col-sm-6 col-xs-12 smallMirco "
     xmlns="http://www.w3.org/1999/html">

    <a itemprop="url" href="{!! URL::route('dashboard.article.show', array('slug'=>$article->slug)) !!}">
        <div class="back">
            <div class="blog {{ $size  }}"
                 style="background-image: url('http://habboaura.com/{!! $article->path . $article->file_name !!}')"
                 itemprop="image">
                <div class="shader"></div>
                @if(\Carbon\Carbon::parse($article->created_at)->diffInHours() < 5)
                    <div class="new">@lang('news.new')</div>
                @endif
                <div class="comments">
                    <img src="/images/icons/min_comments.png" alt="comments" title="{{ $article->commentsCount() }}">
                    {{ $article->commentsCount() }}
                </div>
                <div class="categorie {{ $article->category->color }}">
            <span class="categories {{ $article->category->color }}"
                  itemprop="name">{{ $article->category->title }}</span>
                </div>
            </div>


            <div class="col-sm-12 body">
                <div class="row">
                    <h1 class="h4">
                        {!! $article->title !!}
                        <div class="infos">
                            <span datetime="{!! $article->created_at !!}" class="time">{!! $article->created_at !!}</span>
                            <span>- {{ $article->views() }} @lang('news.show.description.view')</span>
                        </div>
                        <small>{!! nl2br($article->description) !!}</small>
                    </h1>
                </div>
            </div>
            <div class="btn btn-info more {{ $article->category->color }}">
                @lang('news.more')
            </div>
            <div class="clearfix"></div>
        </div>
    </a>
</article>