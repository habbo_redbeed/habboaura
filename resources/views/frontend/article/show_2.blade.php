@extends('frontend/layout/layout')

@section('content')

    <section itemscope itemtype="http://schema.org/NewsArticle" id="blog" class="container-fluid headerClean white">
        <div class="row">
            @if($article->path)
                <div class="col-sm-12 banner"
                     style="background-image: url( 'http://www.habboaura.com/{!! $article->path . $article->file_name !!}' )">

                </div>
            @endif

            @include('frontend/article/sidebar', array($tags, $categories))
            <div class="col-sm-12 header">
                <div class="container">
                    <div class="col-md-12 col-sm-12">
                        <div class="col-sm-8 line">
                            <h2 itemprop="headline">{!! $article->title !!}</h2>
                            <span itemprop="description" class="description">{!! $article->description !!}</span>

                            <div class="infos">
                                <div class="date">
                                    <span itemprop="dateCreated" datetime="{!! $article->created_at !!}" class="time">{!! $article->created_at !!}</span>
                                </div>
                                <div class="user">
                                    <i class="fa fa-eye"></i>
                                    {{ $views }} @lang('news.show.description.view')
                                    {{--{!! $article->users !!}--}}
                                </div>
                                <div class="user">
                                    @include('frontend/likeLink', ['token' => $token, 'type' => $type])
                                </div>
                                <meta itemprop="interactionCount" content="UserPageVisits:{{ $views }}" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="container-fluid description">
                                <div class="col-sm-12">
                                    <div class="user">
                                        <span class="habbo" itemprop="author">{{ $article->users  }}</span>
                                        @lang('news.show.description.where', array( 'cat' => $article->category->title))
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="blog" class="container headerCleanbar white">
        <div class="row">
            <div class="col-md-8 col-sm-12 ">
                <div class="col-sm-12 blog-content">
                    <div itemprop="articleBody">
                        {!! $article->content !!}
                    </div>
                </div>
                @if($article->question && $article->question->description)
                    <div class="col-sm-12 blog-content">
                        <h3><small>@lang('news.question.title')</small><br>{{ $article->question->description }}</h3>
                        @if(\Carbon\Carbon::parse($article->question->available_to) >= \Carbon\Carbon::now())
                            @if(Sentry::check())
                                @if(!$article->question->entrys()->where('user_id', Sentry::getUser()->id)->first())
                                    {!! Form::open(array('route' => ['dashboard.article.quest',  $article->id], 'class' => 'form-horizontal') )!!}
                                        <div class="col-md-10 col-md-offset-1">
                                            <div class="form-group">
                                                {!! Form::label('ldlanswer', trans('news.question.label'), array('for'=>'answer'))!!}
                                                {!! Form::textarea('answer', Input::get('answer'), array('required', 'class'=>'form-control', 'placeholder' => trans('news.question.place'), 'rows' => 5)) !!}
                                                @if($errors->get('answer'))
                                                    <div class="alert alert-danger" role="alert">{{ $errors->first('answer') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-4 col-md-offset-4">
                                                {!! Form::submit(trans('news.question.save'), array('class' => 'btn btn-block btn-primary')) !!}
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                @else
                                    <div class="alert alert-success" role="alert">@lang('news.question.stop')</div>
                                @endif
                            @elseif($article->question->any == 1)
                                @if(!$article->question->entrys()->where('any_session', Session::getId())->first())
                                    {!! Form::open(array('route' => ['dashboard.article.quest.any',  $article->id], 'class' => 'form-horizontal') )!!}
                                        <div class="col-md-10 col-md-offset-1">
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <div class="form-group">
                                                        {!! Form::label('ldlhabbo', trans('news.question.habbo'), array('for'=>'answer'))!!}
                                                        {!! Form::text('habbo', '', array('required', 'class'=>'form-control', 'placeholder' => trans('news.question.habbo'))) !!}
                                                        @if($errors->get('habbo'))
                                                            <div class="alert alert-danger" role="alert">{{ $errors->first('habbo') }}</div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-md-offset-1">
                                                    <div class="form-group">
                                                        {!! Form::label('ldllang', trans('news.question.lang'), array('for'=>'answer'))!!}
                                                        {!! Form::select('lang', ['de' => 'Habbo.de', 'com' => 'Habbo.com', 'tr' => 'Habbo.com.tr'], '', array('required', 'class'=>'form-control')) !!}
                                                        @if($errors->get('lang'))
                                                            <div class="alert alert-danger" role="alert">{{ $errors->first('lang') }}</div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('ldlanswer', trans('news.question.label'), array('for'=>'answer'))!!}
                                                {!! Form::textarea('answer', Input::get('answer'), array('required', 'class'=>'form-control', 'placeholder' => trans('news.question.place'), 'rows' => 5)) !!}
                                                @if($errors->get('answer'))
                                                    <div class="alert alert-danger" role="alert">{{ $errors->first('answer') }}</div>
                                                @endif
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        {!! Recaptcha::render() !!}
                                                        @if($errors->get('g-recaptcha-response'))
                                                            <div class="alert alert-danger" role="alert">{{ $errors->first('g-recaptcha-response') }}</div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-5 col-md-offset-1">
                                                    <div class="form-group">
                                                        {!! Form::submit(trans('news.question.save'), array('class' => 'btn btn-block btn-primary')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                @else
                                    <div class="alert alert-success" role="alert">@lang('news.question.stop')</div>
                                @endif
                            @else
                                <div class="alert alert-danger" role="alert">@lang('news.question.login')</div>
                            @endif
                        @else
                            <div class="alert alert-info" role="alert">@lang('news.question.end', ['date' => \Carbon\Carbon::parse($article->question->available_to)->format('d.m.Y')])</div>
                        @endif


                    </div>
                @endif
                <!--/.col-md-8-->
            </div>


            <div class="col-md-4">
                <div class="blog-comments">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <b class="h3">@lang('news.comments.title')</b>
                            </div>
                        </div>
                    </div>
                    @include('frontend/commentNew')
                    @include('frontend/comments')
                </div>
            </div>
        </div>
        <!--/.row-->
        <div class="clearfix"></div>
    </section>
    <!--/#blog-->
@stop

@section('javascript')

    {!! HTML::style('ckeditor/contents.css') !!}
    {!! HTML::style('code_prettify/css/prettifycss') !!}
    {!! HTML::script('code_prettify/js/prettify.js') !!}
    {!! HTML::script('assets/js/moment-with-langs.min.js') !!}
    <script type="text/javascript">
        moment().format();
        moment.lang(mainLang);

        jQuery(document).ready(function ($) {
            var now = moment();
            $('.time').each(function (i, e) {

                var time = moment($(e).attr('datetime'));
                $(e).html('<i class="icon-calendar"> ' + time.from(now) + '</i>');

            });
        });
    </script>

    <script type="text/javascript">
        !function ($) {
            $(function () {
                window.prettyPrint && prettyPrint()
            })
        }(window.jQuery)
    </script>
@stop

@section('openGraph')

    <meta property="og:title" content="{!! $article->title !!}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="http://www.HabboAura.com/{{ Request::path() }}"/>
    <meta property="og:image" content="http://www.habboaura.com/{!! $article->path . $article->file_name !!}"/>
    <meta property="og:image" content="http://www.HabboAura.com/images/logo/big_1_background.png"/>
    <meta property="og:image:width" content="150" />
    <meta property="og:image:height" content="150" />
    <meta property="article:section" content="{{ $article->category->title  }}"/>
    <meta property="article:published_time" content="{!! \Carbon\Carbon::parse($article->created_at)->toIso8601String() !!}"/>
    <meta property="og:site_name" content="HabboAura.com"/>
    <meta property="og:description" content="{{ $article->description }}"/>
    <meta property="og:locale" content="{{ $article->lang }}_{{ strtoupper($article->lang) }}"/>

@stop


@section('meta_titles') {!! $article->title !!} @stop
@section('meta_description') {{ $article->description }} @stop