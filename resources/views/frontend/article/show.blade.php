@extends('frontend/layout/layout')
@section('content')
    {!! HTML::style('ckeditor/contents.css') !!}
    {!! HTML::style('code_prettify/css/prettifycss') !!}
    {!! HTML::script('code_prettify/js/prettify.js') !!}
    {!! HTML::script('assets/js/moment-with-langs.min.js') !!}
    <script type="text/javascript">
        moment().format();
        moment.lang(mainLang);

        jQuery(document).ready(function ($) {
            var now = moment();
            $('.time').each(function (i, e) {

                var time = moment($(e).attr('datetime'));
                $(e).html('<i class="icon-calendar"> ' + time.from(now) + '</i>');

            });
        });
    </script>
    {{--<section id="title" class="emerald">--}}
    {{--<div class="container">--}}
    {{--<div class="row">--}}
    {{--<div class="col-sm-6">--}}
    {{--<h1>Blog Item</h1>--}}
    {{--<p>Pellentesque habitant morbi tristique senectus et netus et malesuada</p>--}}
    {{--</div>--}}
    {{--<div class="col-sm-6">--}}
    {{--@yield('partial/breadcrumbs', Breadcrumbs::render('blog.post.show', $article))--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</section><!--/#title-->--}}

    <section id="blog" class="container-fluid headerClean">
        <div class="row">
            @if($article->path)
                <div class="col-sm-12 banner"
                     style="background-image: url( '{!! url($article->path . $article->file_name) !!}' )">

                </div>
            @endif

            @include('frontend/article/sidebar', array($tags, $categories))
            <div class="col-sm-12 header">
                <div class="container">
                    <div class="hidden-xs hidden-sm">
                    @include('frontend/article/menu')
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <div class="col-sm-12 line">
                            <h2>{!! $article->title !!}</h2>
                            <span class="description">{!! $article->description !!}</span>

                            <div class="infos">
                                <div class="date">
                                    <span datetime="{!! $article->created_at !!}" class="time"></span>
                                </div>
                                <div class="user">
                                    <i class="fa fa-eye"></i>
                                    {{ $views }} @lang('news.show.description.view')
                                    {{--{!! $article->users !!}--}}
                                </div>
                                <div class="user">
                                    @include('frontend/likeLink', ['token' => $token, 'type' => $type])
                                </div>
                            </div>
                        </div>
                        {{--<div class="col-sm-5">--}}
                        {{--<div class="container-fluid description">--}}
                        {{--<div class="col-sm-3 habbo">--}}
                        {{--<img src="http://www.habbo.de/habbo-imaging/avatarimage?user=HabboAura&action=std&direction=&head_direction=3&gesture=sml&size=l&img_format=gif" alt="habboImage" class="img-circle img-responsive">--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-12">--}}
                        {{--<div class="user">--}}
                        {{--<span class="habbo">{{ $article->users  }}</span>--}}
                        {{--@lang('news.show.description.where', array( 'cat' => $article->category()->title))--}}
                        {{--<span class="likes">--}}
                        {{--@include('frontend/likeLink', ['token' => $token, 'type' => $type])--}}
                        {{--</span>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="blog" class="container headerCleanbar">
        <div class="row">
            <div class="col-md-4 hidden-xs hidden-sm">
            </div>
            <div class="col-md-8 col-sm-12 ">
                <div class="col-sm-12 blog-content">
                    <div class="">

                        {{--<p>{!! BBCode::parse(htmlentities($article->content)) !!}</p>--}}
                        {!! $article->content !!}

                    </div>

                </div>
                <!--/.col-md-8-->

                <div class="col-sm-10 col-sm-offset-1">
                    <div class="blog-comments">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <b class="h3">@lang('news.comments.title')</b>
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('frontend/commentNew')
                    @include('frontend/comments')
                </div>
                <!--/.col-md-8-->
            </div>
        </div>
        <!--/.row-->
    </section>
    <!--/#blog-->
    <script type="text/javascript">
        !function ($) {
            $(function () {
                window.prettyPrint && prettyPrint()
            })
        }(window.jQuery)
    </script>
@stop
