@extends('frontend/layout/layout')
@section('content')
    {!! HTML::script('assets/js/moment-with-langs.min.js') !!}
    <script type="text/javascript">
        moment().format();
        moment.lang('en');

        jQuery(document).ready(function ($) {
            var now = moment();
            $('.time').each(function (i, e) {

                var time = moment($(e).attr('datetime'));
                $(e).html('<i class="icon-calendar"> ' + time.from(now) + '</i>');

            });
        });
    </script>


    <section id="lastNews" class="container-fluid headerClean">
        <div class="row">

            <div class="col-sm-12 banner bottom" style="background-image: url( '' )">
            </div>

            <div class="col-sm-12 header">
                <div class="container">
                    @include('frontend/article/menu')
                    <div class="col-sm-7 col-sm-offset-4">
                        @foreach($articles as $article)
                            @include('frontend/article/min_micro', ['size' => 'small', 'article' => $article])
                        @endforeach

                        <div class="row">
                            <div class="col-xs-12">
                                <ul class="pagination pagination-lg">
                                    {!! $articles->render() !!}
                                </ul>
                            </div>
                        </div>

                        <!--/.pagination-->
                    </div>
                </div>
            </div>
            <!--/.col-md-8-->
        </div>
        <!--/.row-->
    </section><!--/#blog-->
@stop

