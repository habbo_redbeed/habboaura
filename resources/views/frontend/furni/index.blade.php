@extends('frontend/layout/layout')

@section('content')
    <section id="lastFurnis" class="full">
        <div id="overview" class="overview small">
            <div class="container">
                <div class="col-md-6 col-md-offset-3">
                    <div class="input-group furni-search">
                        <span class="input-group-addon" id="search-furni">
                            <i class="fa fa-search"></i>
                        </span>
                        <input type="text" class="form-control" placeholder="@lang('main.search.placeholder')"
                               aria-describedby="search-furni">
                    </div>
                </div>
            </div>
        </div>
        <div id="overview" class="overview" class="furnis">
            @include('frontend/layout/dashboard/furni_overview', $furnis)
        </div>
        <div class="container furniView">
            <div class="col-sm-12">
                @include('frontend/layout/dashboard/furni', $furnis)
            </div>
        </div>
    </section>
@stop