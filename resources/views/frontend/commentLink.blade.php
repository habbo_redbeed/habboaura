@if(Sentry::check())
    <a href="{{ route('micro.comment.view', array('token' => $token, 'type' => $type))  }}"
       class="commentModal ajax-load">
        <i class="fa fa-thumbs-up"></i> {{ Lang::choice('main.comment', count($comments), array('count' => count($comments)))  }}
    </a>
@else
    <a href="{{ route('micro.comment.view', array('token' => $token, 'type' => $type))  }}"
       class="commentModal ajax-load">
        <i class="fa fa-comments"></i> {{ Lang::choice('main.comment', count($comments), array('count' => count($comments)))  }}
    </a>
@endif