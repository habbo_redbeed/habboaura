@if (session('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif
@if(count($comments))
    @foreach($comments as $comment)
        @if($comment->user != null)
            <div class="blog-comments {{ $comment->delete ? 'delete' : ''  }}" itemscope itemtype="http://schema.org/Comment">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-4 col-md-3 text-center smallPadding min">
                            @include('frontend/userImage', ['userid' => $comment->user->id])
                        </div>
                        <div class="col-xs-8 col-md-9 smallPadding">
                            <div class="col-sm-12 smallPadding">
                                <div class="pull-left">
                                    <b>
                                        <a href="{{ route('user.home.lang', array($comment->user->habbo_name, $comment->user->habbo_lang))  }}" itemprop="author">
                                            {{ $comment->user->habbo_name  }}
                                        </a>
                                        <div class="flag {{ $comment->user->habbo_lang  }}"></div>
                                    </b>
                                </div>
                                @if($comment->delete == 0)
                                    @if(Sentry::check())
                                        @if(Sentry::getUser()->id == $comment->user->id || Sentry::getUser()->hasAnyAccess(['admin.comments.destroy']))
                                            <div class="pull-right text-danger">
                                                <a href="{{ url(route('micro.delete.comment', $comment->id)) }}">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                        @endif
                                    @endif
                                @endif
                            </div>
                            <div class="col-xs-12 col-md-10 smallPadding" itemprop="comment">
                                @if($comment->delete == 0)
                                    {!! smilies(BBCode::only('bold')->parse(($comment->text))) !!}
                                @else
                                    @foreach($comment->notice as $notice)
                                        <div class="alert alert-info" role="alert">
                                            @lang($notice->json()->text, $notice->json()->options)
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <div class="col-sm-12 smallPadding">
                                <div class="pull-left small">{{ Carbon\Carbon::parse($comment->created_at)->diffForHumans()  }}</div>
                                @if($comment->delete == 0)
                                    <div class="pull-right">@include('frontend/likeLink', ['token' => $comment->id, 'type' => 'comment', 'likes' => $comment->likesSum()])</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endforeach
@else
    <div class="blog-comments empty">
        @lang('main.comments.empty')
    </div>
@endif
