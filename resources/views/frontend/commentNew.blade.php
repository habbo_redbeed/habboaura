@if(Sentry::check())
    <div class="blog-comments">
        <form class="form-horizontal" action="{{ route('micro.comment', array('token' => $token, 'type' => $type))  }}"
              method="post">
            <div class="form-group">
                <div class="col-sm-12">
                    <textarea class="form-control" rows="3" placeholder="@lang('main.comments.form.placeholder')"
                              name="comment"></textarea>
                </div>
            </div>
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>

            <div class="form-group">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-default">@lang('main.comments.form.submit')</button>
                </div>
            </div>
        </form>
    </div>
@endif