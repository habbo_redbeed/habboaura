@extends('frontend/layout/layout')
@section('content')
    {!! HTML::style('ckeditor/contents.css') !!}

    <section id="blackhole">
        <section class="container">

            <div class="row text-center">
                <div class="col-sm-12">
                    <h1>Sterne</h1>
                    <hr>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8">
                    {!! $page->content !!}
                </div>
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-body text-center"><img alt="" src="/uploads/content/sternbadgesview.png"
                                                                 style="height:249px; width:183px"/>
                        </div>
                        <div class="panel-heading">Bestenliste <img src="/images/icons/star_blue.png" alt="Stars"> </div>
                        <div class="panel-body">
                            <div>

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#month" aria-controls="month" role="tab" data-toggle="tab">Monat</a></li>
                                    <li role="presentation" class="pull-right"><a href="#sum" aria-controls="sum" role="tab" data-toggle="tab">Gesamt</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="month">
                                        @include('frontend/page/include/stars', ['list' => $add['stars']])
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="sum">
                                        @include('frontend/page/include/stars', ['list' => $add['stars_sum']])
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@stop


