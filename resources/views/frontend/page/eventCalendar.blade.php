@extends('frontend/layout/layout')
@section('content')
    {!! HTML::style('ckeditor/contents.css') !!}

    <section id="event" class="calendar">
        <div class="shadow">
            <section class="container">

                <div class="row text-center">
                    <div class="col-sm-12">
                        <img src="/images/pages/event/calendar/logo.png" alt="habboevent Aura logo" title="habboevent">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-7">
                        @if(!Input::get('date'))
                            <div class="panel panel-default">
                                <div class="panel-heading h4">Beschreibung</div>
                                <div class="panel-body">
                                    {!! $page->content !!}
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading h4">10 Aktuelle Events</div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        @include('frontend.page.include.eventTable', ['list' => $add['lastEvents']])
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="panel panel-default">
                                <div class="panel-heading h4">Verfügbare Events am <strong>{{ $add['selectDay']->format('d.m.Y') }}</strong></div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        @include('frontend.page.include.eventTable', ['list' => $add['selectEvents']])
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="col-md-5">
                        <div class="panel panel-default">
                            <div class="panel-heading h4">Kalendar - @lang('date.'.strtolower($add['calendar']['firstDay']->format('F')))</div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>{{ strtoupper(substr(trans('date.monday'), 0, 3)) }}</th>
                                            <th>{{ strtoupper(substr(trans('date.tuesday'), 0, 3)) }}</th>
                                            <th>{{ strtoupper(substr(trans('date.wednesday'), 0, 3)) }}</th>
                                            <th>{{ strtoupper(substr(trans('date.thursday'), 0, 3)) }}</th>
                                            <th>{{ strtoupper(substr(trans('date.friday'), 0, 3)) }}</th>
                                            <th>{{ strtoupper(substr(trans('date.saturday'), 0, 3)) }}</th>
                                            <th>{{ strtoupper(substr(trans('date.sunday'), 0, 3)) }}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>

                                            @foreach($add['calendar']['days'] as $key => $day)
                                                @if(($key % 7) == 0 && $key != 0)
                                                </tr>
                                                <tr>
                                                @endif
                                                <td>
                                                    <a href="{{ Request::url() }}?date={{ $day['day']->format('Y-m-d') }}">
                                                        <span class="label {{ $day['label'] }}">{{ $day['day']->format('d.m') }} {!! $day['events'] ? '<span class="badge">'.$day['events'].'</span>' : '' !!}</span>
                                                    </a>
                                                </td>
                                            @endforeach

                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@stop

@section('meta_titles') Aktuelle Events - @stop
@section('meta_description')n den Weiten des deutschen Habbo's gibt es nahezu anzählige Events, Wettbewerbe oder Competitions. Wir verschaffen Abhilfe!  @stop



