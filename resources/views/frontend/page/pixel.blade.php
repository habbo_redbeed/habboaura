@extends('frontend/layout/layout')
@section('content')
    {!! HTML::style('ckeditor/contents.css') !!}
    <section id="pixel">

        <section class="container">
            <div class="row">
                <div class="col-md-9">
                    {!! $page->content !!}
                </div>
                <div class="col-md-3">
                    @forelse($add['events'] as $event)
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">{{ $event->title  }}</h3>
                            </div>
                            <div class="panel-body">
                                {!! $event->description !!}
                            </div>
                            @if(Sentry::check())
                                @if($event->users()->where('user_id', '=', Sentry::getUser()->id)->count() == 0)
                                    <div class="panel-body">
                                        <button type="button"
                                                class="btn btn-info btn-sm btn-block">@lang('event.pixel.join')</button>
                                    </div>
                                @else
                                    <div class="panel-body">
                                        <div class="alert alert-success" role="alert">
                                            @lang('event.pixel.check')
                                        </div>
                                    </div>
                                @endif
                            @else
                                <div class="panel-body">
                                    <div class="alert alert-danger" role="alert">
                                        @lang('event.pixel.login')
                                    </div>
                                </div>
                            @endif
                            <div class="panel-default">
                                <div class="panel-footer">
                                    @lang('event.pixel.period', ['from' => Carbon\Carbon::parse($event->event_start)->format('d.m.Y H:i'), 'to' => Carbon\Carbon::parse($event->event_end)->format('d.m.Y H:i')])
                                </div>
                            </div>
                        </div>
                    @empty
                        <div class="alert alert-danger" role="alert">
                            @lang('event.pixel.empty')
                        </div>
                    @endforelse
                </div>
            </div>
        </section>
    </section>
@stop


