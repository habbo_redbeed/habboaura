@extends('frontend/layout/layout')
@section('content')
    {!! HTML::style('ckeditor/contents.css') !!}

    {{--<section id="title" class="emerald">--}}
    {{--<div class="container">--}}
    {{--<div class="row">--}}
    {{--<div class="col-sm-6">--}}
    {{--<h1>{!! $page->title !!}</h1>--}}
    {{--</div>--}}
    {{--<div class="col-sm-6">--}}
    {{--@yield('partial/breadcrumbs', Breadcrumbs::render('page.show', $page))--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</section><!--/#title-->--}}

    <section id="bobbaTalk">

        <section class="container">
            <div class="row text-center">
                <h1>Community Talk</h1>
                <hr>
            </div>

            <div class="row">
                {!! $page->content !!}
            </div>
        </section>
    </section>
@stop

@section('meta_titles') Community Talk - @stop
@section('meta_description')Du diskutierst gerne über Gott und die Welt? Findest es spannend was andere Habbos für Meinungen haben und äußerst gerne deine eigene? @stop



