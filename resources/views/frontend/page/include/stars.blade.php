<table class="table table-condensed table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th style="width: 40px;">Image</th>
        <th>Habbo</th>
        <th>Stars</th>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $key => $habbo)
        @if($habbo->habbo_name)
            <tr>
                <th scope="row">{{ $key+1  }}.</th>
                <td>@include('frontend/userImage', ['userid' => $habbo->id, 'style' => 'width:40px; height:40px;' ])</td>
                <td><a href="{{ route('user.home.lang', array($habbo->habbo_name, $habbo->habbo_lang)) }}">{{ $habbo->habbo_name }}</a></td>
                <td>{{ $habbo->stars }} <img src="http://www.habboaura.com/uploads/news/aura_stars.png" alt="Stars"></td>
            </tr>
        @else
            <tr>
                <th scope="row">{{ $key+1  }}.</th>
                <td>@include('frontend/userImage', ['userid' => $habbo->any_habbo.'/cloud/'.$habbo->any_lang, 'style' => 'width:40px; height:40px;' ])</td>
                <td>{{ $habbo->any_habbo }}</td>
                <td>{{ $habbo->stars }} <img src="http://www.habboaura.com/uploads/news/aura_stars.png" alt="Stars"></td>
            </tr>
        @endif
    @endforeach
    </tbody>
</table>