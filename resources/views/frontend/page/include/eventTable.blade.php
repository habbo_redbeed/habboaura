@foreach($list as $date => $events)
    <p>
        <img alt="" src="http://www.habboaura.com//uploads/content/forum_1.gif"
             style="width: 17px; height: 16px; float: left; margin-left: 5px; margin-right: 5px;">
        <strong>@lang('date.'.strtolower(Carbon\Carbon::parse($date)->format('l')))</strong>
        <small>({{ Carbon\Carbon::parse($date)->format('d.m.Y') }})</small>
    </p>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Area</th>
            <th>Von - Bis</th>
            <th style="width: 30%">Beschreibung</th>
            <th style="width: 20%">Gewinn</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($events as $event)
            <tr>
                <th rowspan="2" valign="middle" scope="row" class="text-center">
                    <img src="{{ $event->icon->image }}" title="{{ $event->icon->name }}"
                         ata-toggle="tooltip" data-placement="top">
                </th>
                <td colspan="4">
                    <b>{{ $event->title != '' ? $event->title : str_limit(strip_tags($event->description), 50)   }}</b>
                </td>
            </tr>
            <tr>
                <td>{{ $event->event_start->format('d.m.Y') }} - {{ $event->event_end->format('d.m.Y') }}</td>
                <td>{!! str_limit(strip_tags($event->description), 50) !!}
                    <br>
                    <button type="button" class="btn btn-sm btn-link pull-right" data-toggle="modal" data-target="#event_{{ md5($event->id) }}">...Mehr</button>

                    <div class="modal fade" id="event_{{ md5($event->id) }}" tabindex="-1" role="dialog" aria-labelledby="event_{{ md5($event->id) }}">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">{{ $event->event_start->format('d.m.Y') }} - {{ $event->event_end->format('d.m.Y') }}</h4>
                                </div>
                                <div class="modal-body">
                                    <p>{!! $event->description !!}</p>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->

                </td>
                <td><img src="{{ $event->price }}"></td>
                <td>
                    @if($event->link_room)
                        <a href="{{ $event->link_room }}" target="_blank">
                            <img src="/images/icons/link_room.png" alt="Raum link">
                        </a><br>
                    @endif
                    @if($event->link_external)
                        <a href="{{ $event->link_external }}" target="_blank">
                            <img src="/images/icons/link_extern.png" alt="Extern link">
                        </a><br>
                    @endif
                    @if($event->link_article)
                        <a href="{{ $event->link_article }}" target="_blank">
                            <img src="/images/icons/link_article.png" alt="Artikel link">
                        </a><br>
                    @endif

                </td>
            </tr>
            <tr>
                <td colspan="6">
                    @include('frontend.commentLink', ['token' => $event->id, 'type' => 'event-cal', 'comments' => []])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endforeach
<hr>
<br><strong>Bitte beachte:</strong> Alle Angaben sind ohne Gewähr!