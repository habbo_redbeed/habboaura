@extends('frontend/layout/layout')
@section('content')
    {!! HTML::style('ckeditor/contents.css') !!}
    <section id="pixel">

        <section class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ $event->title  }}</h3>
                        </div>
                        <div class="panel-body">
                            {!! $event->description !!}
                        </div>
                        <div class="panel-footer">
                            @lang('event.pixel.period', ['from' => Carbon\Carbon::parse($event->event_start)->format('d.m.Y H:i'), 'to' => Carbon\Carbon::parse($event->event_end)->format('d.m.Y H:i')])
                        </div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">@lang('event.pixel.form.title')</div>
                        <div class="panel-body">
                            @if(Sentry::check())
                                @if(!$event->users->where('user_id', '=', Sentry::getUser()->id)->count())
                                    {!! Form::open( array( 'route' => array('page.pixel.form.post', $event->id), 'method'
                                    => 'POST',
                                    'files'=>true)) !!}
                                    <div class="form-group">
                                        <label class="control-label" for="text">@lang('event.pixel.form.text')</label>
                                        {!! Form::textarea('text', '',
                                        array('class'=>'form-control', 'id' => 'text', 'rows' => '3',
                                        'placeholder'=>'Content', 'value'=>Input::old('text'))) !!}
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label" for="text">@lang('event.pixel.form.image')</label>
                                        {!! Form::file('image', null, array('accept' => 'image/*')) !!}

                                        <p class="help-block">@lang('event.pixel.form.imageInfo')</p>

                                    </div>
                                    {!! Form::submit('Create', array('class' => 'btn btn-success')) !!}
                                    {!! Form::close() !!}

                                @else
                                    <div class="alert alert-success" role="alert">
                                        @lang('event.pixel.check')
                                    </div>
                                @endif
                            @else
                                <div class="alert alert-danger" role="alert">
                                    @lang('event.pixel.login')
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@stop


