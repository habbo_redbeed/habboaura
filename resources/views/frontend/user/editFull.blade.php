@extends('frontend/layout/layout')

@section('content')
    <section id="edit" class="full headerClean user">

        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-1 step">
                    @section('editContent')
                    @show
                </div>
                <div class="col-md-4 step">
                    <div class="row">
                        <span class="h3 title">
                            @lang('user.edit.title')
                        </span>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-body menu">
                            <a href="{{ url(route('user.edit.habbo'))  }}">
                                <div class="link">@lang('user.edit.menu.habbo')</div>
                            </a>
                            <a href="{{ url(route('user.edit.password'))  }}">
                                <div class="link">@lang('user.edit.menu.password')</div>
                            </a>
                            <a href="{{ url(route('user.edit.image'))  }}">
                                <div class="link">@lang('user.edit.menu.image')</div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@stop