@extends('frontend/layout/layout')
@section('content')

    <section id="join" class="container-fluid headerClean user password">
        <div class="row">

            <div class="col-xs-12 text-center">
                <img src="/images/logo/big_1.png" alt="Logo Big"><br>
                <span class="h1 title">
                    @lang('user.forget.title')
                </span>
            </div>

        </div>

        <div class="row padding">
            <div class="container">
                <div class="row">

                    <div class="col-md-6 col-md-offset-3 active step">
                        <div class="row">
                            <span class="h3 title">
                                @lang('user.forget.step.four.title')
                            </span>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                @lang('user.forget.step.four.body')
                                {!! Form::text('newpass', $passwort, array('class'=>'form-control', 'id' => 'pass',
                                'placeholder'=>$passwort)) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/.row-->
    </section><!--/#blog-->
@stop

