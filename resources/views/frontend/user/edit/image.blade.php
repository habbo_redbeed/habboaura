@extends('frontend/user/editFull')

@section('editContent')

    {!! Form::open(array('route' => array('user.edit.post.image'), 'method' => 'POST', 'class' =>
    'form-horizontal')) !!}
    <div class="row">
        <span class="h3 title">
            @lang('user.edit.image.title')
        </span>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-sm-8">
                <img src="/images/user/edit/frank_14.gif" align="right" alt="Frank Step One">
                @lang('user.edit.image.body')<hr>
                <label class="control-label" for="title">@lang('user.edit.image.field')</label>
                {!! Form::select('backgroundImage', $images, $userData->profil_background, array('class' => 'form-control',
                'value'=>Input::old('backgroundImage'))) !!}
                @if ($errors->first('backgroundImage'))
                    <div class="alert alert-danger" role="alert">{!! $errors->first('backgroundImage') !!}
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
            </div>
            <div class="col-sm-4">
                @include('frontend/userImage', ['userid' => Sentry::getUser()->id])
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-8 col-md-offset-2 ">
                {!! Form::submit(Lang::get('user.edit.image.save'), array('class' => 'btn btn-success btn-block')) !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop