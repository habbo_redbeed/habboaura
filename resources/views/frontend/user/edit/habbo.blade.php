@extends('frontend/user/editFull')

@section('editContent')

    {!! Form::open(array('route' => array('user.edit.post.habbo'), 'method' => 'POST', 'class' =>
    'form-horizontal')) !!}
    <div class="row">
        <span class="h3 title">
            @lang('user.edit.habbo.title')
        </span>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <img src="/images/user/edit/frank_1.png" align="right" alt="Frank Step One">
            @lang('user.edit.habbo.body')
            <label class="control-label" for="title">@lang('user.edit.habbo.code')</label>
            {!! Form::text('misson_code', $hash, array('class'=>'form-control', 'id' => 'user',
            'placeholder'=>$hash,
            'value'=>Input::old('misson_code'))) !!}
            <hr>
            <label class="control-label" for="title">@lang('user.join.step.two.habbo')</label>
            {!! Form::text('habbo_name', '', array('class'=>'form-control', 'id' => 'habbo_name',
            'placeholder'=> Lang::get('user.join.step.two.habbo'),
            'value'=>Input::old('habbo_name'))) !!}
            @if ($errors->first('habbo_name'))
                <div class="alert alert-danger" role="alert">{!! $errors->first('habbo_name') !!}
                </div>
            @endif
            <label class="control-label" for="title">@lang('user.join.step.two.country')</label>
            {!! Form::select('habbo_lang', $hotels, '', array('class' => 'form-control',
            'value'=>Input::old('habbo_lang'))) !!}
            @if ($errors->first('habbo_lang'))
                <div class="alert alert-danger" role="alert">{!! $errors->first('habbo_lang') !!}
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-8 col-md-offset-2 ">
                {!! Form::submit(Lang::get('user.edit.habbo.save'), array('class' => 'btn btn-success btn-block')) !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop