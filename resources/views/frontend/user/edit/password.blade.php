@extends('frontend/user/editFull')

@section('editContent')

    {!! Form::open(array('route' => array('user.edit.post.password'), 'method' => 'POST', 'class' =>
    'form-horizontal')) !!}
    <div class="row">
        <span class="h3 title">
            @lang('user.edit.password.title')
        </span>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <img src="/images/user/edit/frank_2.gif" align="right" alt="Frank Step Two">
            @lang('user.edit.password.body')
            <label class="control-label" for="title">@lang('user.join.step.three.password')</label>
            {!! Form::password('password', array('class'=>'form-control', 'id' => 'password',
            'placeholder'=> Lang::get('user.join.step.three.password'),
            'value'=>Input::old('password'))) !!}
            @if ($errors->first('password'))
                <div class="alert alert-danger" role="alert">{!! $errors->first('password') !!}
                </div>
            @endif
            <label class="control-label"
                   for="title">@lang('user.join.step.three.passwordTwo')</label>
            {!! Form::password('password_confirmation', array('class'=>'form-control', 'id' =>
            'password_confirmation',
            'placeholder'=> Lang::get('user.join.step.three.passwordTwo'),
            'value'=>Input::old('password_confirmation'))) !!}
            @if ($errors->first('password_confirmation'))
                <div class="alert alert-danger" role="alert">{!!
                    $errors->first('password_confirmation') !!}
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-8 col-md-offset-2 ">
                {!! Form::submit(Lang::get('user.edit.password.save'), array('class' => 'btn btn-success btn-block')) !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop