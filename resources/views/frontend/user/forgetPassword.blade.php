@extends('frontend/layout/layout')
@section('content')

    <section id="join" class="container-fluid headerClean user password">
        <div class="row">

            <div class="col-xs-12 text-center">
                <img src="/images/logo/big_1.png" alt="Logo Big"><br>
                <span class="h1 title">
                    @lang('user.forget.title')
                </span>
            </div>

        </div>

        <div class="row padding">
            {!! Form::open(array('route' => array('user.auth.forget.password.post'), 'method' => 'POST', 'class' =>
            'form-horizontal')) !!}

            <div class="container">
                <div class="row">
                    @if ($errors->first())
                        <div class="col-md-6 col-md-offset-3 active step">
                            <div class="alert alert-danger" role="alert">
                                {!! $errors->first() !!}
                            </div>
                        </div>
                    @endif
                    <div class="col-md-6 col-md-offset-3 active step">
                        <div class="row">
                            <span class="h3 title">
                                @lang('user.forget.step.one.title')
                            </span>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <img src="/images/user/join/frank_1.gif" align="right" alt="Frank Step One">
                                @lang('user.forget.step.one.body')
                                {!! Form::text('email', '', array('class'=>'form-control', 'id' => 'email',
                                'placeholder'=> trans('user.placeholder.email'),
                                'value'=>Input::old('email'))) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-md-offset-3 active step">
                        <div class="row">
                            <span class="h3 title">
                                @lang('user.forget.step.two.title')
                            </span>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                @lang('user.forget.step.two.body')
                                {!! Form::text('habbo_name', '', array('class'=>'form-control', 'id' => 'habbo',
                                'placeholder'=> trans('user.placeholder.habboname'),
                                'value'=>Input::old('habbo'))) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-md-offset-3 active step">
                        <div class="row">
                            <span class="h3 title">
                                @lang('user.forget.step.three.title')
                            </span>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                @lang('user.forget.step.three.body')
                                {!! Form::text('misson_code', $hash, array('class'=>'form-control', 'id' => 'user',
                                'placeholder'=>$hash,
                                'value'=>Input::old('misson_code'))) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-md-offset-3 active step">
                        <div class="row">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="col-md-8 col-md-offset-2 ">
                                        <button type="submit" class="btn btn-primary btn-block">@lang('user.join.next')</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}

        </div>
        <!--/.row-->
    </section><!--/#blog-->
@stop

