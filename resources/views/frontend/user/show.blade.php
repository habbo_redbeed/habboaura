@extends('frontend/layout/layout')
@section('content')

    <section id="profil" class="container-fluid user headerClean">
        <div class="row">
            <div class="col-md-4 side">
                <div class="banner" style="">
                    @include('frontend/userImage', ['userid' => $user->id, 'class' => ''])
                </div>
                <div class="description">
                    <div class="col-xs-6">
                        <div class="row">
                            <span class="h3">
                                {{ $user->habbo_name }}
                                <div class="flag {{ $user->habbo_lang }}"></div>
                            </span>
                            @if(Sentry::check() && Sentry::getUser()->id == $user->id)
                                <img src="/images/icons/min_edit.gif" class="pull-right editMe">
                            @endif
                        </div>
                        <div class="row">
                            <span class="motto">
                                {{ $user->data->motto }}
                            </span>
                        </div>
                        <div class="row since">
                            <span class="since">
                                @lang('user.profil.side.since'): <span>{{ $user->created_at->format('d.m.Y') }}</span>
                            </span>
                        </div>
                    </div>
                    <div class="col-xs-6"></div>
                    <div class="clearfix"></div>
                </div>
                <div class="userComments">
                    <div class="new">
                        @include('frontend/commentNew')
                    </div>
                    <div class="list">
                        @include('frontend/comments')
                    </div>
                </div>
            </div>
            <div class="col-md-8 main">
                <div class="col-sm-12 banner"
                     style="background-image: url( '{!! !empty($user->data->image_header) ? $user->data->image_header : '/images/user/profil/header.gif' !!}' )">
                </div>
                <div class="col-sm-12 static">

                    @foreach($static as $name => $home)
                        <div class="row">
                            <div class="col-xs-12">
                                <span class="h3">{{ $name }}</span>
                            </div>
                            @foreach($home as $modul)
                                <div class="col-lg-4 col-md-6 modul">
                                    <img src="/images/icons/{{ $modul['icon']  }}"
                                         alt="{{ $modul['lang']  }}"> {{ $modul['value']  }} {{ $modul['lang']  }}
                                </div>
                            @endforeach
                            <div class="clearfix"></div>
                        </div>
                    @if($name == 'HabboAura')
                        <hr>
                        @endif
                    @endforeach

                    <div class="clearfix"></div>

                </div>
                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 freetext">
                    @if (session('update'))
                        <div class="alert alert-success">
                            {{ session('update') }}
                        </div>
                    @endif
                    @if(!empty($user->data->free_text))
                        <span class="h3">@lang('user.profil.freetext.title')</span>
                        @if(Sentry::check() && Sentry::getUser()->id == $user->id)
                            <img src="/images/icons/min_edit.gif" class="pull-right editMe">
                        @endif
                        <hr>
                        {!! smilies(BBCode::only('bold')->parse(nl2br(htmlentities($user->data->free_text)))) !!}
                    @endif
                </div>
            </div>
        </div>
    </section>

@stop

@section('javascript')

    @if(Sentry::check() && Sentry::getUser()->id == $user->id)
        <div class="hidden">
            <div class="userEdit">
                {!! Form::open(array('route' => array('user.edit.post.profil'), 'method' => 'POST', 'class' =>
                'form-horizontal')) !!}

                <label class="control-label" for="title">@lang('user.edit.profil.motto')</label>
                {!! Form::text('motto', $user->data->motto, array('class'=>'form-control', 'id' => 'motto',
                'placeholder'=>Lang::get('user.edit.profil.motto'),
                'value'=>Input::old('motto'))) !!}
                <hr>

                <label class="control-label" for="title">@lang('user.edit.profil.free_text')</label>
                {!! Form::textarea('free_text', $user->data->free_text, array('class'=>'form-control', 'id' =>
                'freeText',
                'placeholder'=>Lang::get('user.edit.profil.free_text'),
                'value'=>Input::old('free_text'))) !!}
                <hr>

                {!! Form::submit(Lang::get('user.edit.profil.save'), array('class' => 'btn btn-success btn-block')) !!}

                {!! Form::close() !!}
            </div>
        </div>

        <script>

            $(document).ready()
            {
                $('.editMe').click(function () {
                    userEdit();
                });
                function userEdit() {
                    eModal.alert({
                        message: $('.hidden .userEdit').html(),
                        useBin: true,
                        title: '@lang('user.edit.profil.title')',
                        buttons: []
                    });
                }
            }
        </script>
    @endif

@stop

@section('openGraph')

    <meta property="og:title" content="@lang('title.userpage', ['username' => $user->habbo_name.' '.strtoupper($user->habbo_lang)])"/>
    <meta property="og:type" content="profile"/>
    <meta property="og:url" content="http://www.HabboAura.com/{{ Request::path() }}"/>
    <meta property="og:image" content="http://www.habboaura.com/img/profil/{{ $user->id  }}.png"/>
    <meta property="og:image:width" content="150" />
    <meta property="og:image:height" content="150" />
    <meta property="og:image" content="http://www.HabboAura.com/images/logo/big_1_background.png"/>
    <meta property="og:image:width" content="150" />
    <meta property="og:image:height" content="150" />
    <meta property="profile:username" content="{{ $user->habbo_name  }}"/>
    <meta property="og:site_name" content="HabboAura.com"/>
    <meta property="og:description" content="@lang('title.description.user', array('username' => $user->habbo_name, 'lang' => $user->habbo_lang))"/>

@stop


@section('meta_titles') @lang('title.userpage', ['username' => $user->habbo_name.' '.strtoupper($user->habbo_lang)]) - @stop
@section('meta_description') @lang('title.description.user', array('username' => $user->habbo_name, 'lang' => $user->habbo_lang)) @stop

@section('javascript')
    {!! HTML::style('ckeditor/contents.css') !!}
    {!! HTML::style('code_prettify/css/prettifycss') !!}
    {!! HTML::script('code_prettify/js/prettify.js') !!}
    {!! HTML::script('assets/js/moment-with-langs.min.js') !!}
@stop
