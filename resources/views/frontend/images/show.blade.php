@extends('frontend/layout/layout')

@section('content')
    <section id="images" class="full goodies">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <form class="imagesForm">
                        <div class="form-group">
                            <label for="imagesSearch">@lang('main.search.title')</label>
                            <input type="search" class="form-control" id="imagesSearch"
                                   placeholder="@lang('main.search.placeholder_img')...">
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="imagesList">
                    @include('frontend.images.list', ['images' => $images])
                </div>
                <div class="loader">
                    <i class="fa fa-refresh"></i>
                </div>
            </div>
        </div>
    </section>
@stop