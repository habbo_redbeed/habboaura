@extends('frontend/layout/layout')
@section('content')
    <section id="shortnews" class="container-fluid">
        <div class="container">
            <div class="text-center slogen">
                <h1>
                    <img src="/images/logo/big_1.png"> @lang('shortnews.liveticker')
                </h1>
            </div>
            <div class="shortnewsList" style="position:relative">
                <div class="row">
                    @foreach($shortnews as $key => $short)
                        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                            @include('frontend/layout/shortnews')
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@stop

@section('javascript')
    {!! HTML::script('/js/plugins/waterfall-master/js/zepto.js') !!}
    {!! HTML::script('/js/plugins/waterfall-master/jquery.waterfall.js') !!}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.shortnewsList .row').first().waterfall({
                colMinWidth: 300,
                autoresize: true
            });
        });
    </script>

@stop

@section('meta_titles') @lang('shortnews.liveticker') - @stop
