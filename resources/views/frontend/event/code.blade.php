@extends('frontend/layout/layout')
@section('content')

<section id="event" class="container-fluid headerClean code">
    <div class="row">

        <div class="col-xs-12 text-center">
            <img src="/images/pages/event/calendar/logo.png" alt="Logo Event"><br>
        </div>

    </div>

    <div class="row padding">
        <div class="container">
            <div class="row">

                <div class="col-md-6 col-md-offset-3 active step">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h2>@lang('event.code.title')</h2>
                            @lang('event.code.body')
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-md-offset-3 active step">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            @if ($errors->first('error'))
                                <div class="alert alert-danger" role="alert">{!! $errors->first('error') !!}
                                </div>
                            @endif

                            @if (Session::has('message'))
                                <div class="alert alert-success" role="alert">{!! Session::get('message') !!}
                                </div>
                            @endif

                            @if(Sentry::check())
                                {!! Form::open(array('route' => array('event.event.post'), 'method' => 'POST', 'class' => 'form')) !!}
                                    {!! Form::label('lblcode', trans('event.code.form.code')) !!}
                                    {!! Form::text('code', '', array('class'=>'form-control', 'id' => 'pass', 'placeholder'=> trans('event.code.form.code'))) !!}
                                    @if ($errors->first('code'))
                                        <div class="alert alert-danger" role="alert">{!! $errors->first('code') !!}
                                        </div>
                                    @endif
                                    <hr>
                                    {!! Form::submit(Lang::get('event.code.form.submit'), array('class' => 'btn btn-success btn-block')) !!}
                                {!! Form::close() !!}
                            @else
                                {!! Form::open(array('route' => array('event.event.post'), 'method' => 'POST', 'class' => 'form')) !!}
                                <div class="form-group">
                                    {!! Form::label('lblcode', trans('event.code.form.code')) !!}
                                    {!! Form::text('code', '', array('class'=>'form-control', 'id' => 'pass', 'placeholder'=> trans('event.code.form.code'))) !!}
                                    @if ($errors->first('code'))
                                        <div class="alert alert-danger" role="alert">{!! $errors->first('code') !!}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    {!! Form::label('ldlhabbo', trans('news.question.habbo'), array('for'=>'habbo_name'))!!}
                                    {!! Form::text('habbo_name', '', array('required', 'class'=>'form-control', 'placeholder' =>
                                    trans('news.question.habbo'))) !!}
                                    @if($errors->get('habbo_name'))
                                        <div class="alert alert-danger" role="alert">{{ $errors->first('habbo_name') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    {!! Form::label('ldllang', trans('news.question.lang'), array('for'=>'habbo_lang'))!!}
                                    {!! Form::select('habbo_lang', ['de' => 'Habbo.de', 'com' => 'Habbo.com', 'tr' =>
                                    'Habbo.com.tr'], '', array('required', 'class'=>'form-control')) !!}
                                    @if($errors->get('habbo_lang'))
                                        <div class="alert alert-danger" role="alert">{{ $errors->first('habbo_lang') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    {!! Recaptcha::render() !!}
                                    @if($errors->get('g-recaptcha-response'))
                                        <div class="alert alert-danger"
                                             role="alert">{{ $errors->first('g-recaptcha-response') }}</div>
                                    @endif
                                </div>
                                <hr>
                                {!! Form::submit(Lang::get('event.code.form.submit'), array('class' => 'btn btn-success btn-block')) !!}
                                {!! Form::close() !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/.row-->
</section><!--/#blog-->
@stop

