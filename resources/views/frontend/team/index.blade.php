@extends('frontend/layout/layout')

@section('content')
    <section id="team" class="full team">
        <div class="container">
            <div class="row">
                @foreach($teams as $team)
                    @if($team->users->count())
                        <div class="col-md-8 col-md-offset-2">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{ $team->name }}</h3>
                                </div>
                                <div class="panel-body">
                                    @foreach($team->users as $user)
                                        @if($user->user->habbo_name)
                                            <div class="col-md-3 col-sm-4 text-center">
                                                <div style="width: 60px; margin: auto;" class="text-center">
                                                    @include('frontend/userImage', ['userid' => $user->user->id])
                                                </div>
                                                {{ $user->user->habbo_name }}
                                                @if(!empty($user->user->job_title) && $user->user->job_title != '0')
                                                    <br>
                                                    <small>{{ $user->user->job_title }}</small>
                                                @endif
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </section>
@stop