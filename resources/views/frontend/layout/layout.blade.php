<!DOCTYPE html>
<html lang="{{ APP::getLocale() }}" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
    @if(App::environment('local', 'staging'))
        <meta name="robots" content="noindex,nofollow">
    @endif
    <meta charset="utf-8">
    <title>@yield('meta_titles') HabboAura.com</title>
    <meta name="description"
          content="@yield('meta_description', 'HabboAura is a Fansite for many HabboHotel of the World! We love Habbo, and Pixel Images! Come and join the Community of HabboAura.com!')">
    <meta name="keywords" content="@yield('meta_keywords', 'habbo, habboaura, worldwide, images, pixel, furni, blue, news, articles')">
    <meta name="author" content="HabboAura.com | redbeed.com">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

        @section('openGraph')

            <meta property="og:title" content="@yield('meta_titles')"/>
            <meta property="og:type" content="website"/>
            <meta property="og:url" content="http://www.HabboAura.com/{{ Request::path() }}"/>
            <meta property="og:image" content="http://www.HabboAura.com/images/logo/big_1_background.png"/>
            <meta property="og:image:width" content="150" />
            <meta property="og:image:height" content="150" />
            <meta property="og:site_name" content="HabboAura.com"/>
            <meta property="og:description" content="@yield('meta_description', 'HabboAura is a Fansite for many HabboHotel of the World! We love Habbo, and Pixel Images! Come and join the Community of HabboAura.com!')"/>

        @show

    <base href="{{ url() }}/">

    {{--{!! HTML::style("frontend/css/bootstrap.min.css") !!}--}}
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">--}}
    {!! Minify::stylesheet(array(
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css',
    '/frontend/css/font-awesome.min.css',
    '/frontend/css/prettyPhoto.css',
    '/frontend/css/animate.css',
    '/frontend/css/main.css',
    '/assets/css/github-right.css',
    '/js/plugins/owl-carousel/owl.carousel.css',
    '/js/plugins/owl-carousel/owl.theme.css',
    '/js/plugins/Magnific-Popup-1.0.0/dist/magnific-popup.css',
        '/js/plugins/Tokenize-2.4.1/jquery.tokenize.css',
    )) !!}
    {{--{!! HTML::style("frontend/css/font-awesome.min.css") !!}--}}
    {{--{!! HTML::style("frontend/css/prettyPhoto.css") !!}--}}
    {{--{!! HTML::style("frontend/css/animate.css") !!}--}}
    {{--{!! HTML::style("frontend/css/main.css") !!}--}}
    {{--{!! HTML::style("assets/css/github-right.css") !!}--}}
    {{--{!! HTML::style('js/plugins/owl-carousel/owl.carousel.css') !!}--}}
    {{--{!! HTML::style('js/plugins/owl-carousel/owl.theme.css') !!}--}}

    {!! Minify::stylesheet(array(
    '/css/app.css',
    )) !!}
    <!--[if lt IE 9]>
    {!! HTML::script("frontend/js/html5shiv.js") !!}
    {!! HTML::script("frontend/js/respond.min.js") !!}
    <![endif]-->
    <link rel="icon" type="image/png" href="favicon.png"/>
    <script>
        var mainUrl = "{{ url() }}/{{ App::getLocale() }}";
        var mainLang = "{{ App::getLocale() }}";
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-49809549-5', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<!--/head-->
<body>
@include('frontend/layout/menu')
@section('mainBody')
    @include('frontend/layout/bodyUser')
@show

@include('frontend/layout/usermenu')

{!! Minify::javascript([
    '/assets/js/jquery.2.0.3.js',
    '/frontend/js/bootstrap.min.js',
    '/frontend/js/jquery.prettyPhoto.js',
    '/frontend/js/main.js',
    '/js/plugins/eModal.min.js',
    '/js/plugins/owl-carousel/owl.carousel.min.js',
    '/js/plugins/Magnific-Popup-1.0.0/dist/jquery.magnific-popup.min.js',
    '/js/plugins/Tokenize-2.4.1/jquery.tokenize.js',
    '/js/main.js',

]) !!}
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

@section('javascript')
@show
</body>
</html>
