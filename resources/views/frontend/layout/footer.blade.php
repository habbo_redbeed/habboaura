<style>
.tagcloud a, #footer.litex .tagcloud a {
    background: none repeat scroll 0 0 #fefefe;
    border: 2px solid #e1e1e1;
    border-radius: 3px;
    color: #898989;
    display: inline-block;
    font-size: 11px;
    font-weight: bold;
    line-height: 16px;
    margin: 0 5px 5px 0;
    min-width: 18px;
    padding: 4px 10px;
    text-decoration: none;
    text-transform: uppercase;
    width: auto;
}
.tagcloud a:hover, #footer.litex .tagcloud a:hover {
    border-color: #ffcc00;
    color: #333;
}
.tagcloud a {
    background: none repeat scroll 0 0 #363636;
    border: medium none;
    color: #808080;
    display: inline-block;
    margin: 3px;
    padding: 7px 14px;
}
.tagcloud a:hover {
    background: none repeat scroll 0 0 #e84a52;
    color: #fff !important;
}
.litex .footer-in .tagcloud a {
    color: #898989;
}
.litex .footer-in .tagcloud a:hover {
    background: none repeat scroll 0 0 #fff;
    color: #333;
}
</style>
<section id="bottom" class="wet-asphalt">
    <div class="container">
        <div class="row">

            @if(count($articleLast))
                <div class="col-md-3 col-sm-6">
                    <h4>@lang('footer.title.article')</h4>

                    <div>
                        @foreach($articleLast as $article)
                            <div class="media">
                                <div class="pull-left">
                                    @if($article->path && $article->file_name)
                                        <a href="{!! URL::route('dashboard.article.show', array('slug'=>$article->slug)) !!}"><img src="{!! url($article->path . 'thumb_' . $article->file_name) !!}" style="border: 2px solid;" alt=""></a>
                                    @else
                                        <a href="{!! URL::route('dashboard.article.show', array('slug'=>$article->slug)) !!}"><img src="{!! url('assets/images/blog_s.png') !!}" alt="" style="border: 2px solid;"></a>
                                    @endif
                                </div>
                                <div class="media-body">
                                    <span class="media-heading"><a href="{!! URL::route('dashboard.article.show', array('slug'=>$article->slug)) !!}">{!! $article->title !!}</a></span>
                                    <small class="muted">{{ Carbon\Carbon::parse($article->created_at)->format('d.m.Y H:i') }}</small>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif

            @if(App::getLocale() == 'de')
                <div class="col-md-3 col-sm-6">
                    <h4>@lang('footer.title.lexicon')</h4>

                    <div>
                        @foreach($lexiconsLast as $lexicon)
                            <div class="media">
                                <div class="media-body">
                                    <span class="media-heading"><a href="{!! URL::route('lexicon.entry', array('slug'=>$lexicon->entry->slug)) !!}">{!! $lexicon->entry->title !!}</a></span>
                                    <small class="muted"><a href="{{ route('user.home.lang', array($lexicon->user->habbo_name, $lexicon->user->habbo_lang)) }}">{{ $lexicon->user->habbo_name }}</a>  <div class="flag {{ $lexicon->user->habbo_lang }}"></div> {!! Carbon\Carbon::parse($lexicon->created_at)->format('d.m.Y H:i') !!}</small>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif

            <div class="col-md-3 col-sm-6">
                <h4>@lang('footer.title.user')</h4>

                <div>
                    @foreach($userLast as $user)
                        <div class="media">
                            <div class="pull-left" style="width: 60px">
                                @include('frontend/userImage', ['userid' => $user->id])
                            </div>
                            <div class="media-body">
                                <span class="media-heading"><a href="{{ route('user.home.lang', array($user->habbo_name, $user->habbo_lang)) }}">{{ $user->habbo_name }}</a> <div class="flag {{ $user->habbo_lang }}"></div></span>
                                <small class="muted">{!! Carbon\Carbon::parse($user->created_at)->format('d.m.Y H:i') !!}</small>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <h4>@lang('footer.title.comments')</h4>

                <div>
                    @foreach($commentsLast as $comment)
                        <div class="media">
                            <div class="pull-left" style="width: 60px">
                                @include('frontend/userImage', ['userid' => $comment->user->id])
                            </div>
                            <div class="media-body">
                                <span class="media-heading"><a href="{{ route('user.home.lang', array($comment->user->habbo_name, $comment->user->habbo_lang)) }}">{{ $comment->user->habbo_name }}</a> <div class="flag {{ $comment->user->habbo_lang }}"></div> </span>
                                <small class="muted">{{ str_limit($comment->text, $limit = 25, $end = '')  }} <a href="{{ $comment->base->url }}">...@lang('main.more')</a> - {!! Carbon\Carbon::parse($comment->created_at)->format('d.m.Y H:i') !!}</small>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>


            {{--<div class="col-md-3 col-sm-6">--}}
                {{-- <h4>Tags</h4>--}}
            {{--<div class="tagcloud">--}}
                {{--<a href="">Features</a><a href="">Inspiration</a><a href="">Showcase</a>--}}
                {{--<a href="">Graphic Design</a><a href="">Illustration</a><a href="">Design</a>--}}
                {{--<a href="">Web Design</a><a href="">Video</a><a href="">ART</a><a href="">New Work</a>--}}
                {{--<a href="">Animation</a><a href="">Photoshop</a><a href="">Digital Painting</a>--}}
                {{--<a href="">CG</a><a href="">Howto</a>--}}
            {{--</div> --}}
            {{--</div>--}}

            {{--<div class="col-md-3 col-sm-6">--}}
                 {{-- <h4>Address</h4>--}}
                {{--<address>--}}
                    {{--<strong>Twitter, Inc.</strong><br>--}}
                    {{--795 Folsom Ave, Suite 600<br>--}}
                    {{--San Francisco, CA 94107<br>--}}
                    {{--<abbr title="Phone">P:</abbr> (123) 456-7890--}}
                {{--</address>--}}
                {{--<h4>{!!  trans('fully.newsletter') !!}</h4>--}}

                {{--{!! Form::open(array('route' => 'frontend.maillist.post', 'id'=>'newsletterForm', 'novalidate'=>'novalidate')) !!}--}}
                {{--<div class="input-group">--}}
                    {{--<input type="text" class="form-control" autocomplete="off" placeholder="{!!  trans('fully.enter_your_email') !!}">--}}
                    {{--<span class="input-group-btn">--}}
                        {{--<button class="btn btn-danger" type="submit">{!!  trans('fully.button_save') !!}</button>--}}
                    {{--</span>--}}
                {{--</div>--}}
                {{--{!! Form::close() !!} --}}
            {{--</div>--}}
        </div>
        <div class="row">
            <div class="col-xs-12 text-center">
                <ul class="social clearfix">
                    <li><a href="https://www.habbo.de/room/25309702" target="habbo" ><img src="http://www.habbo.de/habbo-imaging/badge/b04114s10144s11167s431177950817c44ae57fd10624cd773575808.gif" title="habboaura badge" alt="habboaura badge"></a></li>
                </ul>
            </div>
            <div class="col-xs-12 text-center">

                <ul class="social clearfix">
                    <li><a href="https://www.facebook.com/HabboAura" target="facebook" title="" data-original-title="Facebook" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-facebook"></i></a></li>
                    {{--<li><a href="#" title="" data-original-title="Google Plus" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-google-plus"></i></a></li>--}}
                    <li><a href="https://twitter.com/HabboAura" title="Twitter" target="twitter" data-original-title="Twitter" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://instagram.com/habboaura/" title="Instagram" target="instagram" data-original-title="Instagram" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="https://vine.co/u/1227550443388186624" title="Vine" target="vine" data-original-title="Vine" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-vine"></i></a></li>
                    {{--<li><a href="#" title="" data-original-title="Youtube" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-youtube"></i></a></li>--}}
                    {{--<li><a href="#" title="" data-original-title="Linkedin" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-linkedin"></i></a></li>--}}
                    {{--<li><a href="#" title="" data-original-title="Dribbble" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-dribbble"></i></a></li>--}}
                    {{--<li><a href="#" title="" data-original-title="Skype" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-skype"></i></a></li>--}}
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<footer id="footer" class="midnight-blue">
    <div class="container">
        <div class="row">
            <div class="col-sm-2 text-center">
                <ul class="language_bar_chooser">
                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                    <li>
                        <a rel="alternate" hreflang="{!!$localeCode!!}" href="{!! LaravelLocalization::getLocalizedURL($localeCode) !!}">
                            {!! $properties['native'] !!}
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-sm-4 text-center">
                &copy; 2015 HabboAura.com {!! trans('fully.all_rights_reserved') !!}.
            </div>
            <div class="col-sm-6 text-center">
                <ul class="pull-right">
                    <li><a href="/">@lang('footer.links.home')</a></li>
                    <li><a href="{{ route('dashboard.page.show', array('jobs')) }}">@lang('footer.links.job')</a></li>
                    <li><a href="{{ route('dashboard.article') }}">@lang('footer.links.news')</a></li>
                    @if(App::getLocale() == 'de') <li><a href="{{ route('lexicon.index') }}">@lang('footer.links.lexicon')</a></li> @endif
                    <li><a href="{{ route('dashboard.page.show', array('impressum')) }}">@lang('footer.links.impressum')</a></li>
                    <li><a href="{{ route('dashboard.page.show', array('datenschutzerklaerung')) }}">@lang('footer.links.datenschutzerklaerung')</a></li>
                    <li><a id="gototop" class="gototop" href="#"><i class="icon-chevron-up"></i></a></li>
                    <!--#gototop-->
                </ul>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->

<!-- login -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">@lang('user.auth.login.title')</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
    </div>
  </div>
</div>
