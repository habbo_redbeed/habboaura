@yield('content')
<section class=" user" id="userOnline">
    <div class="col-sm-10 col-sm-offset-1">
        @foreach($activehabbos as $user)
            <div itemscope itemtype="http://data-vocabulary.org/Person" class="col-lg-2 col-md-3 col-sm-4 col-xs-5 user">
                <a itemprop="url" href="{{ route('user.home.lang', [$user->user->habbo_name, $user->user->habbo_lang ]) }}">
                    <div class="col-xs-3 nopadding vcenter">
                        @include('frontend/userImage', ['userid' => $user->user->id, 'class' => ''])
                    </div>
                    <div class="col-xs-7 nopadding vcenter" itemprop="name">
                        <span itemprop="name">{{ $user->user->habbo_name }}</span> <div class="flag {{ $user->user->habbo_lang }}"></div> <br>
                        <small>{{ $user->user->data->motto or '&nbsp; ' }}</small>
                    </div>
                </a>

                <div class="clearfix"></div>
            </div>
        @endforeach
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</section>
<section class=" user" id="allOnline">
    <div class="col-sm-10 col-sm-offset-1">
        @lang('user.guest.online', ['count' => $onlineUser])
    </div>
    <div class="clearfix"></div>
</section>
<section class="ads text-center" id="footerAd">
    <div class="col-md-6 col-md-offset-3">
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <ins class="adsbygoogle"
             style="display:block; margin: auto"
             data-ad-client="ca-pub-9742479193058902"
             data-ad-slot="6967528071"
             data-ad-format="auto"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</section>
@include('frontend/layout/footer')
