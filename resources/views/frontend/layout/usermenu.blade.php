<div class="usermenu">
    <div class="bookmark">
        @if($messageCount)
            <span class="badge bg-primary">{{ $messageCount }}</span>
        @else
            <span class="waitBadge"></span>
        @endif
    </div>

    @section('usermenu')
        @if(Sentry::getUser())
            <div class="content text-center main">
                <div class="col-xs-6 col-xs-offset-3">
                    @include('frontend.userImage', ['userid' => Sentry::getUser()->id, 'class' => 'img-circle center-block center-block'])
                </div>
                <h2>{{ Sentry::getUser()->habbo_name }}<br><small>{{ Fully\Models\User::find(Sentry::getUser()->id)->starsMonthSum() }} @choice('user.stars', Fully\Models\User::find(Sentry::getUser()->id)->starsMonthSum())</small></h2>
            </div>
            <div class="content">
                <div class="text col-xs-12">
                    <div class="info">@lang('user.menu.title')</div>
                </div>
                <div class="text col-xs-12">
                    <ul class="list-unstyled">
                        <li>
                            <a href="{{ url(route('user.edit.password'))  }}">
                                <img src="/images/icons/min_lock.png" alt="@lang('user.menu.edit')">
                                @lang('user.menu.ul.edit')
                            </a>
                        </li>
                        <li>
                            <a href="{{ url(route('messages'))  }}">
                                <img src="/images/icons/min_group.png" alt="@lang('user.menu.chat')">
                                @lang('user.menu.ul.chat')
                                @if($messageCount)
                                    <span class="badge bg-primary">{{ $messageCount }}</span>
                                @else
                                    <span class="waitBadge"></span>
                                @endif
                            </a>
                        </li>
                        <li>
                            <a href="{{ url(route('user.home.lang', array(Sentry::getUser()->habbo_name, Sentry::getUser()->habbo_lang)))  }}">
                                <img src="/images/icons/min_pic.png" alt="@lang('user.menu.profil')">
                                @lang('user.menu.ul.profil')
                            </a>
                        </li>
                        {{--<li>--}}
                            {{--<a href="">--}}
                                {{--<img src="/images/icons/min_book.png" alt="@lang('user.menu.notification')">--}}
                                {{--@lang('user.menu.ul.notification')--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        <li>
                           <hr>
                        </li>
                        <li>
                            <a href="{{ url(route('user.auth.logout', array(Sentry::getUser()->habbo_name)))  }}">
                                <img src="/images/icons/min_key.gif" alt="@lang('user.menu.logout')">
                                @lang('user.menu.ul.logout')
                            </a>
                        </li>
                        @if ( Sentry::getUser()->hasAnyAccess(['admin.backend']) )
                            <li>
                                <hr>
                            </li>
                            <li>
                                <a href="{{ url(route('admin.dashboard'))  }}">
                                <img src="/images/icons/min_key.gif" alt="BACKEND">
                                BACKEND
                                </a>
                            </li>
                            <li>
                                <a href="http://intranet.habboaura.de" target="intranet">
                                <img src="/images/icons/min_key.gif" alt="INTRANET">
                                INTRANET
                                </a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        @else
            <div class="title login">
                <div class="description">
                    @lang('user.auth.login.title')
                </div>
            </div>
            <div class="content">
                <div class="icon aicon user_group col-xs-2">
                </div>
                <div class="text col-xs-12">
                    @include('user/auth/login')
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="content">
                <div class="icon aicon min_key col-xs-2">
                </div>
                <div class="text col-xs-12">
                    <a href="{{ route('user.auth.join')  }}">
                        <button type="button" class="btn btn-warning center-block btn-block">
                            @lang('user.auth.login.join')
                        </button>
                    </a>
                </div>
                <div class="clearfix"></div>
                <div class="text col-xs-12">
                    <a href="{{ route('user.auth.forget.password')  }}">
                        <button type="button" class="btn btn-warning center-block btn-block">
                            @lang('user.auth.login.forget')
                        </button>
                    </a>
                </div>
                <div class="clearfix"></div>
            </div>
        @endif
    @show
</div>