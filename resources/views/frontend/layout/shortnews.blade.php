@if($short->category)
    <div class="shortnews {{ !in_array($short->type, ['text', 'translate']) ? $short->type : '' }}">
        <div class="head">
            @if($short->icon_class )
                <div class="badgeImage icons {{ $short->icon_class  }}"></div>
            @elseif($short->category && $short->category->icon_class)
                <div class="badgeImage icons {{ $short->category->icon_class  }}"></div>
            @else
                <div class="badgeImage"></div>
            @endif
            {{ $short->user }}
            <br>
            <span class="date">{{ Carbon\Carbon::parse($short->created_at)->diffForHumans()  }}</span>
        </div>
        @if($short->pin == 1)
            <div class="pin">
                <img src="/images/icons/mid_import.gif" alt="import">
            </div>
        @endif
        @if($short->category)
            <div class="categorie {{ $short->category->color  }}">
                    <span class="categories {{ $short->category->color  }}" itemprop="name">{{ $short->category->title }}</span>
                <div class="clearfix"></div>
            </div>
        @endif
        <div class="clearfix"></div>
        <div class="text">
            {!! $short !!}<br>
            @if(!empty($short->screenshot))
                <a data-toggle="lightbox" href="{{ $short->screenshot }}">
                    <div style="background-image: url('{{ $short->screenshot }}')" class="image">
                        <div class="open">@lang('main.open.image')</div>
                    </div>
                </a>
            @endif
        </div>
        <div class="btns">
                <span class="likes btn {{ $short->category->color }}">
                    @include('frontend/likeLink', ['token' => $short->id, 'type' => 'shortnews', 'likes' => $short->likes()])
                </span>
                <span class="comments btn {{ $short->category->color }}">
                    @include('frontend/commentLink', ['token' => $short->id, 'type' => 'shortnews',  'comments' => $short->comments()])
                </span>

            <div class="clearfix"></div>
        </div>
    </div>
@endif