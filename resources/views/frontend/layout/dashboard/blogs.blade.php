@if(count($blogs))

    <div class="col-md-8">
        @if(Carbon\Carbon::parse('2015-08-17 23:59:59') > Carbon\Carbon::now())
            <a href="{{ url(route('dashboard.article.tag', ['habbo15'])) }}">
                <div class="banner season habbo15_1">
                    <div class="description">
                        more #habbo15
                    </div>
                </div>
            </a>
        @endif
        <div class="row">
            @foreach($blogs as $article)
                @include('frontend/article/min_micro', ['size' => 'small', 'article' => $article])
            @endforeach
                <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        <div class="row hidden-xs">
            @foreach($moreBlogs as $article)
                @include('frontend/article/min_micro', ['size' => 'micro', 'article' => $article])
            @endforeach
                <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-4">
        <div class="shortnewsList">

            @if($eventCount && App::getLocale() == 'de')
                @include('frontend.layout.dashboard.shortnewsEvent')
                <hr>
            @endif

            {{-- @if($eventCount && App::getLocale() == 'de')
                <p>
                    <a href="{{ route('dashboard.page.show', ['slug' => 'kalender']) }}">
                        <button type="button" class="btn btn-shortnews btn-block btn-sm">
                                <strong>{{ $eventCount }}</strong> Events verfügbar
                        </button>
                    </a>
                    <div class="eventMiddle">
                        <a href="{{ route('dashboard.page.show', ['slug' => 'kalender']) }}">
                            <button type="button" class="btn btn-shortnews view btn-block">
                                <div class="middle">
                                    @foreach($eventGroup as $event)
                                        <img src="{{ $event->icon->image }}" title="{!! str_limit(strip_tags($event->description), 20) !!}"
                                             ata-toggle="tooltip" data-placement="top">
                                    @endforeach
                                </div>
                            </button>
                        </a>
                    </div>
                    <hr>
                </p>
            @endif --}}

            @foreach($shortnews as $key => $short)
                @if($key > 3)
                    <div class="hidden-xs">
                        @include('frontend/layout/shortnews')
                    </div>
                @else
                    @include('frontend/layout/shortnews')
                @endif
            @endforeach
            <p>
                <a href="{{ route('dashboard.shortnews') }}">
                    <button type="button" class="btn more center-block btn-info btn-block">
                        @lang('main.more')
                    </button>
                </a>
            </p>
        </div>
    </div>
@endif