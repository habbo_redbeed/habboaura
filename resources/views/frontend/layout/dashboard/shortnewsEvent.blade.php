<div class="shortnews event">
    <div class="head">
    </div>
    <div class="categorie background_event">
            <span class="categories background_event"
                  itemprop="name"><strong>{{ $eventCount }}</strong> Events verfügbar</span>

        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
    <div class="text">
        <div class="row">
            <div class="col-xs-12 text-center">
                @foreach($eventGroup as $event)
                    <a href="{{ route('dashboard.page.show', ['slug' => 'kalender']) }}?date={{ $event->event_end->format('Y-m-d') }}">
                        <div class="badge center-block" style="background-image: url('{{ $event->price }}')">
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
    <div class="btns">
            <a href="{{ route('dashboard.page.show', ['slug' => 'kalender']) }}">
                <span class="likes btn background_event">
                    Kalender öffnen <i class="fa fa-angle-double-right"></i>
                </span>
            </a>

        <div class="clearfix"></div>
    </div>
</div>