<div class="container">
    <div class="col-sm-12 list">
        <div class="scroll owl-carousel" id="owl-carousel">
            @foreach($furnis as $key => $furni)

                @if(isset($link) && $link == true)
                    <a href="{{ route('furni.index') }}">
                        @endif
                        <div class="furni" id="furni{{ md5($furni['icon']) }}"
                             onClick="ga('send', 'event', 'Furni', 'Open', 'Overview');">
                            <div class="border">
                                <div class="background" style="background-image: url('{{ $furni['icon'] }}');"
                                     title="{{ $furni['name'] }}">
                                    <div class="new" style="display: {{ $furni['new'] == true ? '' : 'none' }}">NEW
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(isset($link) && $link == true)
                    </a>
                @endif


            @endforeach
        </div>
    </div>
    <div class="col-md-4 col-md-offset-4">
        <a class="btn btn-default center-block" href="{{ route('furni.index') }}" role="button">@lang('main.more')</a>
    </div>
</div>