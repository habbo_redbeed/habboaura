<div class="row">
    <div class="mainBanner lila hidden-xs">
        <div class="images">
            <div class="left"></div>
            <div class="middle">
                <span class="title">@lang('dashboard.badge.banner.title')</span>
                <span class="desc">@lang('dashboard.badge.banner.description')</span>
            </div>
            <div class="right"></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="overview">
            @foreach($badges as $key => $badge)
                <div class="badge" style="background-image: url('{{ $badge['image']  }}')" data-toggle="modal"
                     data-target=".modal-badge-overview-{{ $key  }}"></div>
            @endforeach
        </div>
    </div>
    @foreach($badges as $key => $badge)
        <div class="modal fade modal-badge-overview-{{ $key  }}" tabindex="-1" role="dialog"
             aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div role="tabpanel">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <!-- Nav tabs -->
                                        @foreach( $badge['name'] as $lang => $name )

                                            <li role="presentation" class="{{ $lang ==  $badge['lang'] ? 'active' : '' }}">
                                                <a href="#{{ $lang }}" aria-controls="{{ $lang }}" role="tab"
                                                   data-toggle="tab">{{ $lang }}</a>
                                            </li>

                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach( $badge['name'] as $lang => $name )

                                            <div role="tabpanel" class="tab-pane {{ $lang ==  $badge['lang'] ? 'active' : '' }}" id="{{ $lang }}">
                                                <table class="table table-striped">
                                                    <tr>
                                                        <td>@lang('badges.table.name')</td>
                                                        <td>{{ $name  }}</td>
                                                    </tr>
                                                    @if(isset( $badge['description'][$lang] ))
                                                        <tr>
                                                            <td>@lang('badges.table.description')</td>
                                                            <td>{{ $badge['description'][$lang] }}</td>
                                                        </tr>
                                                    @endif
                                                </table>
                                            </div>


                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
{{--<div class="row">--}}
    {{--<div class="col-md-4 col-md-offset-4">--}}
        {{--<a class="btn btn-default center-block" href="#" role="button">@lang('main.more')</a>--}}
    {{--</div>--}}
{{--</div>--}}
