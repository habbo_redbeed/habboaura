@foreach($furnis as $key => $furni)
    <div class="col-sm-12 furni" title="{{ $furni['name'] }}" data-furni="furni{{ md5($furni['icon']) }}">
        <div class="furniImage">
            <div class="direction">
                @foreach($furni['direction'] as $direction => $image)
                    <img src="{{ $image }}" alt="{{ $furni['name'] }}" id="direct{{ $direction  }}"
                         style="margin-left: -{{  $furni['size']['width']/2 }}px">
                @endforeach
            </div>
            <div class="acts">
                @foreach($furni['act'] as $act => $image)
                    <img src="{{ $image }}" alt="{{ $furni['name'] }}" id="act{{ $act  }}"
                         style="margin-left: -{{  $furni['size']['width']/2 }}px">
                @endforeach
            </div>

            <div class="container-fluid infos">
                <div class="col-sm-12">
                    <span class="title">{{ $furni['name'] }}</span>
                    <span class="description">{{ $furni['description'] }}</span>
                </div>
            </div>
            <div class="container-fluid act">
                <div class="col-xs-3" id="prev">
                    <div class="icon">
                        <div class="inner" onClick="ga('send', 'event', 'Furni', 'Turn Left', 'View Buttons');"><i class="fa fa-arrow-left"></i></div>
                    </div>
                </div>
                <div class="col-xs-6" id="click">
                    <div class="icon">
                        <div class="inner" onClick="ga('send', 'event', 'Furni', 'Click Action', 'View Buttons');"><i class="fa fa-dot-circle-o"></i></div>
                    </div>
                </div>
                <div class="col-xs-3" id="next">
                    <div class="icon">
                        <div class="inner" onClick="ga('send', 'event', 'Furni', 'Turn Right', 'View Buttons');"><i class="fa fa-arrow-right"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach