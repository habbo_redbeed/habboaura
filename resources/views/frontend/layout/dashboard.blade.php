@extends('frontend/layout/layout') @section('content')

    @include('frontend/layout/slider', $sliders)

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    <section id="lastNews" class="">
        {!! HTML::style('ckeditor/contents.css') !!}
        {!! HTML::style('code_prettify/css/prettifycss') !!}
        {!! HTML::script('code_prettify/js/prettify.js') !!}
        {!! HTML::script('assets/js/moment-with-langs.min.js') !!}
        <script type="text/javascript">
            moment().format();
            moment.lang(mainLang);

            jQuery(document).ready(function ($) {
                var now = moment();
                $('.time').each(function (i, e) {

                    var time = moment($(e).attr('datetime'));
                    $(e).html('' + time.from(now) + '');

                });
            });
        </script>
        <div class="container">
            <div class="col-sm-12">
                <div class="container-fluid">
                    <div class="blog">@include('frontend/layout/dashboard/blogs', $blogs)</div>
                </div>
            </div>
        </div>
    </section>

    <section id="lastFurnis" class="home">
        <div class="overview small">
            <div class="container">
                <div class="col-sm-12">
                    <div class="mainBanner red hidden-xs">
                        <div class="images">
                            <div class="left"></div>
                            <div class="middle">
                                <span class="title">@lang('dashboard.furni.banner.title')</span>
                                <span class="desc">@lang('dashboard.furni.banner.description')</span>
                            </div>
                            <div class="right"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="overview" class="overview">
            @include('frontend/layout/dashboard/furni_overview', ['furnis' => $furnis, 'link' => true])
        </div>
        {{--<div class="container furniView">--}}
            {{--<div class="col-sm-12">--}}
                {{--@include('frontend/layout/dashboard/furni', $furnis)--}}
            {{--</div>--}}
        {{--</div>--}}
    </section>

    <section id="badges" class="">
        <div class="container">
            <div class="col-sm-12">
                <div class="container-fluid">
                    <div class="blog">@include('frontend/layout/dashboard/badges', $blogs)</div>
                </div>
            </div>
        </div>
    </section>

    {{-- <section id="services" class="">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="media">
                        <div class="pull-left">
                            <i class="icon-twitter icon-md"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Twitter Marketing</h3>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
                        </div>
                    </div>
                </div><!--/.col-md-4-->
                <div class="col-md-4 col-sm-6">
                    <div class="media">
                        <div class="pull-left">
                            <i class="icon-facebook icon-md"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Facebook Marketing</h3>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
                        </div>
                    </div>
                </div><!--/.col-md-4-->
                <div class="col-md-4 col-sm-6">
                    <div class="media">
                        <div class="pull-left">
                            <i class="icon-google-plus icon-md"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Google Plus Marketing</h3>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
                        </div>
                    </div>
                </div><!--/.col-md-4-->
            </div>
        </div>
    </section><!--/#services--> --}}

    {{--  @include('frontend/layout/project', $projects) --}}

    {{-- <section id="testimonial" class="alizarin">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="center">
                        <h2>What our clients say</h2>
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                    </div>
                    <div class="gap"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <blockquote>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                                <small>Someone famous in <cite title="Source Title">Source Title</cite></small>
                            </blockquote>
                            <blockquote>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                                <small>Someone famous in <cite title="Source Title">Source Title</cite></small>
                            </blockquote>
                        </div>
                        <div class="col-md-6">
                            <blockquote>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                                <small>Someone famous in <cite title="Source Title">Source Title</cite></small>
                            </blockquote>
                            <blockquote>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                                <small>Someone famous in <cite title="Source Title">Source Title</cite></small>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/#testimonial--> --}}
@stop

@section('meta_titles') @lang('title.dashbaord') @stop
