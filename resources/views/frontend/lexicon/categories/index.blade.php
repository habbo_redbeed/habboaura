@extends('frontend/layout/layout')

@section('meta_titles')
    @if($selectId === false)
        @lang('title.lexicon', ['name' => trans('title.lexicon_all')])
    @else
        @foreach($categories as $cat)
            @if($cat->id == $selectId)
                @lang('title.lexicon', ['name' => $cat->name])
            @endif
        @endforeach
    @endif |
@stop

@section('content')
    <section id="categories" class="full lexicon">
        <div class="container">
            <div class="row">

                <div class="col-md-8">
                    @if($selectId !== false)
                        @include('frontend.lexicon.categories.list', ['categories' => $categories])
                    @else
                        @include('frontend.lexicon.categories.lastChange', ['lastChange' => $lastChange])
                    @endif
                </div>

                <div class="col-md-4">
                    @include('frontend.lexicon.categories.categories', ['categories' => $categories])
                </div>
            </div>
        </div>
    </section>
@stop