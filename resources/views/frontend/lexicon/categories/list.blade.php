@foreach($categories as $cat)
    @if($selectId == $cat->id)
        <div class="col-md-12">
            <div class="list">
                <div class="row">
                    <div class="row">
                        <div class="name">
                            <span class="h4">{{ $cat->name  }}</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-11 col-xs-offset-1">
                            @include('frontend.lexicon.categories.list_entrys', ['entrys' => $cat->entrysSort()])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endforeach