<div class="col-md-12">
    <div class="list">
        <h3>@lang('lexicon.change.title')</h3>
    </div>
</div>
@foreach($lastChange as $entry)
    <div class="col-md-12">
        <div class="list last">
            <div class="col-xs-2">
                @include('frontend.userImage', ['userid' => $entry->user->id, 'class' => 'img-circle out'])
            </div>
            <div class="col-xs-10 ">
                @lang('lexicon.change.user', [
                    'user' => '<b>'.$entry->user->habbo_name.'</b>',
                    'text' => '<b><a href="'.route('lexicon.entry', array('slug' => $entry->entry->slug)).'">'.$entry->entry->title.'</a></b>',
                    'date' => Carbon\Carbon::parse($entry->created_at)->format('d.M Y').' um '.Carbon\Carbon::parse($entry->created_at)->format('H:i')
                ])
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
@endforeach