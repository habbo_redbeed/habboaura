@foreach($entrys as $letter => $list)
    <div class="row">
        <div class="row">
            <div class="letter">
                <span class="h4">{{ strtoupper($letter)  }}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-11 col-xs-offset-1">
                <ul>
                    @foreach($list as $entry)

                        @if($entry->texts->count() > 0)
                            <li>
                                <a href="{{ route('lexicon.entry', array('slug' => $entry->slug))  }}">{{ $entry->title  }}</a>
                            </li>
                        @endif

                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endforeach