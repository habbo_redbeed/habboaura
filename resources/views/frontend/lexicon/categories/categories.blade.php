<div class="list">
    @foreach($categories as $cat)
        <a href="{{ route('lexicon.categorie', $cat->slug)  }}">
            <div class="categorie {{ ($cat->id == $selectId) ? 'active' : ''  }}">
                <i class="fa fa-angle-right"></i> {{ $cat->name  }}
            </div>
        </a>
    @endforeach
    @if(Sentry::getUser())
        <a href="{{ route('lexicon.entry.edit')  }}">
            <div class="categorie active">
                <i class="fa fa-plus-circle"></i> @lang('lexicon.new')
            </div>
        </a>
    @endif
    <div class="clearfix"></div>
</div>