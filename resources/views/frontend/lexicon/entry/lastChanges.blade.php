@if(count($entrys) > 0)
    <div class="list">
        <p class="h4">@lang('lexicon.change.title')</p>
        <hr>
        <div>
            @foreach($entrys as $entry)
                <div class="col-xs-3">
                    @include('frontend.userImage', ['userid' => $entry->user->id, 'class' => ' '])
                </div>
                <div class="col-xs-9">
                    @lang('lexicon.change.user', [
                        'user' => '<b>'.$entry->user->habbo_name.'</b>',
                        'text' => '<b><a href="'.route('lexicon.entry', array('slug' => $entry->entry->slug)).'">'.$entry->entry->title.'</a></b>',
                        'date' => Carbon\Carbon::parse($entry->created_at)->format('d.M Y').' um '.Carbon\Carbon::parse($entry->created_at)->format('H:i')
                    ])
                </div>
                <div class="clearfix"></div>
                <hr>
            @endforeach
        </div>
    </div>
@endif