@extends('frontend/layout/layout')

@section('meta_titles') @lang('title.lexicon', ['name' => $entry->title]) - @stop

@section('content')
    <section id="entry" class="full lexicon">
        <div class="container">
            <div class="row">

                @if($entry->status == 1)
                    <div class="col-md-8">
                        <div class="list">
                            <span class="h2">{{ $entry->title  }}</span>
                            <i>
                                <span class="h4"> - {{ $entry->category->name  }}</span>
                            </i>
                            @if(Sentry::getUser() != null)
                                <div class="pull-right">
                                    <a href="{{ route('lexicon.entry.edit', array('slug' => $entry->slug)) }}">
                                        <button type="button" class="btn btn-primary btn-sm"><i
                                                    class="fa fa-pencil-square-o"></i> EDIT
                                        </button>
                                    </a>
                                </div>
                            @endif
                            <hr>
                            @if(is_object($entry->lastTexts) && isset($entry->lastTexts->text))
                                {!! $entry->lastTexts->text !!}
                                <hr>
                                <div class="pull-right description">

                                    @lang('lexicon.change.user', [
                                        'user' => '<b>'.$entry->lastTexts->user->habbo_name.'</b>',
                                        'text' => '<b><a href="'.route('lexicon.entry', array('slug' => $entry->slug)).'">'.$entry->title.'</a></b>',
                                        'date' => Carbon\Carbon::parse($entry->lastTexts->created_at)->format('d.M Y').' um '.Carbon\Carbon::parse($entry->lastTexts->created_at)->format('H:i')
                                    ])
                                </div>
                            @endif
                            <div class="clearfix"></div>
                        </div>

                        <div class="list">
                            @include('frontend/commentNew')
                            @include('frontend/comments')
                        </div>

                    </div>

                    <div class="col-md-4">
                        @include('frontend.lexicon.categories.categories', ['categories' => $categories, 'selectId' => $entry->category->id])
                        @include('frontend.lexicon.entry.lastChanges', ['entrys' => $entry->texts])
                    </div>
                @endif
            </div>
        </div>
    </section>
@stop