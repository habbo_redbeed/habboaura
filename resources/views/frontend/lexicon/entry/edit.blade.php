@extends('frontend/layout/layout')

@section('content')
    {!! HTML::script('ckeditor/ckeditor.js') !!}
    <section id="entry" class="full lexicon">
        <div class="container">
            {!! Form::open( array( 'route' => array('lexicon.entry.save', $entry->id), 'method' => 'POST',
            'files'=>true)) !!}
            <div class="row">

                <div class="col-md-8">
                    <div class="list">
                        <label class="control-label" for="title">Titel</label>
                        {!! Form::text('title', $entry->title, array('class'=>'form-control', 'id' => 'title',
                        'placeholder'=>'Titel',
                        'value'=>Input::old('text'))) !!}
                        <hr>
                        <label class="control-label" for="text">Content</label>
                        {!! Form::textarea('text', (!empty($slug) ? $entry->lastTexts->text : ''), array('class'=>'form-control', 'id' => 'text',
                        'placeholder'=>'Content', 'value'=>Input::old('text'))) !!}

                        <hr>
                        <div class="clearfix"></div>
                        {!! Form::submit('Speichern', array('class' => 'btn btn-success')) !!}
                    </div>

                    <div class="list">
                        @include('frontend/commentNew')
                        @include('frontend/comments')
                    </div>

                </div>

                <div class="col-md-4">
                    <div class="list">
                        <p class="h4">@lang('lexicon.edit.title')</p>
                        <hr>
                        {!! Form::select('categorie_id', $categoriesArray, $entry->categorie_id, array('class' => 'form-control', 'value'=>Input::old('categorie_id'))) !!}
                        <hr>
                        {!! Form::submit('Speichern', array('class' => 'btn btn-success')) !!}
                    </div>
                    @include('frontend.lexicon.entry.lastChanges', ['entrys' => $entry->texts])
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </section>
    <script>
        window.onload = function () {
            CKEDITOR.replace('text', {
                "filebrowserBrowseUrl": "{!! url(route('frontend.imagebrowser')) !!}",
                "skin": "bootstrapck"
            });
        };
    </script>
@stop