@foreach($staffs as $lang => $staffList)
    @if(count($staffList))
        <div class="country">

            <div class="list">
                <span class="h2">@lang('lexicon.staff.country.name.'.$lang)</span>

                <div role="tabpanel">

                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#{{ $lang  }}_listnow"
                                                                  aria-controls="{{ $lang  }}_listnow" role="tab"
                                                                  data-toggle="tab">@lang('lexicon.staff.list.now')</a>
                        </li>
                        <li role="presentation"><a href="#{{ $lang  }}_listold" aria-controls="{{ $lang  }}_listold"
                                                   role="tab" data-toggle="tab">@lang('lexicon.staff.list.old')</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        @foreach($staffList as $status => $jobs)

                            <div role="tabpanel"
                                 class="tab-pane {{ $status == 'now' ? 'in active' : ''  }} fade"
                                 id="{{ $lang  }}_list{{ $status }}">
                                <div class="col-sm-12">
                                    <br>
                                    <div class="panel-group" id="{{ md5($status.$lang)  }}" role="tablist"
                                         aria-multiselectable="true">
                                        @foreach($jobs as $job => $list)
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab"
                                                     id="head_{{ md5($status.$lang)  }}">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse"
                                                           data-parent="#{{ md5($status.$lang)  }}"
                                                           href="#{{ md5($job.$lang) }}" aria-expanded="false"
                                                           aria-controls="{{ md5($job.$lang) }}">
                                                            {{ $job  }}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="{{ md5($job.$lang) }}" class="panel-collapse collapse"
                                                     role="tabpanel"
                                                     aria-labelledby="head_{{ md5($status.$lang)  }}">
                                                    <div class="panel-body">
                                                        <table class="table table-striped">
                                                            <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th>@lang('lexicon.staff.table.head.name')</th>
                                                                <th>@lang('lexicon.staff.table.head.job')</th>
                                                                <th>@lang('lexicon.staff.table.head.date')</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            @foreach($list as $staff)
                                                                <tr class="{{  $staff->job_to != '0000-00-00' ? 'cancel' : '' }}">
                                                                    <td>
                                                                        <img src="/img/profil/{{ $staff->habbo_name  }}/cloud/{{ $staff->habbo_lang  }}.png"
                                                                             class="img-responsive img-circle img"
                                                                             alt="{{ $staff->habbo_name  }} Image">
                                                                    </td>
                                                                    <td>
                                                                        @if(!empty($staff->real_name ))
                                                                            {{ $staff->real_name }}
                                                                            ({{ $staff->habbo_name  }})
                                                                        @else
                                                                            {{ $staff->habbo_name  }}
                                                                        @endif
                                                                    </td>
                                                                    <td>{{ $staff->job_title  }}</td>
                                                                    <td>
                                                                        @if($staff->job_from != '0000-00-00')
                                                                            @if($staff->job_to != '0000-00-00')
                                                                                @lang('lexicon.staff.table.from') {{ $staff->job_from  }} @lang('lexicon.staff.table.to') {{ $staff->job_to  }}
                                                                            @else
                                                                                @lang('lexicon.staff.table.since') {{ $staff->job_from  }}
                                                                            @endif
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            @endforeach

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
@endforeach