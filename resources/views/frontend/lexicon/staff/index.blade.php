@extends('frontend/layout/layout')

@section('content')
    <section id="staff" class="full lexicon">
        <div class="container">
            <div class="row">
                <div class="stafflist">
                    @include('frontend.lexicon.staff.list', ['staffs' => $staffs])
                </div>
            </div>
        </div>
    </section>
@stop

@section('meta_titles') @lang('title.staff') - @stop
