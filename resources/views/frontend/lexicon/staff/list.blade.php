<div class="row">
    <div class="country">
        <div class="row">
            <div class="clearfix"></div>
            <div class="col-sm-1 search min">
                <a id="openStaffSearch"><i class="fa fa-search"></i></a>
            </div>
            <div class="col-sm-11 search line close">
                <form class="imagesForm">
                    <div class="form-group">
                        <input type="search" class="form-control" id="staffSearch"
                               placeholder="@lang('main.search.placeholder_staff')...">
                    </div>
                </form>
            </div>
            <div class="col-sm-8 col-sm-offset-2 center countryList">
                @foreach($langs as $lang)
                    <a href="{!! route('lexicon.staff.index.lang', array($lang)) !!}">
                        <span>
                            @lang('lexicon.staff.country.name.'.$lang)
                        </span>
                    </a>
                @endforeach

            </div>
        </div>
    </div>
    <div class="countryBoxList">
        <div class="replace">
            @include('frontend.lexicon.staff.country', ['staffs' => $staffs])
        </div>
        <div class="loader">
            <i class="fa fa-refresh"></i>
        </div>
    </div>
</div>