@extends('frontend/layout/layout')

@section('content')
    <section id="habboindex" class="goodies">
        <div class="container">
            <div class="row">
                @if($habbo)
                    <div class="col-md-4">

                        <div class="title">
                            <h1>
                                habboIndex. <img src="/images/goodies/infos/habbo_1.png" align="right" alt="habbo"><br>
                                <small>one click. all infos.</small>
                            </h1>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="box">
                            <div class="content">
                                <div class="col-sm-4">
                                    <img src="http://www.habbo.{{ ($tld == 'tr' || $tld == 'br' ) ? 'com.'.$tld : $tld }}/habbo-imaging/avatarimage?user={{ $habbo['user']['name']  }}&head_direction=3&gesture=sml"
                                         class="img-responsive">
                                </div>
                                <div class="col-sm-8">
                                    <dl class="">
                                        <dt>@lang('goodies.infos.habbo.name')</dt>
                                        <dd>{{ $habbo['user']['name']  }} </dd>
                                        <dt>@lang('goodies.infos.habbo.date')</dt>
                                        <dd>{{ Carbon\Carbon::parse($habbo['user']['memberSince'])->format('d.m.Y H:i:s')  }} </dd>
                                        @if(Carbon\Carbon::parse($habbo['user']['memberSince'])->age)
                                            <dt>@lang('goodies.infos.habbo.age')</dt>
                                            <dd>{{ Carbon\Carbon::parse($habbo['user']['memberSince'])->age  }} @lang('goodies.infos.habbo.ageLabel') </dd>
                                        @endif
                                    </dl>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <dl class="">
                                            <dt>@lang('goodies.infos.habbo.motto')</dt>
                                            <dd>{{ $habbo['user']['motto']  }} </dd>
                                        </dl>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        @if(count($habbo['user']['selectedBadges']))
                            <div class="box">
                                <div class="content">
                                    <div class="h4">@lang('goodies.infos.selectBadges.title')
                                        <small> - {{ count($habbo['user']['selectedBadges']) }}</small>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="row">
                                            @foreach($habbo['user']['selectedBadges'] as $badge)
                                                <div class="badge"
                                                     style="background-image: url('http://images.habbo.com/c_images/album1584/{{ $badge['code'] }}.gif')">
                                                </div>
                                            @endforeach
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        @endif

                    </div>

                    @if($message == null)
                        <div class="col-md-8">
                            <div class="box">
                                <div class="content">
                                    <div class="col-md-4 col-sm-6">
                                        <div class="h4">@lang('goodies.infos.badge.title')
                                            <small> - {{ count($habbo['badges']) }}</small>
                                        </div>
                                        <div class="row">
                                            @for($i = 0; $i < $badgesCount; $i++)
                                                <div class="badge"
                                                     style="background-image: url('http://images.habbo.com/c_images/album1584/{{ $habbo['badges'][$i]['code'] }}.gif')">
                                                </div>
                                            @endfor
                                        </div>
                                        @if($badgesMore)
                                            <div class="row">
                                                <button class="btn btn-link" type="button" data-toggle="modal"
                                                        data-target="#badgeModal">
                                                    @lang('goodies.infos.badge.more')
                                                </button>
                                            </div>
                                        @endif
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <div class="h4">@lang('goodies.infos.friends.title')
                                            <small> - {{ count($habbo['friends']) }}</small>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <ul class="list-unstyled">
                                                    @for($i = 0; $i < $friendsCount; $i++)
                                                        <li>
                                                            <a href="{{ route('goodies.habboindex.view', [$habbo['friends'][$i]['name'], $tld]) }}">{{ $habbo['friends'][$i]['name'] }}</a>
                                                        </li>
                                                    @endfor
                                                </ul>
                                            </div>
                                            @if($friendsMore)
                                                <div class="col-md-6 hidden-sm hidden-xs">
                                                    <ul class="list-unstyled">
                                                        @for($i = $friendsCount; $i < ($friendsCount*2 - 1); $i++)
                                                            <li>
                                                                <a href="{{ route('goodies.habboindex.view', [urlencode($habbo['friends'][$i]['name']), $tld]) }}">{{ $habbo['friends'][$i]['name'] }}</a>
                                                            </li>
                                                        @endfor
                                                    </ul>
                                                </div>
                                            @endif
                                        </div>
                                        @if($friendsMore)
                                            <div class="row">
                                                <button class="btn btn-link" type="button" data-toggle="modal"
                                                        data-target="#friendsModal">
                                                    @lang('goodies.infos.friends.more')
                                                </button>
                                            </div>
                                        @endif
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <div class="h4">@lang('goodies.infos.groups.title')
                                            <small> - {{ count($habbo['groups']) }}</small>
                                        </div>
                                        <div class="row">
                                            @for($i = 0; $i < $groupsCount; $i++)
                                                <div class="badge"
                                                     style="background-image: url('http://www.habbo.{{ ($tld == 'tr' || $tld == 'br' )  ? 'com.'.$tld : $tld }}/habbo-imaging/badge/{{ $habbo['groups'][$i]['badgeCode'] }}.gif')">
                                                </div>
                                            @endfor
                                        </div>
                                        @if($groupsMore)
                                            <div class="row">
                                                <button class="btn btn-link" type="button" data-toggle="modal"
                                                        data-target="#groupsModal">
                                                    @lang('goodies.infos.groups.more')
                                                </button>
                                            </div>
                                        @endif
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-md-8">
                            <div class="alert alert-danger" role="alert">{{ $message }}</div>
                        </div>
                    @endif
                @else
                    <div class="col-md-4 col-md-offset-4">

                        <div class="title">
                            <h1>
                                habboIndex. <img src="/images/goodies/infos/habbo_1.png" align="right" alt="habbo"><br>
                                <small>one click. all infos.</small>
                            </h1>
                            <div class="clearfix"></div>
                        </div>

                        @if($message)
                            <div class="alert alert-danger" role="alert">{{ $message }}</div>
                        @endif
                        <div class="clearfix"></div>
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="box">
                        <div class="content">
                            <div class="col-md-4">
                                @lang('goodies.infos.description')
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-8">
                                <div class="col-md-6">
                                    {!! Form::open( array( 'route' => array('goodies.habboindex.view.post'), 'method' =>
                                    'POST',
                                    'files'=>false)) !!}


                                    <label class="control-label">@lang('bestlist.badge.add.name')</label>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            {!! Form::text('habboname', '', array('class'=>'form-control', 'id' =>
                                            'habboname',
                                            'placeholder'=>Lang::get('bestlist.badge.add.name'),
                                            'value'=>Input::old('habboname'))) !!}
                                        </div>
                                        <div class="col-sm-6">
                                            {!! Form::select('hotel', $hotels, $tld, array('class' => 'form-control',
                                            'value'=>Input::old('hotel'))) !!}
                                        </div>
                                        @if ($errors->first('habboname'))
                                            <div class="alert alert-danger" role="alert">{!! $errors->first('habboname')
                                                !!}
                                            </div>
                                        @endif
                                    </div>
                                    <hr>
                                    {!! Form::submit('Go', array('class' => 'btn btn-success')) !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>


        @if($habbo && $message == null)
            <!-- Modal -->
            <div class="modal fade" id="badgeModal" tabindex="-1" role="dialog" aria-labelledby="badgeModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="col-xs-12">
                                <div class="row">
                                    @foreach($habbo['badges'] as $badge)
                                        <div class="col-md-6 col-sm-12">
                                            <div class="badge"
                                                 style="background-image: url('http://images.habbo.com/c_images/album1584/{{ $badge['code'] }}.gif')">
                                            </div>
                                            <div class="col-xs-6 col-md-8">
                                                <dl class="">
                                                    <dt>@lang('badges.table.name')</dt>
                                                    <dd>{{ $badge['name']  }} </dd>
                                                    <dt>@lang('badges.table.description')</dt>
                                                    <dd>{{ $badge['description']  }} </dd>
                                                </dl>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    @endforeach
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="friendsModal" tabindex="-1" role="dialog" aria-labelledby="friendsModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="col-xs-12">
                                <div class="row">
                                    @foreach($habbo['friends'] as $friend)
                                        <div class="col-md-4 col-xs-6 col-lg-3">
                                            <a href="{{ route('goodies.habboindex.view', [urlencode($friend['name']), $tld]) }}">{{ $friend['name'] }}</a>

                                            <div class="clearfix"></div>
                                        </div>
                                    @endforeach
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="groupsModal" tabindex="-1" role="dialog" aria-labelledby="groupsModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="col-xs-12">
                                <div class="row">
                                    @foreach($habbo['groups'] as $group)
                                        <div class="col-md-6 col-sm-12">
                                            <div class="badge"
                                                 style="background-image: url('http://www.habbo.{{ ($tld == 'tr' || $tld == 'br' )  ? 'com.'.$tld : $tld }}/habbo-imaging/badge/{{ $group['badgeCode'] }}.gif')">
                                            </div>
                                            <div class="col-xs-6 col-md-8">
                                                <dl class="">
                                                    <dt>@lang('badges.table.name')</dt>
                                                    <dd>{{ $group['name']  }} </dd>
                                                    <dt>@lang('badges.table.description')</dt>
                                                    <dd>{{ $group['description']  }} </dd>
                                                </dl>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    @endforeach
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

    </section>
@stop