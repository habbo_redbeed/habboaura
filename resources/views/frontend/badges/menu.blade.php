<div class="">
    <div class="row">
        @foreach($menuNames as $name => $button)
            <div class="col-xs-{{ (int)(12/count($menuNames)) }}">
                <a href="{{ route('lexicon.badge.'.$name.'.lang', ['lang' => Request::route('lang')]) }}">
                    <div class="box {{ $menuSelect == $name ? 'active' : '' }}">{{ $button }}</div>
                </a>
            </div>
        @endforeach
    </div>
</div>
<div class="clearfix"></div>