@extends('frontend/layout/layout')
@section('content')
    <section id="badge" class="container-fluid page {{ $notClass or '' }}">
        <div class="container">

            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1 class="white">{{ $title }}</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-md-7">
                    <div class="box space">
                        <div class="row">
                            {!! Form::open( array('method' => 'GET',
                            'files'=>false, 'class' => 'form')) !!}
                            <div class="col-xs-2 col-sm-1 col-md-1">
                                <label class="control-label" for="title"><i class="fa fa-search"></i></label>
                            </div>
                            <div class="col-xs-10 col-md-8">
                                {!! Form::text('s', Input::get('search'), array('class'=>'form-control', 'id' => 'search', 'placeholder' => Lang::get('badges.page.list.search.placeholder'))) !!}
                                @if ($errors->first('search'))
                                    <div class="alert alert-danger" role="alert">{!! $errors->first('search') !!}
                                    </div>
                                @endif
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <button type="submit" class="btn btn-default btn-block">@lang('badges.page.list.search.submit')</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="box space">
                        <!--img src="/images/badges/search.png" alt="Habbos" align="right" -->
                        @lang('badges.page.list.search.info')
                        <br clear="all">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="row">
                        <div class="col-xs-8">
                            <a href="{{ route('lexicon.badge.index') }}">
                                <div class="box hover {{ ('' == $activLang) ? 'active' : '' }}">
                                    @lang('badges.page.list.choose')
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-4">
                            <a data-toggle="modal" data-target=".modal-badge-overview-badgeHelpDesk">
                                <div class="box hover help">
                                </div>
                            </a>
                        </div>
                    </div>
                    @foreach($langs as $lang)
                        <a href="{{ route('lexicon.badge.'.$menuSelect.'.lang', ['lang' => $lang['lang']]) }}">
                            <div class="box hover {{ ($lang['lang'] == $activLang) ? 'active' : '' }}">
                                @lang('badges.page.list.hotel', ['count' => $lang['count'], 'lang' => $lang['lang'] != 'br' ? $lang['lang'] : 'com.'.$lang['lang']]) <!-- div class="flag {{ $lang['lang'] }}"></div -->
                            </div>
                        </a>
                    @endforeach
                </div>
                <div class="col-md-9">

                    @include('frontend.badges.menu')

                    <div class="box list">
                        @foreach($badges as $badge)
                            <a href="#{{ $badge['code']  }}" data-toggle="modal" data-target=".modal-badge-overview-{{ $badge['code']  }}">
                                <div class="badge code" style="background-image: url('{{ $badge['image'] }}')">
                                    <div class="code">
                                        {{ $badge['code']  }}
                                    </div>
                                </div>
                            </a>
                        @endforeach
                        <div class="clearfix"></div>
                    </div>

                    <div class="box text-center">
                        {!! $badges->render() !!}
                    </div>
                </div>
            </div>

        </div>

        @foreach($badges as $badge)
            @include('frontend.badges.page.modal', ['badge' => $badge])
        @endforeach

        @include('frontend.badges.helpDesk')

    </section>
@stop

@section('javascript')
    <script>
        $(function(){
            if(window.location.hash) {
                var hash =  window.location.hash;
                hash = hash.split('-');
                console.log($(hash[0]));
                $(hash[0]).modal('toggle');
            }
        });
    </script>
@stop