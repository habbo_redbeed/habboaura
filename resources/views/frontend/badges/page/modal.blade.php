<div id="{{ $badge['code']  }}" class="modal fade modal-badge-overview-{{ $badge['code']  }}" tabindex="-1" role="dialog"
     aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <div class="badge center-block" style="background-image: url('{{ $badge['image'] }}')"> </div>
                        <p class="h4">{{ $badge['code'] }}

                            @if(isset($badge['used']) && $badge['used'])
                                <br><small>{{ $badge['used'] }}%</small>
                            @endif

                        </p>
                    </div>
                    <div class="col-xs-12">
                        <div role="tabpanel">
                            <ul class="nav nav-pills" role="tablist">
                                <!-- Nav tabs -->
                                @if(is_array($badge['name']))
                                    @foreach( $badge['name'] as $lang => $name )

                                        @if($activLang == '' || $activLang == $lang)
                                            <li role="presentation" class="{{ $lang == $activLang || $lang == $selfLang  ? 'active' : '' }}">
                                                <a href="#{{ $badge['code']  }}-{{ $lang }}" aria-controls="{{ $lang }}" role="tab"
                                                   data-toggle="tab">{{ $lang == 'br' ? 'com.br' : $lang  }}</a>
                                            </li>
                                        @endif

                                    @endforeach
                                @endif
                            </ul>
                            <div class="tab-content">
                                @if(is_array($badge['name']))
                                    @foreach( $badge['name'] as $lang => $name )

                                        @if($activLang == '' || $activLang == $lang)
                                            <div role="tabpanel" class="tab-pane {{ $lang ==  $activLang || $lang == $selfLang  ? 'active' : '' }}" id="{{ $badge['code']  }}-{{ $lang }}">
                                                <table class="table table-striped">
                                                    <tr>
                                                        <td>@lang('badges.table.name')</td>
                                                        <td>{{ $name  }}</td>
                                                    </tr>
                                                    @if(isset( $badge['description'][$lang] ))
                                                        <tr>
                                                            <td>@lang('badges.table.description')</td>
                                                            <td>{{ $badge['description'][$lang] }}</td>
                                                        </tr>
                                                    @endif
                                                </table>
                                            </div>
                                            @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>