@extends('frontend/layout/layout')
@section('content')
    <section id="badge" class="container-fluid page used">
        <div class="container">

            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1 class="white">{{ $title }}</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="row">
                        <div class="col-xs-8">
                            <a href="{{ route('lexicon.badge.all.lang') }}">
                                <div class="box hover {{ ('' == $activLang) ? 'active' : '' }}">
                                    @lang('badges.page.list.choose')
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-4">
                            <a data-toggle="modal" data-target=".modal-badge-overview-badgeHelpDesk">
                                <div class="box hover help">
                                </div>
                            </a>
                        </div>
                    </div>
                    @foreach($langs as $lang)
                        <a href="{{ route('lexicon.badge.used.lang', ['lang' => $lang['lang']]) }}">
                            <div class="box hover {{ ($lang['lang'] == $activLang) ? 'active' : '' }}">
                                @lang('badges.page.list.hotel', ['count' => $lang['count'], 'lang' => $lang['lang'] != 'br' ? $lang['lang'] : 'com.'.$lang['lang']]) <!-- div class="flag {{ $lang['lang'] }}"></div -->
                            </div>
                        </a>
                    @endforeach
                </div>
                <div class="col-md-9">

                    @include('frontend.badges.menu')

                    <div class="box list">
                        @foreach($badges as $badge)
                            <a href="#{{ $badge['code']  }}" data-toggle="modal" data-target=".modal-badge-overview-{{ $badge['code']  }}">
                                <div class="badge code {{ $badge['class']  }}" style="background-image: url('{{ $badge['image'] }}')">
                                    <div class="code">
                                        {{ $badge['code']  }}
                                    </div>
                                </div>
                            </a>
                        @endforeach
                        <div class="clearfix"></div>
                    </div>

                    <div class="box text-center">
                        {!! $badges->render() !!}
                    </div>
                </div>
            </div>

        </div>

        @foreach($badges as $badge)
            @include('frontend.badges.page.modal', ['badge' => $badge])
        @endforeach

        @include('frontend.badges.helpDesk')

    </section>
@stop

@section('javascript')
    <script>
        $(function(){
            if(window.location.hash) {
                var hash =  window.location.hash;
                hash = hash.split('-');
                console.log($(hash[0]));
                $(hash[0]).modal('toggle');
            }
        });
    </script>
@stop