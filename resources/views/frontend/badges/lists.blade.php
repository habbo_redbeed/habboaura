
<div class="col-md-4 list">
    @include('frontend/badges/rank', ['rank' => $listOne, 'plus' => 0])
</div>
<div class="col-md-4 list">
    @include('frontend/badges/rank', ['rank' => $listTwo, 'plus' => $split])
</div>
<div class="col-md-4 list">
    @include('frontend/badges/rank', ['rank' => $listThree, 'plus' => $split*2])
</div>