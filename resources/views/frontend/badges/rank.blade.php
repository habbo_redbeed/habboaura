@foreach($rank as $key => $habbo)
    <div class="row">
        <div class="col-xs-2">
            @if($key + $plus == 0)
                <img src="/images/best/one.png" alt="one">
            @elseif($key + $plus == 1)
                <img src="/images/best/two.png" alt="one">
            @elseif($key + $plus == 2)
                <img src="/images/best/three.png" alt="one">
            @else
                {{ $key + 1 + $plus  }}
            @endif
        </div>
        <div class="col-xs-8">
            {{ $habbo['name'] }}
        </div>
        <div class="col-xs-2">
            {{ $habbo['count'] }}
        </div>
    </div>
@endforeach