<div id="badgeHelpDesk" class="modal fade modal-badge-overview-badgeHelpDesk" tabindex="-1" role="dialog"
     aria-labelledby="mySmallModalLabel" >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                {!! $helpDesk->content !!}
            </div>
        </div>
    </div>
</div>