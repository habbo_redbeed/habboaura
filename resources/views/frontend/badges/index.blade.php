@extends('frontend/layout/layout')
@section('content')
    <section id="badge" class="container-fluid bestlist">
        <div class="container">
            <div class="col-sm-12 text-center banner">
                <span class="h1">@lang('bestlist.badge.title')</span>
            </div>
            <div class="col-sm-12">
                <div class="pull-right col-lg-4 col-md-5 col-sm-6 col-xs-9 langs">
                    <div class="row box">
                        <div class="col-xs-4">
                            <a href="{{ route('best.badge', ['lang' => 'de']) }}">
                                <div class="flag de center-block"></div>
                            </a>
                        </div>
                        <div class="col-xs-4">
                            <a href="{{ route('best.badge', ['lang' => 'com']) }}">
                                <div class="flag com center-block"></div>
                            </a>
                        </div>
                        <div class="col-xs-4">
                            <a href="{{ route('best.badge', ['lang' => 'tr']) }}">
                                <div class="flag tr center-block"></div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row box">
                    @include('frontend/badges/lists')
                </div>
                <div class="row box">
                    <div class="col-md-8">
                        @if (session('message'))
                            <div class="alert alert-success">
                                {!! session('message') !!}
                            </div>
                        @endif
                        <div class="col-sm-6">@lang('bestlist.badge.add.body')</div>
                        <div class="col-sm-6">
                            {!! Form::open( array( 'route' => array('best.add'), 'method' => 'POST',
                            'files'=>false)) !!}
                            <label class="control-label" for="title">@lang('bestlist.badge.add.name')</label>
                            {!! Form::text('habboname', '', array('class'=>'form-control', 'id' => 'habboname',
                            'placeholder'=>Lang::get('bestlist.badge.add.name'),
                            'value'=>Input::old('habboname'))) !!}
                            @if ($errors->first('habboname'))
                                <div class="alert alert-danger" role="alert">{!! $errors->first('habboname') !!}
                                </div>
                            @endif
                            <hr>
                            {!! Form::submit(Lang::get('bestlist.badge.add.submit'), array('class' => 'btn
                            btn-success')) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <img src="/images/best/badge/list.gif">
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop