@extends('frontend/layout/layout')

@section('content')
    <section class="message" id="show">
        <div class="container">
            <div class="col-sm-3">
                @if (Session::has('error_message'))
                    <div class="alert alert-danger" role="alert">
                        {!! Session::get('error_message') !!}
                    </div>
                @endif
                <div class="col-xs-12">
                    <div class="box">
                        <div class="media alert">
                            <a href="{{ route('messages.create') }}">
                                <button type="button" class="btn btn-danger btn-block">
                                    @lang('message.index.new')
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                @if($threads->count() > 0)
                    @foreach($threads as $thread)
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="box">
                                <?php $class = $thread->isUnread($currentUserId) ? 'alert-info' : ''; ?>
                                <div class="media alert {!!$class!!}">
                                    <h4 class="media-heading"><a
                                                href="{{ route('messages.show', array($thread->id)) }}">{{ $thread->subject }}</a>
                                    </h4>

                                    <p class="oneLine">{!! $thread->getLatestMessageAttribute()->body !!}</p>

                                    <hr>

                                    <p class="oneLine">
                                        <small>
                                            @foreach($thread->accessUser() as $user)
                                                @if(is_string($user))
                                                    {{ $user }}
                                                @elseif($user)
                                                    {{ $user->habbo_name }}
                                                    {{--@lang('message.index.count', ['count' => count($thread->accessUser())])--}}
                                                    {{--@include('frontend/userImage', ['userid' => $user->id, 'class' => 'img-circle'])--}}
                                                @endif
                                            @endforeach
                                        </small>
                                    </p>

                                    <hr>

                                    <a href="{{ route('messages.show', [$thread->id]) }}">
                                        <button type="button" class="btn btn-index btn-block">
                                            @lang('message.index.read')
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <p>Sorry, no threads.</p>
                @endif
            </div>
        </div>
    </section>
@stop