@extends('frontend/layout/layout')

@section('content')
    <section class="message" id="show">
        <div class="container">
            <div class="col-md-6 col-md-offset-1">
                <div class="box">
                    <h1>{{ $thread->subject }}</h1>
                    <hr>
                </div>

                @foreach($thread->messages as $message)
                    <div class="box">
                        <div class="media">
                            <div class="col-sm-2">
                                    @include('frontend/userImage', ['userid' => $message->user->id])

                            </div>
                            <div class="col-sm-10">
                                <div class="media-body">
                                    <h5 class="media-heading">{!! $message->user->habbo_name !!}</h5>

                                    <p>{!! smilies($message->body) !!}</p>

                                    <div class="text-muted">
                                        <small>Posted {!! $message->created_at->diffForHumans() !!}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="col-md-4">

                <div class="box">
                    <h2>@lang('message.show.title')</h2>
                    {!! Form::open(['route' => ['messages.update', $thread->id], 'method' => 'PUT']) !!}
                    <!-- Message Form Input -->
                    <div class="form-group">
                        {!! Form::textarea('message', null, ['class' => 'form-control']) !!}
                    </div>

                    @if($users->count() > 0)
                        <div class="form-group">
                            {!! Form::label('tokenize', Lang::get('message.show.subject'), ['class' => 'control-label'])
                            !!}
                            <div class="tokenize Tokenize">
                                <ul class="TokensContainer">
                                    @foreach($thread->accessUser() as $user)
                                        @if(is_string($user))
                                            <li class="Token" data-value="5">
                                                <span>{{ $user }}</span>
                                            </li>
                                        @elseif($user)
                                            <li class="Token" data-value="5">
                                                <span>{{ $user->habbo_name }}</span>
                                            </li>
                                        @endif
                                    @endforeach

                                </ul>
                                <ul class="Dropdown" style="display: none;"></ul>
                            </div>
                            {{--<select id="recipients" name="recipients" multiple="multiple" class="tokenize">--}}
                            {{--@foreach($users as $user)--}}
                            {{--<option value="{!! $user->id !!}">{!! $user->habbo_name !!} {!! $user->name !!}</option>--}}
                            {{--@endforeach--}}
                            {{--</select>--}}
                        </div>
                        @endif

                                <!-- Submit Form Input -->
                        <div class="form-group">
                            {!! Form::submit(Lang::get('message.show.submit'), ['class' => 'btn btn-primary
                            form-control'])
                            !!}
                        </div>
                        {!! Form::close() !!}
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </section>
@stop