@extends('frontend/layout/layout')

@section('content')
    <section class="message" id="show">
        <div class="container">
            <div class="col-md-8 col-md-offset-2">
                <div class="box">
                    <h1>@lang('message.create.title')</h1>
                    {!! Form::open(['route' => 'messages.store']) !!}
                    <div class="col-md-12">
                        <!-- Subject Form Input -->
                        <div class="form-group">
                            {!! Form::label('subject', Lang::get('message.create.subject'), ['class' =>
                            'control-label']) !!}
                            {!! Form::text('subject', null, ['class' => 'form-control']) !!}
                        </div>

                        <!-- Message Form Input -->
                        <div class="form-group">
                            {!! Form::label('message', Lang::get('message.create.message'), ['class' =>
                            'control-label']) !!}
                            {!! Form::textarea('message', null, ['class' => 'form-control']) !!}
                        </div>

                        @if($users->count() > 0)
                            <div class="form-group">
                                {!! Form::label('recipients', Lang::get('message.create.users'), ['class' =>
                                'control-label']) !!}
                                <select id="recipients" name="recipients[]" multiple="multiple" class="tokenizeInput">
                                    @foreach($users as $user)
                                        <option value="{!! $user->id !!}">{!! $user->habbo_name !!} {!! $user->name
                                            !!}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            @endif
                                    <!-- Submit Form Input -->
                            <div class="form-group">
                                {!! Form::submit(Lang::get('message.create.submit'), ['class' => 'btn btn-primary
                                form-control']) !!}
                            </div>
                    </div>
                    {!! Form::close() !!}
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </section>
@stop