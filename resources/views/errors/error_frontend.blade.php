@extends('frontend/layout/layout')
@section('content')
<style>
    .center {text-align: center; margin-left: auto; margin-right: auto; margin-bottom: auto; margin-top: auto;}
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="hero-unit center">
                <img src="/images/error/frank_1.png" alt="Frank" align="right">
                <h1>Oops!</h1>
                <h3>@lang('main.error.notFound')</h3>
                <br/>
                <a href="{{ route('dashboard') }}" class="btn btn-large btn-info"><i class="icon-home icon-white"></i> Take Me Home</a>
            </div>
        </div>
    </div>
</div>
@stop
