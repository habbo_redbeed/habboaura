@extends('backend/layout/layout')
@section('content')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#notification').show().delay(4000).fadeOut(700);
        });
    </script>
    <section class="content-header">
        <h1> <b>Sub</b> Stars
            <small> | Stars</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! url(getLang(). '/admin/user') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">User</li>
        </ol>
    </section>
    <br>
    <div class="container">
        <div class="row"> {{ Notification::showAll() }}


            <hr>
            <div class="col-md-6">
                <h3>Score <b>this</b> month</h3>
                <table class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th style="width: 60px;">Image</th>
                        <th>Habbo</th>
                        <th>Stars</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($score['this'] as $key => $habbo)
                        <tr>
                            <th scope="row">{{ $key+1  }}.</th>
                            <td>@include('frontend/userImage', ['userid' => $habbo->id])</td>
                            <td><a href="{{ route('user.home.lang', array($habbo->habbo_name, $habbo->habbo_lang)) }}">{{ $habbo->habbo_name }}</a></td>
                            <td>{{ $habbo->stars }} <img src="/images/icons/star_gold.png" alt="Stars"></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="col-md-6">
                <h3>Log</h3>
                <table class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th style="width: 60px;">User</th>
                        <th>add / sub</th>
                        <th>Stars</th>
                        <th>Staff</th>
                        <th>Note</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($log as $key => $action)
                        @if($action->any_habbo == '' && $action->user)
                            <tr>
                                <th scope="row">{{ Carbon\Carbon::parse($action->created_at)->format('d.m.y H:i')  }}</th>
                                <td>{{ $action->user->habbo_name }} ({{ $action->user->habbo_lang }})</td>
                                <td>{{ $action->star > 0 ? 'add' : 'sub' }}</td>
                                <td>{{ $action->star }} <img src="/images/icons/star_gold.png" alt="Stars"></td>
                                <td>{{ $action->staff->habbo_name }} ({{ $action->staff->habbo_lang }})</td>
                                <td>{{ $action->note }}</td>
                            </tr>
                        @else
                            <tr>
                                <th scope="row">{{ Carbon\Carbon::parse($action->created_at)->format('d.m.y H:i')  }}</th>
                                <td></td>
                                <td>{{ $action->star > 0 ? 'add' : 'sub' }}</td>
                                <td>{{ $action->star }} <img src="/images/icons/star_gold.png" alt="Stars"></td>
                                <td>{{ $action->staff->habbo_name }} ({{ $action->staff->habbo_lang }})</td>
                                <td>{{ $action->note }}</td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <h3><i>Score last month</i></h3>
                <table class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th style="width: 60px;">Image</th>
                        <th>Habbo</th>
                        <th>Stars</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($score['last'] as $key => $habbo)
                        <tr>
                            <th scope="row">{{ $key+1  }}.</th>
                            <td>@include('frontend/userImage', ['userid' => $habbo->id])</td>
                            <td><a href="{{ route('user.home.lang', array($habbo->habbo_name, $habbo->habbo_lang)) }}">{{ $habbo->habbo_name }}</a></td>
                            <td>{{ $habbo->stars }} <img src="/images/icons/star_gold.png" alt="Stars"></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop