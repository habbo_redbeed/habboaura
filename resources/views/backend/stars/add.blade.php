@extends('backend/layout/layout')
@section('content')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#notification').show().delay(4000).fadeOut(700);
        });
    </script>
    <style>
        div.tokenizeInput {
            width: 100%;
            border-radius: 5px;
        }

        div.Tokenize ul.TokensContainer, div.Tokenize div.Tokenize ul.Dropdown {
            border-radius: 5px;
            padding: 10px;
        }

    </style>
    <section class="content-header">
        <h1> Add Stars
            <small> | Stars</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! url(getLang(). '/admin/user') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">User</li>
        </ol>
    </section>
    <br>
    <div class="container">
        <div class="row"> {{ Notification::showAll() }}

            @if (session('check'))
                <div class="alert alert-success">
                    {{ session('check') }}
                </div>
            @endif

            {!! Form::open( array( 'route' => 'admin.stars.add.post', 'method' => 'POST')) !!}

            <div class="row">
                <div class="col-md-6">
                    <!-- User -->
                    <div class="form-group">
                        {!! Form::label('recipients', 'User(s)', ['class' =>
                        'control-label']) !!}
                        <select id="users" name="users[]" multiple="multiple" class="tokenizeInput">
                            @foreach($users as $user)
                                <option value="{!! $user->id !!}">{!! $user->habbo_name !!} ({!! $user->habbo_lang !!})
                                </option>
                            @endforeach
                        </select>
                        @if ($errors->first('users'))
                            <span class="help-block">{!! $errors->first('users') !!}</span>
                        @endif
                    </div>
                </div>

                <div class="col-md-6">
                    <h4>Anonym Habbo</h4>
                    <div class="col-md-6">
                        <!-- Any -->
                        <div class="form-group">
                            {!! Form::label('lblany_habbo', 'Habboname', ['class' =>
                            'control-label']) !!}
                            {!! Form::text('any_habbo', '', ['class' => 'form-control']) !!}
                            @if ($errors->first('any_habbo'))
                                <span class="help-block">{!! $errors->first('any_habbo') !!}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <!-- Any -->
                        <div class="form-group">
                            {!! Form::label('lblany_habbo', 'Hotel', ['class' =>
                            'control-label']) !!}
                            {!! Form::select('any_lang',  ['de' => 'habbo.de', 'com' => 'habbo.com', 'com.tr' => 'habbo.com.tr', 'es' => 'habbo.es', 'fr' => 'habbo.fr'], '', ['class' => 'form-control']) !!}
                            @if ($errors->first('any_habbo'))
                                <span class="help-block">{!! $errors->first('any_habbo') !!}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <hr>

            <!-- Message Form Input -->
            <div class="form-group">
                {!! Form::label('stars', 'Sub stars', ['class' =>
                'control-label']) !!}
                {!! Form::number('stars', null, ['class' => 'form-control']) !!}
                @if ($errors->first('stars'))
                    <span class="help-block">{!! $errors->first('stars') !!}</span>
                @endif
            </div>

            <!-- Message Form Input -->
            <div class="form-group">
                {!! Form::label('note', 'Note', ['class' =>
                'control-label']) !!}
                {!! Form::textarea('note', null, ['class' => 'form-control', 'rows' => 3]) !!}
                @if ($errors->first('note'))
                    <span class="help-block">{!! $errors->first('note') !!}</span>
                @endif
            </div>

            <!-- Message Form Input -->
            <div class="form-group">
                {!! Form::checkbox('retroactive', 'yes', false) !!}
                @if ($errors->first('retroactive'))
                    <span class="help-block">{!! $errors->first('retroactive') !!}</span>
                @endif
                {!! Form::label('lblretroactive', 'retroactively', ['class' =>
                'control-label']) !!}
            </div>


            <!-- Form actions -->
            {!! Form::submit('Save', array('class' => 'btn btn-success')) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop