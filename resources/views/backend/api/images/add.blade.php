@extends('backend/layout/layout')
@section('content')
    {!! HTML::style('assets/bootstrap/css/bootstrap-tagsinput.css') !!}
    {!! HTML::style('jasny-bootstrap/css/jasny-bootstrap.min.css') !!}
    {!! HTML::script('jasny-bootstrap/js/jasny-bootstrap.min.js') !!}
    {!! HTML::script('ckeditor/ckeditor.js') !!}
    {!! HTML::script('js/plugins/wysibb/jquery.wysibb.js') !!}
    {!! HTML::script('assets/bootstrap/js/bootstrap-tagsinput.js') !!}
    {!! HTML::script('assets/js/jquery.slug.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            $("#title").slug();

            if ($('#tag').length != 0) {
                var elt = $('#tag');
                elt.tagsinput();
            }
        });
    </script>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Add Image</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! url(getLang() . '/admin/api/image/add') !!}"><i class="fa fa-book"></i> Add Image</a></li>
            <li class="active">Add Image</li>
        </ol>
    </section>
    <br>
    <br>
    <div class="container">

        {!! Form::open( array( 'route' => array('admin.api.images.save'), 'method' => 'POST', 'files'=>true)) !!}


        <div class="control-group {!! $errors->has('users') ? 'has-error' : '' !!}">
            <label class="control-label" for="title">Tags</label>

            <div class="controls">
                {!! Form::text('tags', '', array('class'=>'form-control', 'id' => 'user',
                'placeholder'=>'Tags',
                'value'=>Input::old('user'))) !!}
                @if ($errors->first('tag'))
                    <span class="help-block">{!! $errors->first('users') !!}</span>
                @endif
            </div>
        </div>
        <br>

        <div class="container-fluid">
            <!-- Image -->
            <div class="fileinput fileinput-new control-group {!! $errors->has('image') ? 'has-error' : '' !!}"
                 data-provides="fileinput">
                <label class="control-label" for="title">Bild</label><br>

                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 120px; height: 100px;"></div>
                <div>
                    <span class="btn btn-default btn-file">
                        <span class="fileinput-new">Select image</span>
                        <span class="fileinput-exists">Change</span>
                            {!! Form::file('image', null, array('class'=>'form-control', 'id' => 'image', 'placeholder'=>'Image', 'value'=>Input::old('image'))) !!}
                        @if ($errors->first('image'))
                            <span class="help-block">{!! $errors->first('image') !!}</span>
                        @endif
                    </span>
                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                </div>
            </div>
            <br>

            {!! Form::submit('Update', array('class' => 'btn btn-success')) !!}
            {!! Form::close() !!}
            <script>

                $(document).ready(function () {

//                        $("#content").wysibb();

                    if ($('#tag').length != 0) {
                        var elt = $('#tag');
                        elt.tagsinput();
                    }
                });
                window.onload = function () {
                    CKEDITOR.replace('text', {
                        "filebrowserBrowseUrl": "{!! url('filemanager/show') !!}",
                        "skin": "bootstrapck"
                    });
                };
            </script>
            <script type="text/javascript">
                // File Picker modification for FCK Editor v2.0 - www.fckeditor.net
                // by: Pete Forde <pete@unspace.ca> @ Unspace Interactive
                //var urlobj;

                function BrowseServer() {
                    OpenServerBrowser(
                            '/js/plugins/Filemanager-2.1.0/index.html',
                            screen.width * 0.7,
                            screen.height * 0.7);
                }

                function OpenServerBrowser(url, width, height) {
                    var iLeft = (screen.width - width) / 2;
                    var iTop = (screen.height - height) / 2;
                    var sOptions = "toolbar=no,status=no,resizable=yes,dependent=yes";
                    sOptions += ",width=" + width;
                    sOptions += ",height=" + height;
                    sOptions += ",left=" + iLeft;
                    sOptions += ",top=" + iTop;
                    var oWindow = window.open(url, "BrowseWindow", sOptions);
                }

                function SetUrl(url, width, height, alt) {
                    $('[name=SRC]').val(url);
                    //document.getElementById(urlobj).value = url ;
                    oWindow = null;
                }
            </script>
        </div>
    </div>
@stop