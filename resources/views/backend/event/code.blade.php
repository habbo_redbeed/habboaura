@extends('backend/layout/layout')
@section('content')
    {!! HTML::style('assets/bootstrap/css/bootstrap-tagsinput.css') !!}
    {!! HTML::style('jasny-bootstrap/css/jasny-bootstrap.min.css') !!}
    {!! HTML::script('jasny-bootstrap/js/jasny-bootstrap.min.js') !!}
    {!! HTML::script('ckeditor/ckeditor.js') !!}
    {!! HTML::script('js/plugins/wysibb/jquery.wysibb.js') !!}
    {!! HTML::script('assets/bootstrap/js/bootstrap-tagsinput.js') !!}
    {!! HTML::script('assets/js/jquery.slug.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            $("#title").slug();

            if ($('#tag').length != 0) {
                var elt = $('#tag');
                elt.tagsinput();
            }
        });
    </script>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Shortnews</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! url(getLang() . '/admin/shortnews') !!}"><i class="fa fa-book"></i> Shortnews</a></li>
            <li class="active">Shortnews</li>
        </ol>
    </section>
    <br>
    <br>
    <div class="container">

        {!! Form::open( array( 'route' => array('admin.event.code.post'), 'method' => 'POST',
        'files'=>true)) !!}


        <div class="container-fluid">


            <div class="row">
                <div class="col-md-12">

                    @if ($errors->first('error'))
                        <div class="alert alert-danger" role="alert">{!! $errors->first('error') !!}
                        </div>
                    @endif

                    @if (Session::has('message'))
                        <div class="alert alert-success" role="alert">{!! Session::get('message') !!}
                        </div>
                    @endif

                    <!-- Content -->
                    <div class="control-group {!! $errors->has('code') ? 'has-error' : '' !!}">
                        <label class="control-label" for="title">Code</label>

                        <div class="controls">
                            {!! Form::text('code', $randCode, array('class'=>'form-control', 'id' => 'code',
                            'placeholder'=>'Code',)) !!}
                            @if ($errors->first('code'))
                                <span class="help-block">{!! $errors->first('code') !!}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <!-- Content -->
                    <div class="control-group {!! $errors->has('stars') ? 'has-error' : '' !!}">
                        <label class="control-label" for="title">Stars</label>

                        <div class="controls">
                            {!! Form::select('stars', $stars, '', array('class'=>'form-control', 'id' => 'event')) !!}
                            @if ($errors->first('stars'))
                                <span class="help-block">{!! $errors->first('stars') !!}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <!-- Content -->
                    <div class="control-group {!! $errors->has('quantity') ? 'has-error' : '' !!}">
                        <label class="control-label" for="title">Quantity</label>

                        <div class="controls">
                            {!! Form::select('quantity', $quantity, '', array('class'=>'form-control', 'id' =>
                            'quantity')) !!}
                            @if ($errors->first('quantity'))
                                <span class="help-block">{!! $errors->first('quantity') !!}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-xs-10 col-xs-offset-1">

                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Code</th>
                            <th>Stars</th>
                            <th>Used</th>
                            <th>Creator</th>
                        </tr>
                        @foreach($codes as $code)
                            <tr>
                                <td>{{ $code->id }}</td>
                                <td>{{ $code->code }}</td>
                                <td>{{ $code->stars }}</td>
                                <td>{{ $code->users }} Habbos</td>
                                <td>{{ $code->habbo_name }}</td>
                            </tr>
                        @endforeach
                    </table>

                </div>
            </div>


            {!! Form::submit('Update', array('class' => 'btn btn-success')) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop