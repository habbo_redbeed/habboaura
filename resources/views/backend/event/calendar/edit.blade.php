@extends('backend/layout/layout')
@section('content')
    {!! HTML::style('assets/bootstrap/css/bootstrap-tagsinput.css') !!}
    {!! HTML::style('jasny-bootstrap/css/jasny-bootstrap.min.css') !!}
    {!! HTML::script('jasny-bootstrap/js/jasny-bootstrap.min.js') !!}
    {!! HTML::script('ckeditor/ckeditor.js') !!}
    {!! HTML::script('js/plugins/wysibb/jquery.wysibb.js') !!}
    {!! HTML::script('assets/bootstrap/js/bootstrap-tagsinput.js') !!}
    {!! HTML::script('assets/js/jquery.slug.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            $("#title").slug();

            if ($('#tag').length != 0) {
                var elt = $('#tag');
                elt.tagsinput();
            }
        });
    </script>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Shortnews</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! url(getLang() . '/admin/shortnews') !!}"><i class="fa fa-book"></i> Shortnews</a></li>
            <li class="active">Shortnews</li>
        </ol>
    </section>
    <br>
    <br>
    <div class="container">

        {!! Form::open( array( 'route' => array('admin.event.calendar.edit.post', $event->id), 'method' => 'POST',
        'files'=>true)) !!}


        <div class="container-fluid">


            <div class="row">
                <div class="col-md-12">
                    <!-- Content -->
                    <div class="control-group {!! $errors->has('price') ? 'has-error' : '' !!}">
                        <label class="control-label" for="title">Title</label>

                        <div class="controls">
                            {!! Form::text('title', $event->title, array('class'=>'form-control', 'id' => 'title',
                            'placeholder'=>'', 'value'=>Input::old('title'))) !!}
                            @if ($errors->first('title'))
                                <span class="help-block">{!! $errors->first('title') !!}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <!-- Content -->
                    <div class="control-group {!! $errors->has('description') ? 'has-error' : '' !!}">
                        <label class="control-label" for="title">Start (YYYY-MM-DD)</label>

                        <div class="controls">
                            {!! Form::text('event_start', $event->event_start, array('class'=>'form-control', 'id' => 'event_start',
                            'placeholder'=>'', 'datetimepicker' => 'true', 'value'=>Input::old('event_start'))) !!}
                            @if ($errors->first('event_start'))
                                <span class="help-block">{!! $errors->first('event_start') !!}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <!-- Content -->
                    <div class="control-group {!! $errors->has('price') ? 'has-error' : '' !!}">
                        <label class="control-label" for="title">End (YYYY-MM-DD)</label>

                        <div class="controls">
                            {!! Form::text('event_end', $event->event_end, array('class'=>'form-control', 'id' => 'event_end',
                            'placeholder'=>'', 'datetimepicker' => 'true', 'value'=>Input::old('event_end'))) !!}
                            @if ($errors->first('event_end'))
                                <span class="help-block">{!! $errors->first('event_end') !!}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <br><br>

            <!-- area -->
            <div class="control-group {!! $errors->has('area') ? 'error' : '' !!}">
                <label class="control-label" for="area">Area</label>

                <div class="controls">
                    {!! Form::select('area', $icons, $event->area, array('class' => 'form-control',
                    'value'=>Input::old('area'))) !!}
                    @if ($errors->first('area'))
                        <span class="help-block">{!! $errors->first('area') !!}</span>
                    @endif
                </div>
            </div>
            <br>

            <div class="row">
                <div class="col-md-12">
                    <!-- Content -->
                    <div class="control-group {!! $errors->has('description') ? 'has-error' : '' !!}">
                        <label class="control-label" for="title">Description</label>

                        <div class="controls">
                            {!! Form::textarea('description', $event->description, array('class'=>'form-control', 'id' => 'description',
                            'placeholder'=>'Content', 'value'=>Input::old('description'))) !!}
                            @if ($errors->first('description'))
                                <span class="help-block">{!! $errors->first('description') !!}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <!-- Content -->
                    <div class="control-group {!! $errors->has('price') ? 'has-error' : '' !!}">
                        <label class="control-label" for="title">Price  <small>Image! (Urls Example: http://www.habboaura.com/logo.png)</small></label>

                        <div class="controls">
                            {!! Form::text('price', $event->price, array('class'=>'form-control', 'id' => 'text',
                            'placeholder'=>'Price', 'value'=>Input::old('price'))) !!}
                            @if ($errors->first('price'))
                                <span class="help-block">{!! $errors->first('price') !!}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <br><br>


            <!-- area -->
            <div class="control-group {!! $errors->has('link_room') ? 'error' : '' !!}">
                <label class="control-label" for="area">Room URL</label>

                <div class="controls">
                    {!! Form::text('link_room', $event->link_room, array('class' => 'form-control',
                    'value'=>Input::old('link_room'))) !!}
                    @if ($errors->first('link_room'))
                        <span class="help-block">{!! $errors->first('link_room') !!}</span>
                    @endif
                </div>
            </div>
            <br>

            <!-- area -->
            <div class="control-group {!! $errors->has('link_article') ? 'error' : '' !!}">
                <label class="control-label" for="area">Article URL</label>

                <div class="controls">
                    {!! Form::text('link_article', $event->link_article, array('class' => 'form-control',
                    'value'=>Input::old('link_article'))) !!}
                    @if ($errors->first('link_article'))
                        <span class="help-block">{!! $errors->first('link_article') !!}</span>
                    @endif
                </div>
            </div>
            <br>

            <!-- area -->
            <div class="control-group {!! $errors->has('link_external') ? 'error' : '' !!}">
                <label class="control-label" for="area"> Extern (other) Url</label>

                <div class="controls">
                    {!! Form::text('link_external', $event->link_external, array('class' => 'form-control',
                    'value'=>Input::old('link_external'))) !!}
                    @if ($errors->first('link_external'))
                        <span class="help-block">{!! $errors->first('link_external') !!}</span>
                    @endif
                </div>
            </div>
            <br>



            {!! Form::submit('Update', array('class' => 'btn btn-success')) !!}
            {!! Form::close() !!}
            <script>

                window.onload = function () {
                    CKEDITOR.replace('description', {
                        "filebrowserBrowseUrl": "{!! url('filemanager/show') !!}",
                        "skin": "bootstrapck",
                        "height": "300px"
                    });
                };
            </script>
            <script type="text/javascript">
                // File Picker modification for FCK Editor v2.0 - www.fckeditor.net
                // by: Pete Forde <pete@unspace.ca> @ Unspace Interactive
                //var urlobj;

                function BrowseServer() {
                    OpenServerBrowser(
                            '/js/plugins/Filemanager-2.1.0/index.html',
                            screen.width * 0.7,
                            screen.height * 0.7);
                }

                function OpenServerBrowser(url, width, height) {
                    var iLeft = (screen.width - width) / 2;
                    var iTop = (screen.height - height) / 2;
                    var sOptions = "toolbar=no,status=no,resizable=yes,dependent=yes";
                    sOptions += ",width=" + width;
                    sOptions += ",height=" + height;
                    sOptions += ",left=" + iLeft;
                    sOptions += ",top=" + iTop;
                    var oWindow = window.open(url, "BrowseWindow", sOptions);
                }

                function SetUrl(url, width, height, alt) {
                    $('[name=SRC]').val(url);
                    //document.getElementById(urlobj).value = url ;
                    oWindow = null;
                }
            </script>
        </div>
    </div>
@stop