@extends('backend/layout/layout')
@section('content')
    <section class="content-header">
        <h1> Event Calendar
            <small> | Control Panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! url(getLang() . '/admin') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Add Event</li>
        </ol>
    </section>
    <br>

    <div class="container">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                {{ Notification::showAll() }}
                <br>

                <div class="pull-left">
                    <div class="btn-toolbar">
                        <a href="{!! route('admin.event.calendar.edit') !!}" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus"></span>&nbsp;Add Entry/Event </a>
                    </div>
                </div>
                <br> <br> <br>
                @if(count($events))
                    <div class="">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Area</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Event Start</th>
                                <th>Event End</th>
                                <th>Created at</th>
                                <th>Settings</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach( $events as $event )
                                <tr>
                                    <td>
                                        @lang('icons.name.'.$event->icon->name)
                                    </td>
                                    <td>
                                        {{ str_limit($event->title, 100) }}
                                    </td>
                                    <td>
                                        {{ str_limit($event->icon->name, 100) }}
                                    </td>
                                    <td>{!! $event->event_start !!}</td>
                                    <td>{!! $event->event_end !!}</td>
                                    <td>{!! $event->created_at !!}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-danger dropdown-toggle" data-toggle="dropdown" href="#">
                                                Action <span class="caret"></span> </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{!! route('admin.event.calendar.edit', array($event->id)) !!}">
                                                        <span class="glyphicon glyphicon-edit"></span>&nbsp;Edit Event
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="{!! URL::route('admin.event.calendar.delete', array($event->id)) !!}">
                                                        <span class="glyphicon glyphicon-remove-circle"></span>&nbsp;Delete
                                                        Event </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="alert alert-danger">No results found</div>
                @endif
            </div>
        </div>
        <div class="pull-left">
            <ul class="pagination">
                {!! $events->render() !!}
            </ul>
        </div>
    </div>

@stop