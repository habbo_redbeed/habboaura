@extends('backend/layout/layout')
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> Article
            <small> | Show Question</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! langRoute('admin.article.index') !!}"><i class="fa fa-book"></i> Article</a></li>
            <li class="active">Show Question</li>
        </ol>
    </section>
    <br>
    <br>

    <div class="container">
        <div class="pull-right">
            <div class="btn-toolbar">
                <a href="{!! langRoute('admin.article.index') !!}"
                   class="btn btn-primary">
                    <span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Back
                </a>
            </div>
        </div>

        <div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1">
            <h4>NEWS:</h4>
            <h3>
                {{ $article->title }}<br>
                <small>
                    {{ $article->description }}
                </small>
            </h3>
            <hr>
            <h4>FORM:</h4>
            {!! Form::open(array('route' => ['admin.shortnews.quest.save',  $article->id], 'class' => 'form-horizontal') )!!}

            <div class="form-group">
                {!! Form::label('ldlquestion', 'Question', array('for'=>'question'))!!}
                {!! Form::text('question', $entry->description, array('required', 'class'=>'form-control')) !!}
                @if($errors->get('question'))
                    <div class="alert alert-danger" role="alert">{{ $errors->first('question') }}</div>
                @endif
            </div>

            <div class="form-group">
                {!! Form::label('ldlany', 'Who', array('for'=>'question'))!!}
                {!! Form::select('any', [0 => 'only Users', 1 => 'everyone'], $entry->any, array('required', 'class'=>'form-control')) !!}
                @if($errors->get('any'))
                    <div class="alert alert-danger" role="alert">{{ $errors->first('any') }}</div>
                @endif
            </div>

            <div class="form-group">
                {!! Form::label('ldlquestion', 'Available up (YYYY-MM-DD)', array('for'=>'question'))!!}
                {!! Form::date('available_to', $entry->available_to, array('required', 'data-datepicker' => 'datepicker', 'class'=>'form-control')) !!}
                @if($errors->get('available_to'))
                    <div class="alert alert-danger" role="alert">{{ $errors->first('available_to') }}</div>
                @endif
            </div>


            <div class="form-group">
                <div class="col-md-4 col-md-offset-4">
                    {!! Form::submit('Save', array('class' => 'btn btn-block btn-primary')) !!}
                </div>
            </div>

            {!! Form::close() !!}
            <hr>
        </div>

        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Habbo</th>
                    <th>Hotel</th>
                    <th>Text</th>
                    <th>Html-Code</th>
                    <th>user / anonym</th>
                    <th>submitted</th>
                </tr>
                </thead>
                <tbody>
                    @forelse($entry->entrys as $key => $entry)
                        @if($entry->user_id)
                            <tr>
                                <th scope="row">{{ $key + 1 }}</th>
                                <td>{{ $entry->user->habbo_name }}</td>
                                <td>Habbo.{{ $entry->user->habbo_lang }}</td>
                                <td>{{ $entry->text }}</td>
                                <td>
                                    <textarea>
                                        <div class="col-sm-8 col-sm-offset-2">
                                            <div class="row">
                                                <img class="pull-left img-responsive img-rounded" style="max-height: 70px" src="/img/profil/{{ $entry->user->id  }}.png" alt="{{ $entry->user->id  }}">
                                                <div class="h4">{{ $entry->user_id != 0 ? $entry->user->habbo_name : ''  }}</div>
                                                {{ $entry->text  }}
                                            </div>
                                        </div>
                                    </textarea>
                                </td>
                                <td><b>user</b></td>
                                <td>{{ Carbon\Carbon::parse($entry->created_at)->format('d.m.Y H:i:s') }}</td>
                            </tr>
                        @else
                            <tr>
                                <th scope="row">{{ $key + 1 }}</th>
                                <td>{{ $entry->any_habbo }}</td>
                                <td>Habbo.{{ $entry->any_lang }}</td>
                                <td>{{ $entry->text }}</td>
                                <td>
                                    <textarea>
                                        <div class="col-sm-8 col-sm-offset-2">
                                            <div class="row">
                                                <img class="pull-left img-responsive img-rounded" style="max-height: 70px" src="/img/profil/{{ $entry->any_habbo  }}/cloud/{{ $entry->any_lang }}.png" alt="{{ $entry->any_name  }}">
                                                <div class="h4">{{ $entry->any_habbo  }} ({{ $entry->any_lang  }})</div>
                                                {{ $entry->text  }}
                                            </div>
                                        </div>
                                    </textarea>
                                </td>
                                <td><b>anonym</b></td>
                                <td>{{ Carbon\Carbon::parse($entry->created_at)->format('d.m.Y H:i:s') }}</td>
                            </tr>
                        @endif
                    @empty
                        <tr>
                            <td colspan="5">Empty</td>
                        </tr>
                    @endforelse

                </tbody>
            </table>
        </div>
    </div>

@stop