@extends('backend/layout/layout')
@section('content')
    {!! HTML::style('assets/bootstrap/css/bootstrap-tagsinput.css') !!}
    {!! HTML::style('jasny-bootstrap/css/jasny-bootstrap.min.css') !!}
    {!! HTML::style('js/plugins/wysibb/theme/default/wbbtheme.css') !!}
    {!! HTML::script('jasny-bootstrap/js/jasny-bootstrap.min.js') !!}
    {!! HTML::script('ckeditor/ckeditor.js') !!}
    {!! HTML::script('js/plugins/wysibb/jquery.wysibb.js') !!}
    {!! HTML::script('assets/bootstrap/js/bootstrap-tagsinput.js') !!}
    {!! HTML::script('assets/js/jquery.slug.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            $("#title").slug();
        });
    </script>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> Article
            <small> | Add Article</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! url(getLang() . '/admin/article') !!}"><i class="fa fa-book"></i> Article</a></li>
            <li class="active">Add Article</li>
        </ol>
    </section>
    <br>
    <br>
    <div class="container">

        {!! Form::open(array('action' => '\Fully\Http\Controllers\Admin\ArticleController@store', 'files'=>true)) !!}

        <div class="container-fluid">
            <!-- Title -->
            <div class="control-group {!! $errors->has('title') ? 'has-error' : '' !!}">
                <label class="control-label" for="title">Title</label>

                <div class="controls">
                    {!! Form::text('title', null, array('class'=>'form-control', 'id' => 'title',
                    'placeholder'=>'Title',
                    'value'=>Input::old('title'))) !!}
                    @if ($errors->first('title'))
                        <span class="help-block">{!! $errors->first('title') !!}</span>
                    @endif
                </div>
            </div>
            <br>

            <!-- Tag -->
            <!-- div class="control-group {!! $errors->has('tag') ? 'has-error' : '' !!}">
        <label class="control-label" for="title">Tag</label>

        <div class="controls">
            {!! Form::text('tag', null, array('class'=>'form-control', 'id' => 'tag', 'placeholder'=>'Tag', 'value'=>Input::old('tag'))) !!}
            @if ($errors->first('tag'))
            <span class="help-block">{!! $errors->first('tag') !!}</span>
            @endif
                    </div>
                <br -->

            <!-- Create -->
            <div class="control-group {!! $errors->has('users') ? 'has-error' : '' !!}">
                <label class="control-label" for="title">Autor</label>

                <div class="controls">
                    {!! Form::text('users', null, array('class'=>'form-control', 'id' => 'users',
                    'placeholder'=>'Users',
                    'value'=>Input::old('users'))) !!}
                    @if ($errors->first('users'))
                        <span class="help-block">{!! $errors->first('users') !!}</span>
                    @endif
                </div>
            </div>
        </div>
        <br>

        <div class="container-fluid">
            <!-- Category -->
            <div class="control-group {!! $errors->has('category') ? 'error' : '' !!}">
                <label class="control-label" for="title">Category</label>

                <div class="controls">
                    {!! Form::select('category', $categories, null, array('class' => 'form-control',
                    'value'=>Input::old('category'))) !!}
                    @if ($errors->first('category'))
                        <span class="help-block">{!! $errors->first('category') !!}</span>
                    @endif
                </div>
            </div>
            <br>

            <!-- Description -->
            <div class="control-group {!! $errors->has('content') ? 'has-error' : '' !!}">
                <label class="control-label" for="title">Kurzbeschreibungstext</label>

                <div class="controls">
                    {!! Form::textarea('description', null, array('class'=>'form-control', 'id' => 'description',
                    'placeholder'=>'Beschreibungstext...', 'value'=>Input::old('description'), 'style'=>'height: 80px')) !!}
                    @if ($errors->first('content'))
                        <span class="help-block">{!! $errors->first('content') !!}</span>
                    @endif
                </div>
            </div>
            <br>

            <!-- Content -->
            <div class="control-group {!! $errors->has('content') ? 'has-error' : '' !!}">
                <label class="control-label" for="title">Content</label>

                <div class="controls">
                    {!! Form::textarea('content', null, array('class'=>'form-control', 'id' => 'content',
                    'placeholder'=>'Content', 'value'=>Input::old('content'))) !!}
                    @if ($errors->first('content'))
                        <span class="help-block">{!! $errors->first('content') !!}</span>
                    @endif
                </div>
            </div>
            <br>

            <!-- Image -->
            <div class="fileinput fileinput-new control-group {!! $errors->has('image') ? 'has-error' : '' !!}"
                 data-provides="fileinput">
                <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                     style="width: 200px; height: 150px;"></div>
                <div> <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span
                                class="fileinput-exists">Change</span> {!! Form::file('image', null, array('class'=>'form-control', 'id' => 'image', 'placeholder'=>'Image', 'value'=>Input::old('image'))) !!}
                        @if ($errors->first('image')) <span
                                class="help-block">{!! $errors->first('image') !!}</span> @endif </span>
                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a></div>
            </div>
            <br>

            <!-- Tags -->
            <div class="control-group {!! $errors->has('content') ? 'has-error' : '' !!}">
                <label class="control-label" for="title">Tags (Komma getrennt)</label>

                <div class="controls">
                    {!! Form::text('tags', null, array('class'=>'form-control', 'id' => 'content',
                    'placeholder'=>'Tags', 'value'=>Input::old('tags'))) !!}
                    @if ($errors->first('tags'))
                        <span class="help-block">{!! $errors->first('tags') !!}</span>
                    @endif
                </div>
            </div>
            <br>

            @if(Sentry::getUser()->hasAccess('article.extra.publish'))
                <!-- Published -->
                <div class="control-group {!! $errors->has('is_published') ? 'has-error' : '' !!}">

                    <div class="controls">
                        <label class="checkbox">{!! Form::checkbox('is_published', 'is_published') !!} Publish ?</label>
                        @if ($errors->first('is_published'))
                            <span class="help-block">{!! $errors->first('is_published') !!}</span>
                        @endif
                    </div>
                </div>
                <br>

                <!-- Tags -->
                <div class="control-group {!! $errors->has('content') ? 'has-error' : '' !!}">
                    <label class="control-label" for="title">Publish at</label>

                    <div class="controls">
                        {!! Form::text('publish_at', '', array('class'=>'form-control', 'id' => 'publish_at',
                        'placeholder'=>'YYYY-dd-mm HH:ii:ss', 'datetimepicker' => 'true', 'value'=>Input::old('publish_at'))) !!}
                        @if ($errors->first('tags'))
                            <span class="help-block">{!! $errors->first('tags') !!}</span>
                        @endif
                    </div>
                </div>
                <div class="clearfix"></div>
            @endif

            {!! Form::submit('Create', array('class' => 'btn btn-success')) !!}
            {!! Form::close() !!}
            <script type="text/javascript">
                window.onload = function () {
                    CKEDITOR.replace('content', {
                        customConfig: '/ckeditor/config.js',
                        "filebrowserBrowseUrl": "{!! url('filemanager/show') !!}",
                        //"skin": "bootstrapck",
                        //"extraPlugins": 'lightbox'
                    });
                };


                $(document).ready(function () {

//                    $("#content").wysibb();

                    if ($('#tag').length != 0) {
                        var elt = $('#tag');
                        elt.tagsinput();
                    }
                });
            </script>
            <script type="text/javascript">
                // File Picker modification for FCK Editor v2.0 - www.fckeditor.net
                // by: Pete Forde <pete@unspace.ca> @ Unspace Interactive
                //var urlobj;

                function BrowseServer() {
                    OpenServerBrowser(
                            '/js/plugins/Filemanager-2.1.0/index.html',
                            screen.width * 0.7,
                            screen.height * 0.7);
                }


                function OpenServerBrowser(url, width, height) {
                    var iLeft = (screen.width - width) / 2;
                    var iTop = (screen.height - height) / 2;
                    var sOptions = "toolbar=no,status=no,resizable=yes,dependent=yes";
                    sOptions += ",width=" + width;
                    sOptions += ",height=" + height;
                    sOptions += ",left=" + iLeft;
                    sOptions += ",top=" + iTop;
                    var oWindow = window.open(url, "BrowseWindow", sOptions);
                }

                function SetUrl(url, width, height, alt) {
                    $('[name=SRC]').val(url);
                    //document.getElementById(urlobj).value = url ;
                    oWindow = null;
                }

                var submitted = false;
                var userinput = false;

                $("form").submit(function() {
                    submitted = true;
                });

                $(function () {
                    $('form[action*=article] input, form[action*=article] textarea').change(function() {
                        userinput = true;
                    });
                });

                $(window).bind('beforeunload', function() {
                    if (userinput && !submitted) {
                        return 'Sie haben das Formular noch nicht abgesendet. Möchten Sie die Seite wirklich verlassen?';
                    }
                });
            </script>
        </div>
    </div>
@stop