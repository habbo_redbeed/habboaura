@extends('backend/layout/layout')
@section('content')
    <script type="text/javascript">
        $(document).ready(function () {

            // publish settings
            $(".pinit").bind("click", function (e) {
                var id = $(this).attr('id');
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "{!! url(getLang() . '/admin/shortnews/" + id + "/toggle-pin') !!}",
                    headers: {
                        'X-CSRF-Token': $('meta[name="_token"]').attr('content')
                    },
                    success: function (response) {
                        if (response['result'] == 'success') {
                            var imagePath = (response['changed'] == 1) ? "{!!url('/')!!}/assets/images/publish.png" : "{!!url('/')!!}/assets/images/not_publish.png";
                            $("#shortnews-image-" + id).attr('src', imagePath);
                        }
                    },
                    error: function () {
                        alert("error");
                    }
                })
            });
        });
    </script>
    <section class="content-header">
        <h1> Shortnews
            <small> | Control Panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! url(getLang() . '/admin') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Shortnews</li>
        </ol>
    </section>
    <br>

    <div class="container">
        <div class="row">
            {{ Notification::showAll() }}
            <br>

            <div class="pull-left">
                <div class="btn-toolbar">
                    <a href="{!! url(route('admin.shortnews.edit')) !!}" class="btn btn-primary">
                        <span class="glyphicon glyphicon-plus"></span>&nbsp;Add Shortnews </a>
                    <a href="{!! langRoute('admin.category.create') !!}" class="btn btn-primary">
                        <span class="glyphicon glyphicon-plus"></span>&nbsp;Add Category </a>
                </div>
            </div>
            <br> <br> <br>
            @if($shortnews->count())
                <div class="">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Category</th>
                            <th>User</th>
                            <th>Created Date</th>
                            <th>Updated Date</th>
                            <th>Action</th>
                            <th>PIN</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $shortnews as $short )
                            <tr>
                                <td>
                                    {{ $short->category->title  }}
                                </td>
                                <td>
                                    {{ $short->user  }}
                                </td>
                                <td>{!! $short->created_at !!}</td>
                                <td>{!! $short->updated_at !!}</td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-danger dropdown-toggle" data-toggle="dropdown" href="#">
                                            Action <span class="caret"></span> </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{!! url( route('admin.shortnews.edit', array($short->id))) !!}">
                                                    <span class="glyphicon glyphicon-eye-open"></span>&nbsp;Show News
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{!! url( route('admin.shortnews.edit', array($short->id))) !!}">
                                                    <span class="glyphicon glyphicon-edit"></span>&nbsp;Edit News
                                                </a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="{!! url( route('admin.shortnews.delete', array($short->id))) !!}">
                                                    <span class="glyphicon glyphicon-remove-circle"></span>&nbsp;Delete
                                                    News </a>
                                            </li>
                                            <li class="divider"></li>

                                        </ul>
                                    </div>
                                </td>

                                <td>
                                    <a href="#" id="{!! $short->id !!}" class="pinit">
                                        <img id="shortnews-image-{!! $short->id !!}" src="{!! url('/') !!}/assets/images/{!! ($short->pin) ? 'publish.png' : 'not_publish.png'  !!}"/>
                                    </a>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <div class="alert alert-danger">No results found</div>
            @endif
        </div>

    </div>
@stop