@extends('backend/layout/layout')
@section('content')
    {!! HTML::script('ckeditor/ckeditor.js') !!}
    <section class="content-header">
        <h1>  Images </h1>
    </section>
    <br>
    <br>
    <div class="container">

        @foreach($images as $img)
            <div class="col-xs-6 col-sm-4 col-md-3">
                <a href="{{ $img }}" target="_blank">
                    <img src="{{ $img }}" class="image img-responsive">
                </a>
            </div>
        @endforeach

        <script>
            window.onload = function () {
                CKEDITOR.replace('content', {
                    "filebrowserBrowseUrl": "{!! url('filemanager/show') !!}"
                });
            };
        </script>
    </div>
@stop
