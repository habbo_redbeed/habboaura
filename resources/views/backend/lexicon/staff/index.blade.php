@extends('backend/layout/layout')
@section('content')
    <script type="text/javascript">
        $(document).ready(function () {

            $('#notification').edit().delay(4000).fadeOut(700);

            // publish settings
            $(".publish").bind("click", function (e) {
                var id = $(this).attr('id');
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "{!! url(getLang() . '/admin/article/" + id + "/toggle-publish/') !!}",
                    headers: {
                        'X-CSRF-Token': $('meta[name="_token"]').attr('content')
                    },
                    success: function (response) {
                        if (response['result'] == 'success') {
                            var imagePath = (response['changed'] == 1) ? "{!!url('/')!!}/assets/images/publish.png" : "{!!url('/')!!}/assets/images/not_publish.png";
                            $("#publish-image-" + id).attr('src', imagePath);
                        }
                    },
                    error: function () {
                        alert("error");
                    }
                })
            });
        });
    </script>
    <section class="content-header">
        <h1> Stafflist
            <small> | Control Panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! url(getLang() . '/admin') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Shortnews</li>
        </ol>
    </section>
    <br>

    <div class="container">
        <div class="row">
            {{ Notification::showAll() }}
            <br>

            <div class="pull-left">
                <div class="btn-toolbar">
                    <a href="{!! url(route('admin.lexicon.staff.edit')) !!}" class="btn btn-primary">
                        <span class="glyphicon glyphicon-plus"></span>&nbsp;Add Staff </a>
                </div>
            </div>
            <div class="pull-left">
                <div class="btn-group">
                    <a class="btn btn-danger dropdown-toggle" data-toggle="dropdown" href="#">
                        Country <span class="caret"></span> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{!! url( route('admin.lexicon.staff.index')) !!}">
                                ALL
                            </a>
                        </li>
                        @foreach($langs as $lang)
                            <li>
                                <a href="{!! url( route('admin.lexicon.staff.index.lang', array($lang))) !!}">
                                    {{ strtoupper($lang)  }}
                                </a>
                            </li>
                        @endforeach

                    </ul>
                </div>
            </div>
            <br> <br> <br>
            @if($staffs->count())
                <div class="">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Country</th>
                            <th>Job Title</th>
                            <th>Since</th>
                            <th>Until</th>
                            <th>Created Date</th>
                            <th>Updated Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $staffs as $staff )
                            <tr>
                                <td>
                                    @if(!empty($staff->real_name))
                                        {{ $staff->real_name  }} ({{ $staff->habbo_name  }})
                                    @else
                                        {{ $staff->habbo_name  }}
                                    @endif
                                </td>
                                <td>{!! $staff->habbo_lang !!}</td>
                                <td>{!! $staff->job_title !!}</td>
                                <td>{!! $staff->job_from !!}</td>
                                <td>{!! $staff->job_to !!}</td>
                                <td>{!! $staff->created_at !!}</td>
                                <td>{!! $staff->updated_at !!}</td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-danger dropdown-toggle" data-toggle="dropdown" href="#">
                                            Action <span class="caret"></span> </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{!! url( route('admin.lexicon.staff.edit', array($staff->id))) !!}">
                                                    <span class="glyphicon glyphicon-edit"></span>&nbsp;Edit
                                                </a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="{!! url( route('admin.lexicon.staff.delete', array($staff->id))) !!}">
                                                    <span class="glyphicon glyphicon-remove-circle"></span>&nbsp;Delete
                                                </a>
                                            </li>
                                            <li class="divider"></li>

                                        </ul>
                                    </div>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <div class="alert alert-danger">No results found</div>
            @endif
        </div>

    </div>
@stop