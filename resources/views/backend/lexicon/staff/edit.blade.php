@extends('backend/layout/layout')
@section('content')
    {!! HTML::style('assets/bootstrap/css/bootstrap-tagsinput.css') !!}
    {!! HTML::style('jasny-bootstrap/css/jasny-bootstrap.min.css') !!}
    {!! HTML::style('/js/plugins/datepicker/css/bootstrap-datetimepicker.min.css') !!}
    {!! HTML::script('jasny-bootstrap/js/jasny-bootstrap.min.js') !!}
    {!! HTML::script('ckeditor/ckeditor.js') !!}
    {!! HTML::script('js/plugins/wysibb/jquery.wysibb.js') !!}
    {!! HTML::script('assets/bootstrap/js/bootstrap-tagsinput.js') !!}
    {!! HTML::script('assets/js/jquery.slug.js') !!}
    {!! HTML::script('/js/plugins/moment.js') !!}
    {!! HTML::script('/js/plugins/datepicker/js/bootstrap-datetimepicker.min.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            $("#title").slug();

            if ($('#tag').length != 0) {
                var elt = $('#tag');
                elt.tagsinput();
            }
        });
    </script>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Staff</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! url(getLang() . '/admin/lexicon/staff') !!}"><i class="fa fa-book"></i> Staff-List</a></li>
            <li class="active">Staff</li>
        </ol>
    </section>
    <br>
    <br>
    <div class="container">

        {!! Form::open( array( 'route' => array('admin.lexicon.staff.save', $staff->id), 'method' => 'POST',
        'files'=>true)) !!}


        <div class="control-group {!! $errors->has('real_name') ? 'has-error' : '' !!}">
            <label class="control-label" for="title">Real Name</label>

            <div class="controls">
                {!! Form::text('real_name', $staff->real_name, array('class'=>'form-control', 'id' => 'real_name',
                'placeholder'=>'Real Name',
                'value'=>Input::old('real_name'))) !!}
                @if ($errors->first('real_name'))
                    <span class="help-block">{!! $errors->first('real_name') !!}</span>
                @endif
            </div>
        </div>
        <br>

        <!-- Category -->
        <div class="control-group {!! $errors->has('habbo_lang') ? 'error' : '' !!}">
            <label class="control-label" for="title">Country</label>

            <div class="controls">
                {!! Form::select('habbo_lang', [
                    'int' => 'Sulake ',
                    'de' => 'Germany',
                    'com-br' => 'Brasil',
                    'com' => 'International (.com) ',
                    'es' => 'Spain',
                    'fi' => 'Finland',
                    'fr' => 'France',
                    'it' => 'Italy',
                    'nl' => 'Netherlands',
                    'com-tr' => 'Turkey',
                ],
                $staff->habbo_lang, array('class' => 'form-control',
                'value'=>Input::old('habbo_lang'))) !!}
                @if ($errors->first('habbo_lang'))
                    <span class="help-block">{!! $errors->first('habbo_lang') !!}</span>
                @endif
            </div>
        </div>
        <br>

        <div class="control-group {!! $errors->has('habbo_name') ? 'has-error' : '' !!}">
            <label class="control-label" for="title">Habbo Name</label>

            <div class="controls">
                {!! Form::text('habbo_name', $staff->habbo_name, array('class'=>'form-control', 'id' => 'habbo_name',
                'placeholder'=>'Habbo Name',
                'value'=>Input::old('habbo_name'))) !!}
                @if ($errors->first('habbo_name'))
                    <span class="help-block">{!! $errors->first('habbo_name') !!}</span>
                @endif
            </div>
        </div>
        <br>

        <div class="control-group {!! $errors->has('job_title') ? 'has-error' : '' !!}">
            <label class="control-label" for="title">Job Title</label>

            <div class="controls">
                {!! Form::text('job_title', $staff->job_title, array('class'=>'form-control', 'id' => 'job_title',
                'placeholder'=>'Job',
                'value'=>Input::old('job_title'))) !!}
                @if ($errors->first('job_title'))
                    <span class="help-block">{!! $errors->first('job_title') !!}</span>
                @endif
            </div>
        </div>
        <br>

        <div class="control-group {!! $errors->has('job_from') ? 'has-error' : '' !!}">
            <label class="control-label" for="title">Seit</label>

            <div class="controls" style="position: relative">
                {!! Form::text('job_from', $staff->job_from != '0000-00-00' && !empty($staff->job_from) ? Carbon\Carbon::parse($staff->job_from)->format('d.m.Y') : '', array('class'=>'form-control', 'id' => 'job_from',
                'placeholder'=>'Since',
                'value'=>Input::old('job_from'))) !!}
                @if ($errors->first('job_from'))
                    <span class="help-block">{!! $errors->first('job_from') !!}</span>
                @endif
            </div>
            <script type="text/javascript">
                $(function () {
                    $('#job_from').datetimepicker({
                        {!! $staff->job_from != '0000-00-00' && !empty($staff->job_from) ? 'defaultDate: "'.Carbon\Carbon::parse($staff->job_from)->format('d.m.Y').'",' : ''  !!}
                        viewMode: 'years',
                        format: 'DD.MM.YYYY',
                        locale: 'de'
                    });
                });
            </script>
        </div>
        <br>

        <div class="control-group {!! $errors->has('job_to') ? 'has-error' : '' !!}">
            <label class="control-label" for="title">Bis</label>

            <div class="controls" style="position: relative">
                {!! Form::text('job_to', $staff->job_to != '0000-00-00' && !empty($staff->job_to) ? Carbon\Carbon::parse($staff->job_to)->format('d.m.Y') : '', array('class'=>'form-control', 'id' => 'job_to',
                'placeholder'=>'Bis',
                'value'=>Input::old('job_to'))) !!}
                @if ($errors->first('job_to'))
                    <span class="help-block">{!! $errors->first('job_to') !!}</span>
                @endif
            </div>
            <script type="text/javascript">
                $(function () {
                    $('#job_to').datetimepicker({
                        {!! ($staff->job_to != '0000-00-00' && !empty($staff->job_to) ) ? 'defaultDate: "'.Carbon\Carbon::parse($staff->job_to)->format('d.m.Y').'",' : ''  !!}
                        viewMode: 'years',
                        format: 'DD.MM.YYYY',
                        locale: 'de'
                    });
                });
            </script>
        </div>
        <br>

        <br>

        {!! Form::submit('Update', array('class' => 'btn btn-success')) !!}
        {!! Form::close() !!}
        <script>

            $(document).ready(function () {

//                        $("#content").wysibb();

                if ($('#tag').length != 0) {
                    var elt = $('#tag');
                    elt.tagsinput();
                }
            });
            window.onload = function () {
                CKEDITOR.replace('text', {
                    "filebrowserBrowseUrl": "{!! url('filemanager/show') !!}",
                    "skin": "bootstrapck"
                });
            };
        </script>
        <script type="text/javascript">
            // File Picker modification for FCK Editor v2.0 - www.fckeditor.net
            // by: Pete Forde <pete@unspace.ca> @ Unspace Interactive
            //var urlobj;

            function BrowseServer() {
                OpenServerBrowser(
                        '/js/plugins/Filemanager-2.1.0/index.html',
                        screen.width * 0.7,
                        screen.height * 0.7);
            }

            function OpenServerBrowser(url, width, height) {
                var iLeft = (screen.width - width) / 2;
                var iTop = (screen.height - height) / 2;
                var sOptions = "toolbar=no,status=no,resizable=yes,dependent=yes";
                sOptions += ",width=" + width;
                sOptions += ",height=" + height;
                sOptions += ",left=" + iLeft;
                sOptions += ",top=" + iTop;
                var oWindow = window.open(url, "BrowseWindow", sOptions);
            }

            function SetUrl(url, width, height, alt) {
                $('[name=SRC]').val(url);
                //document.getElementById(urlobj).value = url ;
                oWindow = null;
            }
        </script>
    </div>
@stop