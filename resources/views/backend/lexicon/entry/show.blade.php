@extends('backend/layout/layout')
@section('content')
    {!! HTML::style('assets/bootstrap/css/bootstrap-tagsinput.css') !!}
    {!! HTML::style('jasny-bootstrap/css/jasny-bootstrap.min.css') !!}
    {!! HTML::style('js/plugins/wysibb/theme/default/wbbtheme.css') !!}
    {!! HTML::script('jasny-bootstrap/js/jasny-bootstrap.min.js') !!}
    {!! HTML::script('ckeditor/ckeditor.js') !!}
    {!! HTML::script('js/plugins/wysibb/jquery.wysibb.js') !!}
    {!! HTML::script('assets/bootstrap/js/bootstrap-tagsinput.js') !!}
    {!! HTML::script('assets/js/jquery.slug.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            $("#title").slug();

            if ($('#tag').length != 0) {
                var elt = $('#tag');
                elt.tagsinput();
            }
        });
    </script>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> Article
            <small> | Update Article</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! url(getLang() . '/admin/article') !!}"><i class="fa fa-book"></i> Article</a></li>
            <li class="active">Update Article</li>
        </ol>
    </section>
    <br>
    <br>
    <div class="container">

        {!! Form::open( array( 'route' => array('admin.lexicon.entry.save', $text->id), 'method' => 'PATCH',
        'files'=>true)) !!}

        @if(isset($text->new_title) && !empty($text->new_title))
            <!-- Title -->
            <div class="control-group {!! $errors->has('title') ? 'has-error' : '' !!}">
                <label class="control-label" for="title">Title</label>

                <div class="controls">
                    {!! Form::text('title', $text->new_title, array('class'=>'form-control', 'id' => 'title',
                    'placeholder'=>'Title', 'value'=>Input::old('title'))) !!}
                    @if ($errors->first('title'))
                        <span class="help-block">{!! $errors->first('title') !!}</span>
                    @endif
                </div>
            </div>
            <br>

        @endif

        <div class="container-fluid">

            @if(isset($text->new_categorie_id) && !empty($text->new_categorie_id))
                <!-- Category -->
                <div class="control-group {!! $errors->has('category') ? 'error' : '' !!}">
                    <label class="control-label" for="title">Category</label>

                    <div class="controls">
                        {!! Form::select('category', $categoriesArray, $text->new_categorie_id, array('class' =>
                        'form-control',
                        'value'=>Input::old('category'))) !!}
                        @if ($errors->first('category'))
                            <span class="help-block">{!! $errors->first('category') !!}</span>
                        @endif
                    </div>
                </div>
                <br>
                @endif

                        <!-- Content -->
                <div class="control-group {!! $errors->has('content') ? 'has-error' : '' !!}">
                    <label class="control-label" for="title">Content</label>

                    <div class="controls">
                        {!! Form::textarea('content', $text->text, array('class'=>'form-control', 'id' => 'content',
                        'placeholder'=>'Content', 'value'=>Input::old('content'))) !!}
                        @if ($errors->first('content'))
                            <span class="help-block">{!! $errors->first('content') !!}</span>
                        @endif
                    </div>
                </div>
                <br>


                <!-- Image -->
                <div class="fileinput fileinput-new control-group {!! $errors->has('image') ? 'has-error' : '' !!}"
                     data-provides="fileinput">


                    <!-- Published -->
                    <div class="control-group {!! $errors->has('is_published') ? 'has-error' : '' !!}">

                        <div class="controls">
                            <label class="checkbox">{!! Form::checkbox('is_published',
                                'is_published',$text->is_published) !!} Publish ?</label>
                            @if ($errors->first('is_published'))
                                <span class="help-block">{!! $errors->first('is_published') !!}</span>
                            @endif
                        </div>
                    </div>
                    <br>
                    {!! Form::submit('Update', array('class' => 'btn btn-success')) !!}
                    {!! Form::close() !!}
                    <script>

                        $(document).ready(function () {

//                        $("#content").wysibb();

                            if ($('#tag').length != 0) {
                                var elt = $('#tag');
                                elt.tagsinput();
                            }
                        });
                        window.onload = function () {
                            CKEDITOR.replace('content', {
                                "filebrowserBrowseUrl": "{!! url('filemanager/show') !!}",
                                "skin": "bootstrapck"
                            });
                        };
                    </script>
                    <script type="text/javascript">
                        // File Picker modification for FCK Editor v2.0 - www.fckeditor.net
                        // by: Pete Forde <pete@unspace.ca> @ Unspace Interactive
                        //var urlobj;

                        function BrowseServer() {
                            OpenServerBrowser(
                                    '/js/plugins/Filemanager-2.1.0/index.html',
                                    screen.width * 0.7,
                                    screen.height * 0.7);
                        }

                        function OpenServerBrowser(url, width, height) {
                            var iLeft = (screen.width - width) / 2;
                            var iTop = (screen.height - height) / 2;
                            var sOptions = "toolbar=no,status=no,resizable=yes,dependent=yes";
                            sOptions += ",width=" + width;
                            sOptions += ",height=" + height;
                            sOptions += ",left=" + iLeft;
                            sOptions += ",top=" + iTop;
                            var oWindow = window.open(url, "BrowseWindow", sOptions);
                        }

                        function SetUrl(url, width, height, alt) {
                            $('[name=SRC]').val(url);
                            //document.getElementById(urlobj).value = url ;
                            oWindow = null;
                        }
                    </script>
                </div>
        </div>
@stop