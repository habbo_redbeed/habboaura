@extends('backend/layout/layout')
@section('content')
    <script type="text/javascript">
        $(document).ready(function () {

            $('#notification').edit().delay(4000).fadeOut(700);

        });
    </script>
    <section class="content-header">
        <h1> Lexicon Edit's
            <small> | Control Panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! url(getLang() . '/admin') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">texts</li>
        </ol>
    </section>
    <br>

    <div class="container">
        <div class="row">
            {{ Notification::showAll() }}
            <br>
            <div class="pull-left">
                <div class="btn-group">
                    <a class="btn btn-danger dropdown-toggle" data-toggle="dropdown" href="#">
                        Category <span class="caret"></span> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{!! url( route('admin.lexicon.entry.index')) !!}">
                                ALL
                            </a>
                        </li>
                        @foreach($categories as $category)
                            <li>
                                <a href="{!! url( route('admin.lexicon.entry.index', array($category->slug))) !!}">
                                    {{ strtoupper($category->name)  }}
                                </a>
                            </li>
                        @endforeach

                    </ul>
                </div>
            </div>
            <br> <br> <br>
            @if($texts->count())
                <div class="">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Category</th>
                            <th>Titel</th>
                            <th>User</th>
                            <th>Created Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $texts as $text )
                            <tr>
                                <td>
                                    {{ $text->entry->category->name  }}
                                </td>
                                <td>{{ $text->new_title }}</td>
                                <td>{{ $text->user->habbo_name  }}</td>
                                <td>{{ $text->created_at }}</td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-danger dropdown-toggle" data-toggle="dropdown" href="#">
                                            Action <span class="caret"></span> </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{!! url( route('admin.lexicon.entry.show', array($text->id))) !!}">
                                                    <span class="glyphicon glyphicon-edit"></span>&nbsp;Edit
                                                </a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="{!! url( route('admin.lexicon.delete', array($text->id))) !!}">
                                                    <span class="glyphicon glyphicon-remove-circle"></span>&nbsp;Delete
                                                </a>
                                            </li>
                                            <li class="divider"></li>

                                        </ul>
                                    </div>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <div class="alert alert-danger">No results found</div>
            @endif
        </div>

    </div>
@stop