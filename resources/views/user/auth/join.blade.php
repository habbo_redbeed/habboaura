@extends('frontend/layout/layout')
@section('content')

    <section id="join" class="container-fluid headerClean user">
        <div class="row">

            <div class="col-xs-12 text-center">
                <img src="/images/logo/big_1.png" alt="Logo Big"><br>
                <span class="h1 title">
                    @lang('user.join.title')
                </span>
            </div>

        </div>

        <div class="row padding">
            {!! Form::open(array('route' => array('user.auth.post.join'), 'method' => 'POST', 'class' =>
            'form-horizontal')) !!}

            <div class="container">
                <div class="row">
                    @if ($errors->first())
                        <div class="col-md-6 col-md-offset-3 active step">
                            <div class="alert alert-danger" role="alert">
                                {!! $errors->first() !!}
                            </div>
                        </div>
                    @endif
                    <div class="col-md-6 col-md-offset-3 active step">
                        <div class="row">
                            <span class="h3 title">
                                @lang('user.join.step.title.one')
                            </span>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <img src="/images/user/join/frank_1.gif" align="right" alt="Frank Step One">
                                @lang('user.join.step.one.body')
                                {!! Form::text('misson_code', $hash, array('class'=>'form-control', 'id' => 'user',
                                'placeholder'=>$hash,
                                'value'=>Input::old('misson_code'))) !!}
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="col-md-8 col-md-offset-2 ">
                                    <button type="button"
                                            class="btn btn-primary btn-block next">@lang('user.join.next')</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-md-offset-3 step">
                        <div class="row">
                            <span class="h3 title">
                                @lang('user.join.step.title.two')
                            </span>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <img src="/images/user/join/frank_2.gif" align="right" alt="Frank Step One">
                                @lang('user.join.step.two.body')
                                <label class="control-label" for="title">@lang('user.join.step.two.habbo')</label>
                                {!! Form::text('habbo_name', '', array('class'=>'form-control', 'id' => 'habbo_name',
                                'placeholder'=> Lang::get('user.join.step.two.habbo'),
                                'value'=>Input::old('habbo_name'))) !!}
                                @if ($errors->first('habbo_name'))
                                    <div class="alert alert-danger" role="alert">{!! $errors->first('habbo_name') !!}
                                    </div>
                                @endif
                                <label class="control-label" for="title">@lang('user.join.step.two.country')</label>
                                {!! Form::select('habbo_lang', $hotels, '', array('class' => 'form-control',
                                'value'=>Input::old('habbo_lang'))) !!}
                                @if ($errors->first('habbo_lang'))
                                    <div class="alert alert-danger" role="alert">{!! $errors->first('habbo_lang') !!}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="col-md-8 col-md-offset-2 ">
                                    <button type="button"
                                            class="btn btn-primary btn-block next">@lang('user.join.next')</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-md-offset-3 step">
                        <div class="row">
                            <span class="h3 title">
                                @lang('user.join.step.title.three')
                            </span>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <img src="/images/user/join/frank_3.gif" align="right" alt="Frank Step One">
                                @lang('user.join.step.three.body')
                                <label class="control-label" for="title">@lang('user.join.step.three.email')</label>
                                {!! Form::text('email', '', array('class'=>'form-control', 'id' => 'email',
                                'placeholder'=> Lang::get('user.join.step.three.email'),
                                'value'=>Input::old('email'))) !!}
                                @if ($errors->first('email'))
                                    <div class="alert alert-danger" role="alert">{!! $errors->first('email') !!}</div>
                                @endif
                                <label class="control-label" for="title">@lang('user.join.step.three.password')</label>
                                {!! Form::password('password', array('class'=>'form-control', 'id' => 'password',
                                'placeholder'=> Lang::get('user.join.step.three.password'),
                                'value'=>Input::old('password'))) !!}
                                @if ($errors->first('password'))
                                    <div class="alert alert-danger" role="alert">{!! $errors->first('password') !!}
                                    </div>
                                @endif
                                <label class="control-label"
                                       for="title">@lang('user.join.step.three.passwordTwo')</label>
                                {!! Form::password('password_confirmation', array('class'=>'form-control', 'id' =>
                                'password_confirmation',
                                'placeholder'=> Lang::get('user.join.step.three.passwordTwo'),
                                'value'=>Input::old('password_confirmation'))) !!}
                                @if ($errors->first('password_confirmation'))
                                    <div class="alert alert-danger" role="alert">{!!
                                        $errors->first('password_confirmation') !!}
                                    </div>
                                @endif
                                <hr>

                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="col-md-8 col-md-offset-2 ">
                                    {!! Form::submit(Lang::get('user.join.save'), array('class' => 'btn btn-success
                                    btn-block')) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
        <!--/.row-->
    </section><!--/#blog-->
@stop

