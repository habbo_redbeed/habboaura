{!! Form::open(array()) !!} @if ($errors->has('login'))
<div class="alert alert-danger">
	<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>{!!
	$errors->first('login', ':message') !!}
</div>
@endif

<div class="form-group has-feedback">
	<input type="text" class="form-control" name="email"
		placeholder="@lang('user.auth.login.email')" /> <span
		class="glyphicon glyphicon-envelope form-control-feedback"></span>
</div>
<div class="form-group has-feedback">
    <input type="password" class="form-control" name="password"
           placeholder="@lang('user.auth.login.password')" /> <span
            class="glyphicon glyphicon-lock form-control-feedback"></span>
</div>
<div class="form-group has-feedback">
    <input type="checkbox" class="form-control" name="cookieSet"
           placeholder="@lang('user.auth.login.cookie')" /> <span
            class="glyphicon glyphicon-lock form-control-feedback"></span>
</div>
<div class="row">
	<!-- /.col -->
	<div class="col-xs-4">
		<button type="submit" class="btn btn-primary btn-block btn-flat">@lang('user.auth.login.submit')</button>
	</div>
	<!-- /.col -->
</div>
{!! Form::close() !!}

