{!! Form::open(array('class' => '', 'route' => 'user.auth.login.post')) !!}
<div class="col-xs-13">
    @if ($errors->has('login'))
        <div class="alert alert-danger">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>{!!
            $errors->first('login', ':message') !!}
        </div>
    @endif

    <div class="form-group">
        <label for="email" class=" control-label">@lang('user.auth.login.email')</label>

        <input type="email" class="form-control" id="email" name="email"
               placeholder="@lang('user.auth.login.email')">
    </div>
        <div class="form-group">
            <label for="password" class="control-label">@lang('user.auth.login.password')</label>

            <input type="password" class="form-control" id="password" name="password"
                   placeholder="@lang('user.auth.login.password')">

        </div>

        <div class="form-group">
            <label for="rememberMe" class="control-label">@lang('user.auth.login.cookieSet')</label>

            <input type="checkbox" class="form-control" id="rememberMe" name="rememberMe">

        </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block">@lang('user.auth.login.submit')</button>
    </div>
</div>
<!-- Usage as a class -->
<div class="clearfix"></div>
{!! Form::close() !!}

