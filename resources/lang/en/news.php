<?php

return array (
  'show' => 
  array (
    'description' => 
    array (
      'where' => 'has published in :cat',
      'view' => 'views',
      'emo' => 'Choose on of this three emoticons to show your opinion',
    ),
    'recommended' => 'Recommended articles',
    'shortnews' => 'Last shortnews',
    'emo' => 'Have an opinion on this subject?',
  ),
  'comments' => 
  array (
    'title' => 'Comments',
  ),
  'more' => 'Read more...',
  'new' => 'NEW',
  'question' => 
  array (
    'title' => 'What do you say?',
    'stop' => 'You\'ve already given an answer',
    'login' => 'Please login',
    'end' => 'The poll has expired on :date',
    'save' => 'Enter',
    'place' => 'Tell us...',
    'label' => 'Your opinion / submission',
    'habbo' => 'Habbo name',
    'lang' => 'Habbo Hotel',
  ),
);
