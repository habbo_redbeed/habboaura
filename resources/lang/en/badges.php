<?php

return array (
  'table' => 
  array (
    'name' => 'Name',
    'description' => 'Description',
  ),
  'page' => 
  array (
    'list' => 
    array (
      'search' => 
      array (
        'placeholder' => 'Search...',
        'submit' => 'Search',
      ),
      'choose' => 'Overview',
      'hotel' => ':count badges at Habbo.:lang',
    ),
    'title' => 
    array (
      'badges' => 'Badges of Habbo.:lang',
      'notBadges' => 'Unused badges at Habbo.:lang',
      'badgesAll' => 'Badges worldwide',
      'used' => 'Rarely badges at Habbo.:lang',
      'usedAll' => 'Rarely Badges worldwide',
      'notBadgesAll' => 'Unused badges worldwide',
    ),
  ),
  'title' => 
  array (
    'notBadges' => 
    array (
      'used' => 'Rarely badges at Habbo.:lang',
      'usedAll' => 'Rarely badges worldwide',
    ),
  ),
);
