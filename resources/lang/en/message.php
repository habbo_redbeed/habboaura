<?php

return array (
  'create' => 
  array (
    'message' => 'Message',
    'subject' => 'Subject',
    'submit' => 'Submit',
    'title' => 'Title',
    'users' => 'Users',
  ),
  'index' => 
  array (
    'count' => ':count User',
    'new' => 'new conversation',
    'read' => 'open konversation',
  ),
  'show' => 
  array (
    'access' => 
    array (
      'all' => 'Everybody',
    ),
    'subject' => 'Members',
    'submit' => 'Submit',
    'title' => 'Add new message',
  ),
);
