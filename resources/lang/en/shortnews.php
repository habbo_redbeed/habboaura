<?php

return array (
  'furni' => 
  array (
    'new' => ':count new Furniture in Habbo! <br>Take <a href=\':url\'>here</a> a look!',
    'twitter' => ':count new Furniture in Habbo! ',
  ),
  'view' => 
  array (
    'description' => 
    array (
      'where' => ':user left a post <b class=":color">:cat</b> in here',
    ),
  ),
  'liveticker' => 'Liveticker',
  'badge' => 
  array (
    'twitter' => 'Badge :name added!',
  ),
);
