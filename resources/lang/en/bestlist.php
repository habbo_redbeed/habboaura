<?php

return array (
  'badge' => 
  array (
    'title' => 'Badge-Toplist',
    'add' => 
    array (
      'body' => '<p><b>Cant find yourself on our list?</b> <br>Maybe its because our system hasn\'t detected you yet. You can easily change that by adding your Habbo below our list!<br><small><i>Your Habbo page status has to be set to public
</i></small></p>',
      'name' => 'Habboname',
      'submit' => 'Enter',
      'check' => 'Your Habbo has been added to the waiting list. <br> The Habbo should be detected within 20 minutes.
',
    ),
  ),
);
