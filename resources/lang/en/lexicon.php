<?php

return array (
  'staff' => 
  array (
    'list' => 
    array (
      'old' => 'former Staffs',
      'now' => 'Current Staffs',
    ),
    'table' => 
    array (
      'head' => 
      array (
        'name' => 'Name',
        'job' => 'Job Title',
        'date' => 'Tenure',
      ),
      'from' => 'from',
      'to' => 'to',
      'since' => 'since',
    ),
    'country' => 
    array (
      'small' => 
      array (
        'int' => 'Sulake',
        'de' => 'DE',
        'com-br' => 'BR',
        'com' => 'COM',
        'es' => 'ES',
        'fi' => 'FI',
        'fr' => 'FR',
        'it' => 'IT',
        'nl' => 'NL',
        'com-tr' => 'TR',
      ),
      'name' => 
      array (
        'int' => 'Sulake',
        'de' => 'Germany',
        'com-br' => 'Brasil',
        'com' => 'International (.com) ',
        'es' => 'Spain',
        'fi' => 'Finland',
        'fr' => 'France',
        'it' => 'Italy',
        'nl' => 'Netherland',
        'com-tr' => 'Turkey',
      ),
    ),
  ),
  'change' => 
  array (
    'user' => ':user has :text changed it :date',
    'title' => 'Recent changes',
  ),
  'new' => 'add new entry',
  'edit' => 
  array (
    'title' => 'category',
  ),
);
