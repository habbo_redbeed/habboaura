<?php

return array (
  'search' => 
  array (
    'placeholder' => 'Search...',
    'placeholder_img' => 'Search picture...',
    'placeholder_staff' => 'Find Habbo, country or  occupation..',
    'title' => 'Search',
  ),
  'likes' => '{0} likes|[0,1] 1 likes it|[2,Inf] :count likes it',
  'comment' => '{0} comments|[0,1] 1 comment|[2,Inf] :count comments',
  'comments' => 
  array (
    'form' => 
    array (
      'placeholder' => 'leave a comment....',
      'title' => 'Comment',
      'submit' => 'comment on',
    ),
    'empty' => 'no comments found...',
  ),
  'more' => 'more',
  'open' => 
  array (
    'image' => 'OPEN IMAGE',
  ),
  'error' => 
  array (
    'notFound' => 'Wrong way! Sorry!',
  ),
  'menu' => 
  array (
    'me' => 'ME',
    'login' => 'LOGIN',
    'join' => 'JOIN',
    'usermenu' => 'My Aura
',
  ),
  'send' => 'Submit',
  'poll' => 'Survey',
);
