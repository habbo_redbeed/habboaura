<?php

return array (
  'infos' => 
  array (
    'badge' => 
    array (
      'more' => 'more badges',
      'title' => 'Badges',
    ),
    'friends' => 
    array (
      'more' => 'more friends',
      'title' => 'Friends',
    ),
    'groups' => 
    array (
      'more' => 'more groups',
      'title' => 'Groups',
    ),
    'habbo' => 
    array (
      'age' => 'age',
      'ageLabel' => 'Years',
      'date' => 'Created at',
      'motto' => 'Motto',
      'name' => 'Habbo name',
    ),
    'hidden' => 'The Habbo page of the selected Habbo seems to be private!
',
    'selectBadges' => 
    array (
      'title' => 'Selected badges',
    ),
    'error' => 'Ouh... We could not find the Habbo you are searching for!
',
    'description' => 'Here you can scan all Habbos worldwide! <br>We count the badges, groups and friends for you and show you all this information!<br><small>For all information your Habbo profile status has to be set on public</small>',
  ),
);
