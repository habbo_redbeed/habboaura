<?php

return array (
  'title' => 
  array (
    'article' => 'Latest article',
    'lexicon' => 'Lexicon entries',
    'user' => 'Latest users',
    'comments' => 'Latest comments',
  ),
  'links' => 
  array (
    'news' => 'News',
    'lexicon' => 'Lexicon',
    'home' => 'Home',
    'datenschutzerklaerung' => 'Privacy Policy',
    'impressum' => 'Impressum',
    'job' => 'Jobs',
  ),
);
