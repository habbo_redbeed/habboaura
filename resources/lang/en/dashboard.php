<?php

return array (
  'news' => 
  array (
    'banner' => 
    array (
      'title' => 'HABBOAURA NEWS',
      'description' => 'Take a look into the Habboverse!',
    ),
  ),
  'furni' => 
  array (
    'banner' => 
    array (
      'title' => 'LATEST FURNITURE',
      'description' => 'Newest Furniture in Habbo!',
    ),
  ),
  'badge' => 
  array (
    'banner' => 
    array (
      'title' => 'LATEST BADGES',
      'description' => 'Latest Badges in Habbo!',
    ),
  ),
);
