<?php

return array (
  'name' => 
  array (
    'habbo' => 
    array (
      'de' => 'Habbo.com',
      'int' => 'Habbo international',
      'off' => 'Official Habbo',
    ),
    'habclub' => 'HabClub.de',
    'other' => 'Other',
    'fansites' => 'Fansites',
    'helper' => 'Ambassadors',
    'habboaura' => 'HabboAura.com',
  ),
);
