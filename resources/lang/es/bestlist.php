<?php

return array (
  'badge' => 
  array (
    'title' => 'Badge-Bestenliste',
    'add' => 
    array (
      'body' => '<p><b>Du bist nicht in der Liste?</b> <br>Warscheinlich liegt es auch daran das du noch nicht von unserem System erkannt wurdest. Dann füge Dich unten in die Liste ein, um auch dabei zu sein!<br><small><i>Deine HabboHome muss dazu öffentlich sein!</i></small></p>',
      'name' => 'HabboName',
      'submit' => 'Eintragen',
      'check' => 'Dein Habbo wurde in die Warteliste aufgenommen. <br>Dein Habbo sollte in 20 Minuten von uns erfasst sein.',
    ),
  ),
);
