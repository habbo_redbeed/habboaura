<?php

return array (
  'show' => 
  array (
    'description' => 
    array (
      'where' => 'has published in :cat',
      'view' => 'vistas',
    ),
    'shortnews' => 'Última informaciónes',
  ),
  'comments' => 
  array (
    'title' => 'Comentario',
  ),
  'more' => 'Lee mas...',
  'new' => 'NUEVO',
  'question' => 
  array (
    'login' => 'Por favor Iniciar sesión',
    'lang' => 'Habbo Hotel',
  ),
);
