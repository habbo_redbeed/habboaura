<?php

return array (
  'create' => 
  array (
    'message' => 'Mensaje',
    'subject' => 'Respecto',
    'users' => 'Suscriptor',
  ),
  'show' => 
  array (
    'subject' => 'Suscriptor',
    'access' => 
    array (
      'all' => 'Todo el mundo
',
    ),
    'submit' => 'Enviar',
  ),
  'index' => 
  array (
    'count' => ':count Suscriptor
',
  ),
);
