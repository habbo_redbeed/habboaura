<?php

return array (
  'staff' => 
  array (
    'list' => 
    array (
      'old' => 'former Staffs',
      'now' => 'Current Staffs',
    ),
    'table' => 
    array (
      'head' => 
      array (
        'name' => 'Nombre
',
        'job' => 'Job Title',
        'date' => 'Tenencia
',
      ),
      'from' => 'from',
      'to' => 'a',
      'since' => 'Desde',
    ),
    'country' => 
    array (
      'small' => 
      array (
        'int' => 'Sulake',
        'de' => 'DE',
        'com-br' => 'BR',
        'com' => 'COM',
        'es' => 'ES',
        'fi' => 'FI',
        'fr' => 'FR',
        'it' => 'IT',
        'nl' => 'NL',
        'com-tr' => 'TR',
      ),
      'name' => 
      array (
        'int' => 'Sulake',
        'de' => 'Alemania',
        'com-br' => 'Brasil',
        'com' => 'International (.com) ',
        'es' => 'España
',
        'fi' => 'Finlandia
',
        'fr' => 'Francia
',
        'it' => 'Italia',
        'nl' => 'Países Bajos
',
        'com-tr' => 'Turquía',
      ),
    ),
  ),
  'change' => 
  array (
    'user' => ':user has :text changed it :date',
    'title' => 'Recent changes',
  ),
  'new' => 'add new entry',
  'edit' => 
  array (
    'title' => 'Categoría',
  ),
);
