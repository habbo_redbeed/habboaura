<?php

return array (
  'join' => 
  array (
    'title' => 'A strange place with great people!',
    'step' => 
    array (
      'title' => 
      array (
        'one' => 'step one',
        'two' => 'step two',
        'three' => 'Step three',
      ),
      'one' => 
      array (
        'body' => 'Nice that <b>you want to become</b> a part of HabboAura!<br>To be sure
                            you are really the specified Habbo you have to take that code in your Habbo Mission!<br>
                            <hr><b>What are you waiting for? Take the Code you see below in your Habbo Mission.</b>',
      ),
      'two' => 
      array (
        'body' => '<b>Great!</b> If you\'ve changed your Habbo Mission with our Code you\'ll be in the next step!
                            <br>To get to the next step of your registration, you need to share your Habbo Name with us.<hr>',
        'habbo' => 'Habboname',
        'country' => 'Hotel',
      ),
      'three' => 
      array (
        'body' => '<b>Okay</b> - Now we should be able to verify your account.<br>
                            But last, we need your E-Mail Adress as well as your Password.
                            <b>Please be sure you don\'t use the same Password you use in Habbo Hotel!</b>
                            <hr>',
        'password' => 'Passwort',
        'passwordTwo' => 'Passwort wiederholung',
        'email' => 'Email-Adresse',
      ),
    ),
    'save' => 'Account anlegen',
    'next' => 'Weiter',
    'error' => 
    array (
      'habbo' => 
      array (
        'unique' => 'Dieser Habbo wird bereits verwendet!',
      ),
      'motto' => 
      array (
        'notFound' => 'Is your HabboHome visible?<br><i>Please try again later!</i>',
        'incorrect' => 'Your Mission doesn\'t match with the verify code',
      ),
    ),
    'correct' => 'you joined HabboAura succesfully',
  ),
  'auth' => 
  array (
    'login' => 
    array (
      'email' => 'Email-Adresse',
      'password' => 'Passwort',
      'submit' => 'Send',
      'join' => 'Register',
      'title' => 'User Panel',
      'cookieSet' => 'Eingeloggt bleiben?',
    ),
  ),
  'menu' => 
  array (
    'title' => 'NAVIGATION',
    'ul' => 
    array (
      'edit' => 'Settings',
      'chat' => 'Conversation',
      'profil' => 'Me',
      'notification' => 'Notification',
      'logout' => 'Log Out',
    ),
  ),
  'edit' => 
  array (
    'habbo' => 
    array (
      'title' => 'Verify Habbo',
      'body' => 'If you want to verify your Habbo, please take our Code in your Habbo Mission!.
                        <br>To get to the next step of your registration, you need to share your <b>Habbo Name</b> with us..<hr>',
      'code' => 'Verification Code',
      'save' => 'Verify Habbo',
      'check' => 'Your Habbo has been transmitted to our system!',
    ),
    'password' => 
    array (
      'title' => 'Change Password',
      'body' => 'To Log in safely in HabboAura, we recommend you to change your Password every 3 weeks!<br>
                        And Notice: <b>Don\'t use the same Password you use in Habbo Hotel!</b><hr>',
      'save' => 'Passwort ändern',
      'check' => 'Your Password has been safely deposited!',
    ),
    'title' => 'Navigation',
    'menu' => 
    array (
      'password' => 'Change Password',
      'habbo' => 'Verify Habbo',
    ),
    'profil' => 
    array (
      'title' => 'Edit Profile',
      'motto' => 'Motto',
      'free_text' => 'About You',
      'save' => 'Save',
      'check' => 'Your Profile has been edited!',
    ),
  ),
  'profil' => 
  array (
    'side' => 
    array (
      'since' => 'Member since',
    ),
    'freetext' => 
    array (
      'title' => 'About me',
    ),
    'static' => 
    array (
      'comments' => 'Comments',
      'likes' => 'Likes given',
      'views' => 'Profile views',
    ),
  ),
  'stars' => 'Star|Stars',
  'star' => 
  array (
    'month' => 'in this month',
    'sum' => 'total',
  ),
  'forget' => 
  array (
    'step' => 
    array (
      'one' => 
      array (
        'title' => 'E-Mail Adresse',
        'body' => 'Bitte gib deine E-Mail Adresse an, welche mit deinem Account von HabboAura.com verbunden ist!',
      ),
      'two' => 
      array (
        'title' => 'Habbo Name',
        'body' => 'Wir brauchen deinen Habbo Namen, den du bei uns hinterlegt hast.',
      ),
      'three' => 
      array (
        'title' => 'Mission Code',
        'body' => 'Damit wir wissen das du den Habbo besitzt, hinterlege bitte diesen Code in die Habbo Mission.',
      ),
    ),
    'title' => 'Passwort vergessen?',
  ),
);
