<?php

return array (
  'title' => 
  array (
    'article' => 'Latest article',
    'lexicon' => 'Lexicon entries',
    'user' => 'Latest user',
    'comments' => 'Latest Comments',
  ),
  'links' => 
  array (
    'news' => 'Noticias',
    'lexicon' => 'Lexicon',
    'home' => 'Home',
    'datenschutzerklaerung' => 'Política de Privacidad',
    'impressum' => 'Impressum',
  ),
);
