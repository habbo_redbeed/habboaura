<?php

return array (
  'pixel' => 
  array (
    'empty' => 'No Pixelolymp at the moment!',
    'period' => 'from :from to :to',
    'join' => 'Join now!',
    'login' => 'to join you have to be logged in!',
    'check' => 'you already joined please have patience.',
    'form' => 
    array (
      'title' => 'Submit',
      'text' => 'Note',
      'image' => 'image',
      'imageInfo' => 'Only pictures with 1MB size allowed',
    ),
  ),
);
