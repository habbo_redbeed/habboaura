<?php

return array (
  'search' => 
  array (
    'placeholder' => 'buscar',
    'placeholder_img' => 'Búsqueda foto...
',
    'placeholder_staff' => 'Habbo, Land oder Beruf finden...',
    'title' => 'buscar',
  ),
  'likes' => '{0} likes|[0,1] 1 likes it|[2,Inf] :count likes it',
  'comment' => '{0} comments|[0,1] 1 comment|[2,Inf] :count comments',
  'comments' => 
  array (
    'form' => 
    array (
      'placeholder' => 'leave a comment....',
      'title' => 'comentario',
      'submit' => 'comment on',
    ),
    'empty' => 'no comments found...',
  ),
  'more' => 'más',
  'open' => 'abierto',
  'menu' => 
  array (
    'me' => 'ME',
    'login' => 'LOGIN',
    'join' => 'JOIN',
  ),
);
