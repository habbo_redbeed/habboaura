<?php

return array (
  'newsletter' => 'Newsletter',
  'button_save' => 'save',
  'enter_your_email' => 'Email',
  'all_rights_reserved' => 'All Rights Reserved',
  'contact_form' => 'Contacts',
  'our_location' => 'Our Location',
  'name_and_surname' => 'Name and Surname',
  'email' => 'Email',
  'phone_number' => 'Phone Number',
  'subject' => 'Titel',
  'message' => 'Message',
  'button_send' => 'Send',
  'link_previous' => 'return',
  'link_next' => 'next',
);
