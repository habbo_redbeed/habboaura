<?php

return array (
  'infos' => 
  array (
    'badge' => 
    array (
      'more' => 'more badges',
      'title' => 'Badges',
    ),
    'friends' => 
    array (
      'more' => 'more friends',
      'title' => 'Amigos',
    ),
    'groups' => 
    array (
      'more' => 'more groups',
      'title' => 'Groups',
    ),
    'habbo' => 
    array (
      'age' => 'age',
      'ageLabel' => 'Años',
      'date' => 'Created at',
      'motto' => 'Motto',
      'name' => 'Habbo name',
    ),
    'hidden' => 'The Habbo Page of the selected Habbo is hiden',
    'selectBadges' => 
    array (
      'title' => 'Selected badges',
    ),
    'error' => 'Ouh... were is the habbo?',
    'description' => 'Here you can scan all habbos worldwide! <br>We count the Badges, Groups and Friends for you and show you all this infos!<br><small>For all infos muss the HabboProfil set on public </small>',
  ),
);
