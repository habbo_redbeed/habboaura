<?php

return array (
  'infos' => 
  array (
    'badge' => 
    array (
      'more' => 'daha fazla Rozet',
      'title' => 'Rozetler',
    ),
    'error' => 'Ah be, bu Habbo\'yu bulamadık',
    'friends' => 
    array (
      'more' => 'daha fazla Arkadaş',
      'title' => 'Arkadaşlar',
    ),
    'groups' => 
    array (
      'more' => 'daha fazla Grup',
      'title' => 'Gruplar',
    ),
    'habbo' => 
    array (
      'age' => 'Yaş',
      'ageLabel' => 'Yıllar',
      'date' => 'Oluşturulma tarihi',
      'motto' => 'Durum',
      'name' => 'Habbo ismi',
    ),
    'hidden' => 'Bu kullanıcının Profili yüklenemedi!',
    'selectBadges' => 
    array (
      'title' => 'Seçilen Rozetler',
    ),
    'description' => 'HabboAura da, Habbo endeksine sahip kullanıcılar hakkında bilgi al!<br>Seçilen kullanıcının hangi Gruplara üye olduğunu, kimlerle arkadaş olduğunu ve hangi Rozetleri taktığından haberdar ol!<br><small>Habbo Profili herkese açık konumda olmalıdır!</small>',
  ),
);
