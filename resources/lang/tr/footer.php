<?php

return array (
  'title' => 
  array (
    'article' => 'Son Makale',
    'lexicon' => 'Sözlük-girişleri',
    'user' => 'En son Kullanıcılar',
    'comments' => 'En son Yorumlar',
    'active' => 'Aktiflik',
  ),
  'links' => 
  array (
    'news' => 'Haberler',
    'lexicon' => 'Sözlük',
    'home' => 'Ana Sayfa',
    'datenschutzerklaerung' => 'Gizlilik Politikası',
    'impressum' => 'Hakkımızda',
    'job' => 'Boş İş Alanları',
  ),
);
