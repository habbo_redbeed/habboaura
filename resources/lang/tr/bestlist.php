<?php

return array (
  'badge' => 
  array (
    'title' => 'Rozet-Lider Tablosu',
    'add' => 
    array (
      'body' => '<p><b>Listede yok musun?</b> <br>Bu, muhtemelen sistemimiz tarafından algılanmadığından kaynaklanıyor. Aşağıda kendini hemen listeye dahil edebilirsin! <br><small><i>Habbo profilin herkese açık konumda olması gerekiyor!</i></small></p>',
      'name' => 'Habbo Adı',
      'submit' => 'Geçirmek',
      'check' => 'Habbo profilin bekleme listesine alındı. <br>Habbo profilin 20 dakika içerisinde bize ulaşmış olacak.',
    ),
  ),
);
