<?php

return array (
  'pixel' => 
  array (
    'empty' => 'Şu anda ne yazık ki yürürlükte olan bir Pixel-olimpiyatı bulunmamaktadır.',
    'period' => 'dan :den için :to',
    'join' => 'Şimdi katıl!',
    'login' => 'Katılabilmek için oturum açman gerekmektedir.',
    'check' => 'Bu Etkinliğe zaten katılmışsın, lütfen biraz sabırlı ol.',
    'form' => 
    array (
      'title' => 'Göndermek',
      'text' => 'Not',
      'image' => 'Resim',
      'imageInfo' => 'Sadece 1MB boyutuna kadar olan Resimler seçebilirsin.',
    ),
  ),
  'code' => 
  array (
    'form' => 
    array (
      'submit' => 'Kurtarmak',
    ),
  ),
);
