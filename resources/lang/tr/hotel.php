<?php

return array (
  'de' => 
  array (
    'name' => 'Almanya',
    'link' => 'habbo.de',
    'code' => 'de',
  ),
  'com' => 
  array (
    'name' => 'Uluslararası',
    'link' => 'habbo.com',
    'code' => 'com',
  ),
  'tr' => 
  array (
    'code' => 'tr',
    'link' => 'habbo.com.tr',
    'name' => 'Türkiye',
  ),
);
