<?php

return array (
  'staff' => 
  array (
    'list' => 
    array (
      'old' => 'Eski Habbo Personelleri',
      'now' => 'Mevcut Habbo Personelleri',
    ),
    'table' => 
    array (
      'head' => 
      array (
        'name' => 'İsim',
        'job' => 'İş Unvanı',
        'date' => 'Görev Süresi',
      ),
      'from' => 'İtibaren',
      'to' => 'dek',
      'since' => '-den beri',
    ),
    'country' => 
    array (
      'small' => 
      array (
        'int' => 'Sulake',
        'de' => 'DE',
        'com-br' => 'BR',
        'com' => 'COM',
        'es' => 'ES',
        'fi' => 'FI',
        'fr' => 'FR',
        'it' => 'IT',
        'nl' => 'NL',
        'com-tr' => 'TR',
      ),
      'name' => 
      array (
        'int' => 'Sulake',
        'de' => 'Almanya',
        'com-br' => 'Brezilya',
        'com' => 'Uluslararası (.com)',
        'es' => 'İspanya',
        'fi' => 'Finlandiya',
        'fr' => 'Fransa',
        'it' => 'Italya',
        'nl' => 'Hollanda',
        'com-tr' => 'Türkiye',
      ),
    ),
  ),
  'change' => 
  array (
    'user' => ':kullanıcı :yazıyı :date tarihinde güncelledi!',
    'title' => 'Son Değişiklikler',
  ),
  'new' => 'Yeni Girdi ekle',
  'edit' => 
  array (
    'title' => 'Kategori',
  ),
);
