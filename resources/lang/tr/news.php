<?php

return array (
  'show' => 
  array (
    'description' => 
    array (
      'where' => 'tarafından yayınlandı :cat',
      'view' => 'Görüntülenme',
      'emo' => 'Mevcut Yüz ifadelerinden birini seçerek diğer okurlara Fikrini belirt',
    ),
    'emo' => 'Bu konu hakkındaki Fikrin nedir?',
    'recommended' => 'Benzer Makaleler',
    'shortnews' => 'Canlı Senedinde olup biten son Yenilikler',
  ),
  'comments' => 
  array (
    'title' => 'Yorumlar',
  ),
  'more' => 'Daha fazla öğren..',
  'new' => 'YENİ',
  'question' => 
  array (
    'end' => 'Bu Etkinlik :date tarihinde sona erdi.',
    'login' => 'Fikrini paylaşabilmek için lütfen giriş yap.',
    'stop' => 'Zaten bir cevap verdin.',
    'title' => 'Buna ne dersin?',
    'place' => 'Fikrin bizim için oldukça önemli!',
    'save' => 'Gönder',
    'label' => 'Senin Fikrin',
    'habbo' => 'Habbo Adı',
    'lang' => 'Habbo Otel',
  ),
);
