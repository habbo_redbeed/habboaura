<?php

return array (
  'table' => 
  array (
    'name' => 'Rozet Adı',
    'description' => 'Açıklama',
  ),
  'page' => 
  array (
    'list' => 
    array (
      'choose' => 'Otel Seç',
      'hotel' => ':count Rozet Habbo\'da.:lang',
      'search' => 
      array (
        'info' => 'Burada belirli bir Rozeti arayabilirsin!<br>Ayrıca bir Kod, isim veya Açıklama da arayabilirsin!',
        'placeholder' => 'Arama...',
        'submit' => 'Arama',
      ),
    ),
    'title' => 
    array (
      'badges' => 'Habbo\'nun Rozetleri.:lang',
      'notBadges' => 'Habbo\'da kullanılmayan Rozetler.:lang',
      'badgesAll' => 'Uluslararası Rozetler',
      'usedAll' => 'Uluslararası nadir Rozetler',
      'used' => 'Habbo\'nun nadir Rozetleri :lang',
      'notBadgesAll' => 'Uluslararası kullanılmayan rozetler',
    ),
  ),
  'title' => 
  array (
    'notBadges' => 
    array (
      'usedAll' => 'Uluslararası nadir Rozetler',
      'used' => 'Habbo\'nun nadir Rozetleri :lang',
    ),
  ),
);
