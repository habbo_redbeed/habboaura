<?php

return array (
  'accepted' => ':attribute kabul edilmelidir.',
  'active_url' => ':attribute geçerli bir URL değil.',
  'after' => ':attribute :date tarihinden sonra olmalıdır.',
  'alpha' => ':attribute sadece harf içerebilir.',
  'alpha_dash' => ':attribute sadece harf, numara ve işaret içerebilir.',
  'alpha_num' => ':attribute sadece harf ve numara içerebilir.',
  'array' => ':attribute bir dizi olmalıdır.',
  'before' => ':attribute :date tarihinden önce olmalıdır.',
  'between' => 
  array (
    'numeric' => ':attribute :min ve :max arsında bir niteliğe sahip olmalıdır.',
    'file' => ':attribute :min ve :max arasında bir kilobayt niteliği taşımalıdır.',
    'string' => ':attribute :min ve :max arasında karaktere sahip olmalıdır.',
    'array' => ':attribute :min ve :max ürün niteliği arasında olmalıdır.',
  ),
  'boolean' => ':attribute alan doğru ya da yanlış olarak doldurulması gerekir.',
  'confirmed' => ':attribute doğrulaması birbiri ile uyuşmamaktadır.',
  'date' => ':attribute geçerli bir tarih değil.',
  'date_format' => ':attribute biçim ile uyuşmuyor :format.',
  'different' => ':attribute ve :diğeri farklı olmalıdır.',
  'digits' => ':attribute  :digits rakam.',
  'digits_between' => ':attribute :min ve :max arasında rakama sahip olmalıdır.',
  'email' => 'Alanda :attribute bir E-mail adresi olmak zorundadır.',
  'filled' => 'Bu alan :attribute zorunludur.',
  'exists' => 'Seçilmiş olan :attribute geçersizdir.',
  'image' => ':attribute bir Resim olmalıdır.',
  'in' => 'Seçilmiş olan :attribute geçersizdir.',
  'integer' => ':attribute tam olmalıdır.',
  'ip' => ':attribute geçerli bir IP adresi olmalıdır.',
  'max' => 
  array (
    'numeric' => ':attribute :max\'dan daha iyi olmayabilir.',
    'file' => ':attribute :max kilobayt hızından iyi olmayabilir.',
    'string' => ':attribute :max karakter sayısından daha iyi olmayabilir.',
    'array' => ':attribute :max items\'dan fazla olmayabilir.',
  ),
  'mimes' => ':attribute şu dosya tipinden olmalıdır: :values.',
  'min' => 
  array (
    'numeric' => ':attribute :min dakika arasında olmalıdır.',
    'file' => ':attribute minimum :min kilobayt hızında olmalıdır.',
    'string' => ':attribute minimum:min karakter sayısından oluşmalıdır.',
    'array' => ':attribute :min iteme sahip olmalıdır.',
  ),
  'not_in' => 'Seçilen :attribute geçersiz.',
  'numeric' => ':attribute bir numara olmalıdır.',
  'regex' => ':attribute formatı geçersiz.',
  'required' => 'Bu :attribute alan gereklidir.',
  'required_if' => 'The :attribute field is required when :other is :value.',
  'required_with' => 'The :attribute field is required when :values is present.',
  'required_with_all' => 'The :attribute field is required when :values is present.',
  'required_without' => 'The :attribute field is required when :values is not present.',
  'required_without_all' => 'The :attribute field is required when none of :values are present.',
  'same' => 'The :attribute and :other must match.',
  'size' => 
  array (
    'numeric' => 'The :attribute must be :size.',
    'file' => 'The :attribute must be :size kilobytes.',
    'string' => 'The :attribute must be :size characters.',
    'array' => 'The :attribute must contain :size items.',
  ),
  'unique' => 'The :attribute has already been taken.',
  'url' => 'The :attribute format is invalid.',
  'timezone' => 'The :attribute must be a valid zone.',
  'custom' => 
  array (
    'attribute-name' => 
    array (
      'rule-name' => 'Özel-ileti',
    ),
  ),
  'recaptcha' => ':attribute alanı doğru değil.',
);
