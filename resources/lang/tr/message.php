<?php

return array (
  'create' => 
  array (
    'message' => 'Mesaj',
    'subject' => 'Konu',
    'submit' => 'Sohbet başlat',
    'title' => 'Yeni Sohbet başlat',
    'users' => 'Katılımcı',
  ),
  'index' => 
  array (
    'count' => ':count Katılımcı',
    'new' => 'yeni Sohbet',
    'read' => 'Sohbeti aç',
  ),
  'show' => 
  array (
    'access' => 
    array (
      'all' => 'Herkes',
    ),
    'subject' => 'Katılımcı',
    'submit' => 'Gönder',
    'title' => 'Yeni İleti ekleme',
  ),
);
