<?php

return array (
  'join' => 
  array (
    'title' => 'A strange place with great people!',
    'step' => 
    array (
      'title' => 
      array (
        'one' => 'İlk adım',
        'two' => 'İkinci adım',
        'three' => 'Üçüncü adım',
      ),
      'one' => 
      array (
        'body' => 'HabboAura <b>ailesinin bir </b> parçası olmak istemen çok güzel<br>Gerçekten doğrulanmış bir Habbo olduğunu bilebilmemiz için Mottonu değiştirmen şart! <br>
                            <hr><b>Neyi bekliyorsun? Hemen aşağıda bulunan kodu mottona yaz.</b>',
      ),
      'two' => 
      array (
        'body' => '<b>Great!</b>Eğer Habbo Durumunu sana verdiğimiz kod ile değiştirdiysen bir sonraki adıma yönlendirileceksin.
                            <br>Kayıt sürecinin bir sonraki adımına geçebilmek için Habbo kullanıcı adını bizlerle paylaşmalısın.<hr>',
        'habbo' => 'Habbo kullanıcı adı',
        'country' => 'Hotel',
      ),
      'three' => 
      array (
        'body' => '<b>Tamam</b> - Artık seni doğrulayabiliriz.<br>
                         
Ancak bir sonraki ziyaretin için E-mail adresin ve bir Parolaya ihtiyacın var.
                            <b>Lütfen Habbo Hotel\'de kullandığın Parolayı kullanmadığından emin ol!</b>
                            <hr>',
        'password' => 'Şifre',
        'passwordTwo' => 'Şifre tekrarı',
        'email' => 'Email-Adresi',
      ),
    ),
    'save' => 'Hesap edinme',
    'next' => 'Devam',
    'error' => 
    array (
      'habbo' => 
      array (
        'unique' => 'Bu Habbo zaten kullanılıyor!',
      ),
      'motto' => 
      array (
        'notFound' => 'Habbo Profilin herkese açık konumda mı?<br><i>Lütfen daha sonra tekrar dene!</i>',
        'incorrect' => 'Habbo motton, doğrulama kodu ile uyuşmuyor!',
      ),
    ),
    'correct' => 'HabboAura ailesine başarıyla dahil oldun!',
  ),
  'auth' => 
  array (
    'login' => 
    array (
      'email' => 'Email-Adresi',
      'password' => 'Şifre',
      'submit' => 'Gönder',
      'join' => 'Hesap oluştur',
      'title' => 'Kullanıcı Paneli',
      'cookieSet' => 'Seni hatırlayalım mı?',
      'forget' => 'Şifreni mi unuttun?',
    ),
  ),
  'menu' => 
  array (
    'title' => 'NAVIGASYON',
    'ul' => 
    array (
      'edit' => 'Ayarlar',
      'chat' => 'Konuşma',
      'profil' => 'Ben',
      'notification' => 'Bildiri',
      'logout' => 'Çıkış yap',
    ),
  ),
  'edit' => 
  array (
    'habbo' => 
    array (
      'title' => 'Habbo Doğrulama',
      'body' => 'Habbo\'nu doğrulamak için, sistem tarafından verilen Kodu mottona yazman gerekmektedir.
                        <br>Doğrulama işleminde adımlarımızı birlikte atabilmek için bir sonraki aşama<b>Habbo kullanıcı adını</b> bizlerle paylaşman gerekmektedir.<hr>',
      'code' => 'Doğrulama Kodu',
      'save' => 'Habbo Doğrulama',
      'check' => 'Habbo Profilin sistemimize başarıyla eklendi!',
    ),
    'password' => 
    array (
      'title' => 'Şifre değiştirme',
      'body' => 'Sayfamıza süreki güvenli olarak giriş yapabilmek için Şifreni her 3 haftada bir değiştirmeni tavsiye ediyoruz!<br>
                         Not: <b>Lütfen bu işlem esnasında Habbo bilgilerini kullanma.</b><hr>',
      'save' => 'Şifre değiştirme',
      'check' => 'Şifrenin güvenli ve kilitlenmiş bir şekilde saklandı!',
    ),
    'title' => 'Navigasyon',
    'menu' => 
    array (
      'password' => 'Şifre değiştirme',
      'habbo' => 'Habbo Doğrulama',
      'image' => 'Profil resmi deiştirme',
    ),
    'profil' => 
    array (
      'title' => 'Profilini düzenle',
      'motto' => 'Motto',
      'free_text' => 'Hakkında',
      'save' => 'Kaydet',
      'check' => 'Profiliniz başarıyla düzenlendi!',
    ),
    'image' => 
    array (
      'check' => 'Yeni bir Resim oluşturuldu!',
      'field' => 'Resim',
      'save' => 'Resim değiştirme',
      'body' => 'Buradan HabboAura profil resmini değiştirebilirsin.<br>Hesabını özelleştirmek için çeşitli arka planlar seçebilirsin.',
      'title' => 'Profil resmi',
    ),
  ),
  'profil' => 
  array (
    'side' => 
    array (
      'since' => 'tarihinden bu yana üye',
    ),
    'freetext' => 
    array (
      'title' => 'Hakkımda',
    ),
    'static' => 
    array (
      'comments' => 'yorum yazıldı',
      'likes' => 'kez Beğenildi',
      'views' => 'Profil görüntülenmesi',
      'rooms' => 'Odalar',
      'badge' => 'Rozetler',
    ),
  ),
  'stars' => 'Yıldız|Yıldızlar',
  'star' => 
  array (
    'month' => 'bu ay',
    'sum' => 'toplam',
  ),
  'comment' => 
  array (
    'delete' => 
    array (
      'check' => 'Bu Yorum silindi!',
      'error' => 'Üzgünüz, bir şeyler ters gitti!',
      'mod' => 'Bu Yorum bir <b>Topluluk yetkilisi</b>tarafından kaldırıldı.',
      'notice' => 'Bu Kullanıcı kendi yorumunu sildi.',
    ),
  ),
  'image' => 
  array (
    'cloudTwo' => 'Bulutlar',
    'park' => 'Habbo Parkı',
    'cloud' => 'Sabah güneşi',
    'dino' => 'Dinozorlar',
    'visible' => 'kadar kullanılabilir',
  ),
  'guest' => 
  array (
    'online' => ':count kullanıcı şu anda HabboAura\'da aktif!',
  ),
  'forget' => 
  array (
    'step' => 
    array (
      'one' => 
      array (
        'title' => 'E-Mail adresi',
        'body' => 'Lütfen HabboAura.com ile bağlantılı E-mail adresini gir!',
      ),
      'three' => 
      array (
        'body' => 'Seçilen Habbo kullanıcısının sana ait olduğunu doğrulamak için verilen kodu Durumuna yaz.',
        'title' => 'Motto Kodu',
      ),
      'two' => 
      array (
        'body' => 'Bize E-mail adresin ile birlikte verdiğin Habbo kullanıcı adına ihtiyacımız var.',
        'title' => 'Habbo Adı',
      ),
      'four' => 
      array (
        'body' => 'Yeni bir Şifre oluşturduk.<br>Artık HabboAura.com\'a E-mail adresin ve yeni Şifren ile giriş yapabilirsin.
<br><br><i>Şifreni asla kimseyle paylaşma!</i>',
        'title' => 'Hoş Geldin!',
      ),
    ),
    'title' => 'Parolanızı mı unuttunuz?',
    'error' => 
    array (
      'notfound' => 'Bu Habbo E-mail adresini tanıyamadık.',
    ),
  ),
  'placeholder' => 
  array (
    'email' => 'habbo@mail.com',
    'habboname' => 'Habbo Adı',
  ),
  'team' => 
  array (
    'join' => 
    array (
      'subtitle' => 'BİRLEŞTİREN ŞEYİ HİSSET',
      'title' => 'SENİ ARIYORUZ!',
      'info' => 
      array (
        'one' => 
        array (
          'body' => 'HabboAura 21 Haziran 2015 tarihinde açıldı ve sizlere birçok mükemmel ek ve eğlence sunuyor! İlk günümüzden beri aktif, bol etkinlik ve eğlence dolu olmaya çalışıyoruz!',
          'title' => 'HABBOAURA',
        ),
        'three' => 
        array (
          'body' => 'İlk günümüzden bu yana: Sizlere en yeni bilgileri, etkinlikleri ve daha fazla eğlence sunmaya çalışıyoruz! Sizler sayesinde motivasyonumuz daha da yükseliyor ve geliştirme açısından daha da güçleniyoruz.
<br><br> ',
          'title' => 'ÇALIŞKAN',
        ),
        'two' => 
        array (
          'body' => 'HabboAura tam senin yaratıldı.<br><br>',
          'title' => 'AKTİF',
        ),
      ),
    ),
    'month' => 
    array (
      'profil' => 'Seçilen Profili aç',
      'title' => 'Bu ayın en iyi çalışanı...',
      'message' => 'Sohbet başlat',
    ),
  ),
);
