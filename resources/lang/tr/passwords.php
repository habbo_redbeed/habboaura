<?php

return array (
  'password' => 'Parolalar en az 6 karakterden oluşmak ve tekrarı ile uyuşmak zorundadır.',
  'user' => 'Bu E-mail adresine sahip bir kullanıcı bulamadık.',
  'token' => 'Sıfırlama kodunu yanlış girdiniz.',
  'sent' => 'E-mail adresinize Parolanızı sıfırlamanız için bir bağlantı gönderdik!',
  'reset' => 'Parolanız başarıyla sıfırlandı.',
);
