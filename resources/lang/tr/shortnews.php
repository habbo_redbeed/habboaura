<?php

return array (
  'furni' => 
  array (
    'new' => 'Sunucularda :count yeni Mobi ortaya çıktı!<br>Hemen <a href=\':url\'>buradan</a> göz at!',
    'twitter' => ':count Yeni Mobi yüklendi!',
  ),
  'view' => 
  array (
    'description' => 
    array (
      'where' => ':user Buraya bir yazı <b class=":color">:cat</b> bırak',
    ),
  ),
  'liveticker' => 'Canlı senedi',
  'badge' => 
  array (
    'twitter' => ':name Rozet eklendi!',
  ),
);
