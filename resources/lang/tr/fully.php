<?php

return array (
  'newsletter' => 'Haber',
  'button_save' => 'Kaydetmek',
  'enter_your_email' => 'Email-adresi',
  'all_rights_reserved' => 'Tüm Hakları saklıdır',
  'contact_form' => 'İletişim',
  'our_location' => 'Bizim Yerimiz',
  'name_and_surname' => 'İsim ve Soy isim',
  'email' => 'Email-Adresi',
  'phone_number' => 'Telefon Numarası',
  'subject' => 'Başlık',
  'message' => 'Kişisel mesaj',
  'button_send' => 'Göndermek',
  'link_previous' => 'Geri dön',
  'link_next' => 'Sonraki',
);
