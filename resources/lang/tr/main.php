<?php

return array (
  'search' => 
  array (
    'placeholder' => 'Arama...',
    'placeholder_img' => 'Bir Resim ara...',
    'placeholder_staff' => 'Habbo, Ülke ya da Meslek bul...',
    'title' => 'Arama',
  ),
  'likes' => '{0} beğeni|[0,1] bir kişi bunu beğendi|[2,Inf] :count beğendi',
  'comment' => '{0} Yorum|[0,1] bir yorum|[2,Inf] :count yorumlar',
  'comments' => 
  array (
    'form' => 
    array (
      'placeholder' => 'Yorum yap...',
      'title' => 'Yorum',
      'submit' => 'Yorum yap',
    ),
    'empty' => 'Hiç yorum bulunamadı...',
  ),
  'more' => 'Daha fazla',
  'open' => 
  array (
    'image' => 'RESMİ AÇ',
  ),
  'error' => 
  array (
    'notFound' => 'Görünüşe bakılırsa burada doğru değilsin...',
  ),
  'menu' => 
  array (
    'login' => 'GİRİŞ YAP',
    'me' => 'BEN',
    'join' => 'KAYIT OL',
    'usermenu' => 'Benim Aura',
  ),
  'poll' => 'Anket',
  'send' => 'Gönder',
);
