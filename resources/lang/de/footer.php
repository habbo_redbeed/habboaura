<?php

return array (
  'title' => 
  array (
    'article' => 'Letzte Artikel',
    'lexicon' => 'Lexikoneinträge',
    'user' => 'Registrierte User',
    'comments' => 'Letzte Kommentare',
    'active' => 'Aktivität',
  ),
  'links' => 
  array (
    'news' => 'News',
    'lexicon' => 'Lexikon',
    'home' => 'Home',
    'datenschutzerklaerung' => 'Datenschutzerklärung',
    'impressum' => 'Impressum',
    'job' => 'Jobs',
  ),
);
