<?php

return array (
  'badge' => 
  array (
    'title' => 'Badge-Bestenliste',
    'add' => 
    array (
      'body' => '<p><b>Du bist nicht in der Liste?</b> <br>Wahrscheinlich liegt es auch daran, dass du noch nicht von unserem System erkannt wurdest. Dann füge Dich unten in die Liste ein, um auch dabei zu sein!<br><small><i>Deine Habbo Home muss dazu öffentlich sein!</i></small></p>',
      'name' => 'Habboname',
      'submit' => 'Eintragen',
      'check' => 'Dein Habbo wurde in die Warteliste aufgenommen. <br>Dein Habbo sollte in 20 Minuten von uns erfasst sein.',
    ),
  ),
);
