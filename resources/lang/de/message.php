<?php

return array (
  'show' => 
  array (
    'title' => 'Neue Nachricht hinzufügen',
    'subject' => 'Teilnehmer',
    'submit' => 'Absenden',
    'access' => 
    array (
      'all' => 'Jeder',
    ),
  ),
  'create' => 
  array (
    'title' => 'Neue Konversation beginnen',
    'subject' => 'Betreff',
    'message' => 'Nachricht',
    'users' => 'Teilnehmer',
    'submit' => 'Konversation beginnen',
  ),
  'index' => 
  array (
    'new' => 'neue Konversation',
    'read' => 'Konversation öffnen',
    'count' => ':count Teilnehmer',
  ),
);
