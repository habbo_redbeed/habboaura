<?php

return array (
  'table' => 
  array (
    'name' => 'Badgename',
    'description' => 'Beschreibung',
  ),
  'page' => 
  array (
    'list' => 
    array (
      'search' => 
      array (
        'info' => 'Hier kannst du nach einem bestimmten Badge suchen!<br>In der Suche findest du alle Badges - gebe dazu einfach den Code oder ein Name ein! ',
        'placeholder' => 'Suchen...',
        'submit' => 'Suchen',
      ),
      'choose' => 'Übersicht',
      'hotel' => ':count Badges im Habbo.:lang',
    ),
    'title' => 
    array (
      'badges' => 'Badges des Habbo.:lang',
      'notBadges' => 'Unbenutzte Badges im Habbo.:lang',
      'badgesAll' => 'Badges Weltweit',
      'used' => 'Seltene Badges im Habbo.:lang',
      'usedAll' => 'Seltene Badges Weltweit',
      'notBadgesAll' => 'Unbenutze Badges Weltweit
',
    ),
  ),
  'title' => 
  array (
    'notBadges' => 
    array (
      'used' => 'Seltene Badges im Habbo.:lang',
      'usedAll' => 'Seltene Badges Weltweit',
    ),
  ),
);
