<?php

return array (
  'password' => 'Das Passwort muss mindestens sechs Zeichen lang sein und Buchstaben und Zahlen enthalten.',
  'user' => 'Wir konnten keinen User mit dieser E-Mail finden.',
  'token' => 'Dieses Passwort ist ungültig.',
  'sent' => 'Passwort Erinnerung gesendet!',
);
