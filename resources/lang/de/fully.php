<?php

return array (
  'newsletter' => 'Newsletter',
  'button_save' => 'Speichern',
  'enter_your_email' => 'Emailadresse',
  'all_rights_reserved' => 'All Rights Reserved',
  'contact_form' => 'Konatkt',
  'our_location' => 'Our Location',
  'name_and_surname' => 'Name und Nachname',
  'email' => 'Email-Adresse',
  'phone_number' => 'Phone Number',
  'subject' => 'Titel',
  'message' => 'Nachricht',
  'button_send' => 'Senden',
  'link_previous' => 'Zurück',
  'link_next' => 'Weiter',
);
