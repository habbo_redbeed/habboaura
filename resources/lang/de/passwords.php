<?php

return array (
  'password' => 'Passwort muss mindestens 6 Zeichen lang sein und übereinstimmen.',
  'user' => 'Wir können keinen User mit dieser E-Mail Adresse finden.',
  'token' => 'Der Code zum Rücksetzen des Passworts ist falsch.',
  'sent' => 'Wir haben dir einen Passwort-Reset link zugemailt!',
  'reset' => 'Dein Passwort wurde zurückgesetzt!',
);
