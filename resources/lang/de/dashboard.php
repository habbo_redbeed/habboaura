<?php

return array (
  'news' => 
  array (
    'banner' => 
    array (
      'title' => 'HABBOAURA NEUIGKEITEN',
      'description' => 'Ein Blick in die Welten des Habboversum!',
    ),
  ),
  'furni' => 
  array (
    'banner' => 
    array (
      'title' => 'HOCHGELADENE MÖBELSTÜCKE',
      'description' => 'Diese Möbel sind neu im Habbo Hotel!',
    ),
  ),
  'badge' => 
  array (
    'banner' => 
    array (
      'title' => 'LETZTEN BADGES',
      'description' => 'Diese Badges sind neu im Habbo Hotel!',
    ),
  ),
);
