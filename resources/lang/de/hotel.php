<?php

return array (
  'de' => 
  array (
    'name' => 'Deutschland',
    'link' => 'habbo.de',
    'code' => 'de',
  ),
  'com' => 
  array (
    'name' => 'International',
    'link' => 'habbo.com',
    'code' => 'com',
  ),
  'tr' => 
  array (
    'name' => 'Türkei',
    'link' => 'habbo.com.tr',
    'code' => 'tr',
  ),
);
