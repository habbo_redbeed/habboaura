<?php

return array (
  'staff' => 
  array (
    'list' => 
    array (
      'old' => 'Ehemalige Mitarbeiter',
      'now' => 'Aktuelle Mitarbeiter',
    ),
    'table' => 
    array (
      'head' => 
      array (
        'name' => 'Name',
        'job' => 'Berufsbezeichnung',
        'date' => 'Amtzeit',
      ),
      'from' => 'Von',
      'to' => 'bis',
      'since' => 'seit',
    ),
    'country' => 
    array (
      'small' => 
      array (
        'int' => 'Sulake',
        'de' => 'DE',
        'com-br' => 'BR',
        'com' => 'COM',
        'es' => 'ES',
        'fi' => 'FI',
        'fr' => 'FR',
        'it' => 'IT',
        'nl' => 'NL',
        'com-tr' => 'TR',
      ),
      'name' => 
      array (
        'int' => 'Sulake',
        'de' => 'Deutschland',
        'com-br' => 'Brasilien',
        'com' => 'International (.com) ',
        'es' => 'Spanien',
        'fi' => 'Finnland',
        'fr' => 'Frankreich',
        'it' => 'Italien',
        'nl' => 'Niederlande',
        'com-tr' => 'Türkei',
      ),
    ),
  ),
  'change' => 
  array (
    'user' => ':user hat :text am :date erneuert!',
    'title' => 'Letzte Änderungen',
  ),
  'new' => 'Neuen Eintrag hinzufügen',
  'edit' => 
  array (
    'title' => 'Kategorie',
  ),
);
