<?php

return array (
  'furni' => 
  array (
    'new' => 'Es wurden :count neue Möbelstücke im Habbo Hotel hochgeladen!
<br>Schau sie dir <a href=\':url\'>hier</a> direkt an!',
    'twitter' => ':count neue Möbelstücke wurden hochgeladen!',
  ),
  'view' => 
  array (
    'description' => 
    array (
      'where' => ':user in <b class=":color">:cat</b>',
    ),
  ),
  'liveticker' => 'Liveticker',
  'badge' => 
  array (
    'twitter' => 'Badge :name wurde hinzugefügt! ',
  ),
);
