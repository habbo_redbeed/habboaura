<?php

return array (
  'show' => 
  array (
    'description' => 
    array (
      'where' => 'hat in :cat veröffentlicht',
      'view' => 'mal Gesehen',
      'emo' => 'Wähle einen der drei Emoticons, um deine Meinung den Lesern widerzuspiegeln',
    ),
    'recommended' => 'Ähnliche Artikel',
    'shortnews' => 'Letzten Shortnews',
    'emo' => 'Deine Meinung zu diesem Thema?',
  ),
  'comments' => 
  array (
    'title' => 'Kommentare',
  ),
  'new' => 'NEU',
  'more' => 'Erfahre mehr...',
  'question' => 
  array (
    'title' => 'Was sagst du dazu?',
    'stop' => 'Du hast schon eine Antwort abgeben.',
    'login' => 'Bitte Log dich ein um deine Meinung zu sagen.',
    'end' => 'Die Umfrage ist am :date abgelaufen.',
    'place' => 'Sag es uns...',
    'save' => 'Absenden',
    'label' => 'Deine Meinung / Einsendung',
    'habbo' => 'Habbo Name',
    'lang' => 'Habbo Hotel',
  ),
);
