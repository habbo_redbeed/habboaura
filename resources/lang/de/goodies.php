<?php

return array (
  'infos' => 
  array (
    'description' => 'Erfahre alles über einen Habbo mit dem Habbo Index von HabboAura!<br>Erfahre in welchen Gruppen der gewählte Habbo ist, mit wem er befreundet und welche Badges er trägt!<br><small>Deine Habbo Home muss öffentlich sein!</small>',
    'habbo' => 
    array (
      'name' => 'Habbo Name',
      'date' => 'Erstelldatum',
      'age' => 'Alter',
      'ageLabel' => 'Jahre',
      'motto' => 'Mission',
    ),
    'selectBadges' => 
    array (
      'title' => 'Gewählte Badges',
    ),
    'badge' => 
    array (
      'title' => 'Badges',
      'more' => 'mehr Badges',
    ),
    'friends' => 
    array (
      'title' => 'Freunde',
      'more' => 'weitere Freunde',
    ),
    'groups' => 
    array (
      'title' => 'Gruppen',
      'more' => 'mehr Gruppen',
    ),
    'error' => 'Leider kann dieser Habbo nicht abgerufen werden!',
    'hidden' => 'Die Daten des Habbos können nicht abgerufen werden!',
  ),
);
