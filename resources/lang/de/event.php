<?php

return array (
  'pixel' => 
  array (
    'empty' => 'Leider findet aktuell kein Pixelolymp statt.',
    'period' => 'Von :from bis :to',
    'join' => 'Jetzt teilnehmen!',
    'login' => 'Um teilzunehmen musst du eingeloggt sein!',
    'check' => 'Du hast schon teilgenommen, habe etwas Geduld.',
    'form' => 
    array (
      'title' => 'Einsenden',
      'text' => 'Notiz',
      'image' => 'Bild',
      'imageInfo' => 'Es sind nur Bilder bis zu einer Größe von 1 MB erlaubt.',
    ),
  ),
  'code' => 
  array (
    'title' => 'Code einlösen',
    'body' => 'Hier kannst du einen HabboAura Event Code gegen Sterne einlösen!<br>Zögere nie, sonst macht es jemand anderes! ',
    'form' => 
    array (
      'code' => 'Event-Code',
      'correct' => 'Dir wurden :stars Sterne gutgeschrieben!',
      'error' => 
      array (
        'again' => 'Du kannst diesen Code nur einmal einlösen. Nicht mogeln!',
        'count' => 'Dieser Code wurde schon eingelöst!',
        'wrong' => 'Wir haben diese Code nicht gefunden. Hast du dich vertippt?',
      ),
      'submit' => 'Einlösen',
    ),
  ),
);
