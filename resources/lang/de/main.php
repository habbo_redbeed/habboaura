<?php

return array (
  'search' => 
  array (
    'placeholder' => 'Suchen...',
    'placeholder_img' => 'Bild suchen...',
    'placeholder_staff' => 'Habbo, Land oder Beruf finden...',
    'title' => 'Suche',
  ),
  'likes' => '{0} gefällt mir|[0,1] 1 gefällt das|[2,Inf] :count gefällt das',
  'comment' => '{0} kommentieren|[0,1] 1 kommentiert|[2,Inf] :count Kommentare',
  'comments' => 
  array (
    'form' => 
    array (
      'placeholder' => 'Schreib doch einen Kommentar...',
      'title' => 'Kommentar',
      'submit' => 'Kommentieren',
    ),
    'empty' => 'Keine Kommentare gefunden...',
  ),
  'more' => 'Mehr',
  'open' => 
  array (
    'image' => 'BILD ÖFFNEN',
  ),
  'error' => 
  array (
    'notFound' => 'Es scheint als wärst du hier nicht richtig...',
  ),
  'menu' => 
  array (
    'login' => 'LOGIN',
    'me' => 'ME',
    'join' => 'TRETE BEI',
    'usermenu' => 'Mein Aura',
  ),
  'poll' => 'Umfrage',
  'send' => 'Einsenden',
);
