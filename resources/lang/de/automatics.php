<?php


return [
    'news' => [
        'weekTitle' => 'Wochenrückblick',
        'weekDesc' => 'Dies geschah im Habbo, auf HabboAura und auf sämtlichen sozialen Kanälen von Habbo diese Woche.',
        'week' => '<p><img alt="" src="http://www.habboaura.com//uploads/content/newsletter.ne.png" style="float:right; height:105px; margin:5px; width:114px" />Ob neue Badges, Wettbewerbe, technische Ver&auml;nderungen und und und... <br /> <br /> Die Welt der Nachrichten um Habbo steht niemals still. Damit du diese Woche nichts verpasst hast, gibts hier jeden <strong>Sonntag um 10:00 Uhr</strong> die komplette <strong>Zusammenfassung</strong> aller Artikel und Beitr&auml;ge der Shortnews / des Livetickers von <strong>HabboAura</strong>. <br /> <br /> <strong><img alt="" src="http://www.habboaura.com//uploads/icons/habbo_twitter.gif" style="float:left; height:23px; margin:1px 5px; width:23px" />Artikel</strong> <br /> <br /> :article </p> <p> <br /> <strong><img alt="" src="http://www.habboaura.com//uploads/icons/habbo_twitter.gif" style="float:left; height:23px; margin:1px 5px; width:23px" />Shortnews / Liveticker</strong> </p> <p>:shortnews <br /> <br /> <br /> <strong><img alt="" src="http://www.habboaura.com//uploads/icons/habbo_twitter.gif" style="float:left; height:23px; margin:1px 5px; width:23px" />Neuste Badges <br /> <br /></strong> :badges </p> <hr /> <p><em>Wie hat dir die Woche gefallen? Schreib&#39;s in die Kommentare!</em> </p> '
    ]
];