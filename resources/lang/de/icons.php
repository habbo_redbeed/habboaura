<?php

return array (
  'name' => 
  array (
    'habboaura' => 'HabboAura.com',
    'fansites' => 'Fanseiten',
    'habbo' => 
    array (
      'de' => 'Habbo.de',
      'int' => 'Habbo International',
      'off' => 'Habbo Offiziell',
    ),
    'habclub' => 'HabClub.de',
    'helper' => 'Botschafter',
    'other' => 'Sonstiges',
  ),
);
