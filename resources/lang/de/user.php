<?php

return array (
  'join' => 
  array (
    'title' => 'Ein seltsamer Ort mit tollen Leuten!',
    'step' => 
    array (
      'title' => 
      array (
        'one' => 'Schritt eins',
        'two' => 'Schritt zwei',
        'three' => 'Schritt drei',
      ),
      'one' => 
      array (
        'body' => 'Schön das <b>du ein Teil von HabboAura</b> werden willst.<br>Damit wir aber wissen
                            ob wirklich du der angegebene Habbo bist, musst du im ersten
                            Schritt deine Habbo Mission mit diesem Code ändern!<br><hr><b>Worauf wartest
                            du, trag den unteren Code in deine Mission ein.</b>',
      ),
      'two' => 
      array (
        'body' => '<b>Super!</b> Wenn du nun deine Mission mit dem von uns angegebenen Code geändert
                            hast bist du ein Schritt weiter.<br>Damit wir gemeinsam mit deiner Registration
                            fortfahren können musst du uns zunächst deinen <b>Habbonamen</b> bekannt geben.<hr>',
        'habbo' => 'Habboname',
        'country' => 'Hotel',
      ),
      'three' => 
      array (
        'body' => '<b>Okay</b> - Ab jetzt sollten wir dich verifizieren können.<br>Damit du dich für deinen
                            weiteren Besuch auf <b>HabboAura</b> einloggen kannst benötigen wir von dir deine E-Mail
                            Adresse sowie ein Passwort. <b>Bitte benutze hierfür nicht die Daten, die du im Habbo
                            Hotel verwendest!</b><hr>',
        'password' => 'Passwort',
        'passwordTwo' => 'Passwort wiederholung',
        'email' => 'Email-Adresse',
      ),
    ),
    'save' => 'Account anlegen',
    'next' => 'Weiter',
    'error' => 
    array (
      'habbo' => 
      array (
        'unique' => 'Dieser Habbo wird bereits verwendet!',
      ),
      'motto' => 
      array (
        'notFound' => 'Ist deine Habbo Home öffentlich Sichtbar?<br><i>Bitte versuche es später noch einmal!</i>',
        'incorrect' => 'Dein Motto stimmt nicht mit dem Verifikationscode überein!',
      ),
    ),
    'correct' => 'Du bist HabboAura erfolgreich beigetreten!',
  ),
  'auth' => 
  array (
    'login' => 
    array (
      'email' => 'Email-Adresse',
      'password' => 'Passwort',
      'submit' => 'Absenden',
      'join' => 'Registrieren',
      'title' => 'Usermenü',
      'cookieSet' => 'Eingeloggt bleiben?',
      'forget' => 'Passwort vergessen?',
    ),
  ),
  'menu' => 
  array (
    'title' => 'NAVIGATION',
    'ul' => 
    array (
      'edit' => 'Einstellungen',
      'chat' => 'Konversation',
      'profil' => 'Mein Profil',
      'notification' => 'Benachrichtigung',
      'logout' => 'Ausloggen',
    ),
  ),
  'edit' => 
  array (
    'habbo' => 
    array (
      'title' => 'Habbo bestätigen',
      'body' => 'Wenn du ein neuen Habbo bestätigen willst musst du deine Mission mit dem von uns
                        angegebenen Code ändern.<br>Damit wir gemeinsam mit deiner Bestätigung
                        fortfahren können musst du uns zunächst deinen <b>Habbonamen</b> bekannt geben.<hr>',
      'code' => 'Verifikationscode',
      'save' => 'Habbo bestätigen',
      'check' => 'Dein Habbo wurde in unser System übertragen!',
    ),
    'password' => 
    array (
      'title' => 'Passwort ändern',
      'body' => 'Damit du dich weiter sicher auf HabboAura einloggen kannst, sollst du dein
                        Passwort alle 3 Wochen ändern!<br>
                        <b>Bitte benutze hierfür nicht die Daten, die du im Habbo Hotel verwendest.</b><hr>',
      'save' => 'Passwort ändern',
      'check' => 'Dein Passwort wurde sicher und verschlüsselt hinterlegt!',
    ),
    'image' => 
    array (
      'title' => 'Profilbild',
      'body' => 'Hier kannst du dein HabboAura Profilbild ändern.<br>Du kannst verschiedene Hintergründe wählen, um dein Account zu personalisieren.',
      'save' => 'Bild ändern',
      'check' => 'Ein neues Bild wurde erstellt!',
      'field' => 'Bild',
    ),
    'title' => 'Navigation',
    'menu' => 
    array (
      'password' => 'Passwort ändern',
      'habbo' => 'Habbo bestätigen',
      'image' => 'Profilbild ändern',
    ),
    'profil' => 
    array (
      'title' => 'Profil bearbeiten',
      'motto' => 'Motto',
      'free_text' => 'Über dich',
      'save' => 'Speichern',
      'check' => 'Dein Profil wurde bearbeitet!',
    ),
  ),
  'profil' => 
  array (
    'side' => 
    array (
      'since' => 'Mitglied seit',
    ),
    'freetext' => 
    array (
      'title' => 'Über mich',
    ),
    'static' => 
    array (
      'comments' => 'kommentare verfasst',
      'likes' => 'gefällt mir vergeben',
      'views' => 'Profilaufrufe',
      'rooms' => 'Räume',
      'badge' => 'Badges',
    ),
  ),
  'stars' => 'Stern|Sterne',
  'star' => 
  array (
    'month' => 'in diesem Monat',
    'sum' => 'insgesamt',
  ),
  'image' => 
  array (
    'cloudTwo' => 'Wolken',
    'cloud' => 'Morgensonne',
    'park' => 'Habbo Park',
    'dino' => 'Dinos',
    'visible' => 'Verfügbar bis',
  ),
  'guest' => 
  array (
    'online' => ':count Habbos sind momentan auf HabboAura.com',
  ),
  'comment' => 
  array (
    'delete' => 
    array (
      'check' => 'Der Kommentar wurde gelöscht!',
      'error' => 'Da ist etwas schief gegangen!',
      'mod' => 'Der Kommentar wurde von einem <b>Community Guard</b> entfernt.',
      'notice' => 'Der User hat sein Kommentar gelöscht',
    ),
  ),
  'forget' => 
  array (
    'step' => 
    array (
      'one' => 
      array (
        'body' => 'Damit wir dein Passwort zurücksetzen können, benötigen wir wichtige Angaben von dir.<br>
Fülle bitte alle Felder aus.<br><br>Bitte gebe deine E-Mail Adresse an, welches mit deinem Account von HabboAura.com verbunden ist!',
        'title' => 'E-Mail Adresse',
      ),
      'two' => 
      array (
        'body' => 'Wir brauchen deinen Habbo Namen, den du in Kombination mit deiner E-Mailadresse bei uns hinterlegt hast.',
        'title' => 'Habboname',
      ),
      'three' => 
      array (
        'body' => 'Damit wir wissen das du den Habbo besitzt, hinterlege bitte diesen Code in die Habbo Mission.',
        'title' => 'Mission Code',
      ),
      'four' => 
      array (
        'body' => 'Wir haben dir ein neues Passwort erstellt.<br>Du kannst dich nun mit deiner E-Mailadresse und deinem neuen Passwort auf HabboAura.com einloggen.<br><br><i>Gebe niemals dein Passwort weiter!</i>',
        'title' => 'Willkommen zurück!',
      ),
    ),
    'title' => 'Passwort vergessen?',
    'error' => 
    array (
      'notfound' => 'Diese Habbo-Email ist uns nicht bekannt.',
    ),
  ),
  'placeholder' => 
  array (
    'email' => 'habbo@mail.de',
    'habboname' => 'Habboname',
  ),
  'team' => 
  array (
    'month' => 
    array (
      'profil' => 'Profil aufrufen',
      'title' => 'Mitarbeiter des Monats...',
      'message' => 'Konversation starten',
    ),
    'join' => 
    array (
      'subtitle' => 'SPÜRE WAS VERBINDET',
      'title' => 'WIR SUCHEN DICH NOCH HEUTE!',
      'info' => 
      array (
        'one' => 
        array (
          'title' => 'HABBOAURA',
          'body' => 'HabboAura eröffnete am 21. Juni 2015 und bietet dir viele tolle Inhalte, Funktionen und Spielereien! Wir sind seit Tag eins aktiv und bieten dir täglich guten Content, Gewinnspiele und tolle Events.<br><br>

Wir sind ein großes und starkes Team! Auf HabboAura arbeitet jeder Hand in Hand - täglich und ehrenamtlich!<br><br>

Wir sind HabboAura. All in one. 2015.',
        ),
        'two' => 
        array (
          'title' => 'INTERAKTIV',
          'body' => 'HabboAura ist genau für dich zugeschnitten.<br><br>

Unser Team sorgt für die passenden Informationen und Events im Habbo Hotel - auch auf HabboAura! Wir wollen dich konstant durch Aktivität und Qualität beeindrucken!<br><br>

Mit HabboAura steigt der Spaß im Habbo Hotel noch weiter an!',
        ),
        'three' => 
        array (
          'title' => 'PRODUKTIV',
          'body' => 'Wir befolgen unseren Weg seit Tag eins: Euch stets mit neuesten Informationen, Gewinnspielen und weiteren vielen Aktivitäten zu beeindrucken! Dank euch steigt die Motivation täglich umso mehr und wir sind stets bemüht, die Produktionen auf HabboAura weiter anzutreiben.<br><br>

Wäre das nicht etwas für Dich?',
        ),
      ),
    ),
  ),
  'login' => 
  array (
    'title' => 'Einloggen',
  ),
);
