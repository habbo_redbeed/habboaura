<?php

return array (
  'table' => 
  array (
    'name' => 'Nom',
    'description' => 'Description',
  ),
  'page' => 
  array (
    'list' => 
    array (
      'choose' => 'Vue d\'ensemble',
      'hotel' => ':count Badges en Habbo.:lang',
      'search' => 
      array (
        'info' => 'Ici tu peux chercher un Badge particulier!<br>Dans ja Recherche du vois tous les Badges - Il suffit d\'entrer le code ou un nom!',
        'placeholder' => 'Chercher...',
        'submit' => 'Chercher',
      ),
    ),
    'title' => 
    array (
      'badges' => 'Badges d\'Habbo.:lang',
      'badgesAll' => 'Badges mondials',
      'notBadges' => 'Badges non utilisé en Habbo.:lang',
      'notBadgesAll' => 'Badges mondials non utilisés ',
      'used' => 'Badges rare en Habbo.:lang',
      'usedAll' => 'Badges rare mondial ',
    ),
  ),
  'title' => 
  array (
    'notBadges' => 
    array (
      'used' => 'Badges rare en Habbo.:lang',
      'usedAll' => 'Badges rare mondial
',
    ),
  ),
);
