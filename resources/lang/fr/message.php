<?php

return array (
  'create' => 
  array (
    'message' => 'Message',
    'subject' => 'Objet',
    'submit' => 'Commencer une nouvelle conversation',
    'title' => 'Commencer une nouvelle conversation',
    'users' => 'participant',
  ),
  'index' => 
  array (
    'count' => ':count participants',
    'new' => 'nouvelle conversation',
    'read' => 'ouvrir conversation',
  ),
  'show' => 
  array (
    'access' => 
    array (
      'all' => 'Tout le monde',
    ),
    'subject' => 'participant',
    'submit' => 'envoyer',
    'title' => 'Ajouter un nouveau message',
  ),
);
