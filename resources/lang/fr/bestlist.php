<?php

return array (
  'badge' => 
  array (
    'title' => 'Tableau d\'honneur des badges',
    'add' => 
    array (
      'body' => '<p><b>T\'es pas dans la liste?</b> <br>C\'est probablement aussi parce que vous n\'êtes pas encore reconnus par notre système. Puis inscrivez-vous dans la liste ci-dessous pour être de la partie!<br><small><i>Votre HabboHome doit être en public!</i></small></p>',
      'name' => 'Nom d\'Habbo',
      'submit' => 'Inscrire',
      'check' => 'Ton Habbo a accueilli dans la liste d\'attende.<br>Ton Habbo doit être enregistré en 20 minutes par nous.',
    ),
  ),
);
