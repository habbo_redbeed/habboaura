<?php

return array (
  'furni' => 
  array (
    'new' => ':count Nouveaux meubles chargés en Habbo Hotel! <br>Prends <a href=\':url\'>ici</a>un coup d\'oeil!',
    'twitter' => ':count Nouveaux meubles sont téléchargés!',
  ),
  'view' => 
  array (
    'description' => 
    array (
      'where' => ':user a fait un poste <b class=":color">:cat</b> ici',
    ),
  ),
  'liveticker' => 'Liveticker',
  'badge' => 
  array (
    'twitter' => 'Badge :name ajouté!',
  ),
);
