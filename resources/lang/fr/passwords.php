<?php

return array (
  'password' => 'Le mot de passe doit contenir au moins 6 signes et doit correspondre.',
  'user' => 'Nous ne pouvons pas trouver un utilisateur avec cette adresse d\'email',
  'token' => 'Le code pour réinitialiser est faux.',
  'sent' => 'Nous avons envoyé un mail avec le lien de mot de passe-rémise à zéro',
  'reset' => 'Ton mot de passe a été réinitialisé!',
);
