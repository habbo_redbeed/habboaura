<?php

return array (
  'name' => 
  array (
    'habbo' => 
    array (
      'de' => 'Habbo.fr',
      'int' => 'Habbo international',
      'off' => 'Habbo officiellement',
    ),
    'habclub' => 'HabClub.de',
    'helper' => 'Ambassadeur',
    'fansites' => 'Sites de fans',
    'other' => 'Autres',
    'habboaura' => 'HabboAura.com',
  ),
);
