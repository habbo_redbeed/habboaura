<?php

return array (
  'newsletter' => 'Newsletter',
  'button_save' => 'Enregistrer',
  'enter_your_email' => 'Adresse d\'email',
  'all_rights_reserved' => 'Tous droits réservés',
  'contact_form' => 'Contact',
  'our_location' => 'Notre Emplacement',
  'name_and_surname' => 'Nom et nom de famille',
  'email' => 'Adresse e-mail',
  'phone_number' => 'Numéro de téléphone',
  'subject' => 'Titre',
  'message' => 'Message',
  'button_send' => 'Envoyer',
  'link_previous' => 'retourner',
  'link_next' => 'Prochain',
);
