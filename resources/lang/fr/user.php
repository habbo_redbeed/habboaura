<?php

return array (
  'join' => 
  array (
    'title' => 'Un lieu étrange avec des gens formidables!',
    'step' => 
    array (
      'title' => 
      array (
        'one' => 'Première étape',
        'two' => 'deuxième étape',
        'three' => 'troisième étape',
      ),
      'one' => 
      array (
        'body' => 'Bien que <b>tu veux être un part d\'HabboAura.</b> <br>Pour que nous savons que tu est vraiment l\'Habbo indiqué, tu dois changer ta davise d\'Habbo avec ce code!
                          <br>
                            <hr><b>Qu\'est-ce que tu attends donc? Inscis le code ci-dessous dans ta devise.</b>',
      ),
      'two' => 
      array (
        'body' => '<b>Super!</b> Si tu as changé ta devise avec le code donné par nous t\'es une étape plus loin
                            <br>Pour que nous pouvons continuer avec ta régistration tu dois annoncer ton Nom d\'Habbo.<hr>',
        'habbo' => 'Nom d\'Habbo',
        'country' => 'Hôtel',
      ),
      'three' => 
      array (
        'body' => '<b>D\'accord</b> - Maintenant il fallait que nous pouvons te vérifier.<br>
                            Pour que tu peux te connecter pour tes prochaines visites, nous avons besoin de ta adresse e-mail ainsi qu\'un mot de passe.
                            <b>S\'il te plaît n\'utilise pas les mêmes données comme dans l\'Habbo Hotel!</b>
                            <hr>',
        'password' => 'Mot de passe',
        'passwordTwo' => 'répétition mot de passe',
        'email' => 'Adresse e-mail',
      ),
    ),
    'save' => 'Créer un compte',
    'next' => 'Plus avant',
    'error' => 
    array (
      'habbo' => 
      array (
        'unique' => 'Ce Habbo est déjà en cours d\'utilisation!',
      ),
      'motto' => 
      array (
        'notFound' => 'Est ton Profil d\'Habbo visible au public?<br><i>Essayez-le encore une fois plus tard!</i>',
        'incorrect' => 'Ta devise ne correspond pas au code de vérification!',
      ),
    ),
    'correct' => 'T\'as souscris HabboAura avec succes!',
  ),
  'auth' => 
  array (
    'login' => 
    array (
      'email' => 'adresse e-mail',
      'password' => 'Mot de passe',
      'submit' => 'Envoyer',
      'join' => 'Registrer',
      'title' => 'Menu utilisateur',
      'cookieSet' => 'Rester connecté?',
      'forget' => 'Oubliez mot de passe?',
    ),
  ),
  'menu' => 
  array (
    'title' => 'NAVIGATION',
    'ul' => 
    array (
      'edit' => 'Paramètres',
      'chat' => 'Conversation',
      'profil' => 'Moi',
      'notification' => 'Notification',
      'logout' => 'Fermer la session',
    ),
  ),
  'edit' => 
  array (
    'habbo' => 
    array (
      'title' => 'Confirmer Habbo',
      'body' => 'Si tu veux confirmer un nouveau Habbo tu dois changer ton status avec le code donné par nous<br>Pour que nous pouvons continuer avec ta confirmation tu dois publier<b>Habbo Name</b> avec nous<hr>',
      'code' => 'Code de vérification',
      'save' => 'Confirmer Habbo',
      'check' => 'Votre Habbo a été transféré dans notre système!',
    ),
    'password' => 
    array (
      'title' => 'Changer mot de passe',
      'body' => 'Pour que tu peux te connecter en outre assurément sur HabboAura, il fallait que tu change ton mot de passe toutes les 3 semaines!<br>
                        <b>S\'il te plaît n\'utilise pas les données que t\'as utilisé dans le HabboHotel.</b><hr>',
      'save' => 'Changer mot de passe',
      'check' => 'Votre mot de passe a été déposé en toute sécurité!',
    ),
    'title' => 'Navigation',
    'menu' => 
    array (
      'password' => 'Changer mot de passe',
      'habbo' => 'Confirmer Habbo',
      'image' => 'Changer photo de profil',
    ),
    'profil' => 
    array (
      'title' => 'Modifier profil',
      'motto' => 'Devise',
      'free_text' => 'Vous concernant',
      'save' => 'Enregistrer',
      'check' => 'Ton profil a été modifié!',
    ),
    'image' => 
    array (
      'body' => 'Ici tu peux changer ta photo de profil de HabboAura.<br>Tu peux choisir différents fonds pour personaliser ton compte.',
      'check' => 'Une nouvelle image a été crée!',
      'field' => 'Image',
      'save' => 'Changer image',
      'title' => 'Photo de profil',
    ),
  ),
  'profil' => 
  array (
    'side' => 
    array (
      'since' => 'Membre depuis',
    ),
    'freetext' => 
    array (
      'title' => 'Sur moi',
    ),
    'static' => 
    array (
      'comments' => 'Commentaires postés',
      'likes' => 'J\'aime décernés',
      'views' => 'Vues de profil',
      'badge' => 'Badges',
      'rooms' => 'Salles',
    ),
  ),
  'stars' => 'Étoile/Étoiles',
  'star' => 
  array (
    'month' => 'En ce mois',
    'sum' => 'au total',
  ),
  'comment' => 
  array (
    'delete' => 
    array (
      'check' => 'Le commentaire a été supprimé!',
      'error' => 'Quelque chose a mal tourné!',
      'mod' => 'Le commentaire a été enlevé par un <b>Community Guard</b>',
      'notice' => 'L\'utilisateur a supprimé son commentaire',
    ),
  ),
  'guest' => 
  array (
    'online' => ':count Habbos sur HabboAura.com',
  ),
  'image' => 
  array (
    'cloud' => 'soleil levant',
    'cloudTwo' => 'Nuages',
    'dino' => 'Dinosaures',
    'park' => 'Habbo Parc',
    'visible' => 'Disponible jusqu\'à',
  ),
  'forget' => 
  array (
    'title' => 'Mot de passe oublié?',
    'step' => 
    array (
      'one' => 
      array (
        'title' => 'adresse email ',
        'body' => 'Pour que nous pouvons reculer ton mot de passe, nous avons besoin d\'ordonnées importantes de toi.<br>S\'il vous plaît complète tous les cases.<br><br>S\'il vous plaît indique l\'adresse email celle qu\'elle est en ligne avec ton compte de HabboAura.com!',
      ),
      'four' => 
      array (
        'body' => 'Nous avons crée un nouveau mot de passe pour toi.<br>Tu peux te connecter avec ton adresse-email et ton nouveau mot de passe sur HabboAura.com.<br><br><i>Ne jamais transmets ton mot de passe!</i>',
        'title' => 'Bienvenue',
      ),
      'three' => 
      array (
        'body' => 'Pour que nous savons que tu possèdes l\'Habbo, s\'il vous plaît met ce code dans ton Status d\'habbo.',
        'title' => 'Code status',
      ),
      'two' => 
      array (
        'body' => 'Nous avons besoin de ton nom d\'Habbo, celui qui est en combinaison avon ton adresse d\'email chez nous.',
        'title' => 'Nom d\'Habbo',
      ),
    ),
    'error' => 
    array (
      'notfound' => 'Cet adresse-Habbo n\'est pas connu par nous',
    ),
  ),
  'team' => 
  array (
    'month' => 
    array (
      'title' => 'Employé du mois...',
      'profil' => 'Appeler profil',
      'message' => 'commencer conversation',
    ),
    'join' => 
    array (
      'title' => 'NOUS TE CHERCHONS AUJOURD\'HUI!',
      'subtitle' => 'SENTIR CE QUE RACCORDE!',
      'info' => 
      array (
        'two' => 
        array (
          'title' => 'INTERACTIVE',
          'body' => 'HabboAura est exactement concevu pour toi.<br><br>

Note équipe veille pour les informations et événements dans l\'Habbo Hotel - aussi sur HabboAura! Nous voulons t\'impressioner par des activités et qualités!<br><br>

Avec HabboAura la clownerie en Habbo Hotel augmente!',
        ),
        'three' => 
        array (
          'title' => 'PRODUCTIVEMENT',
          'body' => 'Nous suivons note chemin depuis le premier jour: Impressioner-vous constamment avec les informations plus récentes, loteries et beaucoup plus d\'activités! Grâce à vous la motivation monte jour par jour de plus et nous sommes constamment efforcé d\'actionner la production d\'HabboAura.<br><br>

Serait-ca pas quelque chose pour toi?',
        ),
        'one' => 
        array (
          'title' => 'HABBOAURA',
          'body' => 'HabboAura a entamé le 21. Juin 2015 et t\'offres beaucoup de contenus, fonctions et gadgets formidables! Nous sommes activ depouis le premier jour et t\'offrons jour par jour de bons contenus, loteries et des événements formidables.<br><br>

Nous sommes une équipe grande et forte! Sur HabboAura tout le monde travaille main en main - jour par jour et bénévolement!<br><br>

Nous sommes HabboAura. Tout en un. 2015.',
        ),
      ),
    ),
  ),
  'placeholder' => 
  array (
    'habboname' => 'Nom d\'Habbo',
    'email' => 'habbo@mail.de',
  ),
);
