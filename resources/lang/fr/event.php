<?php

return array (
  'pixel' => 
  array (
    'empty' => 'No Pixelolymp at the moment!',
    'period' => 'De :from jusqu\'à :to',
    'join' => 'Participer maintenant!',
    'login' => 'Pour participer tu dois être connecté!',
    'check' => 'Tu as déjà participé, as un peu de patience.',
    'form' => 
    array (
      'title' => 'Envoyer',
      'text' => 'Notation',
      'image' => 'Image',
      'imageInfo' => 'Seules les images d\'une grandeur de 1 MB sont autorisées.',
    ),
  ),
);
