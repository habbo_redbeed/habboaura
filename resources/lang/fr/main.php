<?php

return array (
  'search' => 
  array (
    'placeholder' => 'Rechercher...',
    'placeholder_img' => 'Rechercher image...',
    'placeholder_staff' => 'Trouver Habbo, pays ou profession...',
    'title' => 'Recherche',
  ),
  'likes' => '{0} j\'aime|[0,1] 1 aime ça|[2,Inf] :count aiment ça',
  'comment' => '{0} commenter|[0,1] 1 commenté|[2,Inf] :count commentaires',
  'comments' => 
  array (
    'form' => 
    array (
      'placeholder' => 'Écris un commentaire...',
      'title' => 'Commentaire',
      'submit' => 'commenter',
    ),
    'empty' => 'Aucun commentaire trouvé...',
  ),
  'more' => 'Plus',
  'open' => 'ouvrir',
  'menu' => 
  array (
    'me' => 'MOI',
    'login' => 'OUVERTURE DE SESSION',
    'join' => 'SOUSCRIRE',
  ),
  'error' => 
  array (
    'notFound' => 'Il semble que tu n\'es pas correct ici...',
  ),
);
