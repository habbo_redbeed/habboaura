<?php

return array (
  'staff' => 
  array (
    'list' => 
    array (
      'old' => 'Anciens employés',
      'now' => 'Employés actuels',
    ),
    'table' => 
    array (
      'head' => 
      array (
        'name' => 'Nom',
        'job' => 'Titre professionnel',
        'date' => 'Durée de la tâche',
      ),
      'from' => 'De',
      'to' => 'jusqu\'à',
      'since' => 'depuis',
    ),
    'country' => 
    array (
      'small' => 
      array (
        'int' => 'Sulake',
        'de' => 'DE',
        'com-br' => 'BR',
        'com' => 'COM',
        'es' => 'ES',
        'fi' => 'FI',
        'fr' => 'FR',
        'it' => 'IT',
        'nl' => 'NL',
        'com-tr' => 'TR',
      ),
      'name' => 
      array (
        'int' => 'Sulake',
        'de' => 'Allemagne',
        'com-br' => 'Brésil',
        'com' => 'International (.com) ',
        'es' => 'Espagne',
        'fi' => 'Finlande',
        'fr' => 'France',
        'it' => 'Italie',
        'nl' => 'Pays-Bas',
        'com-tr' => 'Turquie',
      ),
    ),
  ),
  'change' => 
  array (
    'user' => ':user a changé :text en :date',
    'title' => 'Dernières changements',
  ),
  'new' => 'Ajouter une nouvelle entrée',
  'edit' => 
  array (
    'title' => 'Catégorie',
  ),
);
