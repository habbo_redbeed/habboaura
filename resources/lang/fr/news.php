<?php

return array (
  'show' => 
  array (
    'description' => 
    array (
      'where' => 'a publié en :cat',
      'view' => 'affichage',
      'emo' => 'Choisissez l\'une des trois émoticônes pour refléter votre avis aux lecteurs',
    ),
    'emo' => 'Ton avis sur ce sujet?',
    'recommended' => 'articles ressemblants',
    'shortnews' => 'Derniers flashs d\'informations',
  ),
  'comments' => 
  array (
    'title' => 'Commentaires',
  ),
  'more' => 'Lire plus...',
  'new' => 'NOUVEAU',
  'question' => 
  array (
    'login' => 'S\'il vous plaît connectez-vous pour partager votre opinion',
    'end' => 'Le sondage s\'est écoulé le :date',
    'habbo' => 'Nom d\'Habbo',
    'label' => 'Ton opinion / envoi',
    'lang' => 'Habbo Hotel',
    'place' => 'Dis-nous...',
    'save' => 'envoyer',
    'stop' => 'T\'as déjà transmis une réponse',
    'title' => 'Que dis-tu?',
  ),
);
