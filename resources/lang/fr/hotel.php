<?php

return array (
  'de' => 
  array (
    'name' => 'Allemagne',
    'link' => 'habbo.de',
    'code' => 'de',
  ),
  'com' => 
  array (
    'name' => 'International',
    'link' => 'habbo.com',
    'code' => 'com',
  ),
  'tr' => 
  array (
    'code' => 'tr',
    'link' => 'habbo.com.tr',
    'name' => 'Turquie',
  ),
);
