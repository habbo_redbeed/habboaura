<?php

return array (
  'password' => 'Le mot de passe doit avoir six signes et doit contenir des lettres et des chiffres.',
  'user' => 'Nous ne pouvons pas trouver un utilisateur avec cet adresse d\'email.',
  'token' => 'Ce mot de passe est invalide',
  'sent' => 'Rappel de mot de passe envoyé!',
);
