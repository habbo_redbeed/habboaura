<?php

return array (
  'news' => 
  array (
    'banner' => 
    array (
      'title' => 'HABBOAURA NOUVELLES',
      'description' => 'Un coup d\'oeil dans les mondes de l\'Habbovers',
    ),
  ),
  'furni' => 
  array (
    'banner' => 
    array (
      'title' => 'MEUBLES TÉLÉCHARGÉS',
      'description' => 'Ces meubles sont nouveaux dans l\'Habbo Hotel!',
    ),
  ),
  'badge' => 
  array (
    'banner' => 
    array (
      'title' => 'DERNIERS BADGES',
      'description' => 'Ces Badges sont nouveaux dans l\'Habbo Hotel!',
    ),
  ),
);
