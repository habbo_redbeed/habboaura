<?php

return array (
  'infos' => 
  array (
    'badge' => 
    array (
      'more' => 'plus de badges',
      'title' => 'Badges',
    ),
    'friends' => 
    array (
      'more' => 'Plus d\'amis',
      'title' => 'Amis',
    ),
    'groups' => 
    array (
      'more' => 'Plus de groupes',
      'title' => 'Groupes',
    ),
    'habbo' => 
    array (
      'age' => 'Age',
      'ageLabel' => 'Années',
      'date' => 'Date de création',
      'motto' => 'Statut',
      'name' => 'Nom d\'Habbo',
    ),
    'hidden' => 'Les données d\'Habbo sélectionné ne peuvent pas être trouvées!',
    'selectBadges' => 
    array (
      'title' => 'Badges sélectionné',
    ),
    'error' => 'Ouh... où est le habbo?',
    'description' => 'En savoir plus sur un Habbo avec Habbo indice de HabboAura!<br>Découvrez dans quels groupes le Habbo sélectionné est, avec qui il se lie d\'amitié et ce qui les insignes qu\'il porte !<br><small>Pour tous le infos le profil d\'Habbo dot être en public!</small>',
  ),
);
