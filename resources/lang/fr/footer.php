<?php

return array (
  'title' => 
  array (
    'article' => 'Derniers articles',
    'lexicon' => 'Entrées de lexique',
    'user' => 'Utilisateurs registrés',
    'comments' => 'Derniers commentaires',
    'active' => 'Activité',
  ),
  'links' => 
  array (
    'news' => 'Nouvelles',
    'lexicon' => 'Lexique',
    'home' => 'Accueil',
    'datenschutzerklaerung' => 'Déclaration de protection de données',
    'impressum' => 'Mentions légales',
    'job' => 'Jobs',
  ),
);
