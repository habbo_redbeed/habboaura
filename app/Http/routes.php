<?php

/*
|--------------------------------------------------------------------------
| Frontend Routes
|--------------------------------------------------------------------------
*/

$languages = LaravelLocalization::getSupportedLocales();
foreach ($languages as $language => $values) {
    $supportedLocales[] = $language;
}

$locale = Request::segment(1);
if (in_array($locale, $supportedLocales)) {
    LaravelLocalization::setLocale($locale);
    App::setLocale($locale);
}

Route::get('/', ['as' => 'main', 'uses' => 'HomeController@redirect']);

Route::group(array('prefix' => '/img'), function () {

    Route::get('/profil/{userid}/{background}/{lang}.png', ['as' => 'profil', 'uses' => 'ImageController@profil']);
    Route::get('/profil/{userid}/{background}.png', ['as' => 'profil', 'uses' => 'ImageController@profil']);
    Route::get('/profil/{userid}.png', ['as' => 'profil', 'uses' => 'ImageController@profil']);

    });

Route::group(array('prefix' => LaravelLocalization::getCurrentLocale(), 'before' => array('localization', 'before')), function () {

    Session::put('my.locale', LaravelLocalization::getCurrentLocale());

    // frontend dashboard
    Route::get('/', ['as' => 'dashboard', 'uses' => 'HomeController@index']);

    Route::get('/bad/news', ['as' => 'bad.news', 'uses' => 'HomeController@getnews']);
    Route::get('/bad/lexicon', ['as' => 'bad.lexicon', 'uses' => 'HomeController@getLexi']);


    //event
    Route::get('event/code', array('as' => 'event.event.view', 'uses' => 'EventController@code'));
    Route::post('event/code', array('as' => 'event.event.post', 'uses' => 'EventController@codePost'));

    Route::get('/article/tag/{tag}', array('as' => 'dashboard.article.tag', 'uses' => 'CategoryController@tags'));
    // article
    Route::get('/article', array('as' => 'dashboard.article', 'uses' => 'ArticleController@index'));
    Route::post('/article/quest/any/{slug}', array('as' => 'dashboard.article.quest.any', 'uses' => 'ArticleController@questAny'));
    Route::post('/article/quest/{slug}', array('as' => 'dashboard.article.quest', 'uses' => 'ArticleController@quest'));
    Route::get('/article/{slug}', array('as' => 'dashboard.article.show', 'uses' => 'ArticleController@show'));

    Route::get('/shortnews/{slug?}', array('as' => 'dashboard.shortnews', 'uses' => 'ShortnewsController@index'));


    Route::get('/best/badge/{lang?}', array('as' => 'best.badge', 'uses' => 'BadgesScoreController@index'));
    Route::post('/best/add', array('as' => 'best.add', 'uses' => 'BadgesScoreController@add'));


    //microActions
    Route::get('/like/{type}/{token}', array('as' => 'micro.like', 'uses' => 'UserController@saveLike'));
    Route::get('/like/{type}/{token}/{emo}', array('as' => 'micro.like.emo', 'uses' => 'UserController@saveLike'));
    Route::post('/comment/{type}/{token}', array('as' => 'micro.comment', 'uses' => 'UserController@saveComment'));
    Route::get('/comment/{type}/{token}', array('as' => 'micro.comment.view', 'uses' => 'UserController@comments'));

    Route::get('/delete/comment/{id}', array('as' => 'micro.delete.comment', 'uses' => 'UserController@deleteComment'));

    // user
    Route::get('/user/login', array('as' => 'user.auth.login', 'uses' => 'UserController@login'));
    Route::post('/user/login', array('as' => 'user.auth.login.post', 'uses' => 'UserController@loginPost'));

    Route::get('/user/logout', array('as' => 'user.auth.logout', 'uses' => 'UserController@logout'));

    Route::get('/user/forget/password', array('as' => 'user.auth.forget.password', 'uses' => 'UserController@forget'));
    Route::post('/user/forget/password', array('as' => 'user.auth.forget.password.post', 'uses' => 'UserController@forgetPost'));

    // user
    Route::get('/user/join', array('as' => 'user.auth.join', 'uses' => 'UserController@join'));
    Route::post('/user/join', array('as' => 'user.auth.post.join', 'uses' => 'UserController@joinPost'));

    Route::get('/user/edit/habbo', array('as' => 'user.edit.habbo', 'uses' => 'UserController@editHabbo'));
    Route::post('/user/edit/habbo', array('as' => 'user.edit.post.habbo', 'uses' => 'UserController@saveHabbo'));

    Route::get('/user/edit/image', array('as' => 'user.edit.image', 'uses' => 'UserController@editImage'));
    Route::post('/user/edit/image', array('as' => 'user.edit.post.image', 'uses' => 'UserController@saveImage'));

    Route::get('/user/edit/password', array('as' => 'user.edit.password', 'uses' => 'UserController@editPassword'));
    Route::post('/user/edit/password', array('as' => 'user.edit.post.password', 'uses' => 'UserController@savePassword'));

    Route::post('/user/edit/profil', array('as' => 'user.edit.post.profil', 'uses' => 'UserController@saveProfil'));

    Route::group(['prefix' => 'messages'], function () {
        Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
        Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
        Route::get('new', ['as' => 'messages.new', 'uses' => 'MessagesController@newMessages']);
        Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
        Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
        Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
    });

    //USER
    Route::get('/home/{slug}', array('as' => 'user.home', 'uses' => 'UserController@showRedirect'));
    Route::get('/home/{slug}/{lang}', array('as' => 'user.home.lang', 'uses' => 'UserController@show'));

    Route::group(array('prefix' => 'goodies', ), function () {

        Route::get('/images', array('as' => 'goodies.images.view', 'uses' => 'ImagesController@show'));
        Route::get('/images/search/{slug?}', array('as' => 'goodies.images.search', 'uses' => 'ImagesController@search'));

        Route::post('/habboindex/post', array('as' => 'goodies.habboindex.view.post', 'uses' => 'HabboinfoController@post'));
        Route::get('/habboindex/{habbo}/{lang}', array('as' => 'goodies.habboindex.view', 'uses' => 'HabboinfoController@habbo'));
        Route::get('/habboindex', array('as' => 'goodies.habboindex.index', 'uses' => 'HabboinfoController@habbo'));
    });


    Route::group(array('prefix' => 'lexicon', ), function () {

        Route::get('/', array('as' => 'lexicon.index', 'uses' => 'LexiconController@index'));
        Route::get('/categorie/{slug}', array('as' => 'lexicon.categorie', 'uses' => 'LexiconController@index'));

        //Badge
        Route::get('/badges', array('as' => 'lexicon.badge.index', 'uses' => 'BadgesController@index'));
        Route::get('/badges/not/{lang}', array('as' => 'lexicon.badge.not.lang', 'uses' => 'BadgesController@not'));
        Route::get('/badges/used/{lang?}', array('as' => 'lexicon.badge.used.lang', 'uses' => 'BadgesController@used'));
        Route::get('/badges/{lang?}', array('as' => 'lexicon.badge.all.lang', 'uses' => 'BadgesController@index'));
        Route::get('/badges/{lang}', array('as' => 'lexicon.badge.index.lang', 'uses' => 'BadgesController@index'));



        Route::get('/entry/{slug}', array('as' => 'lexicon.entry', 'uses' => 'LexiconController@show'));
        Route::get('/edit/entry/{slug?}', array('as' => 'lexicon.entry.edit', 'uses' => 'LexiconController@edit'));
        Route::post('/edit/entry/{id?}', array('as' => 'lexicon.entry.save', 'uses' => 'LexiconController@save'));

        Route::get('/staff', array('as' => 'lexicon.staff.index', 'uses' => 'StaffController@index'));
        Route::get('/staff/country/{lang}', array('as' => 'lexicon.staff.index.lang', 'uses' => 'StaffController@index'));
        Route::get('/staff/search/{slug}', array('as' => 'lexicon.staff.index.search', 'uses' => 'StaffController@search'));

        //Route::get('/images/search/{slug?}', array('as' => 'goodies.images.search', 'uses' => 'ImagesController@search'));
    });


    // news
    Route::get('/news', array('as' => 'dashboard.news', 'uses' => 'NewsController@index'));
    Route::get('/news/{slug}', array('as' => 'dashboard.news.show', 'uses' => 'NewsController@show'));

    // news
    Route::get('/furni', array('as' => 'furni.index', 'uses' => 'FurniController@index'));
    Route::get('/furni/search/{string}', array('as' => 'furni.search', 'uses' => 'FurniController@search'));

    // tags
    Route::get('/tag/{slug}', array('as' => 'dashboard.tag', 'uses' => 'TagController@index'));

    // categories
    Route::get('/category/{slug}', array('as' => 'dashboard.category', 'uses' => 'CategoryController@index'));

    Route::get('/team', array('as' => 'dashboard.team', 'uses' => 'TeamController@index'));

    // page
    Route::get('/page', array('as' => 'dashboard.page', 'uses' => 'PageController@index'));
    Route::get('/page/{slug}', array('as' => 'dashboard.page.show', 'uses' => 'PageController@show'));

    Route::get('/pixel/{id}', array('as' => 'page.pixel.form', 'uses' => 'PageController@pixelForm'))
        ->where('id', '[0-9]+');
    Route::post('/pixel/{id}/post', array('as' => 'page.pixel.form.post', 'uses' => 'PageController@pixelFormSave'))
        ->where('id', '[0-9]+');

    // photo gallery
    Route::get('/photo-gallery/{slug}', array('as' => 'dashboard.photo_gallery.show',
        'uses' => 'PhotoGalleryController@show'));

    // video
    Route::get('/video', array('as' => 'dashboard.video', 'uses' => 'VideoController@index'));
    Route::get('/video/{slug}', array('as' => 'dashboard.video.show', 'uses' => 'VideoController@show'));

    // projects
    Route::get('/project', array('as' => 'dashboard.project', 'uses' => 'ProjectController@index'));
    Route::get('/project/{slug}', array('as' => 'dashboard.project.show', 'uses' => 'ProjectController@show'));

    // contact
    Route::get('/contact', array('as' => 'dashboard.contact', 'uses' => 'FormPostController@getContact'));

    // faq
    Route::get('/faq', array('as' => 'dashboard.faq', 'uses' => 'FaqController@show'));

    // rss
    Route::get('/rss', array('as' => 'rss', 'uses' => 'RssController@index'));

    // search
    Route::get('/search', ['as' => 'admin.search', 'uses' => 'SearchController@index']);

    // language
    // Route::get('/set-locale/{language}', array('as' => 'language.set', 'uses' => 'LanguageController@setLocale'));

    // maillist
    Route::get('/save-maillist', array('as' => 'frontend.maillist', 'uses' => 'MaillistController@getMaillist'));
    Route::post('/save-maillist', array('as' => 'frontend.maillist.post', 'uses' => 'MaillistController@postMaillist'));
});

/*
|--------------------------------------------------------------------------
| Backend Routes
|--------------------------------------------------------------------------
*/

Route::group(array('prefix' => LaravelLocalization::getCurrentLocale()), function () {

    Route::group(array('prefix' => '/admin',
        'namespace' => 'Admin',
        'middleware' => array('sentry.auth', 'before')), function () {

        // admin dashboard
        Route::get('/', array('as' => 'admin.dashboard', 'uses' => 'DashboardController@index'));

        Route::get('stars', array('as' => 'admin.stars.index', 'uses' => 'StarsController@index'));

        Route::get('stars/add', array('as' => 'admin.stars.add', 'uses' => 'StarsController@add'));
        Route::post('stars/add', array('as' => 'admin.stars.add.post', 'uses' => 'StarsController@addPost'));


        Route::get('stars/sub', array('as' => 'admin.stars.sub', 'uses' => 'StarsController@sub'));
        Route::post('stars/sub', array('as' => 'admin.stars.sub.post', 'uses' => 'StarsController@subPost'));


        // user
        Route::resource('user', 'UserController');
        Route::get('user/{id}/delete', array('as' => 'admin.user.delete',
            'uses' => 'UserController@confirmDestroy'))->where('id', '[0-9]+');

        // group
        Route::resource('group', 'GroupController');
        Route::get('group/{id}/delete', array('as' => 'admin.group.delete',
            'uses' => 'GroupController@confirmDestroy'))->where('id', '[0-9]+');

        // blog
        Route::resource('article', 'ArticleController', array('before' => 'hasAccess:article'));
        Route::get('article/{id}/delete', array('as' => 'admin.article.delete',
            'uses' => 'ArticleController@confirmDestroy'))->where('id', '\d+');

        //article question
        Route::get('article/quest/{id?}', array('as' => 'admin.shortnews.quest.edit', 'uses' => 'ArticleController@fromTextView'));
        Route::post('article/quest/{id?}', array('as' => 'admin.shortnews.quest.save', 'uses' => 'ArticleController@fromTextViewSave'));

        Route::any('event/calendar', array('as' => 'admin.event.calendar.index', 'uses' => 'EventCalendarController@index'));
        Route::get('event/calendar/edit/{id?}', array('as' => 'admin.event.calendar.edit', 'uses' => 'EventCalendarController@edit'));
        Route::post('event/calendar/edit/{id?}', array('as' => 'admin.event.calendar.edit.post', 'uses' => 'EventCalendarController@editPost'));
        Route::get('event/calendar/delete/{id}', array('as' => 'admin.event.calendar.delete', 'uses' => 'EventCalendarController@delete'));

        Route::get('topImage', array('as' => 'admin.page.imageScan', 'uses' => 'PageController@imageScan'));

        Route::get('event/code', array('as' => 'admin.event.code.edit', 'uses' => 'EventCalendarController@code'));
        Route::post('event/code', array('as' => 'admin.event.code.post', 'uses' => 'EventCalendarController@codePost'));

        //shortnews
        Route::any('shortnews', array('as' => 'admin.shortnews.view', 'uses' => 'ArticleController@viewShortNews'));
        Route::get('shortnews/edit/{id?}', array('as' => 'admin.shortnews.edit', 'uses' => 'ArticleController@showShortnews'));
        Route::post('shortnews/edit/{id}/save', array('as' => 'admin.shortnews.save', 'uses' => 'ArticleController@saveShortnews'));
        Route::get('shortnews/edit/{id}/delete', array('as' => 'admin.shortnews.delete', 'uses' => 'ArticleController@deleteShortnews'));
        Route::post('shortnews/{id}/toggle-pin', array('as' => 'admin.shortnews.toggle', 'uses' => 'ArticleController@toggleShortnews'));

        Route::get('lexicon/staff', array('as' => 'admin.lexicon.staff.index', 'uses' => 'StaffController@index'));
        Route::get('lexicon/staff/lang/{lang}', array('as' => 'admin.lexicon.staff.index.lang', 'uses' => 'StaffController@index'));
        Route::get('lexicon/staff/edit/{id?}', array('as' => 'admin.lexicon.staff.edit', 'uses' => 'StaffController@edit'));
        Route::post('lexicon/staff/edit/{id}', array('as' => 'admin.lexicon.staff.save', 'uses' => 'StaffController@save'));
        Route::get('lexicon/staff/delete/{id}', array('as' => 'admin.lexicon.staff.delete', 'uses' => 'StaffController@delete'));

        Route::get('lexicon/entrys', array('as' => 'admin.lexicon.entry.index', 'uses' => 'LexiconController@index'));
        Route::get('lexicon/entry/{id}', array('as' => 'admin.lexicon.entry.show', 'uses' => 'LexiconController@showEntry'));
        Route::any('lexicon/entry/save/{id}', array('as' => 'admin.lexicon.entry.save', 'uses' => 'LexiconController@saveEntry'));
        Route::get('lexicon/entry/delete/{id}', array('as' => 'admin.lexicon.delete', 'uses' => 'LexiconController@deleteEntry'));


        Route::get('api/images/add', array('as' => 'admin.api.images.add', 'uses' => 'PhotoGalleryController@addImage'));
        Route::post('api/images/save', array('as' => 'admin.api.images.save', 'uses' => 'PhotoGalleryController@saveImage'));

        // news
        Route::resource('news', 'NewsController', array('before' => 'hasAccess:news'));
        Route::get('news/{id}/delete', array('as' => 'admin.news.delete',
            'uses' => 'NewsController@confirmDestroy'))->where('id', '[0-9]+');

        // category
        Route::resource('category', 'CategoryController', array('before' => 'hasAccess:category'));
        Route::get('category/{id}/delete', array('as' => 'admin.category.delete',
            'uses' => 'CategoryController@confirmDestroy'))->where('id', '[0-9]+');

        // faq
        Route::resource('faq', 'FaqController', array('before' => 'hasAccess:faq'));
        Route::get('faq/{id}/delete', array('as' => 'admin.faq.delete',
            'uses' => 'FaqController@confirmDestroy'))->where('id', '[0-9]+');

        // project
        Route::resource('project', 'ProjectController');
        Route::get('project/{id}/delete', array('as' => 'admin.project.delete',
            'uses' => 'ProjectController@confirmDestroy'))->where('id', '[0-9]+');

        // page
        Route::resource('page', 'PageController');
        Route::get('page/{id}/delete', array('as' => 'admin.page.delete',
            'uses' => 'PageController@confirmDestroy'))->where('id', '[0-9]+');

        // photo gallery
        Route::resource('photo-gallery', 'PhotoGalleryController');
        Route::get('photo-gallery/{id}/delete', array('as' => 'admin.photo-gallery.delete',
            'uses' => 'PhotoGalleryController@confirmDestroy'))->where('id', '[0-9]+');

        // video
        Route::resource('video', 'VideoController');
        Route::get('video/{id}/delete', array('as' => 'admin.video.delete',
            'uses' => 'VideoController@confirmDestroy'))->where('id', '[0-9]+');
        Route::post('/video/get-video-detail', array('as' => 'admin.video.detail',
            'uses' => 'VideoController@getVideoDetail'))->where('id', '[0-9]+');

        // ajax - blog
        Route::post('article/{id}/toggle-publish', array('as' => 'admin.article.toggle-publish',
            'uses' => 'ArticleController@togglePublish'))->where('id', '[0-9]+');

        // ajax - blog
        Route::post('article/{id}/toggle-topnews', array('as' => 'admin.article.toggle-topnews',
            'uses' => 'ArticleController@toggleTopnews'))->where('id', '[0-9]+');

        // ajax - news
        Route::post('news/{id}/toggle-publish', array('as' => 'admin.news.toggle-publish',
            'uses' => 'NewsController@togglePublish'))->where('id', '[0-9]+');

        // ajax - photo gallery
        Route::post('photo-gallery/{id}/toggle-publish', array('as' => 'admin.photo_gallery.toggle-publish',
            'uses' => 'PhotoGalleryController@togglePublish'))->where('id', '[0-9]+');
        Route::post('photo-gallery/{id}/toggle-menu', array('as' => 'admin.photo_gallery.toggle-menu',
            'uses' => 'PhotoGalleryController@toggleMenu'))->where('id', '[0-9]+');

        // ajax - page
        Route::post('page/{id}/toggle-publish', array('as' => 'admin.page.toggle-publish',
            'uses' => 'PageController@togglePublish'))->where('id', '[0-9]+');
        Route::post('page/{id}/toggle-menu', array('as' => 'admin.page.toggle-menu',
            'uses' => 'PageController@toggleMenu'))->where('id', '[0-9]+');

        // ajax - form post
        Route::post('form-post/{id}/toggle-answer', array('as' => 'admin.form-post.toggle-answer',
            'uses' => 'FormPostController@toggleAnswer'))->where('id', '[0-9]+');

        // file upload photo gallery
        Route::post('/photo-gallery/upload/{id}', array('as' => 'admin.photo.gallery.upload.image',
            'uses' => 'PhotoGalleryController@upload'))->where('id', '[0-9]+');
        Route::post('/photo-gallery-delete-image', array('as' => 'admin.photo.gallery.delete.image',
            'uses' => 'PhotoGalleryController@deleteImage'));

        // settings
        Route::get('/settings', array('as' => 'admin.settings', 'uses' => 'SettingController@index'));
        Route::post('/settings', array('as' => 'admin.settings.save',
            'uses' => 'SettingController@save'), array('before' => 'csrf'));

        // form post
        Route::resource('form-post', 'FormPostController', array('only' => array('index', 'show', 'destroy')));
        Route::get('form-post/{id}/delete', array('as' => 'admin.form-post.delete',
            'uses' => 'FormPostController@confirmDestroy'))->where('id', '[0-9]+');

        // slider
        Route::get('/slider', array(
            'as' => 'admin.slider',
            function () {

                return View::make('backend/slider/index');
            }));

        // slider
        Route::resource('slider', 'SliderController');
        Route::get('slider/{id}/delete', array('as' => 'admin.slider.delete',
            'uses' => 'SliderController@confirmDestroy'))->where('id', '[0-9]+');

        // file upload slider
        Route::post('/slider/upload/{id}', array('as' => 'admin.slider.upload.image',
            'uses' => 'SliderController@upload'))->where('id', '[0-9]+');
        Route::post('/slider-delete-image', array('as' => 'admin.slider.delete.image',
            'uses' => 'SliderController@deleteImage'));

        // menu-managment
        Route::resource('menu', 'MenuController');
        Route::post('menu/save', array('as' => 'admin.menu.save', 'uses' => 'MenuController@save'));
        Route::get('menu/{id}/delete', array('as' => 'admin.menu.delete',
            'uses' => 'MenuController@confirmDestroy'))->where('id', '[0-9]+');
        Route::post('menu/{id}/toggle-publish', array('as' => 'admin.menu.toggle-publish',
            'uses' => 'MenuController@togglePublish'))->where('id', '[0-9]+');

        // log
        Route::any('log', ['as' => 'admin.log', 'uses' => 'LogController@index']);

        // language
        Route::get('language/set-locale/{language}', array('as' => 'admin.language.set',
            'uses' => 'LanguageController@setLocale'));
    });
});

Route::post('/contact', array('as' => 'dashboard.contact.post',
    'uses' => 'FormPostController@postContact'), array('before' => 'csrf'));

// filemanager
Route::get('filemanager/show', function () {

    return View::make('backend/plugins/filemanager');
})->before('sentry.auth');

// filemanager
Route::get('filemanager_user/show', ['as' => 'frontend.imagebrowser', function () {

    return View::make('frontend/plugins/filemanager');
}]);

// login
Route::get('/admin/login', array(
    'as' => 'admin.login',
    function () {

        return View::make('backend/auth/login');
    }));

Route::group(array('namespace' => 'Admin'), function () {

    // admin auth
    Route::get('admin/logout', array('as' => 'admin.logout', 'uses' => 'AuthController@getLogout'));
    Route::get('admin/login', array('as' => 'admin.login', 'uses' => 'AuthController@getLogin'));
    Route::post('admin/login', array('as' => 'admin.login.post', 'uses' => 'AuthController@postLogin'));

    // admin password reminder
    Route::get('admin/forgot-password', array('as' => 'admin.forgot.password',
        'uses' => 'AuthController@getForgotPassword'));
    Route::post('admin/forgot-password', array('as' => 'admin.forgot.password.post',
        'uses' => 'AuthController@postForgotPassword'));

    Route::get('admin/{id}/reset/{code}', array('as' => 'admin.reset.password',
        'uses' => 'AuthController@getResetPassword'))->where('id', '[0-9]+');
    Route::post('admin/reset-password', array('as' => 'admin.reset.password.post',
        'uses' => 'AuthController@postResetPassword'));
});

/*
|--------------------------------------------------------------------------
| General Routes
|--------------------------------------------------------------------------
*/

// error


// 404 page not found

