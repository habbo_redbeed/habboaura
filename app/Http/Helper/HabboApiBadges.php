<?php namespace HabboIndex\Helpers;

use Storage;
use Carbon\Carbon;
use Cache;
use HabboIndex\Helpers\HabboApi;

class HabboApiBadges extends HabboApi
{

    public function all($lang = ''){
        $key = 'habbo_badges_all_'.$lang;
        return Cache::remember($key, 20, function () use ($lang) {

            $url = 'badges/';
            if(!empty($lang)){
                $url .= $lang.'/';
            }else{
                $url .= 'all/';
            }

            $contents = $this->client->get($this->mainUrl . $url);
            $json = $contents->json();
            return $json;
        });
    }

    public function used($sort = 'ASC', $lang = ''){
        $key = 'habbo_badge_used_class_sort_list_'.$sort.'_'.$lang;
        return Cache::remember($key, 20, function () use ($lang, $sort) {

            $url = 'badges/used/';
            if(!empty($sort)) {
                $url .= $sort . '/';
                if (!empty($lang)) {
                    $url .= $lang . '/';
                }
            }

            $contents = $this->client->get($this->mainUrl . $url);
            $json = $contents->json();

            foreach($json as $key => $badge){
                $json[$key]['class'] = '';
                if((float)$badge['used'] < 5.0){
                    $json[$key]['class'] = 'rare';
                }else if((float)$badge['used'] < 35.0){
                    $json[$key]['class'] = 'normal';
                }else{
                    $json[$key]['class'] = '-';
                }
            }

            return $json;
        });
    }

    public function allNot($lang, $search = ''){
        $key = 'habbo_badges_all_not_'.$lang.'_'.md5($search);
        return Cache::remember($key, 20, function () use ($lang, $search) {

            $url = 'badges/not/'.$lang;
            if(!empty($search)){
                $url .= '/?s='.$search;
            }

            $contents = $this->client->get($this->mainUrl . $url);
            $json = $contents->json();
            return $json;
        });
    }

    public function search($lang = 'all', $sting){
        if($lang == ''){
            $lang = 'all';
        }

        $key = 'habbo_badges_all_'.md5($sting).'_'.$lang.'_4';
        return Cache::remember($key, 20, function () use ($sting, $lang) {
            $url = 'badges/';
            if(!empty($lang)){
                $url .= $lang.'/';
            }

            $contents = $this->client->get($this->mainUrl . $url.'?s='.rawurlencode($sting));
            $json = $contents->json();
            return $json;
        });
    }

    public function langs(){
        $key = 'habbo_badges_langs';
        return Cache::remember($key, 10, function () {
            $contents = $this->client->get($this->mainUrl . 'badges/langs');
            $json = $contents->json();
            return $json;
        });
    }


}