<?php namespace HabboIndex\Helpers;

use Storage;
use Carbon\Carbon;
use Cache;

class HabboApi
{

    protected $mainUrl = 'http://api.habbos.redbeed.de/api/';
    protected $client;

    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client();
    }

    public function images($count = 20)
    {
        $return = Cache::remember('images_last_' . $count, 30, function () use ($count) {
            try {
                $contents = $this->client->get($this->mainUrl . 'images/' . $count);
                $json = $contents->json();
                if (count($json)) {
                    return $json;
                } else {
                    return array();
                }
            } catch (Illuminate\Filesystem\FileNotFoundException $exception) {
                die("The file doesn't exist");
            }
        });

        return $return;
    }

    public function imageAdd($jsonData)
    {

        try {
            $contents = $this->client->put($this->mainUrl . 'images/add', ['json' => ($jsonData)]);
            $json = $contents->json();
            if (count($json)) {
                return $json;
            } else {
                return array();
            }
        } catch (Illuminate\Filesystem\FileNotFoundException $exception) {
            die("The file doesn't exist");
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            echo $e->getRequest();
            if ($e->hasResponse()) {
                echo $e->getResponse();
            }
            die();
        }

        return $return;
    }

    public function imagesSearch($count = 20, $search)
    {
        $return = Cache::remember('images_last_' . $count . '_' . md5($search), 30, function () use ($count, $search) {
            try {
                $contents = $this->client->get($this->mainUrl . 'images/search/' . $search . '/' . $count);
                $json = $contents->json();
                if (count($json)) {
                    return $json;
                } else {
                    return array();
                }
            } catch (Illuminate\Filesystem\FileNotFoundException $exception) {
                die("The file doesn't exist");
            }
        });

        return $return;
    }

    public function furnisSinceId($id)
    {
        try {
            $contents = $this->client->get($this->mainUrl . 'furni/since/' . $id);
            $json = $contents->json();
            if (count($json)) {

                if (isset($json['count'])) {
                    $retrun = [
                        'count' => $json['count'],
                    ];
                    $furnis = array();
                    $last = array();

                    foreach ($json['furnis'] as $furni) {
                        $newData = $furni;

                        $newData['dt'] = Carbon::parse($furni['dt']);

                        if (!isset($last['dt']) || $last['dt'] < $newData['dt']) {
                            $last = $newData;
                        }

                        $furnis[] = $newData;

                    }

                    $retrun['furnis'] = $furnis;
                    $retrun['last'] = $last;
                    return $retrun;
                } else {
                    return array();
                }
            } else {
                return array();
            }
        } catch (Illuminate\Filesystem\FileNotFoundException $exception) {
            die("The file doesn't exist");
        }
    }


    public function furni($count = 50)
    {
        $return = Cache::remember('furni_last_' . $count, 300, function () use ($count) {
            try {
                $contents = $this->client->get($this->mainUrl . 'furni/last/' . $count);
                $json = $contents->json();
                if (count($json)) {
                    $furnis = array();

                    foreach ($json as $furnis) {
                        foreach ($furnis as $key => $furni) {
                            $newData = $furni;
                            $newData['new'] = false;

                            $diff = Carbon::now()->diff(Carbon::parse($furni['dt']));
                            if ($diff->y == 0
                                && $diff->m == 0
                                && $diff->d == 0
                                && $diff->h < 12
                            ) {
                                $newData['new'] = true;
                            }

                            $imagePath = current($furni['direction']); //direction

                            if (!$imagePath && $furni['image']) {
                                $imagePath = $furni['image'];
                            }

                            if ($imagePath) {
                                list($width, $height) = getimagesize($imagePath);

                                $newData['size'] = array(
                                    'width' => $width,
                                    'height' => $height
                                );

                                $furnis[$key] = $newData;
                            } else {
                                unset($furnis);
                            }
                        }
                    }
                    return $furnis;
                } else {
                    return array();
                }
            } catch (Illuminate\Filesystem\FileNotFoundException $exception) {
                die("The file doesn't exist");
            }
        });

        return $return;
    }

    public function furniSearch($string)
    {
        try {
            $contents = $this->client->get($this->mainUrl . 'furni/search/' . $string);
            $json = $contents->json();
            if (count($json)) {
                $furnis = array();

                foreach ($json as $furnis) {
                    foreach ($furnis as $key => $furni) {
                        $newData = $furni;
                        $newData['new'] = false;

                        $diff = Carbon::now()->diff(Carbon::createFromTimestamp($furni['dt']['sec']));
                        if ($diff->y == 0
                            && $diff->m == 0
                            && $diff->d == 0
                            && $diff->h < 12
                        ) {
                            $newData['new'] = true;
                        }

                        list($width, $height) = getimagesize($furni['image']);
                        $newData['size'] = array(
                            'width' => $width,
                            'height' => $height
                        );

                        $furnis[$key] = $newData;
                    }
                }
                return $furnis;
            } else {
                return array();
            }
        } catch (Illuminate\Filesystem\FileNotFoundException $exception) {
            die("The file doesn't exist");
        }
    }

    public function count($type)
    {

        try {
            $contents = $this->client->get($this->mainUrl . $type);
            $json = $contents->json();
            if (isset($json[$type])) {
                return $json[$type];
            } else {
                return 0;
            }
        } catch (Illuminate\Filesystem\FileNotFoundException $exception) {
            die("The file doesn't exist");
        }
    }

    public function habboMotto ($tld, $habboname){
        try {
            $contents = $this->client->get($this->mainUrl . 'habbo/motto/' . $tld . '/' . $habboname);
            $json = $contents->json();
            return $json;
        }catch (\Exception $e){
            return array('success' => false);
        }
    }

    public function bestlist($type, $limit = 0, $offset = 0, $lang = 'de')
    {
        return Cache::remember('bestlist_'.$type.'_limit_'.$limit.'_offset_'.$offset.'_lang_'.$lang, 20, function () use ($type, $limit, $offset, $lang) {
            $contents = $this->client->get($this->mainUrl . 'habbos/list/' . $type . '/desc/' . $offset . '/' . $limit. '/'.$lang);
            return $contents->json();
        });
    }

    public function badge($code)
    {
        $contents = $this->client->get($this->mainUrl . 'badge/' . $code);
        $json = $contents->json();
        return $json;
    }

    public function badgeLast($size)
    {
        $return = Cache::remember('badge_last_' . $size, 60, function () use ($size) {
            $contents = $this->client->get($this->mainUrl . 'badges/last/' . $size);
            $json = $contents->json();
            return $json;
        });
        return $return;
    }

    public function badgeWeek()
    {
        $return = Cache::remember('badge_last_week'.date('d-m-y H:i') , 60, function () {
            $contents = $this->client->get($this->mainUrl . 'badges/week');
            $json = $contents->json();
            return $json;
        });
        return $return;
    }

    public function badgeOwn($code)
    {
        $contents = $this->client->get($this->mainUrl . 'badge/' . $code . '/own');
        $json = $contents->json();
        return $json;
    }

    public function badges($search = null)
    {
        if ($search == null || empty($search)) {
            $contents = $this->client->get($this->mainUrl . 'badges/list');
        } else {
            $contents = $this->client->get($this->mainUrl . 'badges/search/' . $search);
        }
        $json = $contents->json();

        return $json;
    }

    public function habbo($name = '', $tld = 'de'){#
        $key = 'habbo_' . $name.'_tld_'.$tld;
        $return = Cache::remember($key, 60, function () use ($name, $tld) {
            $contents = $this->client->get($this->mainUrl . 'habbo/' . rawurldecode($name));
            $json = $contents->json();
            return $json;
        });

        if(isset($return['wait'])){

            Cache::forget($key);
            return array();
        }else {
            return $return;
        }
    }


}