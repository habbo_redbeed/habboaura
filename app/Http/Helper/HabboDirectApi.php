<?php namespace HabboIndex\Helpers;

use Storage;
use Carbon\Carbon;
use Cache;
use HabboIndex\Helpers\HabboApi;

class HabboDirectApi extends HabboApi
{

    public function habbo($name = '', $tld = 'de'){
        $key = 'habbo_' . $name.'_tld_'.$tld.'_direct_api_v2';
        $name = urldecode($name);
        return Cache::remember($key, 20, function () use ($name, $tld) {
            //var_dump($this->mainUrl . 'direct/habbo/' . urlencode($name).'/'.$tld); die;
            $contents = $this->client->get($this->mainUrl . 'direct/habbo/'.$tld.'/'. urlencode($name));
            $json = $contents->json();
            return $json;
        });
    }


}