<?php namespace Fully\Http\Controllers;

use Fully\Http\Controllers\Controller;
use Redirect;
use View;
use Input;
use Config;
use LaravelLocalization;
use Fully\Models\Staff;
use Request;
use Validator;
use Notification;
use Carbon;
use Cache;

class StaffController extends Controller
{

    public function index($langCode = "")
    {
        $staffs = null;
        $langs = Staff::groupBy('habbo_lang')
            ->where('habbo_lang', '!=', '');
        if (!empty($langCode)) {
            $langs->where('habbo_lang', '=', $langCode);
        }
        $langs = $langs->lists('habbo_lang');

        $staffs = Cache::remember('staff_list_' . $langCode, 30, function () use ($langs) {
            $staffs = array();
            foreach ($langs as $lang) {

                $staffs[$lang] = array(
                    'now' => array(),
                    'old' => array(),
                );

                $jobs = Staff::groupBy('job_title')
                    ->where('habbo_lang', '!=', '')
                    ->where('habbo_lang', '=', $lang)
                    ->lists('job_title');


                foreach ($jobs as $job) {
                    $staffs[$lang]['now'][$job] = Staff::where('habbo_lang', '=', $lang)
                        ->where('job_title', 'LIKE', $job)
                        ->where(function ($query) {
                            $query->where('habbo_name', '!=', '')
                                ->orWhere(function ($query) {
                                    $query->where('real_name', '!=', '');
                                });
                        })
                        ->where('job_to', '=', '0000-00-00')
                        ->orderBy('job_to', 'ASC')
                        ->orderBy('job_title', 'ASC')
                        ->orderBy('habbo_name', 'ASC')
                        ->get();

                    if (count($staffs[$lang]['now'][$job]) <= 0) {
                        unset($staffs[$lang]['now'][$job]);
                    }

                    $staffs[$lang]['old'][$job] = Staff::where('habbo_lang', '=', $lang)
                        ->where('job_title', 'LIKE', $job)
                        ->where(function ($query) {
                            $query->where('habbo_name', '!=', '')
                                ->orWhere(function ($query) {
                                    $query->where('real_name', '!=', '');
                                });
                        })
                        ->where('job_to', '!=', '0000-00-00')
                        ->orderBy('job_to', 'ASC')
                        ->orderBy('job_title', 'ASC')
                        ->orderBy('habbo_name', 'ASC')
                        ->get();

                    if (count($staffs[$lang]['old'][$job]) <= 0) {
                        unset($staffs[$lang]['old'][$job]);
                    }
                }
            }
            return $staffs;
        });

        if (!empty($langCode)) {
            $langs = Staff::groupBy('habbo_lang')
                ->where('habbo_lang', '!=', '')
                ->lists('habbo_lang');
        }

        return view('frontend.lexicon.staff.index', compact('staffs', 'langs', 'langCode'));
    }

    public function search($slug, $langCode = '')
    {

        $staffs = null;
        $langs = Staff::groupBy('habbo_lang')
            ->where('habbo_lang', '!=', '');

        if (!empty($lang)) {
            $langs->where('habbo_lang', '=', $langCode);
        }

        $langs = $langs->lists('habbo_lang');

        $staffs = array();
        foreach ($langs as $lang) {
            $staffs[$lang] = array();
            $staffs[$lang]['now'] = Staff::where('habbo_lang', '=', $lang)
                ->where(function ($query) {
                    $query->where('habbo_name', '!=', '')
                        ->orWhere(function ($query) {
                            $query->where('real_name', '!=', '');
                        });
                })
                ->where('job_to', '=', '0000-00-00')
                ->orderBy('job_to', 'ASC')
                ->orderBy('job_title', 'ASC')
                ->orderBy('habbo_name', 'ASC')
                ->get();

            $staffs[$lang]['old'] = Staff::where('habbo_lang', '=', $lang)
                ->where(function ($query) {
                    $query->where('habbo_name', '!=', '')
                        ->orWhere(function ($query) {
                            $query->where('real_name', '!=', '');
                        });
                })
                ->where('job_to', '!=', '0000-00-00')
                ->orderBy('job_to', 'ASC')
                ->orderBy('job_title', 'ASC')
                ->orderBy('habbo_name', 'ASC')
                ->get();
        }

        $list = view('frontend.lexicon.staff.country', compact('staffs'));
        return response()->json(['html' => (string)$list]);
    }

}