<?php namespace Fully\Http\Controllers;

use LaravelLocalization;
use HabboIndex\Helpers\HabboApi as HabboApi;
use Tracker;

/**
 * Class FaqController
 * @author Sefa Karagöz
 */
class ImagesController extends Controller {
    
    /**
     * Display page
     * @param $id
     * @return \Illuminate\View\View
     */
    public function show() {
        $habbo = new HabboApi();
        $images = $habbo->images(40);
        return view('frontend.images.show', compact('images'));
    }

    public function search($slug = ''){
        $habbo = new HabboApi();
        $images = array();
        if(!empty($slug)) {
            $images = $habbo->imagesSearch(40, $slug);
        }else{
            $images = $habbo->images(40);
        }
        $view = view('frontend.images.list', compact('images'));
        return response()->json(['html' => (string)$view]);
    }
}
