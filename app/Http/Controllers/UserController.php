<?php namespace Fully\Http\Controllers;

use Fully\Models\CommentsTexts;
use Fully\Models\LikesUsers;
use Fully\Models\UserData;
use HabboIndex\Helpers\HabboApi;
use Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use View;
use Sentry;
use Redirect;
use Fully\Models\User;
use Session;
use Lang;
use Request;
use Fully\Models\Comments;
use Fully\Models\Likes;
use Fully\Models\ProfilImages;
use Validator;
use Tracker;
use Route;
use App;
use Carbon;


/**
 * Class ArticleController
 * @author Sefa Karagöz
 */
class UserController extends Controller
{

    private $user = null;

    /**
     * Display the login page
     * @return View
     */
    public function login()
    {
        if (!Sentry::check() && $this->user == null) {
            return $this->render('user/auth/login');
        } else {
            return Redirect::route('dashboard');
        }
    }

    /**
     * Logout action
     * @return Redirect
     */
    public function logout() {

        $user = Sentry::getUser();

        Sentry::logout();
        return Redirect::route('dashboard');
    }

    public function join()
    {
//        if (!Sentry::check() && $this->user == null) {

        $code = Session::get('habbo_code_06_2');
        if (!$code) {
            $code = md5(time());
            $code = substr($code, 1, 2).' '.substr($code, 4, 2);
            $code = 'HA_' . $code;
            Session::put('habbo_code_06_2', $code);
        }
        $hash = $code;

        $hotels = [
            'de' => Lang::get('hotel.de.link'),
            'com' => Lang::get('hotel.com.link'),
            'tr' => Lang::get('hotel.tr.link')
        ];

        return $this->render('user/auth/join', compact('hash', 'hotels'));
//        } else {
//            return Redirect::route('dashboard');
//        }
    }

    public function forget()
    {

        $code = Session::get('habbo_code_06_2');
        if (!$code) {
            $code = md5(time());
            $code = substr($code, 1, 2).' '.substr($code, 4, 2);
            $code = 'HA_' . $code;
            Session::put('habbo_code_06_2', $code);
        }
        $hash = $code;

        $hotels = [
            'de' => Lang::get('hotel.de.link'),
            'com' => Lang::get('hotel.com.link'),
            'tr' => Lang::get('hotel.tr.link')
        ];

        return $this->render('frontend/user/forgetPassword', compact('hash', 'hotels'));
    }

    public function forgetPost()
    {
        $validator = Validator::make(Request::all(), [
            'habbo_name' => 'required|max:255',
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return Redirect::route('user.auth.forget.password')
                ->withErrors($validator)
                ->withInput();
        } else {

            $habbo = User::where('habbo_name', 'LIKE', Input::get('habbo'))
                ->where('email', Input::get('email'))
                ->first();

            if($habbo){

                $habboApi = new HabboApi();
                $mottoObject = $habboApi->habboMotto($habbo->habbo_lang, $habbo->habbo_name);

                if ($mottoObject['success'] == false) {
                    return Redirect::route('user.auth.forget.password')
                        ->withErrors([
                            'habbo_name' => lang::get('user.join.error.motto.notFound')
                        ])
                        ->withInput();
                }

                $code = Session::get('habbo_code_06_2');

                if ($mottoObject['motto'] != $code) {
                    return Redirect::route('user.auth.forget.password')
                        ->withErrors([
                            'habbo_name' => lang::get('user.join.error.motto.incorrect')
                        ])
                        ->withInput();
                }

                $passwort = md5($code.'-'.$habbo->habbo_name.'-habboaura-2014');
                $passwort = md5($passwort);
                $passwort = substr($passwort, 1, 10);

                $habbo->password = $passwort;
                $habbo->save();

                return $this->render('frontend/user/forgetPasswordNew', compact('passwort'));


            }else{
                return Redirect::route('user.auth.forget.password')
                    ->withErrors([
                        'habbo_name' => lang::get('user.forget.error.notfound')
                    ])
                    ->withInput();
            }

        }
    }

    public function joinPost()
    {
        if (Request::isMethod('post')) {
            $validator = Validator::make(Request::all(), [
                'habbo_name' => 'required|max:255',
                'habbo_lang' => 'required|max:8',
                'email' => 'required|unique:users,email|email',
                'password' => 'required|min:7|confirmed',
                'password_confirmation' => 'required|min:7'
            ]);

            if ($validator->fails()) {
                return Redirect::route('user.auth.join')
                    ->withErrors($validator)
                    ->withInput();
            } else {

                $habbo = User::where('habbo_name', '=', Request::input('habbo_name'))
                    ->where('habbo_lang', '=', Request::input('habbo_lang'))
                    ->first();

                if($habbo){
                    return Redirect::route('user.auth.join')
                        ->withErrors([
                            'habbo_name' => lang::get('user.join.error.habbo.unique')
                        ])
                        ->withInput();
                }

                $habboApi = new HabboApi();
                $mottoObject = $habboApi->habboMotto(Request::input('habbo_lang'), Request::input('habbo_name'));

                if ($mottoObject['success'] == false) {
                    return Redirect::route('user.auth.join')
                        ->withErrors([
                            'habbo_name' => lang::get('user.join.error.motto.notFound')
                        ])
                        ->withInput();
                }

                $code = Session::get('habbo_code_06_2');

                if ($mottoObject['motto'] != $code) {
                    return Redirect::route('user.auth.join')
                        ->withErrors([
                            'habbo_name' => lang::get('user.join.error.motto.incorrect')
                        ])
                        ->withInput();
                }

                $user = Sentry::createUser(array(
                    'habbo_lang' => Request::input('habbo_lang'),
                    'habbo_name' => Request::input('habbo_name'),
                    'email' => Request::input('email'),
                    'password' => Request::input('password'),
                    'activated' => 1,
                ));

                Session::put('habbo_code_06_2', false);


                return Redirect::route('dashboard')
                    ->with('message', Lang::get('user.join.correct'));

            }
        }
    }

    /**
     * Login action
     * @return Redirect
     */
    public function loginPost()
    {

        $credentials = array(
            'email' => Input::get('email'),
            'password' => Input::get('password')
        );
        $rememberMe = Input::get('rememberMe');

        try {

            if (!empty($rememberMe)) {
                $this->user = Sentry::authenticate($credentials, true);
            } else {
                $this->user = Sentry::authenticate($credentials, false);
            }

            if ($this->user) {

                $this->events->fire('user.login', $this->user);
                return Redirect::route('dashboard');
            }
        } catch (\Exception $e) {
            return Redirect::route('dashboard')->withErrors(array('login' => $e->getMessage()));
        }
    }

    public function showRedirect($userName){

        $user = User::where('habbo_name', '=', (string)$userName)
            ->orderBy('created_at')
            ->first();

        return redirect(route('user.home.lang', [
            'slug' => $user->habbo_name,
            'lang' => $user->habbo_lang,
        ]), 301);
    }

    public function show($userSlug, $lang = '')
    {
        $user = null;
        if ($lang == '') {
            if (is_numeric($userSlug)) {
                $user = User::find((int)$userSlug);
            } else {
                $user = User::where('habbo_name', '=', $userSlug)
                    ->orderBy('created_at', 'ASC')
                    ->first();
            }
        } else {
            if (is_numeric($userSlug)) {
                $user = User::find(array(
                    'id' => (int)$userSlug,
                    'habbo_lang' => (string)$lang
                ));
            } else {
                $user = User::where('habbo_name', '=', $userSlug)
                    ->where('habbo_lang', '=', (string)$lang)
                    ->orderBy('created_at', 'ASC')
                    ->first();
            }
        }

        $token = $user->id;
        $type = 'user';

        $comments = Comments::firstOrCreate(array(
            'token' => $token,
            'type' => $type
        ));

        $comments = $comments->comments;

        $likes = Likes::firstOrCreate(array(
            'token' => $token,
            'type' => $type
        ));

        $likes = $likes->likes;

        //// STATTIC

        $static ['HabboAura']['comments'] = array(
            'value' => CommentsTexts::where('user_id', '=', $user->id)->count(),
            'icon' => 'min_comments.png',
            'lang' => Lang::get('user.profil.static.comments')
        );
        $static ['HabboAura']['given_likes'] = array(
            'value' => LikesUsers::where('user_id', '=', $user->id)->count(),
            'icon' => 'min_likes.gif',
            'lang' => Lang::get('user.profil.static.likes')
        );
        $static ['HabboAura']['views'] = array(
            'value' => $this->views($userSlug),
            'icon' => 'min_view.gif',
            'lang' => Lang::get('user.profil.static.views')
        );

        $static ['HabboAura']['stars'] = array(
            'value' => $user->starsMonthSum().' '.Lang::choice('user.stars', $user->starsMonthSum()) ,
            'icon' => '../../uploads/news/aura_stars.png',
            'lang' => Lang::get('user.star.month')
        );

        $static ['HabboAura']['starssum'] = array(
            'value' => $user->starsSum().' '.Lang::choice('user.stars', $user->starsSum()),
            'icon' => 'star_blue.png',
            'lang' => Lang::get('user.star.sum')
        );


        $habboApi = new HabboApi();
        $api = $habboApi->habbo($user->habbo_name);
        if($api && count($api)){

            if(isset($api['badges']) && count($api['badges'])) {
                $static ['Habbo']['badge'] = array(
                    'value' => count($api['badges']),
                    'icon' => 'min_badge.png',
                    'lang' => Lang::get('user.profil.static.badge')
                );
            }

            if(isset($api['rooms']) && count($api['rooms'])) {
                $static ['Habbo']['room'] = array(
                    'value' => count($api['rooms']),
                    'icon' => 'min_rooms.gif',
                    'lang' => Lang::get('user.profil.static.rooms')
                );
            }
        }

        return view('frontend/user/show', compact('user', 'token', 'type', 'comments', 'likes', 'static'));
    }

    private function views($slug)
    {
        $count = Tracker::logByRouteName( Route::currentRouteName() )
            ->where(function ($query) use ($slug) {
                $query
                    ->where('parameter', 'slug')
                    ->where('value', $slug);
            })
            ->groupBy('tracker_log.session_id')
            ->get();
        return count($count);
    }


    public function editPassword() {

        if (Sentry::check()) {
            return view('frontend/user/edit/password');
        } else {
            return Redirect::route('dashboard');
        }

    }

    public function savePassword() {

        if (Request::isMethod('post') && SEntry::check()) {
            $validator = Validator::make(Request::all(), [
                'password' => 'required|min:7|confirmed',
                'password_confirmation' => 'required|min:7'
            ]);

            if ($validator->fails()) {
                return Redirect::route('user.edit.password')
                    ->withErrors($validator)
                    ->withInput();
            } else {

                $user = Sentry::getUser();

                $user->password = Request::input('password');

                $user->save();

                return Redirect::route('user.edit.password')
                    ->with('message', Lang::get('user.edit.password.check'))
                    ->withInput();

            }
        }

    }

    public function saveProfil() {
        if (Request::isMethod('post')) {
            if(SEntry::check()) {
                $validator = Validator::make(Request::all(), [
                    'motto' => 'max:70',
                    'free_text' => 'max:500'
                ]);

                $user = Sentry::getUser();

                if ($validator->fails()) {
                    return Redirect::route('user.home.lang', array($user->habbo_name, $user->habbo_lang))
                        ->withErrors($validator)
                        ->withInput();
                } else {
                    $data = UserData::where('user_id', '=', $user->id)->first();
                    $data->motto = Request::input('motto');
                    $data->free_text = Request::input('free_text');

                    $data->save();
                    $user->save();

                    return Redirect::route('user.home', array($user->habbo_name, $user->habbo_lang))
                        ->with('update', Lang::get('user.edit.profil.check'))
                        ->withInput();

                }
            }
        }

    }

    public function saveImage() {
        if (Request::isMethod('post')) {
            if(SEntry::check()) {
                $validator = Validator::make(Request::all(), [
                    'backgroundImage' => 'max:20',
                ]);

                $user = Sentry::getUser();

                if ($validator->fails()) {
                    return Redirect::route('user.edit.image')
                        ->withErrors($validator)
                        ->withInput();
                } else {
                    $data = UserData::where('user_id', '=', $user->id)->first();

                    $imgs = ProfilImages::where('visible_to', '>=', date('Y-m-d H:i:s'))
                        ->orWhere('hidden', '=', 0)
                        ->get();

                    $images = $data->backgroundImages();
                    foreach($imgs as $img){
                        if($img->visible_to && $img->visible_to != null && $img->visible_to != '') {
                            $images[$img->name] = Lang::get($img->lang) . ' (' . Lang::get('user.image.visible') . ' ' . Carbon\Carbon::parse($img->visible_to)->format('d.m.Y') . ')';
                        }else{
                            $images[$img->name] = Lang::get($img->lang);
                        }
                    }

                    if(isset($images[Request::input('backgroundImage')])) {

                        $data->profil_background = Request::input('backgroundImage');
                        $data->save();


                        $listener = App::make('Fully\Http\Controllers\ImageController')->profil($user->id, '', '', true);

                        return Redirect::route('user.edit.image')
                            ->with('message', Lang::get('user.edit.image.check'))
                            ->withInput();
                    }

                }
            }
        }

        return Redirect::route('user.edit.image')
            ->withInput();

    }

    public function editImage()
    {
        if (Sentry::check()) {

            $userId = Sentry::getUser()->id;
            $userData = User::find($userId)->data;

            $images = $userData->backgroundImages();
            foreach($images as $key => $lang){
                $images[$key] = Lang::get($lang);
            }

            $imgs = ProfilImages::where('visible_to', '>=', date('Y-m-d H:i:s'))
                ->orWhere('hidden', '=', 0)
                ->orderby('hidden')
                ->orderby('lang', 'DESC')
                ->orderby('visible_to')
                ->get();

            foreach($imgs as $img){
                if($img->visible_to && $img->visible_to != null && $img->visible_to != '') {
                    $images[$img->name] = Lang::get($img->lang) . ' (' . Lang::get('user.image.visible') . ' ' . Carbon\Carbon::parse($img->visible_to)->format('d.m.Y') . ')';
                }else{
                    $images[$img->name] = Lang::get($img->lang);
                }
            }

            return view('frontend/user/edit/image', compact('images', 'userData'));
        } else {
            return Redirect::route('dashboard');
        }
    }

    public function editHabbo()
    {
        if (Sentry::check()) {

            $code = Session::get('habbo_code');
            if (!$code) {
                $code = md5(time());
                $code = substr($code, 1, 4);
                $code = 'HA_' . $code;
                Session::put('habbo_code', $code);
            }
            $hash = $code;

            $hotels = [
                'de' => Lang::get('hotel.de.link'),
                'com' => Lang::get('hotel.com.link')
            ];

            return view('frontend/user/edit/habbo', compact('hash', 'hotels'));
        } else {
            return Redirect::route('dashboard');
        }
    }

    public function saveHabbo()
    {
        if (Request::isMethod('post')) {
            $validator = Validator::make(Request::all(), [
                'habbo_name' => 'required|max:255',
                'habbo_lang' => 'required|max:8',
            ]);

            if ($validator->fails()) {
                return Redirect::route('user.edit.habbo')
                    ->withErrors($validator)
                    ->withInput();
            } else {

                $habbo = User::where('habbo_name', '=', Request::input('habbo_name'))
                    ->where('habbo_lang', '=', Request::input('habbo_lang'))
                    ->first();

                if($habbo){
                    return Redirect::route('user.edit.habbo')
                        ->withErrors([
                            'habbo_name' => lang::get('user.join.error.habbo.unique')
                        ])
                        ->withInput();
                }

                $habboApi = new HabboApi();
                $mottoObject = $habboApi->habboMotto(Request::input('habbo_lang'), Request::input('habbo_name'));

                if ($mottoObject['success'] == false) {
                    return Redirect::route('user.edit.habbo')
                        ->withErrors([
                            'habbo_name' => lang::get('user.join.error.motto.notFound')
                        ])
                        ->withInput();
                }

                $code = Session::get('habbo_code');

                if ($mottoObject['motto'] != $code) {
                    return Redirect::route('user.edit.habbo')
                        ->withErrors([
                            'habbo_name' => lang::get('user.join.error.motto.incorrect')
                        ])
                        ->withInput();
                }

                $user = Sentry::getUser();

                $user->habbo_name = Request::input('habbo_name');
                $user->habbo_lang = Request::input('habbo_lang');

                $user->save();

                return Redirect::route('user.edit.habbo')
                    ->with('message', Lang::get('user.edit.habbo.check'))
                    ->withInput();

            }
        }

        return Redirect::route('user.edit.habbo')
            ->withInput();

    }


}