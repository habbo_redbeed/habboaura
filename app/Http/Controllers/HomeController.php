<?php namespace Fully\Http\Controllers;

use Fully\Repositories\Project\ProjectInterface;
use Fully\Repositories\Slider\SliderInterface;
use Fully\Repositories\Article\ArticleInterface;
use LaravelLocalization;
use HabboIndex\Helpers\HabboApi as HabboApi;
use Redirect;
use Fully\Models\Shortnews;
use Htmldom;
use Fully\Models\Article;
use Fully\Models\Category;
use Fully\Models\LexiconCategory;
use Fully\Models\LexiconEntry;
use Fully\Models\LexiconEntryText;
use Fully\Models\EventCalendar;
use Carbon;
use Fully\Models\User;
use DB;
use App;
use Twitter;
use Bitly;

/**
 * Class HomeController
 * @author Sefa Karagöz
 */
class HomeController extends Controller
{

    protected $slider;
    protected $project;
    protected $news;
    protected $api;

    public function redirect()
    {
        return Redirect::to(LaravelLocalization::getCurrentLocale(), 302);
    }

    public function __construct(SliderInterface $slider, ArticleInterface $news)
    {

        parent::__construct();
        $this->slider = $slider;
        $this->news = $news;
        $this->api = new HabboApi();
    }

    public function index()
    {

        $languages = LaravelLocalization::getSupportedLocales();
        $lang = LaravelLocalization::getCurrentLocale();

        $articleLang = config('article.lang.'.App::getLocale());


        $oldDate = Carbon\Carbon::now()->subWeek();
        $sliders = Article::where('is_topnews', '>=', 1)
            ->where('lang', '=', $articleLang)
            ->where('is_published', '=', 1)
            ->where('updated_at', '>=', $oldDate->format('Y-m-d H:i:s'))
            ->orderBy('is_topnews', 'DESC')
            ->orderBy('updated_at', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->take(8)
            ->get();

        $blogs = Article::where('is_published', '=', 1)
            ->where('lang', '=', $articleLang)
            ->orderBy('created_at', 'DESC')
            ->take(8)
            ->get();

        $moreBlogs = array();
        $moreBlogs/*[]*/ = Article::where('is_published', '=', 1)
            ->where('lang', '=', $articleLang)
            ->orderBy('created_at', 'DESC')
            ->offset(8)
            ->take(8)
            ->get();

        //$moreBlogs = array_merge($moreBlogs[0], $moreBlogs[1]);

        $shortnews = Shortnews::orderBy('pin', 'desc')
            ->orderBy('created_at', 'desc')
            ->where('category_id', '!=', 0)
            ->where(function ($query) use ($articleLang){
                $query->where('lang', '=', $articleLang)
                    ->orWhere('type', 'translate');
            })
            ->take(7)
            ->get();


        $furnis = $this->api->furni(40);
        if (count($furnis) == 1) {
            $furnis = current($furnis);
        }

        $badgesList = $this->api->badgeLast(40);
        $badges = array();
        if (isset($badgesList['badges'])) {
            $badges = $badgesList['badges'];
        }

        foreach ($badges as $key => $badge) {
            if (isset($badge['name'][$lang])) {
                $badges[$key]['lang'] = $lang;
            } else if (isset($badge['name']['com'])) {
                $badges[$key]['lang'] = 'com';
            } else {
                $keys = array_keys($badge['name']);
                $badges[$key]['lang'] = current($keys);
            }
        }

        //$randomHabbo = User::orderBy(DB::raw('RAND()'))->first();

        $nowDate = Carbon\Carbon::now();
        $eventCount = EventCalendar::where('event_start', '<=', $nowDate->format('Y-m-d H:i:s'))
            ->where('event_end', '>=', $nowDate->format('Y-m-d H:i:s'))
            ->count();

        $eventGroup = EventCalendar::where('event_start', '<=', $nowDate->format('Y-m-d H:i:s'))
            ->where('event_end', '>=', $nowDate->format('Y-m-d H:i:s'))
            ->orderBy('event_end')
            ->take('4')
            ->get();

        return view('frontend/layout/dashboard', compact(
            'sliders', 'languages', 'blogs', 'moreBlogs',
            'furnis', 'badges', 'shortnews', 'eventCount',
            'eventGroup'
        ));
    }

}
