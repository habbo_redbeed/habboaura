<?php namespace Fully\Http\Controllers;

use Fully\Http\Controllers\Controller;
use Redirect;
use View;
use Input;
use Config;
use LaravelLocalization;
use Fully\Models\LexiconCategory;
use Fully\Models\LexiconEntryText;
use Fully\Models\LexiconEntry;
use Fully\Models\Comments;
use Fully\Models\Likes;
use Request;
use Validator;
use Notification;
use Carbon;
use Cache;
use Sentry;



class LexiconController extends Controller
{

    public function index($slug = "")
    {

        $select = LexiconCategory::where('slug', '=', $slug)->first();
        $selectId = false;
        if($select){
            $selectId = $select->id;
        }

        $categories = LexiconCategory::where('status', '=', 1)->get();
        $lastChange = LexiconEntryText::where('status', '=', 'ENABLE')
            ->where('text', '!=', '')
            ->orderBy('created_at', 'DESC')
            ->take(20)
            ->get();

        return view('frontend.lexicon.categories.index', compact('categories', 'selectId', 'lastChange'));
    }

    public function show($slug){

        $entry = LexiconEntry::where('slug', '=', $slug)->first();
        if(!$entry && count($entry->entrys) == 0 || !$entry->texts->count()){
            return redirect(route('lexicon.index'));
        }

        $categories = LexiconCategory::where('status', '=', 1)->get();

        $token = $entry->id;
        $type = 'lexicon_entry';

        $comments = Comments::firstOrCreate(array(
            'token' => $token,
            'type' => $type
        ));

        $comments = $comments->comments;

        $likes = Likes::firstOrCreate(array(
            'token' => $token,
            'type' => $type
        ));

        $likes = $likes->likes;

        return view('frontend.lexicon.entry.view', compact(
            'entry',
            'categories',
            'comments',
            'likes',
            'token',
            'type'));

    }

    public function edit($slug = ""){

        if(!Sentry::getUser()){
            return redirect(route('lexicon.index'));
        }
        if(!empty($slug)) {
            $entry = LexiconEntry::where('slug', '=', $slug)->first();
        }else{
            $entry = new LexiconEntry;
        }

        if(!empty($slug) && !$entry && count($entry->entrys) == 0){
            return redirect(route('lexicon.index'));
        }

        $categories = LexiconCategory::where('status', '=', 1)->get();

        $categoriesArray = Cache::remember('lexicon_categories_array', 50, function () use ($categories) {
            $return = array();

            foreach($categories as $cat){
                $return[$cat->id] = $cat->name;
            }

            return $return;
        });

        $token = $entry->id;
        if($token == null){
            $token = time().'_created';
        }
        $type = 'lexicon_entry';

        $comments = Comments::firstOrCreate(array(
            'token' => $token,
            'type' => $type
        ));

        $comments = $comments->comments;

        $likes = Likes::firstOrCreate(array(
            'token' => $token,
            'type' => $type
        ));

        $likes = $likes->likes;

        return view('frontend.lexicon.entry.edit', compact(
            'entry',
            'categories',
            'comments',
            'likes',
            'token',
            'type',
            'categoriesArray',
            'slug'));

    }

    public function save($id = 0) {

        if(!Sentry::getUser()){
            return redirect(route('lexicon.index'));
        }

        if($id != 0) {
            $entry = LexiconEntry::find($id);
        }else{
            $entry = LexiconEntry::firstOrCreate(array(
                'title' => Request::input('title'),
                'categorie_id' => (int)Request::input('categorie_id')
            ));

            $entry->status = 2;
            $entry->save();
        }

        if($id != 0 && !$entry && count($entry->entrys) == 0){
            return redirect(route('lexicon.index'));
        }

        $entryText = new LexiconEntryText([
            'text' => Request::input('text'),
            'user_id' => Sentry::getUser()->id,
            'new_title' => Request::input('title'),
            'new_categorie_id' => (int)Request::input('categorie_id'),
            'entry_id' => $entry->id
        ]);

        $entry->texts()->save($entryText);

        return redirect(route('lexicon.entry', array('slug' => $entry->slug)));

    }

}