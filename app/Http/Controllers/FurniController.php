<?php namespace Fully\Http\Controllers;

use LaravelLocalization;
use HabboIndex\Helpers\HabboApi as HabboApi;
use Tracker;

/**
 * Class HomeController
 * @author Sefa Karagöz
 */
class FurniController extends Controller
{

    protected $furni;

    public function __construct()
    {
        parent::__construct();
        $this->furni = new HabboApi();
    }

    public function index()
    {

        //$visitor = Tracker::currentSession();
        //var_dump( $visitor->device->is_mobile );
        $languages = LaravelLocalization::getSupportedLocales();
        $furnis = $this->furni->furni(100);
        if (count($furnis) == 1) {
            $furnis = current($furnis);
        }

        return view('frontend/furni/index', compact('furnis'));
    }

    public function search($string)
    {

        $languages = LaravelLocalization::getSupportedLocales();
        $furnis = $this->furni->furniSearch($string);

        $overview = view('frontend/layout/dashboard/furni_overview', compact('furnis'));
        $list = view('frontend/layout/dashboard/furni', compact('furnis'));

        return response()->json(['html' => array(
            "list" => $list->render(),
            "overview" => $overview->render()
        )]);

    }
}
