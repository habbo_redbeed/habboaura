<?php namespace Fully\Http\Controllers;

use LaravelLocalization;
use HabboIndex\Helpers\HabboApi as HabboApi;
use Input;
use Validator;
use DB;
use Sentry;
use Session;

/**
 * Class HomeController
 * @author Sefa Karagöz
 */
class EventController extends Controller
{

    protected $furni;

    public function __construct()
    {
        parent::__construct();
        $this->furni = new HabboApi();
    }

    public function code()
    {
        return view('frontend/event/code');
    }

    public function codePost() {
        $validate = Validator::make(Input::all(), [
            'code' => 'required|max:100',
            'habbo_name' => 'required_with:habbo_lang|max:100|min:2',
            'habbo_lang' => 'required_with:habbo_name|max:5',
            'g-recaptcha-response' => 'required_with:habbo_name'.(Input::has('habbo_lang') ? '|recaptcha' : ''),
        ]);

        if($validate->fails()){
            return back()->withInput()->withErrors($validate);
        }

        $code = DB::select("SELECT code, id, count, stars FROM event_code WHERE code = ?", [Input::get('code')]);
        if(count($code) == 1 && head($code)->code == Input::get('code')){

            $code = head($code);
            $count = head(DB::select("SELECT count(*) as count FROM event_code_user WHERE code_id = ?", [$code->id]));
            if($count->count < $code->count){

                $sessionId = Session::getId();
                $userName = Input::get('habbo_name', '');
                $userLang = Input::get('habbo_lang', '');
                $userId = 0;
                if(Sentry::check()){
                    $userId = Sentry::getUser()->id;
                }

                $check = head(DB::select("
                  SELECT code_id
                  FROM event_code_user
                  WHERE ((user_id = ? AND user_id != 0)
                    OR (session = ?)
                    OR (habbo_name = ? AND habbo_name != '' AND habbo_lang = ? AND habbo_lang != '')) AND code_id = ?", [$userId, $sessionId, $userName, $userLang, $code->id]));

                if(!$check || $check->code_id != $code->id){

                    DB::insert("
                    INSERT INTO event_code_user
                    (code_id, user_id, habbo_name, habbo_lang, session)
                    VALUES (?, ?, ?, ?, ?)
                    ",  [$code->id, $userId, $userName, $userLang, $sessionId]);

                    DB::insert("
                    INSERT INTO user_stars
                    (user_id, any_habbo, any_lang, stars, note, created_at)
                    VALUES (?, ?, ?, ?, ?, NOW())
                    ",  [$userId, $userName, $userLang, $code->stars, 'Übergeben per Code (ID: '.$code->id.')']);

                    return back()->with('message', trans('event.code.form.correct', ['stars' => $code->stars]));
                }else{
                    return back()->withErrors([
                        'error' => trans('event.code.form.error.again')
                    ]);
                }
            }else{
                return back()->withErrors([
                    'error' => trans('event.code.form.error.count')
                ]);
            }
        }else{
            return back()->withErrors([
                'error' => trans('event.code.form.error.wrong')
            ]);
        }
    }

}
