<?php namespace Fully\Http\Controllers;

use Fully\Models\EventPixelUser;
use Fully\Repositories\Page\PageInterface;
use Fully\Repositories\Page\PageRepository as Page;
use Carbon;
use Fully\Models\User;
use Fully\Models\EventCalendar;
use Fully\Models\EventPixel;
use Fully\Models\Page as PageModel;
use DB;
use Sentry;
use Response;
use Validator;
use Request;
use Cache;
use Input;

/**
 * Class PageController
 * @author Sefa Karagöz
 */
class PageController extends Controller {

    protected $page;

    public function __construct(PageInterface $page) {

        parent::__construct();
        $this->page = $page;
    }

    /**
     * Display page
     * @param $slug
     * @return \Illuminate\View\View
     */
    public function show($slug) {

        $page = $this->page->getBySlug($slug, true);

        if($page === null)
            return Response::view('errors.missing', array(), 404);


        $oldDate = Carbon\Carbon::now();
        $add = array();

        if($page->view_blade == 'stars') {
            $stars = [];
            $stars = DB::select(DB::raw("
              SELECT sum(user_stars.stars) as stars, habbo_name, habbo_lang, users.id as id, any_habbo, any_lang  FROM `user_stars`
              LEFT JOIN users
                ON (user_stars.user_id = users.id OR (user_stars.any_habbo = users.habbo_name AND user_stars.any_lang = users.habbo_lang))
                WHERE user_stars.`created_at` > '" . $oldDate->format('Y-m-01 00:00:00') . "' AND user_stars.`created_at` < '" . $oldDate->format('Y-m-' . $oldDate->daysInMonth . ' 23:59:59') . "'
                GROUP BY users.id, any_habbo, any_lang ORDER BY sum(user_stars.stars) DESC LIMIT 60"));

            $add['stars'] = $stars;

            $stars = DB::select(DB::raw("
              SELECT sum(user_stars.stars) as stars, habbo_name, habbo_lang, users.id as id, any_habbo, any_lang  FROM `user_stars`
              LEFT JOIN users
                ON (user_stars.user_id = users.id OR (user_stars.any_habbo = users.habbo_name AND user_stars.any_lang = users.habbo_lang))
                GROUP BY users.id, any_habbo, any_lang ORDER BY sum(user_stars.stars) DESC LIMIT 30"));

            $add['stars_sum'] = $stars;
        }else if($page->view_blade == 'pixel') {
            $add['events'] = EventPixel::where('event_start', '<=', $oldDate->format('Y-m-d H:i:s'))
                ->where('event_end', '>', $oldDate->format('Y-m-d H:i:s'))
                ->orderBy('event_end')
                ->get();
        }else if($page->view_blade == 'eventCalendar') {

            $add['lastEvents'] = Cache::remember('event_calendar_last_10', 1, function () use ($oldDate) {

                $events = EventCalendar::where('event_end', '>=', $oldDate->format('Y-m-d H:i:s'))
                    ->where('area', '!=', 0)
                    ->orderBy('event_start')
                    ->orderBy('event_end')
                    ->orderBy('area')
                    ->take('10')
                    ->get();

                $eventArray = [];

                foreach($events as $event){
                    $eventArray[$event->event_end->format('Ymd')][] = $event;
                }

                return $eventArray;

            });

            $add['calendar'] = Cache::remember('event_calendar_calendar', 1, function () use ($oldDate) {

                $returnArray = [];

                $firstDay  = $oldDate; //= new Carbon\Carbon('first day of '.$oldDate->format('F').' '.$oldDate->format('Y'));

                $returnArray['firstDay'] = $firstDay;
                $returnArray['dayOfWeek'] = $firstDay->dayOfWeek - 1;
                $returnArray['daysInMonth'] = $firstDay->daysInMonth;
                $returnArray['days'] = [];

                $lastDay = clone $firstDay;
                $lastDay = $lastDay->addDays($returnArray['dayOfWeek'] - 1);

                for($day = 0; $returnArray['dayOfWeek'] > $day; $day ++){
                    $dayObject = clone $firstDay;
                    $returnArray['days'][] = [
                        'day' => $dayObject->subDays($returnArray['dayOfWeek'] - $day),
                        'label' => 'label-default',
                        'events' => false
                    ];
                }

                for($day = 0; ($returnArray['daysInMonth'] + ($returnArray['dayOfWeek'])) > $day; $day ++){
                    $dayObject = clone $firstDay;
                    $dayObject = $dayObject->addDays($day);
                    $label = 'label-primary';

                    if($dayObject->format('d') < $oldDate->format('d') && $dayObject->format('m') <= $oldDate->format('m')){
                        $label = 'label-default';
                    }else if($dayObject->format('m') > $oldDate->format('m')) {
                        $label = 'label-info';
                    }else if($dayObject->format('d') == $oldDate->format('d')){
                        $label = 'label-warning';
                    }

                    $eventCount = false;
                    if($label != 'label-default' ){
                        $eventCount = EventCalendar::where(function ($query) use ($dayObject){
                                $query->where(function ($start) use ($dayObject){
                                   $start->where('event_start', '>=', $dayObject->format('Y-m-d').' 00:00:00')
                                       ->where('event_start', '<=', $dayObject->format('Y-m-d').' 23:59:59');
                                })
                                ->orWhere(function ($end) use ($dayObject){
                                    $end->where('event_end', '>=', $dayObject->format('Y-m-d').' 00:00:00')
                                        ->where('event_end', '<=', $dayObject->format('Y-m-d').' 23:59:59');
                                });
                            })
                            ->where('area', '!=', 0)
                            ->count();
                    }

                    $returnArray['days'][] = [
                        'day' => $dayObject,
                        'label' => $label,
                        'events' => $eventCount
                    ];
                }

                return $returnArray;

            });

            if(Input::has('date') && Input::get('date')) {
                $add['selectDay'] = Cache::remember('event_calendar_day_'.Input::get('date'), 1, function () use ($oldDate) {

                    return Carbon\Carbon::parse(Input::get('date'));

                });

                $add['selectEvents'] = Cache::remember('event_calendar_last_'.Input::get('date'), 1, function () use ($oldDate, $add) {

                    $events = EventCalendar::where(function ($query) use ($add){
                            $query->where(function ($start) use ($add){
                                $start->where('event_start', '>=', $add['selectDay']->format('Y-m-d').' 00:00:00')
                                    ->where('event_start', '<=', $add['selectDay']->format('Y-m-d').' 23:59:59');
                            })
                                ->orWhere(function ($end) use ($add){
                                    $end->where('event_end', '>=', $add['selectDay']->format('Y-m-d').' 00:00:00')
                                        ->where('event_end', '<=', $add['selectDay']->format('Y-m-d').' 23:59:59');
                                });
                        })
                        ->where('area', '!=', 0)
                        ->orderBy('event_start')
                        ->orderBy('event_end')
                        ->orderBy('area')
                        ->take('10')
                        ->get();

                    $eventArray = [];

                    foreach($events as $event){
                        $eventArray[$event->event_end->format('Ymd')][] = $event;
                    }

                    return $eventArray;

                });
            }

        }


        return view('frontend.page.'.$page->view_blade, compact('page', 'add'));
    }

    public function pixelForm($id) {
        if(Sentry::check()) {
            $oldDate = Carbon\Carbon::now();
            $event = EventPixel::where('event_start', '<=', $oldDate->format('Y-m-d H:i:s'))
                ->where('event_end', '>', $oldDate->format('Y-m-d H:i:s'))
                ->where('id', '=', (int)$id)
                ->orderBy('event_end')
                ->first();

            if ($event) {
                return view('frontend.page.pixelForm', compact('event'));
            }
        }

        return Response::view('errors.missing', array(), 404);
    }

    public function pixelFormSave($id) {
        if(Sentry::check()) {
            $oldDate = Carbon\Carbon::now();
            $event = EventPixel::where('event_start', '<=', $oldDate->format('Y-m-d H:i:s'))
                ->where('event_end', '>', $oldDate->format('Y-m-d H:i:s'))
                ->where('id', '=', (int)$id)
                ->orderBy('event_end')
                ->first();

            if ($event) {

                if(!$event->users->where('user_id', '=', Sentry::getUser()->id)->count()){

                    $validator = Validator::make(Request::all(), [
                        'image' => 'required|image',
                        'text' => 'max:400',
                    ]);

                    if ($validator->fails()) {
                        return Redirect::route('page.pixel.form')
                            ->withErrors($validator)
                            ->withInput();
                    } else {

                        $imageName = md5(time()) . '.' . Request::file('image')->getClientOriginalExtension();
                        $path = '/uploads/pixel/catalog/';
                        Request::file('image')->move(
                            public_path() . '/uploads/pixel/catalog/',
                            $imageName
                        );

                        $img = $path.$imageName;

                        $post = new EventPixelUser(array(
                            'user_id' => Sentry::getUser()->id,
                            'image' => $img,
                            'text' => Request::input('text'),
                            'event_id' => $event->id
                        ));

                        $event->users()->save($post);

                        $page = PageModel::find(8);
                        return redirect()->route('dashboard.page.show', array($page->slug));
                    }

                }
                return view('page.pixel.form', compact('event'));
            }
        }

        return Response::view('errors.missing', array(), 404);
    }
}
