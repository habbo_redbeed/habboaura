<?php namespace Fully\Http\Controllers;

use Fully\Models\Likes;
use Fully\Models\LikesUsers;
use Fully\Models\Comments;
use Fully\Models\CommentsTexts;
use Fully\Models\CommentTextNotice;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Sentry;
use Cache;
use Fully\Models\LexiconEntryText;
use Fully\Models\Article;
use Carbon;
use View;
use Fully\Models\User;
use Fully\Models\UserActive;
use URL;
use DB;
use Tracker;
use Lang;
use App;
use Cookie;
use Response;


abstract class Controller extends BaseController
{

    use DispatchesCommands, ValidatesRequests;

    public function __construct()
    {


        if(!Cookie::has('aura_lc')) {
            $end = 60 * 24;
            $end *= 30;
            $end *= 12;
            $end *= 10;
            Cookie::queue('aura_lc', md5(time()), $end);
        }

        $lastUsers = Carbon\Carbon::now()->subMinutes(5)->format('Y-m-d H:i:s');
        $online = DB::table('tracker_sessions')
            ->where('updated_at', '>=', $lastUsers)
            ->count();//Tracker::sessions(5)->count();
        View::share('onlineUser', $online);

        if (Sentry::check()) {
            $active = UserActive::firstOrCreate(array(
                'user_id' => Sentry::getUser()->id
            ));
            $active->last_active = date('Y-m-d H:i:s');
            $active->save();
        }

        $last = Carbon\Carbon::now()->subMinutes(15)->format('Y-m-d H:i:s');
        $activehabbos = UserActive::where('last_active', '>=', $last)
            ->orderBy('last_active')
            ->get();

        View::share('activehabbos', $activehabbos);

        $lexiconsLast = Cache::remember('footer_lexicon_cache_3_'.App::getLocale(), 40, function () {
            return LexiconEntryText::where('status', '=', 'ENABLE')
                ->where('entry_id', '!=', 0)
                ->orderBy('updated_at', 'DESC')
                ->take(3)
                ->get();
        });
        View::share('lexiconsLast', $lexiconsLast);


        $articleLast = Cache::remember('footer_article_cache_3_'.App::getLocale(), 10, function () {
            return Article::where('is_published', '=', 1)
                ->where('lang', '=', App::getLocale())
                ->orderBy('created_at', 'DESC')
                ->take(3)
                ->get();
        });
        View::share('articleLast', $articleLast);

        $userLast = Cache::remember('footer_user_cache_3_'.App::getLocale(), 20, function () {
            return User::where('activated', '=', 1)
                ->where('habbo_name', '!=', '')
                ->orderBy('created_at', 'DESC')
                ->take(3)
                ->get();
        });
        View::share('userLast', $userLast);

        $commentsLast = Cache::remember('footer_comment_cache_3', 10, function () {
            return CommentsTexts::where('status', '=', 1)
                ->where('comment_id', '!=', '')
                ->orderBy('created_at', 'DESC')
                ->take(3)
                ->get();
        });
        View::share('commentsLast', $commentsLast);

        View::share('messageCount', 0);
        if (Sentry::check()) {
            $user = User::find(Sentry::getUser()->id);
            View::share('messageCount', $user->newMessagesCount());
        }
    }

    public function render($tempName = '', $params = array())
    {

        if (Request::wantsJson() == true) {
            $view = view($tempName . 'Ajax', $params);
            return response()->json(['html' => (string)$view]);
        } else {
            return view($tempName, $params);
        }
    }

    public function comments($type, $token)
    {

        $commentBlock = Comments::firstOrCreate(array(
            'token' => $token,
            'type' => $type
        ));

        $comments = $commentBlock->comments;
        $return = view('frontend/commentsPopup', compact('comments', 'token', 'type'));

        if (Request::wantsJson() == true) {
            return response()->json(['html' => (string)$return]);
        } else {
            return $return;
        }

    }

    public function deleteComment($id)
    {
        if (Sentry::check()) {
            if (is_numeric($id)) {
                $comment = CommentsTexts::find($id);

                if ($comment) {
                    if ($comment->user->id == Sentry::getUser()->id || Sentry::getUser()->hasAnyAccess(['admin.comments.destroy'])) {
                        if ($comment->delete == 0) {
                            $comment->delete = 1;

                            $notice = 'user.comment.delete.notice';
                            if ($comment->user->id != Sentry::getUser()->id) {
                                $notice = 'user.comment.delete.mod';
                            }

                            $options = [];

                            $json = [
                                'text' => $notice,
                                'options' => $options,
                            ];

                            $commentTextNotice = new CommentTextNotice(
                                [
                                    'comment_text_id' => $comment->id,
                                    'creater_id' => Sentry::getUser()->id,
                                    'notice' => json_encode($json),
                                ]
                            );

                            $comment->user->addStar(-1, Sentry::getUser()->habbo_name . ' (' . Sentry::getUser()->id . ') delete Comment ' . $comment->id);
                            $commentTextNotice->save();
                            $comment->save();

                            return redirect()->back()->withInput()->with('message', Lang::get('user.comment.delete.check'));
                        }
                    }
                }
            }
        }
        return redirect()->back()->with('message', Lang::get('user.comment.delete.error'));
    }

    public function saveComment($type, $token)
    {
        $comment = Request::input('comment');
        if (is_string($comment) && $comment != '') {
            if (Sentry::check()) {

                if (Sentry::getUser() && Sentry::getUser()->id) {

                    $count = CommentsTexts::where('user_id', '=', Sentry::getUser()->id)
                        ->where('created_at', '>=', Carbon\Carbon::now()->format('Y-m-d 00:00:00'))
                        ->orderBy('created_at', 'DESC')
                        ->count();

                    $last = CommentsTexts::where('user_id', '=', Sentry::getUser()->id)
                        ->orderBy('created_at', 'DESC')
                        ->first();

                    $seconds = 20;
                    if ($count > 20) {
                        $seconds = 60 * 10; // 10 mins
                    } else if ($count > 10) {
                        $seconds = 60 * 5; // 5 Mins
                    } else if ($count > 5) {
                        $seconds = 60 * 2;
                    }

                    if (isset($last->created_at)) {
                        $date = Carbon\Carbon::parse($last->created_at)->diffInSeconds();
                    } else {
                        $date = false;
                    }

                    if ($date >= $seconds || $date === false) {
                        $comments = Comments::firstOrCreate(array(
                            'token' => $token,
                            'type' => $type
                        ));


                        $url = URL::previous();
                        $parsed = parse_url($url);
                        $comments->url = $parsed['path'];
                        $comments->save();

                        $comments->comments()->save(new CommentsTexts(
                            array(
                                "text" => htmlentities($comment),
                                "user_id" => Sentry::getUser()->id
                            )
                        ));

                        User::find(Sentry::getUser()->id)->addStar(1, $url);

                    } else {
                        if ($seconds <= 120) {
                            return redirect()->back()->withInput()->with('message', 'Kommentare sind nur alle ' . $seconds . ' Sekunden erlaubt!');
                        } else {
                            return redirect()->back()->withInput()->with('message', 'Kommentare sind nur alle ' . (int)($seconds / 60) . ' Minuten erlaubt!');
                        }
                    }
                }
            }
        }
        return redirect()->back()->withInput();
    }

    public function saveLike($type, $token, $emo = 'like')
    {

        $emoArray = [
            'like', 'sad', 'okay'
        ];

        $like = Likes::firstOrCreate(array(
            'token' => $token,
            'type' => $type
        ));

        $userid = 0;
        if(isset(Sentry::getUser()->id)){
            $userid = Sentry::getUser()->id;
        }

        $cookie = Cookie::get('aura_lc');
        if($cookie || $userid) {
            $check = $like->likes()
                ->where(function ($query) use ($userid, $cookie) {
                    $query->where('user_id', '=', $userid)
                        ->orWhere('cookie_user', '=', $cookie);
                })
                ->first();


            if ($check && $check->emo == $emo) {
                $check->delete();
            } else {

                if (!in_array($emo, $emoArray)) {
                    $emo = 'like';
                }

                if ($check) {
                    $check->emo = $emo;
                    $check->cookie_user = $cookie;
                    $check->save();
                } else {
                    $like->likes()->save(new LikesUsers(
                        array(
                            "user_id" => $userid,
                            'cookie_user' => $cookie,
                            'emo' => $emo
                        )
                    ));
                }
            }
        }

        if (Request::ajax() || Request::wantsJson()) {
            $likes = $like->likes;
            $view = (string)view('frontend.likeLink', compact('type', 'token', 'likes'));
            return Response::json([
                'html' => $view,
                'id' => 'likeLink_'.$token.'_'.$type
            ]);
        }else{
            return redirect()->back();
        }
    }


}
