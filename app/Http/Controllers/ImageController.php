<?php namespace Fully\Http\Controllers;

use Fully\Repositories\Project\ProjectInterface;
use Fully\Repositories\Slider\SliderInterface;
use Fully\Repositories\Article\ArticleInterface;
use LaravelLocalization;
use HabboIndex\Helpers\HabboApi as HabboApi;
use Fully\Models\User;
use Fully\Models\CommentsTexts;
use Response;
use Cache;
use Carbon;

/**
 * Class HomeController
 * @author Sefa Karagöz
 */
class ImageController extends Controller {

    public function profil($userid, $background = '', $langCode = 'de', $force = false) {


        $lang = str_replace('-', '.', $langCode);
        if(is_numeric($userid)){
            $fileName = $userid . '.png';
        }else {
            $fileName = 'name_' . $userid . '_background_' . $background . '_lang_' . $lang . '.png';
        }
        $path = public_path().'/img/profil/';


        if(!file_exists($path.$fileName) || Carbon\Carbon::createFromTimestamp(filemtime($path.$fileName))->diffInHours() >= 12 || $force === true ) {

            $habboUrl = "http://www.habbo.de/habbo-imaging/avatarimage?user=HabboAura&action=std&direction=&head_direction=3&gesture=sml&size=l&img_format=gif";
            $user = User::find($userid);
            if ($user && $user->habbo_name) {
                $lang = $user->habbo_lang ? $user->habbo_lang : 'de';
                $name = $user->habbo_name;
            } else if (is_string($userid)) {
                $name = (string)$userid;
            }

            if($lang == 'tr'){
                $lang = 'com.tr';
            }

            if ($name == 'unknow_habbo' || in_array($lang, ['int'])) {
                $habboUrl = public_path() . "/images/user/empty.gif";
            } else {
                $habboUrl = "https://www.habbo." . $lang . "/habbo-imaging/avatarimage?user=" . rawurlencode($name) . "&action=std&direction=&head_direction=3&gesture=sml&size=l&img_format=gif";
            }

            if(isset($user->data->profil_background) && !empty($user->data->profil_background)){
                $background = $user->data->profil_background;
            }else if(empty($background)){
                $background = 'cloud.png';
            }else if(strpos($background, '.') === false){
                $background.= '.png';
            }

            $imagesPath = '../public/images/user/profil';
            $image = imagecreatetruecolor(150, 150);
            imagealphablending($image, true);
            imagesavealpha($image, true);

            $base = imagecreatefrompng($imagesPath . "/" . $background);
            imagealphablending($base, true);
            imagesavealpha($base, true);

            imagecopy($image, $base, 0, 0, 0, 0, 150, 150);

            set_time_limit(20);
            $habbo = imagecreatefromstring($this->request($habboUrl, null));
            //$habbo = imagecreatefromgif($habboUrl);
            imagealphablending($habbo, true);
            imagesavealpha($habbo, true);

            $size = getimagesize($habboUrl);

            imagecopy($image, $habbo, 10, 0, 0, 0, $size[0], $size[1]);

            $image = imagepng($image, $path.$fileName);
        }

        //$response->header('Content-Type', 'image/png');

        $img = file_get_contents($path.$fileName);

        $days = 1;
        $expires = 24 * $days;
        $expires = $expires * 60;
        $expires = $expires * 60;

        $headers = [
            'Content-Type'        => 'image/png',
            'Content-Disposition' => 'inline',
            //'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Pragma'              => 'public, max-age='.$expires,
            'Content-Length'      => filesize($path.$fileName),
            'Etag'                => md5($img),
            'Expires'             => date('D, d M Y H:i:s ', time() + $expires).'GMT',
        ];

        $response = Response::make($img, 200);
        foreach($headers as $key => $header){
            $response->headers->set($key, $header);
        }

        return $response;
    }

    public function request ($url, $cookies) {

        $header = array();
        $header[] = 'Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5';
        $header[] = 'Cache-Control: max-age=0';
        $header[] = 'Connection: keep-alive';
        $header[] = 'Keep-Alive: 300';
        $header[] = 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7';
        $header[] = 'Accept-Language: en-us,en;q=0.5';
        $header[] = 'Pragma: ';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.11) Gecko/2009060215 Firefox/3.0.11 (.NET CLR 3.5.30729)');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20); //timeout in seconds
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_COOKIE, $cookies);
        return curl_exec($ch);
    }

}
