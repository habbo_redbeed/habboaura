<?php namespace Fully\Http\Controllers\Admin;

use Fully\Http\Controllers\Controller;
use Redirect;
use View;
use Input;
use Config;
use LaravelLocalization;
use Fully\Models\Staff;
use Request;
use Validator;
use Notification;
use Carbon;


class StaffController extends Controller
{

    public function index($lang = "")
    {
        $staffs = null;
        if (!empty($lang)) {
            $staffs = Staff::where('habbo_lang', '=', $lang)
                ->where(function ($query) {
                    $query->where('habbo_name', '!=', '')
                        ->orWhere(function ($query) {
                            $query->where('real_name', '!=', '');
                        });
                })
                ->orderBy('habbo_lang', 'DESC')
                ->orderBy('job_from', 'DESC')
                ->get();
        } else {
            $staffs = Staff::where(function ($query) {
                    $query->where('habbo_name', '!=', '')
                        ->orWhere(function ($query) {
                            $query->where('real_name', '!=', '');
                        });
                })
                ->orderBy('job_from', 'DESC')
                ->orderBy('habbo_lang', 'DESC')
                ->get();
        }

        $langs = Staff::groupBy('habbo_lang')->lists('habbo_lang');

        return view('backend.lexicon.staff.index', compact('staffs', 'langs'));
    }

    public function delete($id){
        $staff = Staff::find($id);
        if($staff){
            $staff->delete();
            Notification::success('Article was successfully deleted');
        }
        return redirect(route('admin.lexicon.staff.index'));
    }

    public function edit($id = 0)
    {

        $staff = Staff::firstOrCreate(array('id' => $id));
        return view('backend.lexicon.staff.edit', compact('staff'));

    }

    public function save($id = 0)
    {
        if (Request::isMethod('post')) {

            $validator = Validator::make(
                Input::all(),
                array(
                    'habbo_name' => 'required',
                    'habbo_lang' => 'required',
                    'job_title' => 'required',
                )
            );

            if ($validator->fails()) {
                return redirect(route('admin.lexicon.staff.edit', array($id)))->withInput()->withErrors($validator->errors());
            } else {
                $staff = Staff::firstOrCreate(array('id' => $id));

                $staff->habbo_name = Request::input('habbo_name');
                $staff->habbo_lang = Request::input('habbo_lang');

                $staff->real_name = Request::input('real_name');

                $staff->job_title = Request::input('job_title');

                $form = Request::input('job_from');
                if(!empty($form)) {
                    $staff->job_from = Carbon\Carbon::parse($form)->format('Y-m-d');
                }else{
                    $staff->job_from = null;
                }

                $to = Request::input('job_to');
                if(!empty($to)) {
                    $staff->job_to = Carbon\Carbon::parse($to)->format('Y-m-d');
                }else{
                    $staff->job_to = null;
                }

                $staff->save();
            }

            return redirect(route('admin.lexicon.staff.index'));
        } else {
            return redirect(route('admin.lexicon.staff.edit', array($id)))->withInput();
        }

    }

}