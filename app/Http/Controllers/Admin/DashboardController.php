<?php namespace Fully\Http\Controllers\Admin;

use Fully\Http\Controllers\Controller;
use Fully\Models\User;
use Fully\Models\UserActive;
use Fully\Models\Article;
use Fully\Models\LexiconEntry;
use Fully\Models\Shortnews;
use Fully\Models\Likes;
use Fully\Models\LikesUsers;
use Fully\Models\Comments;
use Fully\Models\Logger;
use Tracker;
use Carbon;
use Cache;

/**
 * Class DashboardController
 * @package Fully\Controllers\Admin
 * @author Sefa Karagöz
 */
class DashboardController extends Controller {

    function index() {

        $logger = new Logger();
        /*$chartData = $logger->getLogPercent();*/

        $chartData = array();

        $cpu = sys_getloadavg();
        $micros = [
            'cpu' => [
                'label' => 'CPU TRAFFIC',
                'value' => $cpu[0],
                'count' => '%',
                'color' => 'bg-aqua',
                'icon' => 'ion-ios-gear-outline',
            ],
            'user' => [
                'label' => 'Users',
                'value' => Cache::remember('admin_values_user_count', 200, function (){ return User::count(); }),
                'count' => '',

                'color' => 'bg-red',
                'icon' => 'ion-ios-people-outline',
            ],
            'likes' => [
                'label' => 'Likes on HabboAura',
                'value' => Cache::remember('admin_values_user_count', 200, function (){ LikesUsers::count(); }),
                'count' => '',
                'color' => 'bg-green',
                'icon' => 'ion-thumbsup',
            ],
            'comments' => [
                'label' => 'Comments on HabboAura',
                'value' => Cache::remember('admin_values_user_count', 200, function (){ Comments::count(); }),
                'count' => '',
                'color' => 'bg-yellow',
                'icon' => 'ion-ios-chatbubble-outline',
            ],
            'articels' => [
                'label' => 'Articels on HabboAura',
                'value' => Cache::remember('admin_values_user_count', 200, function (){ Article::count(); }),
                'count' => '',
                'color' => 'bg-maroon',
                'icon' => 'ion-edit',
            ],
            'lexicon' => [
                'label' => 'Lexicon Entrys on HabboAura',
                'value' => Cache::remember('admin_values_user_count', 200, function (){ LexiconEntry::count(); }),
                'count' => '',
                'color' => 'bg-olive',
                'icon' => 'ion-ios-bookmarks-outline',
            ],
            'shortnews' => [
                'label' => 'Shortnews on HabboAura',
                'value' => Cache::remember('admin_values_user_count', 200, function (){ Shortnews::count(); }),
                'count' => '',
                'color' => 'bg-orange',
                'icon' => 'ion-ios-information-outline',
            ],
        ];

        $microList = [
            'session_24' => [
                'label' => 'Vistitors last 25 Hours',
                'value' => Cache::remember('admin_values_user_count', 200, function (){ Tracker::sessions(60 * 24)->count(); }),
                'count' => 'Visitors',
                'color' => 'bg-yellow',
                'icon' => 'ion-ios-people-outline',
            ],
            'users_24' => [
                'label' => 'Users visits last 24 Hours',
                'value' => Cache::remember('admin_values_user_count', 200, function (){ UserActive::where('last_active', '>=', Carbon\Carbon::now()->subDay()->format('Y-m-d H:i:s'))->count(); }),
                'count' => 'Users',
                'color' => 'bg-red',
                'icon' => 'ion-ios-person-outline',
            ],
            'new_users_24' => [
                'label' => 'New Users last 24 Hours',
                'value' => Cache::remember('admin_values_user_count', 200, function (){ User::where('created_at', '>=', Carbon\Carbon::now()->subDay()->format('Y-m-d H:i:s'))->count(); }),
                'count' => 'Users',
                'color' => 'bg-green',
                'icon' => 'ion-ios-personadd-outline',
            ],
        ];

        return view('backend/layout/dashboard', compact('chartData', 'micros', 'microList'))->with('active', 'home');
    }
}
