<?php namespace Fully\Http\Controllers\Admin;

use Fully\Http\Controllers\Controller;
use Redirect;
use Sentry;
use View;
use Input;
use Validator;
use Fully\Models\User;
use Fully\Models\StarsLog;
use Fully\Models\Stars;
use Fully\Models\Group;
use Notification;
use Carbon;
use DB;

/**
 * Class UserController
 * @package App\Controllers\Admin
 * @author Sefa Karagöz
 */
class StarsController extends AdminController {

    public function index() {

        $thisMonth = Carbon\Carbon::now();
        $lastMonth = Carbon\Carbon::now()->subMonth(1);

        $score = [];
        $score['this'] = DB::select(DB::raw("
          SELECT sum(user_stars.stars) as stars, habbo_name, habbo_lang, users.id as id  FROM `user_stars`
          LEFT JOIN users
            ON (user_stars.user_id = users.id)
            WHERE user_stars.`created_at` > '" . $thisMonth->format('Y-m-01 00:00:00') . "' AND user_stars.`created_at` < '" . $thisMonth->format('Y-m-' . $thisMonth->daysInMonth . ' 23:59:59') . "'
            GROUP BY user_stars.user_id ORDER BY sum(user_stars.stars) DESC LIMIT 50"));

        $score['last'] = DB::select(DB::raw("
          SELECT sum(user_stars.stars) as stars, habbo_name, habbo_lang, users.id as id  FROM `user_stars`
          LEFT JOIN users
            ON (user_stars.user_id = users.id)
            WHERE user_stars.`created_at` > '" . $lastMonth->format('Y-m-01 00:00:00') . "' AND user_stars.`created_at` < '" . $lastMonth->format('Y-m-' . $lastMonth->daysInMonth . ' 23:59:59') . "'
            GROUP BY user_stars.user_id ORDER BY sum(user_stars.stars) DESC LIMIT 50"));

        $log = StarsLog::orderBy('created_at', 'DESC')->take('50')->get();

        return view('backend.stars.index', compact('score', 'log'));
    }

    public function add() {

        if($this->controllerAccess('admin.stars.store') !== true){
            return $this->controllerAccess('admin.stars.store');
        }

        $users = User::orderBy('created_at', 'DESC')->get();

        return view('backend.stars.add', compact('users'));
    }


    public function addPost() {

        if($this->controllerAccess('admin.user.store') !== true) {
            return $this->controllerAccess('admin.user.store');
        }

        $rules = array(
            'stars'        => 'required|integer',
            'users'        => 'required_without:any_habbo',
            'note'         => 'required|min:5',
        );

        $validation = Validator::make(Input::all(), $rules);

        if ($validation->fails()) {
            return back()
                ->withErrors($validation)
                ->withInput();
        }

        $stars = abs((int)Input::get('stars'));

        $date = Carbon\Carbon::now();
        if(Input::has('retroactive') && Input::get('retroactive')){
            $date = Carbon\Carbon::parse('1994-06-27 23:59:00');
        }

        if(Input::get('any_habbo') && Input::get('any_lang')){
            $habbo = Input::get('any_habbo');
            $lang = Input::get('any_lang');

            $save = Stars::create([
                'any_habbo' => $habbo,
                'any_lang' => $lang,
                'stars' => $stars,
                'note' => 'Anonym by id '.Sentry::getUser()->id
            ]);

            $save->created_at = $date->format('Y-m-d H:i:s');
            $save->save();

            StarsLog::create([
                'staff_id' => Sentry::getUser()->id,
                'user_id' => 0,
                'star' => $stars,
                'note' => 'Anonym to '.$habbo.' ('.$lang.'): '.Input::get('note')
            ]);
        }

        $userIds = Input::get('users');
        $userNames = [];
        if(count($userIds)) {
            foreach ($userIds as $userid) {
                $user = User::find($userid);
                if ($user) {
                    $save = $user->addStar($stars, Input::get('note'));
                    $save->created_at = $date->format('Y-m-d H:i:s');
                    $save->save();

                    $userNames [] = $user->habbo_name . ' (' . $user->habbo_lang . ') ';

                    StarsLog::create([
                        'staff_id' => Sentry::getUser()->id,
                        'user_id' => $user->id,
                        'star' => $stars,
                        'note' => Input::get('note')
                    ]);

                }

            }
        }
        return redirect(route('admin.stars.add'))->with('check', 'Add '.$stars.' Star(s) for '.implode(', ', $userNames));
    }

    public function sub() {

        if($this->controllerAccess('admin.stars.destroy') !== true){
            return $this->controllerAccess('admin.stars.destroy');
        }

        $users = User::orderBy('created_at', 'DESC')->get();

        return view('backend.stars.sub', compact('users'));
    }


    public function subPost() {

        if($this->controllerAccess('admin.user.destroy') !== true) {
            return $this->controllerAccess('admin.user.destroy');
        }

        $rules = array(
            'stars'        => 'required|integer',
            'users'        => 'required_without:any_habbo',
            'note'         => 'required|min:5',
        );

        $validation = Validator::make(Input::all(), $rules);

        if ($validation->fails()) {
            return back()
                ->withErrors($validation)
                ->withInput();
        }


        $stars = - abs((int)Input::get('stars'));

        if(Input::get('any_habbo') && Input::get('any_lang')){
            $habbo = Input::get('any_habbo');
            $lang = Input::get('any_lang');

            Stars::create([
                'any_habbo' => $habbo,
                'any_lang' => $lang,
                'stars' => $stars,
                'note' => 'Anonym by id '.Sentry::getUser()->id
            ]);

            StarsLog::create([
                'staff_id' => Sentry::getUser()->id,
                'user_id' => 0,
                'star' => $stars,
                'note' => 'Anonym to '.$habbo.' ('.$lang.'): '.Input::get('note')
            ]);
        }

        $userIds = Input::get('users');
        $userNames = [];
        if(count($userIds)) {
            foreach ($userIds as $userid) {
                $user = User::find($userid);
                if ($user) {
                    $user->addStar($stars, Input::get('note'));

                    $userNames [] = $user->habbo_name . ' (' . $user->habbo_lang . ') ';

                    StarsLog::create([
                        'staff_id' => Sentry::getUser()->id,
                        'user_id' => $user->id,
                        'star' => $stars,
                        'note' => Input::get('note')
                    ]);

                }

            }
        }
        return redirect(route('admin.stars.sub'))->with('check', 'Add '.$stars.' Star(s) for '.implode(', ', $userNames));
    }


}
