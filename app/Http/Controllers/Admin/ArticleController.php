<?php namespace Fully\Http\Controllers\Admin;

use Fully\Repositories\Article\ArticleInterface;
use Fully\Repositories\Category\CategoryInterface;
use Redirect;
use View;
use Input;
use Response;
use Request;
use Tag;
use Str;
use Notification;
use Fully\Repositories\Article\ArticleRepository as Article;
use Fully\Models\ArticleFormText;
use Fully\Models\Article as ArticleModel;
use Fully\Repositories\Category\CategoryRepository as Category;
use Fully\Exceptions\Validation\ValidationException;
use Validator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Fully\Models\Shortnews;
use App;
use Bitly;
use Twitter;
use DB;
use Sentry;

/**
 * Class ArticleController
 * @package App\Controllers\Admin
 * @author Sefa Karagöz
 */
class ArticleController extends AdminController {

    protected $article;
    protected $category;

    public function __construct(ArticleInterface $article, CategoryInterface $category) {

        View::share('active', 'blog');
        $this->article = $article;
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {

        if($this->controllerAccess('admin.article.index') !== true){
            return $this->controllerAccess('admin.article.index');
        }

        //$articles = $this->article->paginate(null, true);

        $page = Input::get('page', 1);
        $perPage = 10;
        $pagiData = $this->article->paginate($page, $perPage, true);

        $articles = new LengthAwarePaginator($pagiData->items, $pagiData->totalItems, $perPage, [
            'path' => Paginator::resolveCurrentPath()
        ]);

        $articles->setPath("");

        return view('backend.article.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {

        if($this->controllerAccess('admin.article.create') !== true){
            return $this->controllerAccess('admin.article.create');
        }

        $categories = $this->category->lists();
        return view('backend.article.create', compact('categories'));
    }

    public function viewShortNews () {

//        if($this->controllerAccess('admin.shortnews.view') !== true){
//            return $this->controllerAccess('admin.shortnews.view');
//        }

        $shortnews = Shortnews::whereIn('type', array('text', 'import', 'info', ''))
            ->where('category_id', '!=', 0)
            ->where('lang', '=', App::getLocale())
            ->orderby('created_at', 'DESC')
            ->get();

        return view('backend.shortnews.view', compact('shortnews'));
    }

    public function showShortnews ($id = null) {
        $short = Shortnews::firstOrCreate(array('id' => $id));

        if($this->controllerAccess('article.extra.edit.all') !== true) {
            if ($this->controllerAccess('admin.article.edit') !== true) {
                return $this->controllerAccess('admin.article.edit');
            }else if($short->creator_id != Sentry::getUser()->id && $short->creator_id){
                return redirect(App::getLocale().'/admin/shortnews');
            }
        }

        $categories = $this->category->lists();
        return view('backend.shortnews.show', compact('short', 'categories'));
    }

    public function toggleShortnews($id) {
        $short = Shortnews::firstOrCreate(array('id' => $id));

        if($this->controllerAccess('article.extra.topnews') !== true) {
            return redirect(App::getLocale().'/admin/shortnews');
        }

        $short->pin = ($short->pin) ? false : true;
        $short->save();
        return Response::json(array('result' => 'success', 'changed' => ($short->pin) ? 1 : 0));
    }

    public function deleteShortnews($id) {
        $short = Shortnews::firstOrCreate(array('id' => $id));

        if($this->controllerAccess('article.extra.delete.all') !== true) {
            if ($this->controllerAccess('admin.article.delete') !== true) {
                return $this->controllerAccess('admin.article.delete');
            }else if($short->creator_id != Sentry::getUser()->id && $short->creator_id){
                return redirect(App::getLocale().'/admin/shortnews');
            }
        }

        if($short) {
            $short->delete();
        }
        return redirect(route('admin.shortnews.view'));
    }

    private function shorter($input, $length)
    {
        //no need to trim, already shorter than trim length
        if (strlen($input) <= $length) {
            return $input;
        }

        //find last space within length
        $last_space = strrpos(substr($input, 0, $length), ' ');
        if(!$last_space) $last_space = $length;
        $trimmed_text = substr($input, 0, $last_space);

        //add ellipses (...)
        $trimmed_text .= '...';

        return $trimmed_text;
    }

    public function fromTextView ($articleId = '') {

        $article = ArticleModel::find($articleId);

        if($article) {
            $entry = ArticleFormText::firstOrCreate([
                'article_id' => $article->id,
            ]);

            return view('backend.article.fromText.view', compact('entry', 'article'));
        }

        return back();
    }

    public function fromTextViewSave ($articleId = '') {

        $article = ArticleModel::find($articleId);

        if($article) {

            $validator = Validator::make(Request::all(), [
                'question' => 'required|max:90',
                'available_to' => 'required|date_format:"Y-m-d"',
                'any' => 'required'
            ]);

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $entry = ArticleFormText::firstOrCreate([
                'article_id' => $article->id,
            ]);

            $entry->description = Input::get('question');
            $entry->available_to = Input::get('available_to');
            $entry->any = (int)Input::get('any');
            $entry->save();

            return back();
        }

        return back();
    }

    public function saveShortnews ($id) {
        if (Request::isMethod('post')) {
            $short = Shortnews::firstOrCreate(array('id' => $id));

            if($this->controllerAccess('article.extra.edit.all') !== true) {
                if ($this->controllerAccess('admin.article.edit') !== true) {
                    return $this->controllerAccess('admin.article.edit');
                }else if($short->creator_id != Sentry::getUser()->id && $short->creator_id){
                    return redirect(App::getLocale().'/admin/shortnews');
                }
            }

            if($short->text == ''){
                $short->lang = App::getLocale();

                $short->creator_id = Sentry::getUser()->id;

                try{
                    $url = Bitly::shorten('http://www.habboaura.com/'.App::getLocale().'/shortnews')['data']['url'];
                    $end = $url.' #habbo #live';
                    $endLenght = strlen($end);
                    $tweet = Request::input('text');
                    $tweet = htmlspecialchars_decode($tweet);
                    $tweet = str_replace(['&auml;','&Auml;','&ouml;','&Ouml;','&uuml;','&Uuml;', '&szlig;'], ['ä', 'Ä','ö','Ö','ü','Ü', 'ß'], $tweet);
                    $tweet = json_encode(strip_tags($tweet.' '.$end));
                    $tweet = json_decode($tweet);
                    $tweet = $this->shorter(strip_tags($tweet), 130 - $endLenght);
                    Twitter::postTweet(['status' => strip_tags($tweet.' '.$end), 'format' => 'json']);
                }
                catch (Exception $e)
                {

                }

            }

            $short->text = Request::input('text');
            $short->category_id = Request::input('category_id');
            $short->user = Request::input('user');
            $short->tags = Request::input('tags');
            $short->type = Request::input('type');


            if (Request::hasFile('image') && Request::file('image')->isValid())
            {
                $dir = '/uploads/short/';
                $file = Request::file('image');

                $ext = $file->guessExtension();
                if($ext == ''){
                    $ext = $file->getExtension();
                }
                $newFileName = md5($file->getClientOriginalName()).'_'.time().'.'.$ext;
                $file = Request::file('image')->move(public_path().$dir, $newFileName);

                $short->screenshot = $dir.$newFileName;
            }

            $short->save();
        }
        return redirect(route('admin.shortnews.view')); //, array($id))->withInput()->withErrors($e->getErrors());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {

        if($this->controllerAccess('admin.article.store') !== true){
            return $this->controllerAccess('admin.article.store');
        }

        try {
            $this->article->create(Input::all());
            Notification::success('Article was successfully added');
            return langRedirectRoute('admin.article.index');
        } catch (ValidationException $e) {
            return langRedirectRoute('admin.article.create')->withInput()->withErrors($e->getErrors());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id) {

        if($this->controllerAccess('admin.article.show') !== true){
            return $this->controllerAccess('admin.article.show');
        }

        $article = $this->article->find($id);
        return view('backend.article.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id) {

        if($this->controllerAccess('article.extra.edit.all') !== true) {
            if ($this->controllerAccess('admin.article.edit') !== true) {
                return $this->controllerAccess('admin.article.edit');
            }else if($this->article->find($id)->creator_id != Sentry::getUser()->id){
                return redirect(App::getLocale().'/admin/article');
            }
        }

        $article = $this->article->find($id);
        $tags = $article->tags;

        $tags = substr($tags, 1);
        $categories = $this->category->lists();

        $publishAt = '';

        $dbSelect = DB::select('SELECT publish_time FROM article_publish_cron WHERE article_id = '.$id);
        if(count($dbSelect)){
            $publishAt = current($dbSelect)->publish_time;
        }

        return view('backend.article.edit', compact('article', 'tags', 'categories', 'publishAt'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id) {

        if($this->controllerAccess('article.extra.edit.all') !== true) {
            if ($this->controllerAccess('admin.article.update') !== true) {
                return $this->controllerAccess('admin.article.update');
            }else if($this->article->find($id)->creator_id != Sentry::getUser()->id){
                return redirect(App::getLocale().'/admin/article');
            }
        }

        try {
            $this->article->update($id, Input::all());

            if(Input::has('publish_at') && Input::get('publish_at')){
                DB::insert('INSERT INTO article_publish_cron (article_id, publish_time) VALUES ('.$id.', "'.Input::get('publish_at').'") ON DUPLICATE KEY UPDATE publish_time = "'.Input::get('publish_at').'"');
            }

            Notification::success('Article was successfully updated');
            return langRedirectRoute('admin.article.index');
        } catch (ValidationException $e) {
            return langRedirectRoute('admin.article.edit', array($id))->withInput()->withErrors($e->getErrors());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id) {

        if($this->controllerAccess('article.extra.edit.all') !== true) {
            if ($this->controllerAccess('admin.article.destroy') !== true) {
                return $this->controllerAccess('admin.article.destroy');
            }else if($this->article->find($id)->creator_id != Sentry::getUser()->id){
                return redirect(App::getLocale().'/admin/article');
            }
        }

        $this->article->delete($id);
        Notification::success('Article was successfully deleted');
        return langRedirectRoute('admin.article.index');
    }

    public function confirmDestroy($id) {

        $article = $this->article->find($id);
        return view('backend.article.confirm-destroy', compact('article'));
    }

    public function togglePublish($id) {

       if ($this->controllerAccess('article.extra.publish') !== true) {
           return $this->controllerAccess('article.extra.publish');
       }

        return $this->article->togglePublish($id);
    }

    public function toggleTopnews($id) {

        if ($this->controllerAccess('article.extra.topnews') !== true) {
            return $this->controllerAccess('article.extra.topnews');
        }

        return $this->article->toggleTopnews($id);
    }
}
