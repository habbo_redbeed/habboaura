<?php namespace Fully\Http\Controllers\Admin;

use Fully\Http\Controllers\Controller;
use Fully\Models\EventCalendar;
use Fully\Models\Icon;
use Tracker;
use Carbon;
use Cache;
use Validator;
use Request;
use Input;
use DB;
use Sentry;

/**
 * Class DashboardController
 * @package Fully\Controllers\Admin
 * @author Sefa Karagöz
 */
class EventCalendarController extends AdminController {

    public function index() {

        if($this->controllerAccess('admin.event_calendar.index') !== true){
            return $this->controllerAccess('admin.event_calendar.index');
        }

        $now = Carbon\Carbon::now();
        $events = EventCalendar::where('event_end', '>=', $now->format('Y-m-d H:i:s'))
            ->orderBy('event_end')
            ->orderBy('event_start')
            ->orderBy('created_at')
            ->paginate(15);


        return view('backend.event.calendar.index', compact('events'));

    }

    public function delete($id){
        $event = EventCalendar::find($id);
        if($event){
            $event->delete();
        }

        return back();
    }

    public function edit($id = 0) {

        if($this->controllerAccess('admin.event_calendar.edit') !== true){
            return $this->controllerAccess('admin.event_calendar.edit');
        }

        $event = EventCalendar::firstOrCreate([
            'id' => $id
        ]);

        $iconsObjects = Icon::where('see_calendar', 1)
            ->get();

        $icons = [];
        foreach($iconsObjects as $icon){
            $icons[$icon['id']] = trans('icons.name.'.$icon['name']);
        }

        return view('backend.event.calendar.edit', compact('event', 'icons'));
    }

    public function editPost($id) {


        if($this->controllerAccess('admin.event_calendar.store') !== true){
            return $this->controllerAccess('admin.event_calendar.store');
        }

        $rules = [
            'title' => 'required|max:100',
            'event_start' => 'required|date_format:Y-m-d H:i:s',
            'event_end' => 'required|date_format:Y-m-d H:i:s',

            'area' => 'required|integer',

            'description' => 'required|max:2500',
            'price' => 'required|max:300',

            'link_room' => '',
            'link_external' => '',
            'link_article' => '',
        ];

        $validator = Validator::make(Request::all(), $rules);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $event = EventCalendar::find($id);

        foreach(Request::all() as $key => $input){
            if(isset($rules[$key])) {
                $event->{$key} = $input;
            }
        }

        $event->save();

        return redirect(route('admin.event.calendar.index'));
    }

    public function code() {

        $randCode = 'HA - '.substr(md5(time()), 3, 2).' - '.substr(md5(time() + time()), 3, 3);

        $stars = [];
        for($i = 1; $i <= 50; $i ++){
            $stars[$i] = $i.' Stars';
        }

        $quantity = [];
        for($i = 1; $i <= 100; $i ++){
            $quantity[$i] = $i.' Habbos';
        }

        $codes = DB::select("SELECT event_code . * , users.habbo_name, (
            SELECT COUNT( * )
            FROM event_code_user
            WHERE code_id = event_code.id
            ) AS users
            FROM  `event_code`
            JOIN users ON ( users.id = event_code.creator )
            ORDER BY event_code.created_at DESC
            LIMIT 0 , 100");

        return view('backend.event.code', compact('stars', 'quantity', 'randCode', 'codes'));
    }

    public function codePost() {
        $validate = Validator::make(Input::all(), [
            'code' => 'required|max:100',
            'stars' => 'required|integer',
            'quantity' => 'required|integer',
        ]);

        if($validate->fails()){
            return back()->withInput()->withErrors($validate);
        }

        $code = head(DB::select("SELECT code, id, count, stars FROM event_code WHERE code = ?", [Input::get('code')]));
        if(!$code){
            DB::insert('
            INSERT INTO event_code
            (code, stars, count, created_at, creator)
            VALUES (?, ?, ?, NOW(), ?)
            ', [Input::get('code'), Input::get('stars'), Input::get('quantity'), Sentry::getUser()->id]);

            return back()->with('message', 'Create Code!');

        }else{
            return back()->withErrors([
                'error' => 'Code still exist'
            ]);
        }

    }
}
