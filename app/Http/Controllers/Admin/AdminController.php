<?php namespace Fully\Http\Controllers\Admin;

use Fully\Models\Likes;
use Fully\Http\Controllers\Controller;
use Fully\Models\LikesUsers;
use Fully\Models\Comments;
use Fully\Models\CommentsTexts;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Sentry;
use Cache;
use Notification;


abstract class AdminController extends Controller
{

    public function controllerAccess ($permission){
        if(is_string($permission)){
            if(Sentry::getUser()->hasAnyAccess([$permission])){
                return true;
            }else {
                return redirect(url(getLang() . '/admin'))->with('message', 'Gehe zurück auf Los und ziehe keine Taler ein!');
            }
        }else{
            return true;
        }
    }


}
