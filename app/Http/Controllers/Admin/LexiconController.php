<?php namespace Fully\Http\Controllers\Admin;

use Fully\Http\Controllers\Controller;
use Redirect;
use View;
use Input;
use Config;
use LaravelLocalization;
use Fully\Models\Staff;
use Request;
use Validator;
use Notification;
use Carbon;
use Fully\Models\LexiconEntryText;
use Fully\Models\LexiconEntry;
use Fully\Models\LexiconCategory;
use Cache;


class LexiconController extends Controller
{
    public function index($categorie = null)
    {
        $texts = LexiconEntryText::where('status', '=', 'PAUSED')
            ->orderBy('created_at')
            ->get();

        $categories = LexiconCategory::get();
        return view('backend.lexicon.entry.index', compact('texts', 'categories'));
    }

    public function showEntry($id = null)
    {
        $text = LexiconEntryText::where('status', '=', 'PAUSED')
            ->where('id', '=', (int)$id)
            ->orderBy('created_at')
            ->first();

        $categories = LexiconCategory::get();

        $categoriesArray = Cache::remember('lexicon_categories_array', 50, function () use ($categories) {
            $return = array();

            foreach ($categories as $cat) {
                $return[$cat->id] = $cat->name;
            }

            return $return;
        });

        return view('backend.lexicon.entry.show', compact('text', 'categories', 'categoriesArray'));
    }

    public function saveEntry($id = null)
    {
        $text = LexiconEntryText::where('status', '=', 'PAUSED')
            ->where('id', '=', (int)$id)
            ->orderBy('created_at')
            ->first();

        if(!$text){
            return redirect(route('admin.lexicon.entry.index'));
        }

        $entry = $text->entry;

        $text->text = Request::input('content');

        if(Request::has('category')) {
            $text->new_categorie_id = Request::input('category');
            $entry->categorie_id = Request::input('category');
        }

        if(Request::has('title')) {
            $text->new_title = Request::input('title');
            $entry->title = Request::input('title');
        }

        if(Request::has('is_published')) {
            $text->status = 'ENABLE';
            $entry->status = 1;
        }else{
            $text->status = 'PAUSED';
        }

        $text->save();
        $entry->save();
        return redirect(route('admin.lexicon.entry.index'));


    }

    public function deleteEntry($id = null)
    {
        $text = LexiconEntryText::where('status', '=', 'PAUSED')
            ->where('id', '=', (int)$id)
            ->orderBy('created_at')
            ->first();

        if (!$text) {
            return redirect(route('admin.lexicon.entry.index'));
        }

        $text->status = 'DISABLE';
        $text->save();

        return redirect(route('admin.lexicon.entry.index'));
    }

} 