<?php namespace Fully\Http\Controllers;

use Fully\Http\Controllers\Controller;
use Redirect;
use View;
use Input;
use Config;
use LaravelLocalization;
use Fully\Models\LexiconCategory;
use Fully\Models\LexiconEntryText;
use Fully\Models\LexiconEntry;
use Fully\Models\Comments;
use Fully\Models\Likes;
use Request;
use Validator;
use Notification;
use Carbon;
use Cache;
use Sentry;
use HabboIndex\Helpers\HabboApi;
use Lang;
use Illuminate\Pagination\LengthAwarePaginator;



class BadgesScoreController extends Controller
{

    public function index($lang = "de")
    {

        $show = 120;
        if($lang == 'com'){
            $show = 240;
        }

        $split = $show / 3;

        $habboApi = new HabboApi();
        $listOne = $habboApi->bestlist('badges', $split, 0, $lang);
        $listTwo = $habboApi->bestlist('badges', $split, $split, $lang);
        $listThree = $habboApi->bestlist('badges', $split, $split * 2, $lang);

        return view('frontend.badges.index', compact('listOne', 'listTwo', 'listThree', 'lang', 'split'));
    }

    public function add() {
        if (Request::isMethod('post') && SEntry::check()) {
            $validator = Validator::make(Request::all(), [
                'habboname' => 'required|min:3',
            ]);

            if ($validator->fails()) {
                return Redirect::route('best.badge')
                    ->withErrors($validator)
                    ->withInput();
            } else {

                $api = new HabboApi();
                $api->habbo(Request::input('habboname'));

                return Redirect::route('best.badge')
                    ->with('message', Lang::get('bestlist.badge.add.check'))
                    ->withInput();

            }
        }
    }


}