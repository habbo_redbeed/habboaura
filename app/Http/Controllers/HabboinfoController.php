<?php namespace Fully\Http\Controllers;

use HabboIndex\Helpers\HabboDirectApi;
use LaravelLocalization;
use HabboIndex\Helpers\HabboApi as HabboApi;
use Tracker;
use Request;
use Lang;

/**
 * Class HomeController
 * @author Sefa Karagöz
 */
class HabboinfoController extends Controller
{

    public function post () {
        $name = Request::input('habboname');
        $hotel = Request::input('hotel');

        if(empty($name) || empty($hotel)){
            return redirect(route('goodies.habboindex.index'));
        }

        return redirect(route('goodies.habboindex.view', [$name, $hotel]));
    }

    public function habbo($habboname = '', $tld = '')
    {

        $api = new HabboDirectApi();

        $habbo = null;
        $message = '';
        if(!empty($habboname) && !empty($tld)) {
            $habbo = $api->habbo($habboname, $tld);
            if(isset($habbo['success']) && $habbo['success'] == false){

                $message = Lang::get('goodies.infos.error');
                if(isset($habbo['fallback']) ){
                    $habbo = [
                        'user' => $habbo['fallback'],
                    ];
                    $message = Lang::get('goodies.infos.hidden');
                }else{
                    $habbo = null;
                }
            }
        }

        $badgesCount = 15;
        $badgesMore = false;
        if($message == null ) {
            if (count($habbo['badges']) <= $badgesCount) {
                $badgesCount = count($habbo['badges']);
            } else {
                $badgesMore = true;
            }
        }

        $friendsCount = 14;
        $friendsMore = false;
        if($message == null ) {
            if (count($habbo['friends']) <= $friendsCount) {
                $friendsCount = count($habbo['friends']);
            } else if (count($habbo['friends']) > ($friendsCount * 2)) {
                $friendsMore = true;
            }
        }

        $groupsCount = 15;
        $groupsMore = false;
        if($message == null ) {
            if (count($habbo['groups']) <= $groupsCount) {
                $groupsCount = count($habbo['groups']);
            } else {
                $groupsMore = true;
            }
        }

        $hotels = [
            'de' => '.de',
            'tr' => '.com.tr',
            'com' => '.com',
            'br' => '.com.br',
            'es' => '.es',
            'fi' => '.fi',
            'fr' => '.fr',
            'it' => '.it',
            'nl' => '.nl',

        ];

        return view('frontend/habboinfo/habbo', compact(
            'habbo',
            'tld',
            'hotels',
            'badgesCount',
            'badgesMore',
            'friendsCount',
            'friendsMore',
            'groupsCount',
            'groupsMore',
            'message'
        ));
    }
}
