<?php namespace Fully\Http\Controllers;

use Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use View;
use Route;
use Tracker;
use Response;
use Cache;
use Fully\Models\Shortnews;
use Fully\Models\Category;
use App;
use LaravelLocalization;

/**
 * Class ArticleController
 * @author Sefa Karagöz
 */
class ShortnewsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($category = '')
    {

        $categoryId = null;
        if(!empty($category)){
            $cat = Category::where('slug', $category)->first();
            if($cat){
                $categoryId = $cat->id;
            }
        }

        $lang = LaravelLocalization::getCurrentLocale();
        $shortnews = Shortnews::orderBy('created_at', 'desc')
            ->where('category_id', '!=', 0)
            ->where(function ($query) use ($lang){
                $query->where('lang', '=', $lang)
                    ->orWhere('type', 'translate');
            })
            ->take(100);

        if($categoryId){
            $shortnews = $shortnews->where('category_id', $categoryId);
        }

        $shortnews = $shortnews->get();

        return view('frontend.shortnews.index', compact('shortnews'));
    }
}
