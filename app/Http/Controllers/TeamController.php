<?php namespace Fully\Http\Controllers;

use Fully\Models\CommentsTexts;
use Fully\Models\LikesUsers;
use Fully\Models\UserData;
use HabboIndex\Helpers\HabboApi;
use Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use View;
use Sentry;
use Redirect;
use Fully\Models\User;
use Session;
use Lang;
use Request;
use Fully\Models\Comments;
use Fully\Models\Likes;
use Validator;
use Tracker;
use Route;
use Fully\Models\Group;


/**
 * Class ArticleController
 * @author Sefa Karagöz
 */
class TeamController extends Controller
{

    /**
     * Display the login page
     * @return View
     */
    public function index()
    {
        $teams = Group::where('display', '=', 1)
            ->orderBy('sort', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->get();

        return view('frontend.team.index', compact('teams'));
    }




}