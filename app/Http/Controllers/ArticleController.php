<?php namespace Fully\Http\Controllers;

use Fully\Repositories\Article\ArticleInterface;
use Fully\Repositories\Tag\TagInterface;
use Fully\Repositories\Category\CategoryInterface;
use Fully\Repositories\Category\CategoryRepository as Category;
use Fully\Models\ArticleFormText;
use Fully\Models\Article;
use Fully\Models\ArticleFormTextEntry;
use Fully\Models\Category as CategoryModel;
use Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use View;
use Route;
use Tracker;
use Response;
use Fully\Models\Comments;
use Fully\Models\Likes;
use Fully\Models\Shortnews;
use Cache;
use Validator;
use Request;
use Session;
use Sentry;
use App;

/**
 * Class ArticleController
 * @author Sefa Karagöz
 */
class ArticleController extends Controller
{

    protected $article;
    protected $tag;
    protected $category;

    public function __construct(ArticleInterface $article, TagInterface $tag, CategoryInterface $category)
    {
        parent::__construct();
        $this->article = $article;
        $this->tag = $tag;
        $this->category = $category;
    }

    private function views($slug)
    {
        $count = Tracker::logByRouteName( Route::currentRouteName() )
            ->where(function ($query) use ($slug) {
                $query
                    ->where('parameter', 'slug')
                    ->where('value', $slug);
            })
            ->groupBy('tracker_log.session_id')
            ->get();
        return count($count);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $page = Input::get('page', 1);
        $perPage = 5;
        $pagiData = $this->article->paginate($page, $perPage, false);


        $articles = new LengthAwarePaginator($pagiData->items, $pagiData->totalItems, $perPage, [
            'path' => Paginator::resolveCurrentPath()
        ]);

        $articles->setPath("");

        $tags = $this->tag->all();
        $categories = $this->categoriesWithArticels();
        return view('frontend.article.index', compact('articles', 'tags', 'categories'));
    }

    public function quest ($articleId = '') {

        $article = Article::find($articleId);

        $validator = Validator::make(Request::all(), [
            'answer' => 'required|max:300',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        if($article && !$article->question->entrys()->where('user_id', Sentry::getUser()->id)->first()) {
            $entry = ArticleFormText::firstOrCreate([
                'article_id' => $article->id,
            ]);

            $foo = ArticleFormTextEntry::create([
                'from_text_id' => $entry->id,
                'user_id' => Sentry::getUser()->id,
                'text' => Input::get('answer')
            ]);


            return back();
        }

        return back();
    }

    public function questAny ($articleId = '') {

        $article = Article::find($articleId);

        $validator = Validator::make(Request::all(), [
            'answer' => 'required|max:300',
            'habbo' => 'required|max:100|min:2',
            'lang' => 'required|max:5',
            'g-recaptcha-response' => 'required|recaptcha',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        if($article) {
            $entry = ArticleFormText::firstOrCreate([
                'article_id' => $article->id,
            ]);

            $foo = ArticleFormTextEntry::create([
                'from_text_id' => $entry->id,
                'any_habbo' => Input::get('habbo'),
                'any_lang' => Input::get('lang'),
                'any_session' => Session::getId(),
                'text' => Input::get('answer')
            ]);


            return back();
        }

        return back();
    }

    private function categoriesWithArticels() {
        return Cache::remember('categories_with_articels_'.App::getLocale(), 60, function () {
            $categories = CategoryModel::where('lang', App::getLocale())
                ->orderBy('sort', 'DESC')
                ->orderBy('title')
                ->get();
            $return = array();
            foreach($categories as $key => $category){
                if($category->articles->count()){
                    $return[$key] = $category;
                }
            }

            return $return;
        });
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function show($slug)
    {

        $article = $this->article->getBySlug($slug);

        if($article->is_published == 0 && (!Sentry::check() || !Sentry::getUser()->hasAccess('admin.article.index'))){
            return Response::view('errors.missing', array(), 404);
        }

        if ($article === null)
            return Response::view('errors.missing', array(), 404);

        $token = $article->id;
        $type = 'article';

        View::composer('frontend/layout/layout', function ($view) use ($article) {

            $view->with('meta_keywords', $article->meta_keywords);
            $view->with('meta_description', $article->meta_description);
        });

        $comments = Comments::firstOrCreate(array(
            'token' => $article->id,
            'type' => 'article'
        ));


        $comments = $comments->comments;

        $likes = Likes::firstOrCreate(array(
            'token' => $token,
            'type' => $type
        ));

        $recommended = Article::where('category_id', $article->category_id)
            ->where('created_at', '<', $article->created_at)
            ->where('id', '!=', $article->id)
            ->orderBy('created_at', 'DESC')
            ->take(2)
            ->get();

        $lastShortnews = Shortnews::where('category_id', $article->category_id)
            ->take(3)
            ->orderBy('created_at', 'DESC')
            ->get();

        $likesObject = $likes;
        $likes = $likes->likes;
        //var_dump($likes); die;



        $categories = $this->category->all();

        $tags = $this->tag->all();
        $views = $this->views($slug);
        $categories = $this->categoriesWithArticels();
        return view('frontend.article.show_3', compact(
            'article',
            'categories',
            'tags',
            'views',
            'comments',
            'token',
            'type',
            'likes',
            'categories',
            'recommended',
            'lastShortnews',
            'likesObject'
        ));
    }
}
