<?php namespace Fully\Http\Controllers;

use Fully\Repositories\Article\ArticleRepository as Article;
use Fully\Models\Article as ArticleObject;
use Fully\Models\Category as CategoryObject;
use Fully\Models\Shortnews;
use Fully\Repositories\Category\CategoryRepository as Category;
use Fully\Repositories\Tag\TagRepository as Tag;
use Fully\Repositories\Category\CategoryInterface;
use Fully\Repositories\Article\ArticleInterface;
use Fully\Repositories\Tag\TagInterface;
use Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Cache;
use App;
use Response;

/**
 * Class CategoryController
 * @author Sefa Karagöz
 */
class CategoryController extends Controller {

    protected $article;
    protected $tag;
    protected $category;

    public function __construct(ArticleInterface $article, TagInterface $tag, CategoryInterface $category) {

        parent::__construct();
        $this->article = $article;
        $this->tag = $tag;
        $this->category = $category;
    }

    /**
     * Display a listing of the resource by slug.
     * @param $slug
     * @return mixed
     */
    public function index($slug) {

        $articleLang = config('article.lang.'.App::getLocale());

        $category = CategoryObject::where('slug', '=', $slug)
            ->where('lang', $articleLang)
            ->first();//$this->category->getArticlesBySlug($slug);

        if(!$category){
            return Response::view('errors.missing_frontend', array(), 404);
        }

        $tags = $this->tag->all();
        //$categories = $this->category->all();

        $articles = $category->articles()
            ->where('is_published', '=', 1)
            ->orderBy('created_at', 'DESC')
            ->paginate(10);
        //$pagiData = $this->category->paginate($page, $perPage, false);

        $categories = $this->categoriesWithArticels();


        return view('frontend.article.index', compact('articles', 'tags', 'categories', 'pagiData'));
    }

    /**
     * Display a listing of the resource by slug.
     * @param $slug
     * @return mixed
     */
    public function tags($tags) {

        $articles = ArticleObject::where('tags', 'LIKE', '%'.$tags.'%')
            ->orderBy('created_at', 'DESC')
            ->get();
        $shortnews = Shortnews::where('tags', 'LIKE', '%'.$tags.'%')
            ->orderBy('created_at', 'DESC')
            ->get();

        $categories = $this->categoriesWithArticels();

        $banner = null;
        if($tags == 'habbo15'){
            $banner = 'habbo15_1';
        }


        return view('frontend.article.index_tags', compact('articles', 'tags', 'categories', 'shortnews', 'banner'));
    }

    private function categoriesWithArticels() {
        $articleLang = config('article.lang.'.App::getLocale());
        return Cache::remember('categories_with_articels_'.$articleLang, 60, function () use ($articleLang) {
            $categories = CategoryObject::orderBy('title')
                ->where('lang', $articleLang)
                ->get();
            $return = array();
            foreach($categories as $key => $category){
                if($category->articles->count()){
                    $return[$key] = $category;
                }
            }

            return $return;
        });
    }
}
