<?php namespace Fully\Http\Controllers;

use Redirect;
use View;
use Input;
use Config;
use LaravelLocalization;
use Request;
use Validator;
use Notification;
use Carbon;
use Cache;
use Sentry;
use HabboIndex\Helpers\HabboApiBadges;
use Lang;
use App;
use Illuminate\Pagination\LengthAwarePaginator;
use Fully\Models\Page;



class BadgesController extends Controller
{

    public function __construct()
    {
        parent::__construct();

        $menu = [];
        if(Request::route('lang')) {
            $lang = Request::route('lang');
            $menu = [
                'all' => trans('badges.page.title.badges', ['lang' => $lang]),
                'not' => trans('badges.page.title.notBadges', ['lang' => $lang])
            ];

            if(in_array($lang, ['tr', 'com', 'de'])){
                $menu['used'] = trans('badges.page.title.used', ['lang' => $lang]);
            }
        }else{
            $menu = [
                'all' => trans('badges.page.title.badgesAll'),
                'used' => trans('badges.page.title.usedAll')
            ];
        }

        View::share('menuNames', $menu);

        $helpDesk = Page::where('view_blade', 'badges_help')->first();
        View::share('helpDesk', $helpDesk);

    }

    protected $badgePerSite = 156;

    public function index($lang = '')
    {

        $menuSelect = 'all';

        $title = trans('badges.page.title.badgesAll', ['lang' => $lang]);
        if($lang){
            $title = trans('badges.page.title.badges', ['lang' => $lang]);
        }

        $search = '';
        if(Input::has('s') && Input::get('s')){
            $search = Input::get('s');
        }

        $activLang = '';
        $selfLang = App::getLocale();
        if($lang){
            $activLang = $lang;
        }

        $api = new HabboApiBadges();
        if(empty($search)) {
            $badgeArray = $api->all($lang);
        }else{
            $badgeArray = $api->search($lang, $search);
        }

        $langs = $api->langs();

        $page = Input::get('page') - 1;
        if($page < 0){
            $page  = 0;
        }
        $perPage = $this->badgePerSite;
        $offset = (($page + 1)  * $perPage) - $perPage;
        $badges = new LengthAwarePaginator ( array_slice ( $badgeArray, $offset, $this->badgePerSite, true ), count($badgeArray), $perPage, ($page +1), [
            'path' => Request::url (),
            'query' => Request::query ()
        ] );

        return view('frontend.badges.page.index', compact(
            'badges', 'langs', 'activLang', 'selfLang', 'title', 'menuSelect'));
    }

    public function not($lang)
    {

        $menuSelect = 'not';

        $title = trans('badges.page.title.badgesAll', ['lang' => $lang]);
        if($lang){
            $title = trans('badges.page.title.notBadges', ['lang' => $lang]);
        }

        $search = '';
        if(Input::has('s') && Input::get('s')){
            $search = Input::get('s');
        }

        $activLang = '';
        $selfLang = App::getLocale();
        if($lang){
            $activLang = $lang;
        }

        $api = new HabboApiBadges();
        $badgeArray = $api->allNot($lang, $search);

        $langs = $api->langs();

        $page = Input::get('page') - 1;
        if($page < 0){
            $page  = 0;
        }
        $perPage = $this->badgePerSite;
        $offset = (($page + 1) * $perPage) - $perPage;
        $badges = new LengthAwarePaginator ( array_slice ( $badgeArray, $offset, $this->badgePerSite, true ), count($badgeArray), $perPage, ($page +1), [
            'path' => Request::url (),
            'query' => Request::query ()
        ] );

        $notClass = 'not';

        return view('frontend.badges.page.index', compact(
            'badges', 'langs', 'activLang', 'selfLang', 'title', 'notClass', 'menuSelect'));
    }

    public function used($lang = ''){

        $menuSelect = 'used';

        $title = trans('badges.page.title.usedAll', ['lang' => $lang]);
        if($lang){
            $title = trans('badges.page.title.used', ['lang' => $lang]);
        }

        $activLang = '';
        $selfLang = App::getLocale();
        if($lang){
            $activLang = $lang;
        }

        $api = new HabboApiBadges();
        $allBadges = $api->used('ASC', $lang);

        $langs = $api->langs();

        $page = Input::get('page') - 1;
        if($page <= 0){
            $page  = 0;
        }
        $perPage = $this->badgePerSite;
        $offset = (($page + 1) * $perPage) - $perPage;
        $badges = new LengthAwarePaginator ( array_slice ( $allBadges, $offset, $this->badgePerSite, true ), count($allBadges), $perPage, ($page +1), [
            'path' => Request::url (),
            'query' => Request::query ()
        ] );

        return view('frontend.badges.used.index', compact(
            'badges', 'langs', 'activLang', 'selfLang', 'title', 'menuSelect'));
    }




}