<?php namespace Fully\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use HabboIndex\Helpers\HabboApi;
use Fully\Models\FurniLog;
use Fully\Models\Shortnews;
use Cache;
use Lang;
use App;
use Bitly;
use Twitter;
use File;
use Fully\Models\BadgeApi;

class BadgeCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'badge:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $action = $this->argument('action');
        switch ($action) {
            case 'twitter':
                $this->check();
                break;

        }
    }

    private function shorter($input, $length)
    {
        //no need to trim, already shorter than trim length
        if (strlen($input) <= $length) {
            return $input;
        }

        //find last space within length
        $last_space = strrpos(substr($input, 0, $length), ' ');
        if(!$last_space) $last_space = $length;
        $trimmed_text = substr($input, 0, $last_space);

        //add ellipses (...)
        $trimmed_text .= '...';

        return $trimmed_text;
    }

    private function twitter($badge){
        try {
            App::setLocale('en');
            $url = Bitly::shorten('http://www.habboaura.com/')['data']['url'];
            $end = $url.' #habbo #badge #add #image #new';
            $endLenght = strlen($end);
            $tweet = $this->shorter(Lang::get("shortnews.badge.twitter", array('name' => $badge->code)), 120 - $endLenght);
            $tweet = str_replace('<br>', "\n", $tweet);
            $tweet = ['status' => strip_tags($tweet.' '.$end), 'format' => 'json'];

            try {
                $img = file_get_contents('http://images.habbo.com/c_images/album1584/'.$badge->code.'.gif');
                if($img){
                    $uploaded_media = Twitter::uploadMedia(['media' => $img]);
                    $tweet['media_ids'] = $uploaded_media->media_id_string;

                    echo 'Tweet '.$badge->code."\n";

                    //Twitter::postTweet($tweet);

                    $badge->posted_twitter = 1;
                    $badge->save();
                }
            }catch (\Exception $e){

            }
        }
        catch (Exception $e)
        {
        }
    }

    private function check()
    {

        $badges = BadgeApi::orderBy('created_at', 'desc')
            ->where('posted_twitter', '0')
            ->get();

        if ($badges) {
           foreach($badges as $badge){
               $this->twitter($badge);
           }
        }

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['action', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

}
