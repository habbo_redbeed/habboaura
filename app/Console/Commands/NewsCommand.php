<?php namespace Fully\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use HabboIndex\Helpers\HabboApi;
use Fully\Models\FurniLog;
use Fully\Models\Article;
use Fully\Models\Shortnews;
use Cache;
use Lang;
use App;
use Bitly;
use Twitter;
use File;
use LaravelLocalization;
use DB;


class NewsCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'news:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $action = $this->argument('action');
        switch ($action) {
            case 'week':
                $this->week();
                break;
            case 'publish':
                $this->publish();
                break;

        }
    }

    public function publish () {
        $articles = DB::select('SELECT article_id, publish_time FROM article_publish_cron WHERE publish_time <= NOW()');

        foreach ($articles as $articleData) {
            $article = Article::find($articleData->article_id);
            if($article){
                $article->created_at = $articleData->publish_time;
                $article->updated_at = $articleData->publish_time;
                $article->is_published = 1;
                $article->save();

                DB::delete('DELETE FROM article_publish_cron WHERE article_id = '.$articleData->article_id);
            }
        }

    }

    public function week() {
        $langs = LaravelLocalization::getSupportedLocales();
        foreach($langs as $lang => $data) {

            App::setLocale($lang);

            echo 'Get Articles for '.$lang."\n";

            $today = \Carbon\Carbon::now();
            $weekStart = \Carbon\Carbon::now()->startOfWeek();
            $weekStop = \Carbon\Carbon::now()->endOfWeek();

            $articles = Article::where('created_at', '>=', $weekStart->format('Y-m-d H:i:s'))
                ->where('created_at', '<=', $weekStop->format('Y-m-d H:i:s'))
                ->where('is_published', 1)
                ->where('lang', $lang)
                ->orderBy('created_at', 'desc')
                ->get();

            $articlesArray = [];
            $startDate = null;
            foreach($articles as $article){
                if($startDate == null){
                    $startDate = \Carbon\Carbon::parse($article->created_at)->startOfDay();
                }

                if($startDate != \Carbon\Carbon::parse($article->created_at)->startOfDay()){
                    $articlesArray[] = '<hr>';
                    $startDate = \Carbon\Carbon::parse($article->created_at)->startOfDay();
                }
                $articlesArray[] = \Carbon\Carbon::parse($article->created_at)->format('d.m.y').': '.$article->title.'  <a href="'.$lang.'/article/'.$article->slug.'"> <i class="fa fa-arrow-circle-right"></i></a>';
            }

            echo 'Get Shortnews for '.$lang."\n";

            $shortnews = Shortnews::where('created_at', '>=', $weekStart->format('Y-m-d H:i:s'))
                ->where('created_at', '<=', $weekStop->format('Y-m-d H:i:s'))
                ->where('lang', $lang)
                ->orderBy('created_at', 'desc')
                ->get();

            $shortnewsArray = [];
            $startDate = null;
            foreach($shortnews as $shortnew){

                if($startDate == null){
                    $startDate = \Carbon\Carbon::parse($shortnew->created_at)->startOfDay();
                    $shortnewsArray[] = '<b>'.\Carbon\Carbon::parse($shortnew->created_at)->format('d.m.y').'</b>';

                }

                if($startDate != \Carbon\Carbon::parse($shortnew->created_at)->startOfDay()){
                    $shortnewsArray[] = '<hr>';
                    $shortnewsArray[] = '<b>'.\Carbon\Carbon::parse($shortnew->created_at)->format('d.m.y').'</b>';
                    $startDate = \Carbon\Carbon::parse($shortnew->created_at)->startOfDay();
                }
                $html = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $shortnew);
                if(trim(strip_tags($html))) {
                    $shortnewsArray[] = \Carbon\Carbon::parse($shortnew->created_at)->format('H:i') . ': ' . $this->shorter(strip_tags($html), 60) . '  <a href="' . $lang . '/shortnews"> <i class="fa fa-arrow-circle-right"></i></a>';
                }
            }

            echo 'Get Badges'."\n";

            $habboApi = new HabboApi();
            $badges = $habboApi->badgeWeek();

            $badgesArray = [];
            foreach ($badges['badges'] as $badge) {
                $badgesArray[] = '<img src="'.$badge['image'].'" alt="'.$badge['code'].'">';
            }

            $articleHtml = implode('<br>', $articlesArray);
            $shortnewsHtml = implode('<br>', $shortnewsArray);
            $badgesHtml = implode(' ', $badgesArray);

            $check = trim(strip_tags($shortnewsHtml));
            if(empty($check)){
                $shortnewsHtml = '';
            }
            $check = trim(strip_tags($articleHtml));
            if(empty($check)){
                $articleHtml = '';
            }

            $article = Lang::get('automatics.news.week', array(
                'article' => $articleHtml,
                'shortnews' => $shortnewsHtml,
                'badges' => $badgesHtml,
                )
            );

            $cat = 0;
            switch($lang){
                case 'de':
                    $cat = 7;
                    break;
                case 'en':
                    $cat = 24;
                    break;
                case 'tr':
                    $cat = 33;
                    break;
            }

            Article::create([
                'users' => 'HabboAura.com',
                'title' => Lang::get('automatics.news.weekTitle'),
                'description' => Lang::get('automatics.news.weekDesc'),
                'content' => $article,
                'tags' => 'weekly',
                'is_published' => 1,
                'is_topnews' => 1,
                'path' => '/uploads/article/',
                'file_name' => '308TOP_news_01.png',
                'file_size' => '4212',
                'lang' => $lang,
                'category_id' => $cat
            ]);


        }
    }

    private function shorter($input, $length)
    {
        //no need to trim, already shorter than trim length
        if (strlen($input) <= $length) {
            return $input;
        }

        //find last space within length
        $last_space = strrpos(substr($input, 0, $length), ' ');
        if(!$last_space) $last_space = $length;
        $trimmed_text = substr($input, 0, $last_space);

        //add ellipses (...)
        $trimmed_text .= '...';

        return $trimmed_text;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['action', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

}
