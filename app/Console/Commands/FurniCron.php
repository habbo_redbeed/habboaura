<?php namespace Fully\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use HabboIndex\Helpers\HabboApi;
use Fully\Models\FurniLog;
use Fully\Models\Shortnews;
use Cache;
use Lang;
use App;
use Bitly;
use Twitter;
use File;

class FurniCron extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'furni:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $action = $this->argument('action');
        switch ($action) {
            case 'check':
                $this->check();
                break;

        }
    }

    private function shorter($input, $length)
    {
        //no need to trim, already shorter than trim length
        if (strlen($input) <= $length) {
            return $input;
        }

        //find last space within length
        $last_space = strrpos(substr($input, 0, $length), ' ');
        if(!$last_space) $last_space = $length;
        $trimmed_text = substr($input, 0, $last_space);

        //add ellipses (...)
        $trimmed_text .= '...';

        return $trimmed_text;
    }

    private function twitter($count, $furni){
        try {
            App::setLocale('en');
            $url = Bitly::shorten('http://www.habboaura.com/'.App::getLocale().'/furni')['data']['url'];
            $end = $url.' #habbo #furni #new';
            $endLenght = strlen($end);
            $tweet = $this->shorter(Lang::get("shortnews.furni.twitter", array('count' => (int)$count)), 120 - $endLenght);
            $tweet = str_replace('<br>', "\n", $tweet);
            $tweet = ['status' => strip_tags($tweet.' '.$end), 'format' => 'json'];

            if($furni['image']) {
                $uploaded_media = Twitter::uploadMedia(['media' => file_get_contents($furni['image'])]);
                $tweet['media_ids'] = $uploaded_media->media_id_string;
            }

            if(count($tweet)) {
                Twitter::postTweet($tweet);
            }
        }
        catch (Exception $e)
        {
        }
    }

    private function check()
    {

        $last = FurniLog::orderBy('created_at', 'desc')
            ->first();

        $habboApi = new HabboApi();
        $json = $habboApi->furnisSinceId((string)$last->slug);

        if (isset($json['count']) && $json['count'] > 0) {
            $newLast = $json['last'];
            if (true || isset($newLast['uid']) && $newLast['uid'] != (string)$last->slug && !empty($newLast['uid'])) {
                FurniLog::firstOrCreate([
                    'slug' => (string) $newLast['uid'],
                ]);

                $text = [
                    "lang" => "shortnews.furni.new",
                    "values" => [
                        "count" => $json['count']
                    ],
                ];

                $this->twitter($json['count'], $newLast);

                echo "NEW: ".$json['count']."\n";

                $shortnews = Shortnews::create(array(
                    'type' => 'translate',
                    'user' => 'HabboAura',
                    'category_id' => 10,
                    'text' => json_encode($text),
                ));

                Cache::flush();
            }
        }

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['action', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

}
