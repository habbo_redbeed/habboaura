<?php

if (!function_exists('gratavarUrl')) {
    /**
     * Gravatar URL from Email address
     *
     * @param string $email Email address
     * @param string $size Size in pixels
     * @param string $default Default image [ 404 | mm | identicon | monsterid | wavatar ]
     * @param string $rating Max rating [ g | pg | r | x ]
     *
     * @return string
     */
    function gratavarUrl($email, $size = 60, $default = 'mm', $rating = 'g') {

        return 'http://www.gravatar.com/avatar/' . md5(strtolower(trim($email))) . "?s={$size}&d={$default}&r={$rating}";
    }
}

function smilies($text){

    $emos = [
        ':-(' => 'Traurig.png',
        ':(' => 'Traurig.png',

        ':-)' => 'Grinsen.png',
        ':)' => 'Grinsen.png',

        ':-D' => 'Lachen.png',
        ':D' => 'Lachen.png',

        ';-(' => 'Weinen.png',
        ';(' => 'Weinen.png',

        'o/' => 'Winken.png',

        '>:-(' => 'Zornig.png',
        '>:(' => 'Zornig.png',

        ':-|' => 'Mhm.png',
        ':|' => 'Mhm.png',

        ':-o' => 'Geschockt.png',
        ':o' => 'Geschockt.png',

        ':-O' => 'Geschockt.png',
        ':O' => 'Geschockt.png',

        '(y)' => 'Like.png',
        '_B' => 'Like.png',
        '(like)' => 'Like.png',

        '(dislike)' => 'Dislike.png',
        '_P' => 'Dislike.png',

        '8-)' => 'Cool.png',
        '8)' => 'Cool.png',

        'B-)' => 'Cool.png',
        'B)' => 'Cool.png',

        '(lol)' => 'Lol.png',
        '(LOL)' => 'Lol.png',

        'LOL' => 'Lol.png',
        'lol' => 'Lol.png',

        'xD' => 'Lol.png',

        ':-P' => 'Zungestrecken.png',
        ':P' => 'Zungestrecken.png',

        ';-)' => 'Zwinkern.png',
        ';)' => 'Zwinkern.png',

        //SONDER
        '(balloon)' => 'balloon.png',
        '(BALLOON)' => 'balloon.png',

        '(habbo15)' => 'habbo15.png',
        '(HABBO15)' => 'habbo15.png',

        '(habboaura)' => 'habboaura_1.png',
        '(habboaura)' => 'habboaura_1.png',

    ];

    foreach($emos as $icon => $image) {
        $icon = preg_quote($icon);

        $image = ' <img src="/images/Smileys/'.$image.'" alt="'.$image.'"> ';

        $text = preg_replace('~(([[:blank:]]|^)'.$icon.')([[:blank:]]|$)~',$image,$text);
    }

    return $text;
}


/**
 * Backend menu active
 * @param $path
 * @param string $active
 * @return string
 */
function setActive($path, $active = 'active') {

    if (is_array($path)) {

        foreach ($path as $k => $v) {
            $path[$k] = getLang() . "/" . $v;
        }
    } else {
        $path = getLang() . "/" . $path;
    }

    return call_user_func_array('Request::is', (array)$path) ? $active : '';
}

/**
 * @return mixed
 */
function getLang() {

    return LaravelLocalization::getCurrentLocale();
}

/**
 * @param null $url
 * @return mixed
 */
function langURL($url = null) {

    //return LaravelLocalization::getLocalizedURL(getLang(), $url);

    return getLang() . $url;
}

/**
 * @param $route
 * @return mixed
 */
function langRoute($route, $parameters = array()) {

    return URL::route(getLang() . "." . $route, $parameters);
}

/**
 * @param $route
 * @return mixed
 */
function langRedirectRoute($route, $params = array()) {

    return Redirect::route(getLang() . "." . $route, $params);
}