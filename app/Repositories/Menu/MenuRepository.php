<?php namespace Fully\Repositories\Menu;

use Fully\Models\Menu;
use Config;
use Response;
use Fully\Models\Tag;
use Fully\Models\Category;
use Str;
use Event;
use Image;
use File;
use Fully\Repositories\RepositoryAbstract;
use Fully\Repositories\CrudableInterface as CrudableInterface;
use Fully\Repositories\RepositoryInterface as RepositoryInterface;
use Fully\Exceptions\Validation\ValidationException;
use Fully\Repositories\AbstractValidator as Validator;
use Cache;
use Lang;
use Sentry;
use App;


/**
 * Class MenuRepository
 * @package Fully\Repositories\Menu
 * @author Sefa Karagöz
 */
class MenuRepository extends RepositoryAbstract implements MenuInterface
{

    /**
     * @var \Menu
     */
    protected $menu;

    /**
     * @param Menu $menu
     */
    public function __construct(Menu $menu)
    {

        $this->menu = $menu;
    }

    /**
     * @return mixed
     */
    public function all()
    {

        return $this->menu->where('is_published', 1)->where('lang', $this->getLang())->orderBy('order', 'asc')->get();
    }

    /**
     * @param $menu
     * @param int $parentId
     * @param bool $starter
     * @return null|string
     */
    public function generateFrontMenu($menu, $parentId = 0, $starter = false)
    {

        $logged = Sentry::check() ? 'login' : 'logout';

        return Cache::remember('front_menu_parents_'.$parentId.'_'.($starter ? '1' : 0).'_'.md5($menu).'_'.$logged.'_lang_'.App::getLocale(), 60, function () use ($menu, $parentId, $starter){
            $result = null;

            foreach ($menu as $item) {

                if ($item->parent_id == $parentId) {

                    $childItem = $this->hasChildItems($item->id);
                    $check = str_replace(array('de', '/'), '', $item->url);

                    $childHtml = null;
                    if ($item->type == 'module' && $item->option == 'blog') {
                        $childHtml = $this->generateNewsMenu();
                    } else if($childItem) {
                        $childHtml = $this->generateFrontMenu(Menu::where('parent_id', $item->id)->orderBy('order')->get(), $item->id);
                    }

                    $html = "<li itemprop='itemListElement' class='menu-item " . (($childItem) ? 'dropdown' : null) . (($childItem && $item->parent_id != 0) ? ' dropdown-submenu' : null) . "'>
                                <a href='" . url($item->url) . "' " . (($childItem) ? 'class="dropdown-toggle" data-toggle="dropdown"' : null) . ">
                                    {$item->title}" . (($childItem) ? '<b class="caret"></b>' : null) .
                        "</a>" . $childHtml . "
                            </li>";

                    if (!empty($check) || $childItem === true && !empty($childHtml) || $item->title == 'Home') {
                        $result .= $html;
                    }
                }
            }

            if($parentId == 0){
                if(Sentry::check()) {
                    $result .= "<li itemprop='itemListElement' class='menu-item'>
                                 <a href='#openUsermenu'>".Lang::get('main.menu.me')."</a>
                        </li>";
                }else{
                    $result .= "<li itemprop='itemListElement' class='menu-item'>
                                <a href='" . url(route('user.auth.join')) . "'>".Lang::get('main.menu.join')."</a>
                        </li>";
                    $result .= "<li itemprop='itemListElement' class='menu-item'>
                                <a href='#openUsermenu'>".Lang::get('main.menu.login')."</a>
                        </li>";
                }
            }

            return $result ? "\n<ul itemscope itemtype='http://schema.org/ItemList' class='" . (($starter) ? ' nav navbar-nav navbar-right ' : null) . ((!$starter) ? ' dropdown-menu ' : null) . "'>\n$result</ul>\n" : null;
        });
    }

    public function generateNewsMenu()
    {
        $menu = Cache::remember('categories_with_articels_'.App::getLocale(), 60, function () {
            $categories = Category::orderBy('title')->get();
            $return = array();
            foreach ($categories as $key => $category) {
                if ($category->articles->where('lang', Lang::getLocale())->count()) {
                    $return[$key] = $category;
                }
            }

            return $return;
        });

        $result = '';

        foreach ($menu as $item) {

            $html = "<li itemprop='itemListElement' class='menu-item dropdown-submenu'>
                        <a href='" . url(route('dashboard.category', array($item->slug))) . "'> ".$item->title."</a>
                    </li>";

            $result .= $html;


        }

        $html = "<li itemprop='itemListElement' class='menu-item dropdown-submenu'>
                        <a href='" . url(route('dashboard.shortnews')) . "'> ".trans('shortnews.liveticker')."</a>
                    </li>";

        $result .= $html;


        return '<ul itemscope itemtype="http://schema.org/ItemList" class="dropdown-menu">'.$result.'</ul>';
    }

    /**
     * @param $items
     * @return null|string
     */
    public
    function getFrontMenuHTML($items)
    {

        return $this->generateFrontMenu($items, 0, true);
    }

    /**
     * @param $id
     * @return bool
     */
    public
    function hasChildItems($id)
    {

        $count = $this->menu->where('parent_id', $id)->where('is_published', 1)->where('lang', $this->getLang())->get()->count();
        if ($count === 0) return false;
        return true;
    }
}
