<?php namespace Fully\Models;

use Fully\Interfaces\ModelInterface as ModelInterface;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @author Sefa Karagöz
 */
class LexiconEntry extends Model implements ModelInterface, SluggableInterface {

    use SluggableTrait;

    public $table = 'lexicon_entry';
    protected $fillable = ['title', 'categorie_id', 'status'];
    protected $appends = ['url'];

    protected $sluggable = array(
        'build_from' => 'title',
        'save_to'    => 'slug',
    );

    public function texts() {

        return $this->hasMany('Fully\Models\LexiconEntryText', 'entry_id', 'id')
            ->where('status', '=', 'ENABLE')
            ->orderBy('created_at', 'DESC');
    }

    public function lastTexts() {

        return $this->hasOne('Fully\Models\LexiconEntryText', 'entry_id', 'id')
            ->where('status', '=', 'ENABLE')
            ->orderBy('created_at', 'DESC');
    }

    public function category() {
        return $this->hasOne('Fully\Models\LexiconCategory', 'id', 'categorie_id');
    }

    public function setUrlAttribute($value) {

        $this->attributes['url'] = $value;
    }

    public function getUrlAttribute() {

        return "entry/" . $this->attributes['slug'];
    }
}
