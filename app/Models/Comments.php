<?php namespace Fully\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Comments extends Model {

    public $table = 'comments';
    protected $fillable = ['token', 'type', 'status', 'url'];

    public function comments()
    {
        return $this->hasMany('Fully\Models\CommentsTexts', 'comment_id')->orderBy('created_at', 'desc');
    }
}
