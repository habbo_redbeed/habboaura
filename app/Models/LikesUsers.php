<?php namespace Fully\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

/**
 * Class Log
 * @author Sefa Karagöz
 */
class LikesUsers extends Model {

    public $table = 'likes_user';
    protected $fillable = ['user_id', 'like_id', 'emo', 'cookie_user'];

    public function user()
    {
        return $this->hasOne('Fully\Models\User', 'user_id');
    }

}
