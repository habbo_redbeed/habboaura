<?php namespace Fully\Models;

use Fully\Interfaces\ModelInterface as ModelInterface;
use Lang;

class ProfilImages extends BaseModel {

    public $table = 'profil_images';
    protected $fillable = ['name', 'lang', 'hidden', 'visible_to'];

}
