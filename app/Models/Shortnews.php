<?php namespace Fully\Models;

use Fully\Interfaces\ModelInterface as ModelInterface;
use Lang;
use Cache;
use App;

/**
 * Class Article
 * @author Sefa Karagöz
 */
class Shortnews extends ModernMagic {

    public $table = 'shortnews';
    protected $fillable = ['type', 'text', 'user', 'category_id', 'icon_class', 'pin', 'tags', 'creator_id'];

    public function category() {
        return Cache::remember('category_model__'.$this->category_id.'_'.App::getLocale(), 360, function () {
            $find  = $this->hasOne('Fully\Models\Category', 'id', 'category_id')->first();
            if($find){
                return $find;
            }else {
                return $this->hasOne('Fully\Models\Category', 'id', 'category_id')->orWhere('lang', App::getLocale())->first();
            }
        });
    }

    public function __toString(){
        return (string) Cache::remember('shortnews_string_cache_'.md5($this->text).'_'.md5($this->type).'_'.App::getLocale(), 120, function () {
            try {
                if ($this->type == 'translate') {
                    $json = json_decode($this->text);
                    if(!isset($json->values->url)){
                        $json->values->url = route('furni.index');
                    }
                    return Lang::get($json->lang, (array)$json->values).'<br>';
                } else {
                    return $this->text;
                }
                return "";
            }catch (\Exception $e){
                return $e->getMessage();
            }
        });
    }

    public function likes() {
        return Cache::remember($this->cacheKeyLikes(), 5, function () {
            $like = Likes::where('token', '=', $this->id)
                ->where('type', '=', 'shortnews')
                ->first();
            if ($like) {
                return $like->likes;
            } else {
                return array();
            }
        });
    }

    public function comments() {
        return Cache::remember($this->cacheKeyComments(), 5, function () {
            $comment = Comments::where('token', '=', $this->id)
                ->where('type', '=', 'shortnews')
                ->first();

            if ($comment) {
                return $comment->comments;
            } else {
                return array();
            }
        });
    }

    public function cacheKeyLikes () {
        return 'shortnews_likes_cache_'.md5($this->text).'_'.md5($this->type).'_'.App::getLocale();
    }

    public function cacheKeyComments () {
        return 'shortnews_comments_cache_'.md5($this->text).'_'.md5($this->type).'_'.App::getLocale();
    }


}
