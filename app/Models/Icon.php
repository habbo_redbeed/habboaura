<?php namespace Fully\Models;

use Tracker;
use Cache;

/**
 * Class Article
 * @author Sefa Karagöz
 */
class Icon extends BaseModel {


    public $table = 'icons';
    protected $fillable = ['name', 'image', 'see_calendar'];

}
