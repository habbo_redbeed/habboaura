<?php namespace Fully\Models;

use Fully\Interfaces\ModelInterface as ModelInterface;
use Lang;
use Carbon;

/**
 * Class Article
 * @author Sefa Karagöz
 */
class Stars extends BaseModel {

    public $table = 'user_stars';
    protected $fillable = ['user_id', 'stars', 'status', 'stars', 'note', 'any_habbo', 'any_lang'];

    public function user() {
        return $this->hasOne('Fully\Models\User', 'id', 'user_id');
    }

    public function __toString(){
        try {
            return Lang::choice('user.stars', $this->stars);
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function scopeAll($query)
    {
        return $query->where('stars', '>', 0)
            ->where('user_id', '!=', 0)
            ->sum('stars');
    }

    public function scopeMonth($query)
    {
        $oldDate = Carbon\Carbon::now();
        return $query->where('stars', '>', 0)
            ->where('user_id', '!=', 0)
            ->where('created_at', '>=', $oldDate->format('Y-m-01 00:00:00'))
            ->where('created_at', '>=', $oldDate->format('Y-m-'.$oldDate->daysInMonth.' 23:59:59'))
            ->sum('stars');
    }


}
