<?php namespace Fully\Models;

use Sentry;

class UserData extends BaseModel {

    public $table = 'user_data';
    protected $fillable = ['image_header', 'free_text', 'motto', 'profil_background', 'user_id'];

    public $backgroundImage = [
        'cloud.png' => 'user.image.cloud',
        'cloud_2.png' => 'user.image.cloudTwo',
        'cloud_park_1.png' => 'user.image.park',
    ];

    public function user()
    {
        return $this->hasOne('Fully\Models\User');
    }

    public function backgroundImages() {
        $userRights = Sentry::findUserById($this->user_id);
        $return = $this->backgroundImage;

        if ($userRights->hasAccess('admin.backend')) {
            $return['worker.png'] = 'Mitarbeiter Hintergrund';
        }

        if ($userRights->hasAccess('admin.settings.save')) {
            $return['admin_1.png'] = 'Admin Hintergrund';
        }

        if ($userRights->hasAccess('admin.comments.destroy')) {
            $return['guard.png'] = 'Guard Hintergrund (MOD)';
        }

        return $return;
    }

}