<?php namespace Fully\Models;

use Fully\Interfaces\ModelInterface as ModelInterface;
use Lang;

/**
 * Class Article
 * @author Sefa Karagöz
 */
class EventPixelUser extends BaseModel {

    public $table = 'event_pixel_user';
    protected $fillable = ['event_id', 'user_id', 'image', 'text'];

    public function event() {
        return $this->hasOne('Fully\Models\EventPixel', 'id', 'event_id');
    }


}
