<?php namespace Fully\Models;

use Illuminate\Database\Eloquent\Model;
use Fully\Models\Likes;
use DB;

/**
 *
 * @author Chris Woelk
 */
class CommentTextNotice extends Model {

    public $table = 'comments_text_notice';
    protected $fillable = ['comment_text_id', 'creater_id', 'notice'];

    public function creator()
    {
        return $this->hasOne('Fully\Models\User', 'id', 'creater_id');
    }

    public function comment()
    {
        return $this->hasOne('Fully\Models\CommentsText', 'id', 'comment_text_id');
    }

    public function json() {
        return json_decode($this->notice);
    }

}
