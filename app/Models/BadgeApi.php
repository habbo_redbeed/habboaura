<?php namespace Fully\Models;

class BadgeApi extends BaseModel
{

    protected $table = 'habbo_badges';
    protected $connection = 'api';

    protected $fillable = array(
        'code',
        'posted_twitter'
    );

}

?>