<?php namespace Fully\Models;

use Illuminate\Database\Eloquent\Model;
use Fully\Models\CommentTextNotice;
use DB;

/**
 * Class Log
 * @author Sefa Karagöz
 */
class CommentsTexts extends Model {

    public $table = 'comments_text';
    protected $fillable = ['comment_id', 'user_id', 'text', 'status', 'delete'];

    public function user()
    {
        return $this->hasOne('Fully\Models\User', 'id', 'user_id');
    }

    public function base()
    {
        return $this->hasOne('Fully\Models\Comments', 'id', 'comment_id');
    }

    public function notice()
    {
        return $this->hasMany('Fully\Models\CommentTextNotice', 'comment_text_id', 'id');
    }

    public function likes() {
        return Likes::where('token', '=', $this->id)
            ->where('type', '=', 'comment')
            ->get();
    }

    public function likesSum() {
         $like = Likes::where('token', '=', $this->id)
            ->where('type', '=', 'comment')
            ->first();
        if($like) {
            return $like->likes;
        }else{
            return array();
        }
    }

}
