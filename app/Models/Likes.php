<?php namespace Fully\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Likes extends Model {

    public $table = 'likes';
    protected $fillable = ['token', 'type'];

    public function likes()
    {
        return $this->hasMany('Fully\Models\LikesUsers', 'like_id');
    }

    public function likesEmo($emo = '')
    {
        $likes = null;
        if($emo == '' || $emo == 'like'){
            $likes =  $this->hasMany('Fully\Models\LikesUsers', 'like_id')
                ->whereIn('emo', ['', 'like']);
        }else{
            $likes =  $this->hasMany('Fully\Models\LikesUsers', 'like_id')
                ->where('emo', $emo);
        }

        return $likes;
    }
}
