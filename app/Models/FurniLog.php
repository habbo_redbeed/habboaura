<?php namespace Fully\Models;

use Fully\Interfaces\ModelInterface as ModelInterface;

class FurniLog extends BaseModel  {

    public $table = 'log_furni';
    protected $fillable = ['slug'];
}
