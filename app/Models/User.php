<?php namespace Fully\Models;

use Fully\Models\UserData;
use Carbon;
use Fully\Models\Stars;
use Cmgmyr\Messenger\Traits\Messagable;
use Cache;

/**
 * Class User
 * @author Sefa Karagöz
 */
class User extends ModernMagicUser {

    use Messagable;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::created(function($model)
        {
            $profile = new UserData;
            $profile->user_id = $model->id;
            $profile->save();
        });

    }

    public function data()
    {

        $key = 'user_data_infos'.$this->id;
        if (Cache::has($key)) {
            return Cache::get($key);
        }

        $data = $this->hasOne('Fully\Models\UserData');
        if ($data->first()) {

            Cache::put($key, $data->first(), 10);
            return $data->first();

        } else {

            $profile = new UserData;
            $profile->user_id = $this->id;
            $profile->save();
            return $this->hasOne('Fully\Models\UserData')->first();

        }
    }
//
//    public function stars() {
//        return $this->hasMany('Fully\Models\Stars')
//            ->where('user_id', $this->id)
//            ->orWhere(function ($query){
//                $query->where('any_habbo', $this->habbo_name)
//                    ->where('any_lang', $this->habbo_lang);
//            });
//    }

    public function comments() {
        return $this->hasMany('Fully\Models\CommentsText', 'user_id', 'id');
    }

    public function active()
    {
        return $this->hasOne('Fully\Models\UserActive');
    }

    public function addStar ($stars = 0, $note = ''){
        return Stars::create(array(
            'user_id' => $this->id,
            'stars' => (int)$stars,
            'note' => json_encode($note)
        ));
    }

    public function starsSum() {
        return (int) Stars::where(function ($query) {
                $query->where('user_id', $this->id)
                    ->orWhere(function ($query) {
                        $query->where('any_habbo', $this->habbo_name)
                            ->where('any_lang', $this->habbo_lang);
                    });
            })
            ->sum('stars');
    }

//    public function starsMonth() {
//        $oldDate = Carbon\Carbon::now();
//        return $this->hasMany('Fully\Models\Stars', 'user_id', 'id')
//            ->where('user_id', $this->id)
//            ->orWhere(function ($query){
//                $query->where('any_habbo', $this->habbo_name)
//                    ->where('any_lang', $this->habbo_lang);
//            })
//            ->where('created_at', '>=', $oldDate->format('Y-m-01 00:00:00'))
//            ->where('created_at', '<=', $oldDate->format('Y-m-'.$oldDate->daysInMonth.' 23:59:59'));
//    }

    public function starsMonthSum() {
        $oldDate = Carbon\Carbon::now();
        return (int) Stars::where(function ($query) {
            $query->where('user_id', $this->id)
                ->orWhere(function ($query) {
                    $query->where('any_habbo', $this->habbo_name)
                        ->where('any_lang', $this->habbo_lang);
                });
            })
            ->where('created_at', '>=', $oldDate->format('Y-m-01 00:00:00'))
            ->where('created_at', '<=', $oldDate->format('Y-m-'.$oldDate->daysInMonth.' 23:59:59'))
            ->sum('stars');
    }

    public function groups()
    {
        return $this->hasMany('Fully\Models\UserGroup', 'user_id', 'id');
    }


}