<?php namespace Fully\Models;

use Fully\Interfaces\ModelInterface as ModelInterface;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;
use Fully\Models\LexiconEntry;
use Cache;

/**
 * Class Category
 * @author Sefa Karagöz
 */
class LexiconCategory extends Model implements ModelInterface, SluggableInterface {

    use SluggableTrait;

    public $table = 'lexicon_categories';
    protected $fillable = ['name', 'lang', 'status'];
    protected $appends = ['url'];

    protected $sluggable = array(
        'build_from' => 'name',
        'save_to'    => 'slug',
    );

    public function entrys() {

        return $this->hasMany('Fully\Models\LexiconEntry')->orderBy('title');
    }

    public function entrysSort() {
        $entrys = Cache::remember('LexiconEntry_'.$this->id.'_sort', 20, function () {
            $entrys = LexiconEntry::where('categorie_id', '=', $this->id)
                ->orderBy('title')
                ->get();

            $sort = array();
            foreach($entrys as $entry){
                if($entry->title) {
                    $first = $entry->title{0};
                    $sort[$first][] = $entry;
                }
            }
            ksort($sort);

            return $sort;
        });

        return $entrys;
    }

    public function setUrlAttribute($value) {

        $this->attributes['url'] = $value;
    }

    public function getUrlAttribute() {

        return "category/" . $this->attributes['slug'];
    }
}
