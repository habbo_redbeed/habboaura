<?php namespace Fully\Models;

use Tracker;
use Cache;

/**
 * Class Article
 * @author Sefa Karagöz
 */
class EventCalendar extends BaseModel {

    public $table = 'event_calendar';
    protected $fillable = ['title', 'description', 'help', 'area', 'link_room', 'link_external', 'link_article', 'price'];
    protected $dates = ['event_start', 'event_end'];


    public function icon() {
        return $this->hasOne('Fully\Models\Icon', 'id', 'area');
    }

}
