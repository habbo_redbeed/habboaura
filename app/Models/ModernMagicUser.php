<?php namespace Fully\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseModel
 * @author Chris Woelk
 */
class ModernMagicUser extends \Cartalyst\Sentry\Users\Eloquent\User  {

    /**
     * Dynamically retrieve attributes on the model.
     *
     * @param  string  $key
     * @return mixed
     */
    public function __get($key)
    {
        try {
            return $this->getAttribute($key);
        }catch (\Exception $e){
            if(method_exists($this, $key)){
                return $this->{$key}();
            }else{
                throw $e;
            }
        }
    }
    /**
     * Dynamically set attributes on the model.
     *
     * @param  string  $key
     * @param  mixed   $value
     * @return void
     */
    public function __set($key, $value)
    {
        $this->setAttribute($key, $value);
    }

}
