<?php namespace Fully\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Fully\Models\ArticleFormTextEntry;

class ArticleFormText extends Model {

    public $table = 'articles_form_text';
    protected $fillable = ['id', 'article_id', 'creator_id', 'description', 'available_to', 'any'];

    public function creator()
    {
        return $this->hasMany('Fully\Models\User', 'creator_id', 'id');
    }

    public function entrys()
    {
        return $this->hasMany('Fully\Models\ArticleFormTextEntry', 'from_text_id', 'id')
            ->orderBy('created_at');
    }
}
