<?php namespace Fully\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ArticleFormTextEntry extends Model {

    public $table = 'articles_form_text_entry';
    protected $fillable = ['id', 'from_text_id', 'user_id', 'text', 'any_habbo', 'any_lang', 'any_session'];

    public function user()
    {
        return $this->HasOne('Fully\Models\User', 'id', 'user_id');
    }

    public function parent()
    {
        return $this->HasOne('Fully\Models\ArticleFormText', 'from_text_id', 'id');
    }
}
