<?php namespace Fully\Models;

use Fully\Interfaces\ModelInterface as ModelInterface;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @author Sefa Karagöz
 */
class LexiconEntryText extends Model  {

    public $table = 'lexicon_entry_text';
    protected $fillable = ['entry_id', 'text', 'user_id', 'display_name', 'status', 'new_categorie_id', 'new_title'];

    public function entry() {

        return $this->hasOne('Fully\Models\LexiconEntry', 'id', 'entry_id');
    }

    public function user () {
        return $this->hasOne('Fully\Models\User', 'id', 'user_id');
    }
}
