<?php namespace Fully\Models;

class UserActive extends BaseModel {

    public $table = 'user_active';
    protected $fillable = ['last_active', 'user_id'];
    public $timestamps

        = false;

    public function user()
    {
        return $this->hasOne('Fully\Models\User', 'id', 'user_id');
    }

}