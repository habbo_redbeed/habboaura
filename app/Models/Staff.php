<?php namespace Fully\Models;


/**
 * Class Article
 * @author Sefa Karagöz
 */
class Staff extends BaseModel {

    public $table = 'lexicon_staffs';
    protected $fillable = ['habbo_name', 'habbo_lang', 'job_title', 'job_from', 'job_to', 'active', 'real_name'];

}
