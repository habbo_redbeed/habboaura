<?php namespace Fully\Models;

use Fully\Interfaces\ModelInterface as ModelInterface;

class StarsLog extends BaseModel  {

    public $table = 'log_stars';
    protected $fillable = ['staff_id', 'user_id', 'star', 'note'];

    public function user() {
        return $this->hasOne('Fully\Models\User', 'id', 'user_id');
    }

    public function staff() {
        return $this->hasOne('Fully\Models\User', 'id', 'staff_id');
    }

}
