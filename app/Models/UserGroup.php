<?php namespace Fully\Models;

class UserGroup extends BaseModel {

    public $table = 'users_groups';
    protected $fillable = ['user_id', 'group_id'];

    public function user()
    {
        return $this->hasOne('Fully\Models\User', 'id', 'user_id');
    }

    public function group()
    {
        return $this->hasOne('Fully\Models\Group');
    }

}