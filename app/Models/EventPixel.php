<?php namespace Fully\Models;

use Fully\Interfaces\ModelInterface as ModelInterface;
use Lang;

/**
 * Class Article
 * @author Sefa Karagöz
 */
class EventPixel extends BaseModel {

    public $table = 'event_pixel';
    protected $fillable = ['created_user_id', 'title', 'description', 'event_start', 'event_end'];

    public function users() {
        return $this->hasMany('Fully\Models\EventPixelUser', 'event_id', 'id');
    }

}
