<?php namespace Fully\Models;

use Fully\Interfaces\ModelInterface as ModelInterface;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Tracker;
use Route;
use Cache;

/**
 * Class Article
 * @author Sefa Karagöz
 */
class Article extends ModernMagic implements ModelInterface, SluggableInterface {

    use SluggableTrait;

    public $table = 'articles';
    protected $fillable = ['title', 'lang', 'content', 'category_id', 'tags', 'users', 'meta_keywords', 'description', 'meta_description', 'is_published', 'is_topnews', 'path', 'file_name', 'file_size', 'creator_id'];
    protected $appends = ['url'];

    protected $sluggable = array(
        'build_from' => 'title',
        'save_to'    => 'slug',
    );


    public function category() {
        return Cache::remember('category_model_'.$this->category_id, 360, function () {
            return $this->hasOne('Fully\Models\Category', 'id', 'category_id')->first();
        });
        //return Cache::remember('has_relation_'.'category_id_'.$this->category_id.'_category', 30, function () {
//        return $this->hasOne('Fully\Models\Category', 'id', 'category_id');
        //});
    }

    public function question() {
        return $this->hasOne('Fully\Models\ArticleFormText', 'article_id', 'id');
    }

    public function setUrlAttribute($value) {

        $this->attributes['url'] = $value;
    }

    public function getUrlAttribute() {

        return "article/" . $this->attributes['slug'];
    }

    public function views()
    {
        $slug = $this->attributes['slug'];
        return count(Cache::remember('count_'.'views_'.md5($slug).'_category_new', 10, function () use($slug) {
            return Tracker::logByRouteName('dashboard.article.show')
                ->where(function ($query) use ($slug) {
                    $query
                        ->where('parameter', 'slug')
                        ->where('value', $slug);
                })
                ->groupBy('tracker_log.session_id')
                ->get();
        }));
    }

    public function comments() {
        $comment = Cache::remember('model_article_'.'comments_'.$this->id.'_model', 30, function () {
            return Comments::where('token', '=', $this->id)
                ->where('type', '=', 'article')
                ->first();
        });

        if($comment){
            return $comment->comments;
        }else{
            return array();
        }
    }

    public function commentsCount() {
        $comments = Cache::remember('model_article_'.'comments_'.$this->id.'_model_count', 5, function () {
            $comment = Comments::where('token', '=', $this->id)
                ->where('type', '=', 'article')
                ->first();
            if($comment && $comment->comments()->count()) {
                return $comment->comments()->count();
            }else{
                return 0;
            }
        });

        return $comments;
    }
}
